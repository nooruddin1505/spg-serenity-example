package dependencies.adapter;


import dependencies.adapter.abstractstuff.TestData;

public interface TestDataInterface {
  /**
   * Obtains the input value associated to a key.
   * <p>
   * Implementation specific.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the input value associated to a key
   */
  String getInputValue(String key, String... subkeys);

  /**
   * Obtains the expected result associated to a key.
   * <p>
   * Implementation specific.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the expected result associated to a key
   */
  String getExpectedResult(String key, String... subkeys);

  /**
   * Obtains the keys in the set of input values tracked by this test data instance.
   * <p>
   * Implementation specific.
   *
   * @return the keys in this test data instance's input values
   */
  String[] getInputValueKeys();

  /**
   * Obtains the keys in the set of expected results tracked by this test data instance.
   * <p>
   * Implementation specific.
   *
   * @return the keys in this test data instance's expected results
   */
  String[] getExpectedResultKeys();

  /**
   * Obtains the Boolean expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Boolean expected result associated to a key
   */
  boolean getBooleanExpectedResult(String key, String... subkeys);

  /**
   * Obtains the Byte expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Byte expected result associated to a key
   */
  byte getByteExpectedResult(String key, String... subkeys);

  /**
   * Obtains the Double expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Double expected result associated to a key
   */
  double getDoubleExpectedResult(String key, String... subkeys);

  /**
   * Obtains the Float expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Float expected result associated to a key
   */
  float getFloatExpectedResult(String key, String... subkeys);

  /**
   * Obtains the Int expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Int expected result associated to a key
   */
  int getIntExpectedResult(String key, String... subkeys);

  /**
   * Obtains the Long expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Long expected result associated to a key
   */
  long getLongExpectedResult(String key, String... subkeys);

  /**
   * Obtains the Short expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Short expected result associated to a key
   */
  short getShortExpectedResult(String key, String... subkeys);

  /**
   * Obtains the Boolean input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Boolean input value associated to a key
   */
  boolean getBooleanInputValue(String key, String... subkeys);

  /**
   * Obtains the Byte input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Byte input value associated to a key
   */
  byte getByteInputValue(String key, String... subkeys);

  /**
   * Obtains the Double input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Double input value associated to a key
   */
  double getDoubleInputValue(String key, String... subkeys);

  /**
   * Obtains the Float input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Float input value associated to a key
   */
  float getFloatInputValue(String key, String... subkeys);

  /**
   * Obtains the Int input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Int input value associated to a key
   */
  int getIntInputValue(String key, String... subkeys);

  /**
   * Obtains the Long input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Long input value associated to a key
   */
  long getLongInputValue(String key, String... subkeys);

  /**
   * Obtains the Short input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Short input value associated to a key
   */
  short getShortInputValue(String key, String... subkeys);

  /*
     * String representation of this test data object.
     */
  String toString(String inputValuesLabel, String inputValues, String expectedResultsLabel, String expectedValues);

  /**
   * Add the test data in the given object to this one.
   *
   * @param newTestData the object whose data will be added to this one
   */
  void add(TestData newTestData);
}

package dependencies.adapter;


import dependencies.adapter.abstractstuff.BaseAction;

import java.util.LinkedList;
import java.util.List;

/**
 * Step tracker for detailed reporting of test executions.
 */
public class BaseTracer {

  /**
   * Sequence of features.steps tracked by this tracer.
   */
  private List<TraceEntry> trace;

  /**
   * Default constructor.
   * <p>
   * Initialises the sequence of features.steps tracked by this tracer to an empty sequence.
   */
  public BaseTracer() {
    this.trace = new LinkedList<>();
  }

  /**
   * Adds a tracing step with an action and its associated string parameter.
   *
   * @param action the action to be traced
   * @param parameter the string parameter to associate with the action being traced
   * @deprecated Use {@link #trace(BaseAction, BaseElement...)}.
   */
  @Deprecated
  public void trace(BaseAction action, String parameter) {
    trace.add(new TraceEntry(action, parameter));
  }

  /**
   * Adds a tracing step with an action and the element it is performed on.
   *
   * @param action the action to be traced
   * @param parameters the parameters the action is performed on
   */
  public void trace(BaseAction action, BaseElement... parameters) {
    trace.add(new TraceEntry(action, parameters));
  }

  /**
   * Adds a tracing step with an action, the element it is performed on and its associated string
   * parameter.
   *
   * @param action the action to be traced
   * @param element the element the action is performed on
   * @param parameter the string parameter to associate with the action being traced
   * @deprecated Use {@link #trace(BaseAction, BaseElement...)}.
   */
  @Deprecated
  public void trace(BaseAction action, BaseElement element, String parameter) {
    trace.add(new TraceEntry(action, element, parameter));
  }

  /**
   * Dumps each step tracked by this tracer.
   *
   * @see TraceEntry#dump()
   */
  public void dump() {
    for (TraceEntry entry : trace) {
      entry.dump();
    }
  }

  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();
    for (TraceEntry entry : trace) {
      result.append(entry.toString());
      result.append("\n");
    }
    return result.toString();
  }

  /**
   * Obtains all the features.steps tracked by this tracer.
   *
   * @return the sequence of features.steps recorded by this tracer
   */
  public List<TraceEntry> getTraces() {
    return trace;
  }

}

package dependencies.adapter;

/**
 * Representation of traceable elements.
 */
public class BaseElement {

  /**
   * To use in String representations of unknown elements.
   */
  protected static final String UNKNOWN_DESCRIPTION = "unknown";

  /**
   * To use in String representations.
   */
  protected static final String SEPARATOR = " ";

  /**
   * A description to represent an element in the trace output.
   */
  protected String description;

  /**
   * Base constructor to set the description associated with this element instance.
   *
   * @param description the description to associate with this element
   */
  public BaseElement(String description) {
    this.description = description;
  }

  /**
   * Returns a string representation of this element.
   * <p>
   * The element is represented by its associated description.
   *
   * @return the string representation of this element
   */
  @Override
  public String toString() {
    return (description == null  ||  description.equals("")) ? BaseElement.UNKNOWN_DESCRIPTION : description;
  }

}

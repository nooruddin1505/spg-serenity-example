package dependencies.adapter.abstractstuff;

import java.util.Collection;
import java.util.List;


/**
 * A container for the configuration data governing the execution of the tests.
 */
public abstract class ConfigData {

  /**
   * The default hierarchy separator to be used if none if provided in the environment.
   */
  private static final String DEFAULT_KEYWORD_HIERARCHY_SEPARATOR = ".";

  /**
   * The keyword hierarchy separator to be used by this configuration data instance.
   */
  protected final String KEYWORD_HIERARCHY_SEPARATOR;

  /**
   * Base constructor.
   * <p>
   * Sets the keyword hierarchy separator to the value in the
   * CONFIG_DATA_KEYWORD_HIERARCHY_SEPARATOR environment variable, if available, or to a default
   * value otherwise.
   */
  public ConfigData() {

    String s = System.getenv("CONFIG_DATA_KEYWORD_HIERARCHY_SEPARATOR");
    KEYWORD_HIERARCHY_SEPARATOR = (s == null) ? DEFAULT_KEYWORD_HIERARCHY_SEPARATOR : s;
  }

  /**
   * Base constructor.
   * <p>
   * Sets the keyword hierarchy separator to the value in the
   * CONFIG_DATA_KEYWORD_HIERARCHY_SEPARATOR environment variable, if available, or to a default
   * value otherwise.
   *
   * @param filepaths the location of the files containing the configuration data to be loaded
   */
  public ConfigData(List<String> filepaths) {

    String hierarchySeparator = System.getenv("CONFIG_DATA_KEYWORD_HIERARCHY_SEPARATOR");
    KEYWORD_HIERARCHY_SEPARATOR =
            (hierarchySeparator == null) ? DEFAULT_KEYWORD_HIERARCHY_SEPARATOR : hierarchySeparator;

    for (String file : filepaths) {
      loadConfigData(file);
    }
  }

  /**
   * Loads the configuration data from a given file.
   * <p>
   * Implementation specific.
   *
   * @param filePath the location of the file containing the configuration data to be loaded
   */
  protected abstract void loadConfigData(String filePath);

  /**
   * Obtains all keys associated to this configuration data file.
   *
   * @return the String keys
   */
  public abstract Collection<String> getKeys();

  /**
   * Obtains the value associated to a key.
   * <p>
   * Implementation specific.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the value associated to a key
   */
  public abstract String getProperty(String key, String... subkeys);

  /**
   * Obtains the Boolean value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Boolean value associated to a key
   */
  public final boolean getBooleanProperty(String key, String... subkeys) {
    return Boolean.parseBoolean(getProperty(key, subkeys));
  }

  /**
   * Obtains the Byte value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Byte value associated to a key
   */
  public final byte getByteProperty(String key, String... subkeys) {
    return Byte.parseByte(getProperty(key, subkeys));
  }

  /**
   * Obtains the Double value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Double value associated to a key
   */
  public final double getDoubleProperty(String key, String... subkeys) {
    return Double.parseDouble(getProperty(key, subkeys));
  }

  /**
   * Obtains the Float value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Float value associated to a key
   */
  public final float getFloatProperty(String key, String... subkeys) {
    return Float.parseFloat(getProperty(key, subkeys));
  }

  /**
   * Obtains the Int value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Int value associated to a key
   */
  public final int getIntProperty(String key, String... subkeys) {
    return Integer.parseInt(getProperty(key, subkeys));
  }

  /**
   * Obtains the Long value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Long value associated to a key
   */
  public final long getLongProperty(String key, String... subkeys) {
    return Long.parseLong(getProperty(key, subkeys));
  }

  /**
   * Obtains the Long value associated to a key. If the key is not present, or does not contain a
   * Long value, it returns a provided default value instead.
   *
   * @param value default value to return when key not found or does not contain a valid long
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Long value associated to a key, or the provided default value if the key does not
   *         exist or does not contain a valid long
   */
  public final long getLongProperty(long value, String key, String... subkeys) {
    try {
      value = Long.parseLong(getProperty(key, subkeys));
    } catch (Exception e) {
    }
    return value;
  }

  /**
   * Obtains the Short value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Short value associated to a key
   */
  public final short getShortProperty(String key, String... subkeys) {
    return Short.parseShort(getProperty(key, subkeys));
  }

  /**
   * Obtains the String value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the String value associated to a key
   */
  public final String getStringProperty(String key, String... subkeys) {
    return getProperty(key, subkeys);
  }

}

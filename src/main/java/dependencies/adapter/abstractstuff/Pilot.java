package dependencies.adapter.abstractstuff;

import dependencies.adapter.driver.PageElement;
import dependencies.adapter.driver.SeleniumPilot;
import dependencies.adapter.driver.TracingPilot;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.security.Credentials;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Set;


/**
 * An interface for the wrappers to the Selenium {@link WebDriver}.
 * <p>
 * Different implementations provide alternative ways to drive the browser, including a plain
 * Selenium-only wrapper ({@link SeleniumPilot}) and a tracing wrapper ({@link TracingPilot}), which
 * adds tracing features.steps to the generated report.
 *
 * @see WebDriver
 * @see PageElement
 * @see URL
 */
public interface Pilot {

  /**
   * Close the current browser page.
   */
  void close();

  /**
   * Obtain the URL of the current browser page.
   *
   * @return a {@link String} representation of the current browser page's URL
   */
  String getCurrentUrl();

  /**
   * Obtain the source (HTML) code of the current browser page.
   *
   * @return a {@link String} representation of the current browser page's source code
   */
  String getPageSource();

  /**
   * Capture a screenshot of the current browser page.
   *
   * @return the path to the file containing the captured screenshot
   */
  String getScreenshot();

  /**
   * Obtain the (HTML) title of the current browser page.
   *
   * @return a {@link String} containing the current browser page's title
   */
  String getTitle();

  /**
   * Quit the underlying Selenium driver.
   */
  void quit();

  /**
   * Navigate back to the immediately preceding page in browsing history, if any.
   */
  void navigateBack();

  /**
   * Navigate forward to the immediately following page in browsing history, if any.
   */
  void navigateForward();

  /**
   * Refresh the page currently displayed in the browser.
   */
  void navigateRefresh();

  /**
   * Navigate to a specified URL.
   *
   * @param url a String representing the URL to navigate to
   */
  void navigateTo(String url);

  /**
   * Navigate to a specified URL.
   *
   * @param url a URL identifying the page to navigate to
   */
  void navigateTo(URL url);

  /**
   * Clear a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be cleared
   */
  void clear(PageElement element);

  /**
   * Click on a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be clicked
   */
  void click(PageElement element);

  /**
   * Obtain the text associated to a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element whose text is required
   * @return the text
   */
  String getText(PageElement element);

  /**
   * Verify whether a specified page's element is displayed on the current page or not.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be verified
   * @return true if the specified element is displayed on the current page, false otherwise
   */
  boolean isDisplayed(PageElement element);

  /**
   * Verify whether a specified page's element is enabled on the current page or not.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be verified
   * @return true if the specified element is enabled on the current page, false otherwise
   */
  boolean isEnabled(PageElement element);

  /**
   * Verify whether a specified page's element is selected on the current page or not.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be verified
   * @return true if the specified element is selected on the current page, false otherwise
   */
  boolean isSelected(PageElement element);

  /**
   * Send a sequence of characters to a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be modified
   * @param value the string to be sent
   */
  void sendKeys(PageElement element, String value);

  /**
   * Switch focus to the active element on the browser.
   *
   */
  void switchToActiveElement();

  /**
   * Switch focus to the active alert on the browser.
   */
  void switchToAlert();

  /**
   * Switch focus to the default content on the browser.
   */
  void switchToDefaultContent();

  /**
   * Switch focus to a specified frame.
   * <p>
   * The frame is identified by its (zero-based) index.
   *
   * @param id the (zero-based) index identifying the target frame
   * @see WebDriver.TargetLocator#frame(int)
   */
  void switchToFrame(int id);

  /**
   * Switch focus to a specified frame.
   * <p>
   * The frame is identified by its name or ID.
   *
   * @param id the name or ID identifying the target frame
   * @see WebDriver.TargetLocator#frame(String)
   */
  void switchToFrame(String id);

  /**
   * Switch focus to a specified frame.
   * <p>
   * The frame is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference identifying the target frame
   * @see WebDriver.TargetLocator#frame(WebElement)
   */
  void switchToFrame(PageElement element);

  /**
   * Selects a specified option element from within a select element
   * <p>
   * The select is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference identifying the target frame
   * @param value the textual value of the option to be select
   * @see WebDriver#findElement(By)
   */
  void selectValueFromField(PageElement element, String value);

  /**
   * Obtain the textual value of the specified attribute of an element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element where an attribute value is required
   * @param attribute the name of the attribute to be obtained
   * @return the attribute
   */
  String getAttribute(PageElement element, String attribute);

  /**
   * Obtain a cookie that is set in the current driver
   *
   * @param name the name of the desired cookie object to be retrieved
   * @return the cookie
   */
  Cookie getCookie(String name);

  /**
   * Execute a fragment of JavaScript on the browser.
   *
   * @param js the JavaScript code to be executed
   * @param args1 arguments to pass to the JavaScript code
   */
  void executeJavaScript(String js, Object... args1);

  /**
   * Get the underlying WebDriver instance.
   *
   * @return the underlying WebDriver instance.
   */
  WebDriver getWebDriver();

  /**
   * Get the underlying WebDriverWait instance, for explicit waits.
   *
   * @return the underlying WebDriverWait instance.
   */
  WebDriverWait getWebDriverWait();

  /**
   * Explicit wait until a condition is met.
   *
   * @param condition the condition to wait for.
   */
  void waitUntil(ExpectedCondition<?> condition);

  /**
   * Capture a screenshot of the current browser page.
   *
   * @param screenshotFileName the path to the file where to save the screenshot.
   * @return the file containing the captured screenshot.
   * @throws IOException if the screenshot cannot be saved to the given path.
   */
  File saveScreenshot(String screenshotFileName) throws IOException;

  /**
   * Delete all cookies held on the browser.
   */
  void deleteAllCookies();

  /**
   * Select value from dropdown.
   *
   * @param element unique reference to the dropdown
   * @param value the value to be selected
   */
  void selectValue(PageElement element, String value);

  /**
   * Select value from dropdown based upon the text visible.
   *
   * @param element unique reference to the dropdown
   * @param text the selection based upon text visible
   */
  void selectFromVisibleText(PageElement element, String text);

  /**
   * Checks if the given element exists anywhere on the page.
   *
   * @param element a unique reference identifying the target element.
   * @return true if the element exists, false otherwise.
   */
  boolean elementExists(PageElement element);

  /**
   * Accept browser alert.
   */
  void acceptAlert();

  /**
   * Dismiss browser alert.
   */
  void dismissAlert();

  /**
   * Send a sequence of characters to the browser.
   * <p>
   *
   * @param keysToSend the individual keys to be sent, in order
   */
  void sendKeys(CharSequence... keysToSend);

  /**
   * Clears the contens of a field before sending the specified keys.
   *
   * @param element a unique reference to the element to be modified
   * @param value the string to be sent
   */
  void clearAndSendKeys(PageElement element, String value);

  /**
   * An implicit wait is to tell WebDriver to poll the DOM for a certain amount of time when trying
   * to find an element or elements if they are not immediately available. The default setting is 0.
   * Once set, the implicit wait is set for the life of the WebDriver object instance.
   *
   * @param waitInSeconds time to wait in seconds.
   */
  public void setImplicitWait(int waitInSeconds);

  /**
   * Double click on a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be clicked
   * @see WebDriver#findElement(By)
   * @see Actions#doubleClick(WebElement)
   */
  void doubleClick(PageElement element);

  /**
   * wait for element to be displayed.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element the element to wait for before returning.
   */
  void waitForElementPresent(PageElement element);

  /**
   * Wait for given element to not be visible.
   *
   * @param element to wait for.
   */
  void waitForElementNotPresent(PageElement element);

  /**
   * Get the Current Window Handle, this allows for switching windows
   *
   * @return name of the current window handle
   *
   */
  String getCurrentWindowHandle();

  /**
   * Return all names of Windows Handles
   *
   * @return set of window handle names
   */
  Set<String> getAllWindowHandles();

  /**
   * Switch to Window with the supplied handle name
   *
   * @param windowHandleName the handle name of the window to switch to.
   * @return new pilot for interacting with Window
   */
  Pilot switchToWindowHandle(String windowHandleName);

  /**
   * Wait for page DOM to be ready.
   */
  void waitForPageToLoad();

  /**
   * Click on a specified page's element and wait for the document.readystate to be complete
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be clicked
   */
  void clickAndWait(PageElement element);

  /**
   * Obtain 'value' attribute text associated to a specified page's element.
   * <p>
   * Returns <b>null</b> if value attribute not set.
   *
   * @param element the page's element with a 'value' attribute.
   * @return text value.
   */
  String getValue(PageElement element);

  /**
   * Obtain a list of WebElements matching specified page's element.
   *
   * @param element a reference identifying the target elements.
   * @return a list of the elements matching the given criteria.
   */
  List<WebElement> getMatches(PageElement element);

  /**
   * Authenticate an HTTP Basic Auth dialog using provided credentials.
   *
   * @param credentials the credentials used to login to the auth dialog.
   */
  void authenticateUsing(Credentials credentials);

  /**
   * Moves the mouse to the middle of the element.
   *
   * @param element Unique reference to the element.
   */
  void moveToElement(PageElement element);

  /**
   * Moves the mouse to an offset from the top-left corner of the element.
   *
   * @param element Unique reference to the element.
   * @param xOffset Offset from the top-left corner. A negative value means coordinates left from the element.
   * @param yOffset Offset from the top-left corner. A negative value means coordinates above the element.
   */
  void moveToElement(PageElement element, int xOffset, int yOffset);

  /**
   * Clicks at the current location of the mouse cursor.
   */
  void clickAtMouseLocation();

  /**
   * Maximize the current active window
   */
  void maximize();

}

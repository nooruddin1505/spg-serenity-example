package dependencies.adapter.abstractstuff;

public abstract class LazyMatcher {

  public abstract boolean matches();

  public static LazyMatcher forCondition(final boolean condition) {
    return new LazyMatcher() {
      public boolean matches() {
        return condition;
      }
    };
  }

}

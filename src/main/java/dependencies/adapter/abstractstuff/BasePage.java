package dependencies.adapter.abstractstuff;

import dependencies.adapter.driver.ExecutionContext;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

/**
 * Base class for the page object hierarchy, which provide a convenient abstraction to use in test
 * scripts.
 * <p>
 * UI-based tests use the Selenium library to drive the browser in the same way a manual user would.
 * Page objects offer a higher abstraction over the low level instructions of the Selenium WebDriver
 * interface, in order to provide more declarative test scripts as well as easier to maintain code.
 * <p>
 * The pilot to be used is obtained from the execution context and must have been initialised in
 * advance.
 * <p>
 * To define new page objects:
 * <ol>
 * <li>Create a sub-class of {@link BasePage}</li>
 * <li>Define class constants for each of the elements referred to in the object as follows:
 *
 * <pre>
 * private static final PageElement ELEMENT = new PageElement(By.id(…),"&lt;Description&gt;");
 * </pre>
 *
 * where:
 * <ol>
 * <li>By.id(…) uniquely identifies the specific element on the page. Any of the
 * org.openqa.selenium.By.* family of functions can be used for this purpose, e.g.
 * By.xpath("//title").</li>
 * <li>&lt;Description&gt; will be used to refer to the element in the generated report.</li>
 * </ol>
 * Define methods to mimic the user interaction with the page as follows:
 * <li>For non-navigational operations, i.e. interaction that does not cause a new page to be
 * displayed, create:
 * <ol>
 * <li>void set(…) methods to input information on the page; and,</li>
 * <li>&lt;Java Type&gt; get(…) methods to get information from the page.</li>
 * </ol>
 * <li>For navigational operations, i.e. interactions that cause new pages to be displayed, create:
 * <ol>
 * <li>NewPage do(…) methods to execute the operations that trigger the navigation, e.g. click on a
 * link or button. These methods should generally end with a return new NewPage(); instruction.</li>
 * </ol>
 * <li>Use pilot.* functions to drive the browser with Selenium.</li>
 * <li>If the page constitutes the entry point of a script, then add a static method ThisPage
 * launch() to navigate to the specific URL and return a new page object. Refer to the
 * uk.gov.homeoffice.virtuoso.sample.page.* classes for examples.</li>
 * </ol>
 *
 * @see By
 * @see ExecutionContext
 * @see Pilot
 * @see SeleniumTest#seleniumTestSetup()
 */
public abstract class BasePage extends PageObject {

  /**
   * Handy access to the pilot stored in the execution context.
   */
  protected final Pilot pilot;

  /**
   * Default constructor.
   * <p>
   * Sets the instance member to remember the pilot stored in the execution context, which has been
   * previously initialised.
   *
   * @see ExecutionContext
   * @see Pilot
   * @see SeleniumTest#seleniumTestSetup()
   */
  protected BasePage() {
    this.pilot = ExecutionContext.getInstance().getPilot();
  }

  /**
   * Close the current browser page.
   *
   * @see Pilot
   */
  public void close() {
    pilot.close();
  }

  /**
   * Obtain the URL of the current browser page.
   *
   * @return a {@link String} representation of the current browser page's URL
   * @see Pilot
   */
  public String getCurrentUrl() {
    return pilot.getCurrentUrl();
  }

  /**
   * Obtain the source (HTML) code of the current browser page.
   *
   * @return a {@link String} representation of the current browser page's source code.
   * @see Pilot
   */
  public String getSource() {
    return pilot.getPageSource();
  }

  /**
   * Obtain the (HTML) title of the current browser page.
   *
   * @return a {@link String} containing the current browser page's title.
   * @see Pilot
   */
  public String getTitle() {
    return pilot.getTitle();
  }

}

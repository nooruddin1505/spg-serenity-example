package dependencies.adapter.abstractstuff;

import dependencies.adapter.TestRunDetails;

/**
 * Supporting class for the report generation component.
 *
 * @see BaseReportGenerator
 */
public interface ReportWriterInterface {

  public void appendToReport(TestRunDetails testResult);

  public void finalise();

}

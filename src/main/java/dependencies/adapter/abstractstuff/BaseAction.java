package dependencies.adapter.abstractstuff;

import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

/**
 * Representation of traceable actions.
 */
public abstract class BaseAction {
  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();

  /**
   * To separate elements of the String representation.
   */
  protected static final String SEPARATOR = ": ";

  /**
   * To use in String representation of unknown actions.
   */
  protected static final String UNKNOWN_DESCRIPTION = "unknown";

  /**
   * A description to represent an action in the trace output.
   */
  protected String description;

  /**
   * Base constructor to set the description associated with this action instance.
   *
   * @param description the description to associate with this action
   */
  public BaseAction(String description) {
    this.description = description;
  }

  /**
   * Returns a string representation of this action.
   * <p>
   * The action is represented by its associated description.
   *
   * @return the string representation of this action
   */
  @Override
  public String toString() {
    return "".equals(description) ? BaseAction.UNKNOWN_DESCRIPTION : description;
  }

}

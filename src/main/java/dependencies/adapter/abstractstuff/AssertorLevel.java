package dependencies.adapter.abstractstuff;


public enum AssertorLevel {

  ASSERT, WARN, LOG, DEBUG;

}

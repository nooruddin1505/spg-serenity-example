package dependencies.adapter.abstractstuff;

import org.hamcrest.Matcher;

public interface Assertor {


  // LAZY MATCHER

  void assertThat(String reason, LazyMatcher matcher);

  // TODO: ARRAYEQUALS (boolean[])
  // TODO: ARRAYEQUALS (byte[])
  // TODO: ARRAYEQUALS (char[])
  // TODO: ARRAYEQUALS (double[])
  // TODO: ARRAYEQUALS (float[])
  // TODO: ARRAYEQUALS (int[])
  // TODO: ARRAYEQUALS (long[])
  // TODO: ARRAYEQUALS (Object[])
  // TODO: ARRAYEQUALS (short[])


  // EQUALS (double)

  @Deprecated
  void assertEquals(double expected, double actual);

  @Deprecated
  void assertEquals(String reason, double expected, double actual);

  @Deprecated
  void assertEquals(double expected, double actual, double delta);

  void assertEquals(String reason, double expected, double actual, double delta);


  // EQUALS (float)

  @Deprecated
  void assertEquals(float expected, float actual);

  void assertEquals(String reason, float expected, float actual);


  // EQUALS (long)

  @Deprecated
  void assertEquals(long expected, long actual);

  void assertEquals(String reason, long expected, long actual);


  // EQUALS (Object[])

  @Deprecated
  void assertEquals(Object[] expected, Object[] actual);

  @Deprecated
  void assertEquals(String reason, Object[] expected, Object[] actual);


  // EQUALS (OBJECT)

  @Deprecated
  void assertEquals(Object expected, Object actual);

  void assertEquals(String reason, Object expected, Object actual);


  // FALSE

  @Deprecated
  void assertFalse(boolean condition);

  void assertFalse(String reason, boolean condition);


  // NOT EQUALS (double)

  @Deprecated
  void assertNotEquals(double expected, double actual);

  @Deprecated
  void assertNotEquals(String reason, double expected, double actual);

  @Deprecated
  void assertNotEquals(double expected, double actual, double delta);

  void assertNotEquals(String reason, double expected, double actual, double delta);


  // NOT EQUALS (float)

  @Deprecated
  void assertNotEquals(float expected, float actual);

  void assertNotEquals(String reason, float expected, float actual);


  // NOT EQUALS (long)

  @Deprecated
  void assertNotEquals(long expected, long actual);

  void assertNotEquals(String reason, long expected, long actual);


  // NOT EQUALS (Object[])

  @Deprecated
  void assertNotEquals(Object[] expected, Object[] actual);

  @Deprecated
  void assertNotEquals(String reason, Object[] expected, Object[] actual);


  // NOT EQUALS (OBJECT)

  @Deprecated
  void assertNotEquals(Object expected, Object actual);

  void assertNotEquals(String reason, Object expected, Object actual);


  // NOT NULL

  @Deprecated
  void assertNotNull(Object actual);

  void assertNotNull(String reason, Object actual);


  // NOT SAME

  @Deprecated
  void assertNotSame(Object expected, Object actual);

  void assertNotSame(String reason, Object expected, Object actual);


  // NULL

  @Deprecated
  void assertNulL(Object actual);

  void assertNull(String reason, Object actual);


  // SAME

  @Deprecated
  void assertSame(Object expected, Object actual);

  void assertSame(String reason, Object expected, Object actual);


  // HAMCREST MATCHER

  @Deprecated
  <T> void assertThat(T actual, Matcher<T> matcher);

  <T> void assertThat(String reason, T actual, Matcher<T> matcher);


  // TRUE

  @Deprecated
  void assertTrue(boolean condition);

  void assertTrue(String reason, boolean condition);


  // FAIL, etc.

  @Deprecated
  void fail();

  void fail(String reason);

}

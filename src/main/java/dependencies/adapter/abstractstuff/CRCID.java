package dependencies.adapter.abstractstuff;


public enum CRCID {
  CRC00("CRC00"),
  CRC01("CRC01"),
  CRC02("CRC02"),
  CRC03("CRC03"),
  CRC04("CRC04"),
  CRC05("CRC05"),
  CRC06("CRC06"),
  CRC07("CRC07"),
  CRC08("CRC08"),
  CRC09("CRC09"),
  CRC10("CRC10"),
  CRC11("CRC11"),
  CRC12("CRC12"),
  CRC13("CRC13"),
  CRC14("CRC14"),
  CRC15("CRC15"),
  CRC16("CRC16"),
  CRC17("CRC17"),
  CRC18("CRC18"),
  CRC19("CRC19"),
  CRC20("CRC20"),
  CRC21("CRC21"),
  CRC22("CRC22"),
  CRC23("CRC23"),
  CRC24("CRC24"),
  CRC30("CRC30");



  private final String text;

  CRCID(final String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return text;
  }

}


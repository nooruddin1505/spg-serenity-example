package dependencies.adapter.abstractstuff;

import dependencies.adapter.TestDataInterface;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * A container for the test data used by a test.
 */
public abstract class TestData implements TestDataInterface {

  /**
   * The keyword hierarchy separator to be used by this test data instance.
   */
  protected static final String HIERARCHY_SEPARATOR = ".";

  /**
   * The files used to load the data in this instance.
   */
  protected List<String> filePaths;

  /**
   * Default constructor.
   * <p>
   * Initialises the list of files used to load the data in this instance to an empty sequence.
   */
  public TestData() {
    filePaths = new LinkedList<String>();
  }

  /**
   * Loads the test data from a given file.
   * <p>
   * Implementation specific.
   *
   * @param testDataFile the location of the file containing the configuration data to be loaded
   * @param key the field to use as criteria to determine the data to be loaded
   * @param value the value to use as criteria to determine the data to be loaded
   */
  protected abstract void load(File testDataFile, String key, String value);

  /**
   * Obtains the input value associated to a key.
   * <p>
   * Implementation specific.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the input value associated to a key
   */
  public abstract String getInputValue(String key, String... subkeys);

  /**
   * Obtains the expected result associated to a key.
   * <p>
   * Implementation specific.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the expected result associated to a key
   */
  public abstract String getExpectedResult(String key, String... subkeys);

  /**
   * Obtains the keys in the set of input values tracked by this test data instance.
   * <p>
   * Implementation specific.
   *
   * @return the keys in this test data instance's input values
   */
  public abstract String[] getInputValueKeys();

  /**
   * Obtains the keys in the set of expected results tracked by this test data instance.
   * <p>
   * Implementation specific.
   *
   * @return the keys in this test data instance's expected results
   */
  public abstract String[] getExpectedResultKeys();

  /**
   * Obtains the Boolean expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Boolean expected result associated to a key
   */
  @Override
  public final boolean getBooleanExpectedResult(String key, String... subkeys) {
    return Boolean.parseBoolean(getExpectedResult(key, subkeys));
  }

  /**
   * Obtains the Byte expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Byte expected result associated to a key
   */
  @Override
  public final byte getByteExpectedResult(String key, String... subkeys) {
    return Byte.parseByte(getExpectedResult(key, subkeys));
  }

  /**
   * Obtains the Double expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Double expected result associated to a key
   */
  @Override
  public final double getDoubleExpectedResult(String key, String... subkeys) {
    return Double.parseDouble(getExpectedResult(key, subkeys));
  }

  /**
   * Obtains the Float expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Float expected result associated to a key
   */
  @Override
  public final float getFloatExpectedResult(String key, String... subkeys) {
    return Float.parseFloat(getExpectedResult(key, subkeys));
  }

  /**
   * Obtains the Int expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Int expected result associated to a key
   */
  @Override
  public final int getIntExpectedResult(String key, String... subkeys) {
    return Integer.parseInt(getExpectedResult(key, subkeys));
  }

  /**
   * Obtains the Long expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Long expected result associated to a key
   */
  @Override
  public final long getLongExpectedResult(String key, String... subkeys) {
    return Long.parseLong(getExpectedResult(key, subkeys));
  }

  /**
   * Obtains the Short expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Short expected result associated to a key
   */
  @Override
  public final short getShortExpectedResult(String key, String... subkeys) {
    return Short.parseShort(getExpectedResult(key, subkeys));
  }

  /**
   * Obtains the Boolean input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Boolean input value associated to a key
   */
  @Override
  public final boolean getBooleanInputValue(String key, String... subkeys) {
    return Boolean.parseBoolean(getInputValue(key, subkeys));
  }

  /**
   * Obtains the Byte input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Byte input value associated to a key
   */
  @Override
  public final byte getByteInputValue(String key, String... subkeys) {
    return Byte.parseByte(getInputValue(key, subkeys));
  }

  /**
   * Obtains the Double input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Double input value associated to a key
   */
  @Override
  public final double getDoubleInputValue(String key, String... subkeys) {
    return Double.parseDouble(getInputValue(key, subkeys));
  }

  /**
   * Obtains the Float input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Float input value associated to a key
   */
  @Override
  public final float getFloatInputValue(String key, String... subkeys) {
    return Float.parseFloat(getInputValue(key, subkeys));
  }

  /**
   * Obtains the Int input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Int input value associated to a key
   */
  @Override
  public final int getIntInputValue(String key, String... subkeys) {
    return Integer.parseInt(getInputValue(key, subkeys));
  }

  /**
   * Obtains the Long input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Long input value associated to a key
   */
  @Override
  public final long getLongInputValue(String key, String... subkeys) {
    return Long.parseLong(getInputValue(key, subkeys));
  }

  /**
   * Obtains the Short input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the Short input value associated to a key
   */
  @Override
  public final short getShortInputValue(String key, String... subkeys) {
    return Short.parseShort(getInputValue(key, subkeys));
  }

  /*
   * String representation of this test data object.
   */
  @Override
  public String toString(String inputValuesLabel, String inputValues, String expectedResultsLabel, String expectedValues) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("Test Data Files");
    buffer.append('\n');
    buffer.append(filePaths);
    buffer.append('\n');
    buffer.append(inputValuesLabel);
    buffer.append('\n');
    buffer.append(inputValues);
    buffer.append('\n');
    buffer.append(expectedResultsLabel);
    buffer.append('\n');
    buffer.append(expectedValues);
    return buffer.toString();
  }

  /**
   * Add the test data in the given object to this one.
   *
   * @param newTestData the object whose data will be added to this one
   */
  @Override
  public void add(TestData newTestData) {
    filePaths.addAll(newTestData.filePaths);
  }
}

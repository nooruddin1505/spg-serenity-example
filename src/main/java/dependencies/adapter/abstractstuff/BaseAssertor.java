package dependencies.adapter.abstractstuff;

import dependencies.adapter.driver.ExecutionContext;
import org.hamcrest.Matcher;

public class BaseAssertor implements Assertor {

  private static final AssertorLevel DEFAULT_EXECUTION_LEVEL = AssertorLevel.ASSERT;

  private AssertorLevel executionLevel = null;

  public BaseAssertor() {
    ExecutionContext context = ExecutionContext.getInstance();
    String executionLevelString =
            context.getConfigData().getProperty("Assertor", "Execution", "Level");
    this.executionLevel =
            (executionLevelString == null || executionLevelString.equals("")) ? DEFAULT_EXECUTION_LEVEL
                    : AssertorLevel.valueOf(executionLevelString);
  }

  protected void processAssertionResult(AssertorLevel level, String reason, boolean result) {}

  protected final void assertThat(AssertorLevel level, String reason, LazyMatcher matcher) {
    if (executionLevel.compareTo(level) >= 0) {
      boolean result = matcher.matches();
      processAssertionResult(level, reason, result);
      if (!result && AssertorLevel.ASSERT.equals(level)) {
        throw reason == null ? new AssertionError() : new AssertionError(reason);
      }
    }
  }

  // LAZY MATCHER

  public final void assertThat(String reason, LazyMatcher matcher) {
    assertThat(AssertorLevel.ASSERT, reason, matcher);
  }

  // TODO: ARRAYEQUALS (boolean[])
  // TODO: ARRAYEQUALS (byte[])
  // TODO: ARRAYEQUALS (char[])
  // TODO: ARRAYEQUALS (double[])
  // TODO: ARRAYEQUALS (float[])
  // TODO: ARRAYEQUALS (int[])
  // TODO: ARRAYEQUALS (long[])
  // TODO: ARRAYEQUALS (Object[])
  // TODO: ARRAYEQUALS (short[])

  // EQUALS (double)

  @Deprecated
  public final void assertEquals(double expected, double actual) {
    assertEquals(null, expected, actual, 0.0D);
  }

  @Deprecated
  public final void assertEquals(String reason, double expected, double actual) {
    assertEquals(reason, expected, actual, 0.0D);
  }

  @Deprecated
  public final void assertEquals(double expected, double actual, double delta) {
    assertEquals(null, expected, actual, delta);
  }

  public final void assertEquals(String reason, double expected, double actual, double delta) {
    assertThat(reason, LazyMatcher.forCondition(Math.abs(expected - actual) <= delta));
  }

  // EQUALS (float)

  @Deprecated
  public final void assertEquals(float expected, float actual) {
    assertEquals(null, expected, actual);
  }

  public final void assertEquals(String reason, float expected, float actual) {
    assertThat(reason, LazyMatcher.forCondition(expected == actual));
  }

  // EQUALS (long)

  @Deprecated
  public final void assertEquals(long expected, long actual) {
    assertEquals(null, expected, actual);
  }

  public final void assertEquals(String reason, long expected, long actual) {
    assertThat(reason, LazyMatcher.forCondition(expected == actual));
  }

  // EQUALS (Object[])

  @Deprecated
  public final void assertEquals(Object[] expected, Object[] actual) {
    assertEquals(null, expected, actual);
  }

  @Deprecated
  public final void assertEquals(String reason, Object[] expected, Object[] actual) {
    assertThat(reason, LazyMatcher.forCondition(expected == actual));
  }

  // EQUALS (OBJECT)

  @Deprecated
  public final void assertEquals(Object expected, Object actual) {
    assertEquals(null, expected, actual);
  }

  public final void assertEquals(String reason, Object expected, Object actual) {
    assertThat(reason, LazyMatcher.forCondition(expected.equals(actual)));
  }

  // FALSE

  @Deprecated
  public final void assertFalse(boolean condition) {
    assertFalse(null, condition);
  }

  public final void assertFalse(String reason, boolean condition) {
    assertThat(reason, LazyMatcher.forCondition(!condition));
  }

  // NOT EQUALS (double)

  @Deprecated
  public final void assertNotEquals(double expected, double actual) {
    assertNotEquals(null, expected, actual, 0.0D);
  }

  @Deprecated
  public final void assertNotEquals(String reason, double expected, double actual) {
    assertNotEquals(reason, expected, actual, 0.0D);
  }

  @Deprecated
  public final void assertNotEquals(double expected, double actual, double delta) {
    assertNotEquals(null, expected, actual, delta);
  }

  public final void assertNotEquals(String reason, double expected, double actual, double delta) {
    assertThat(reason, LazyMatcher.forCondition(Math.abs(expected - actual) > delta));
  }

  // NOT EQUALS (float)

  @Deprecated
  public final void assertNotEquals(float expected, float actual) {
    assertNotEquals(null, expected, actual);
  }

  public final void assertNotEquals(String reason, float expected, float actual) {
    assertThat(reason, LazyMatcher.forCondition(expected != actual));
  }

  // NOT EQUALS (long)

  @Deprecated
  public final void assertNotEquals(long expected, long actual) {
    assertNotEquals(null, expected, actual);
  }

  public final void assertNotEquals(String reason, long expected, long actual) {
    assertThat(reason, LazyMatcher.forCondition(expected != actual));
  }

  // NOT EQUALS (Object[])

  @Deprecated
  public final void assertNotEquals(Object[] expected, Object[] actual) {
    assertNotEquals(null, expected, actual);
  }

  @Deprecated
  public final void assertNotEquals(String reason, Object[] expected, Object[] actual) {
    assertThat(reason, LazyMatcher.forCondition(expected != actual));
  }

  // NOT EQUALS (OBJECT)

  @Deprecated
  public final void assertNotEquals(Object expected, Object actual) {
    assertNotEquals(null, expected, actual);
  }

  public final void assertNotEquals(String reason, Object expected, Object actual) {
    assertThat(reason, LazyMatcher.forCondition(!expected.equals(actual)));
  }

  // NOT NULL

  @Deprecated
  public final void assertNotNull(Object actual) {
    assertNotNull(null, actual);
  }

  public final void assertNotNull(String reason, Object actual) {
    assertThat(reason, LazyMatcher.forCondition(actual != null));
  }

  // NOT SAME

  @Deprecated
  public final void assertNotSame(Object expected, Object actual) {
    assertNotSame(null, expected, actual);
  }

  public final void assertNotSame(String reason, Object expected, Object actual) {
    assertThat(reason, LazyMatcher.forCondition(expected != actual));
  }

  // NULL

  @Deprecated
  public final void assertNulL(Object actual) {
    assertNull(null, actual);
  }

  public final void assertNull(String reason, Object actual) {
    assertThat(reason, LazyMatcher.forCondition(actual == null));
  }

  // SAME

  @Deprecated
  public final void assertSame(Object expected, Object actual) {
    assertSame(null, expected, actual);
  }

  public final void assertSame(String reason, Object expected, Object actual) {
    assertThat(reason, LazyMatcher.forCondition(expected == actual));
  }

  // HAMCREST MATCHER

  @Deprecated
  public final <T> void assertThat(T actual, Matcher<T> matcher) {
    assertThat(null, actual, matcher);
  }

  public final <T> void assertThat(String reason, T actual, Matcher<T> matcher) {
    assertThat(reason, LazyMatcher.forCondition(matcher.matches(actual)));
  }

  // TRUE

  @Deprecated
  public final void assertTrue(boolean condition) {
    assertTrue(null, condition);
  }

  public final void assertTrue(String reason, boolean condition) {
    assertThat(reason, LazyMatcher.forCondition(condition));
  }

  // FAIL, etc.

  @Deprecated
  public final void fail() {
    fail(null);
  }

  public final void fail(String reason) {
    assertThat(reason, LazyMatcher.forCondition(false));
  }

}

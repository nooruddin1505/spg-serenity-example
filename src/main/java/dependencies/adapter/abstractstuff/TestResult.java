package dependencies.adapter.abstractstuff;


/**
 * Test result types.
 * <ul>
 * <li>PASS: the test completed successfully</li>
 * <li>IGNORED: the test was not included in the run</li>
 * <li>FAILURE: the test failed due to an assertion not being met</li>
 * <li>ERROR: an unexpected exceptional condition was raised during the execution of the test</li>
 * </ul>
 */
public enum TestResult {
  PASS, IGNORED, FAILURE, ERROR
}

package dependencies.adapter.abstractstuff;

import dependencies.adapter.driver.ExecutionContext;
import dependencies.adapter.driver.PilotFactory;
import dependencies.adapter.rules.SeleniumTestWatcher;
import org.junit.After;
import org.junit.Rule;
import org.junit.rules.TestWatcher;

/**
 * Root for Selenium-based tests.
 * <p>
 * Initialises the {@link Pilot} to be used by the test scripts.
 *
 * @see Pilot
 * @see PilotFactory
 */
public abstract class SeleniumTest extends BaseTest {

  /**
   * Default constructor.
   * <p>
   * Placeholder for any specific initialisation required by Selenium-based tests.
   */
  protected SeleniumTest() {
    this(null, null);
  }

  /**
   * Constructor with specified test data selector.
   * <p>
   * The test will use the data identified by the (key,value) pair given as parameter.
   *
   * @param key the identifier for the test data selection criteria
   * @param value the value for the test data selection criteria
   * @see TestData
   * @see #init(String, String)
   */
  protected SeleniumTest(String key, String value) {
    super(key, value);
    seleniumTestSetup();
  }

  /**
   * Initialises the {@link Pilot} to be used by the test scripts.
   *
   * @see Pilot
   * @see PilotFactory#createTestPilot()
   */
  public final void seleniumTestSetup() {
    if (ExecutionContext.getInstance().getPilot() == null) {
      ExecutionContext.getInstance().setPilot(PilotFactory.createTestPilot());
    }
  }

  /**
   * Placeholder for any actions to take place upon completion of each individual test execution.
   */
  @After
  public final void seleniumTestTearDown() {
    // any changes to this method should be reflected in the CucumberTestListener.testFinished method
  }

  @Rule
  public TestWatcher watchman = new SeleniumTestWatcher();

}

package dependencies.adapter.abstractstuff;

import dependencies.adapter.BaseTracer;
import dependencies.adapter.data.ConfigDataLoader;
import dependencies.adapter.driver.ExecutionContext;
import dependencies.adapter.actions.ExecutionContextActions;
import dependencies.adapter.TestDataLoader;
import dependencies.adapter.TracerAssertor;
import dependencies.adapter.rules.BaseTestWatcher;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.rules.TestWatcher;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * Test scripts correspond to Java functions marked with the relevant JUnit annotations, and
 * included in classes belonging to the test hierarchy. The base elements of this hierarchy
 * initialise the required components of the framework depending on the type of test.
 * <p>
 * Standard Java language features, such as inheritance and overloading, should be used in order to
 * create modular, reusable test scripts.
 * <p>
 * To define new test objects:
 * <ol>
 * <li>Create a sub-class of: a. uk.gov.homeoffice.virtuoso.framework.test.SeleniumTest, for UI
 * tests; or, b. uk.gov.homeoffice.virtuoso.framework.test.CxfTest, for Service tests.</li>
 * <li>Create a method void testName() for each test to be carried out. Note: testName will be used
 * to identify the test in the generated reports.</li>
 * <li>Mark the method with the @org.junit.Test annotation.</li>
 * <li>The script for testName() will:
 * <ol>
 * <li>Use the testData instance member to access input values and expected results.</li>
 * <li>Use page objects (see section 1.3) or service objects (see section 1.4) as required to
 * specify the script features.steps.</li>
 * <li>Use any of the org.junit.Assert.* static functions to introduce checks in the script as
 * required.</li>
 * </ol>
 * </li>
 * <li>To ignore a test mark it with the {@link Ignore} annotation.</li>
 * <li>To include additional test data or override the default behaviour, create a method void
 * myTestSetup(), mark it with the {@link Before} annotation, and set the testData instance member
 * as required. Note: the identifier myTestSetup cannot be one of: seleniumTestSetup or
 * baseTestSetup.</li>
 * </ol>
 * <p>
 * Refer to the uk.gov.homeoffice.virtuoso.sample.test.* classes for examples.
 *
 * @see TestData
 */
public abstract class BaseTest{

  private static final String FILE_EXT_SEPARATOR = ".";

  /**
   * The test data to be used in the execution of an individual test script.
   */
  protected TestData testData;

  /**
   *
   */
  protected Assertor assertor;

  /**
   * The identifier for the test data selection criteria.
   */
  protected String key;

  /**
   * The value for the test data selection criteria.
   */
  protected String value;

  /**
   * Default constructor.
   * <p>
   * The test will use a default entry from the data source.
   *
   * @see TestData
   * @see #init(String, String)
   */
  protected BaseTest() {
    init(null, null);
  }

  /**
   * Constructor with specified test data selector.
   * <p>
   * The test will use the data identified by the (key,value) pair given as parameter.
   *
   * @param key the identifier for the test data selection criteria
   * @param value the value for the test data selection criteria
   * @see TestData
   * @see #init(String, String)
   */
  protected BaseTest(String key, String value) {
    init(key, value);
  }

  /**
   * Sets the test data selection criteria as specified in the (key,value) pair given as parameter.
   *
   * @param key the identifier for the test data selection criteria
   * @param value the value for the test data selection criteria
   */
  protected void init(String key, String value) {
    this.key = key;
    this.value = value;
    this.name = new TestName();
    ExecutionContext context = ExecutionContext.getInstance();
    ConfigData configData = context.getConfigData();

    String className = getClass().getSimpleName();

    // test data loading
    List<TestData> dataFilesToLoad = Arrays.asList(
            TestDataLoader.load(configData.getStringProperty("Data", "Common"), key, value),
            TestDataLoader.load(className, key, value),
            TestDataLoader.load(configData.getStringProperty("Data", className, "Specific"), key, value)
    );
    for (TestData data : dataFilesToLoad) {
      appendTestData(data);
    }

    context.setTestData(testData);
    context.setTracer(new BaseTracer());
    context.setAssertor(this.assertor = new TracerAssertor());
  }

  /**
   * Initialises the test data object or appends data to an already initialised instance.
   * <p>
   * @param data TestData to be added
   */
  private void appendTestData(TestData data) {
    if (data != null) {
      if (testData == null) {
        testData = data;
      } else {
        testData.add(data);
      }
    }
  }

  /**
   * Test setup routine carried out prior to the execution of each individual test.
   * <p>
   * Loads the test data associated to an individual test. Data is load from the following sources,
   * in the order provided:
   * <ol>
   * <li>The file specified in the Data.Common configuration data property</li>
   * <li>The file specified by the name of the class containing the script to be executed</li>
   * <li>The files specified in the Data.&lt;test-class-name&gt;.Specific configuration data
   * property, where &lt;test-class-name&gt; is the name of the class containing the script to be
   * executed.</li>
   * <li>ScreenRecording configuration property specifies whether to record the test run or not.
   * Does not record by default.
   * </ol>
   *
   * @see ConfigData
   * @see TestData
   */
  @Before
  public final void baseTestSetup() {
    ExecutionContextActions.onTestMethodSetup(getClass().getSimpleName(), name.getMethodName(), key, value);    // the name object is only populated here - not in the constructor
  }

  /**
   * Currently for stopping the test recording if it is running
   */
  @After
  public final void baseTestTearDown() {
    //ExecutionContextActions.stopScreenRecorder();  // any changes to this method should be reflected in the CucumberTestListener.testFinished method
  }

  /**
   * Includes the test case identifier in the prefix to use when constructing the name of a results
   * file, where appropriate.
   *
   * @return the prefix to use when constructing the name of a results file, including the test case
   *         identifier, if appropriate
   *
   * @see Parameterized.Parameters
   */
  protected String getResultsFilePrefix() {
    String filePrefix = ExecutionContext.getInstance().getRunID();
    if (this.value != null) {
      filePrefix = filePrefix + "_" + this.value;
    }
    return filePrefix;
  }

  /**
   * Opens a file for writing results from a test.
   *
   * @param fileSuffix the suffix to add to the results file prefix
   * @param fileExt the extension to add to the results file prefix
   * @return a stream for the output file
   *
   * @see #getResultsFilePrefix
   * @see PrintStream
   */
  public final PrintStream openResultsFile(String fileSuffix, String fileExt) {
    ConfigData configData = ExecutionContext.getInstance().getConfigData();
    String resultsDir = configData.getProperty("Results", "Dir");
    fileSuffix = (fileSuffix != null) ? fileSuffix : "";
    fileExt = (fileExt != null) ? fileExt : "";
    String sep = configData.getProperty("Results", "File", "Ext", "Separator");
    String filePath =
            resultsDir + File.separator + getResultsFilePrefix() + fileSuffix + ""
                    + (sep != null ? sep : FILE_EXT_SEPARATOR) + fileExt;
    PrintStream stream = null;
    try {
      stream = new PrintStream(filePath);
    } catch (Exception e) {
      e.printStackTrace(System.err);
    }
    return stream;
  }

  /**
   * Get the path to a file in the test data folder.
   *
   * @param fileName the file to locate in the test data folder
   * @return the path to the given file within the test data folder
   */
  public String getTestDataFilePath(String fileName) {
    ConfigData configData = ExecutionContext.getInstance().getConfigData();
    String testDataDir = configData.getProperty("Data", "Dir");
    return testDataDir + File.separator + fileName;
  }

  @Rule
  public TestName name = new TestName();

  @Rule
  public TestWatcher watchman = new BaseTestWatcher();

  @BeforeClass
  public final static void baseTestClassSetup() {
    ExecutionContext context = ExecutionContext.getInstance();
    if (context.getConfigData() == null) {
      context.setConfigData(ConfigDataLoader.load());
    }
  }

  @AfterClass
  public final static void baseTestClassTearDown() {}

}

package dependencies.adapter.abstractstuff;

import dependencies.adapter.TestRunDetails;

/**
 * Routines for the report generation functionality.
 */
public interface BaseReportGenerator {

  /**
   * Initiates a new run record with the given description.
   *
   * @param id the description of the started run
   */
  void startRunRecord(String id);

  /**
   * Adds details of a test run to the report.
   *
   * @param currentTestResults the details of the test run to add
   */
  void addResult(TestRunDetails currentTestResults);

  /**
   * Outputs the report.
   */
  void outputReport();

}

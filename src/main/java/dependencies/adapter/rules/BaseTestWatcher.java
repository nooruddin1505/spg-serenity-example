package dependencies.adapter.rules;

import dependencies.adapter.driver.ExecutionContext;
import dependencies.adapter.actions.ExecutionContextActions;
import dependencies.adapter.abstractstuff.TestResult;
import junit.framework.AssertionFailedError;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class BaseTestWatcher extends TestWatcher {

  @Override
  protected void failed(Throwable e, Description description) {
    if (e instanceof AssertionFailedError || e instanceof AssertionError) {
      ExecutionContext.getInstance().getTestRunDetails().setResult(TestResult.FAILURE);
    } else {
      ExecutionContext.getInstance().getTestRunDetails().setResult(TestResult.ERROR);

      // Validate exception message content
      String message = e.getMessage();
      if (message == null || message.isEmpty()) {
        message = "No Exception message returned.";
      } else {
        message = message.split("\n")[0];
      }

      // Log Exception message
//      ExecutionContext.getInstance().getTracer().trace(
//              new FailureAction("Exception", null),
//              new BaseElement(e.getClass().getSimpleName() + ": " + message)
//      );
    }
  }

  @Override
  protected void succeeded(Description description) {
    ExecutionContext.getInstance().getTestRunDetails().setResult(TestResult.PASS);
  }

  @Override
  protected void finished(Description description) {
    ExecutionContextActions.onTestFinished();
  }

}

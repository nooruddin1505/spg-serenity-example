package dependencies.adapter.rules;

import dependencies.adapter.actions.ExecutionContextActions;
import org.junit.runner.Description;


public class SeleniumTestWatcher extends BaseTestWatcher {

  @Override
  protected void failed(Throwable e, Description description) {
    super.failed(e, description);
    ExecutionContextActions.captureScreenshot();
  }

  @Override
  protected void finished(Description description) {
    super.finished(description);
    ExecutionContextActions.quitPilot();
  }

}

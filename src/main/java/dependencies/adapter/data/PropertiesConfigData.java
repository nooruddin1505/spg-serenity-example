package dependencies.adapter.data;

import dependencies.adapter.abstractstuff.ConfigData;
import dependencies.adapter.utils.StringUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * An implementation of configuration data based on the properties (key=value) format.
 */
public class PropertiesConfigData extends ConfigData {

  /**
   * The instance linked to the properties keeping the configuration data.
   */
  protected Properties configData;

  /**
   * Base constructor that delegates initialisation to the superclass.
   *
   * @param filepaths the location of the files containing the configuration data to be loaded
   */
  public PropertiesConfigData(List<String> filepaths) {
    super(filepaths);
  }

  /**
   * Loads configuration data from the given file in properties (key=value) format.
   *
   * @param filePath the location of the file containing the configuration data to be loaded
   */
  @Override
  protected void loadConfigData(String filePath) {
    if (filePath == null) {
      throw new IllegalArgumentException(
              "PropertiesConfigData: loadConfigData(String): error: filePath argument cannot be null");
    }

    InputStream is = this.getClass().getClassLoader().getResourceAsStream(filePath);
    if (is == null) {
      try {
        is = new FileInputStream(filePath);
      } catch (FileNotFoundException e) {
        throw new RuntimeException(
                "PropertiesConfigData: loadConfigData(String): error: cannot find " + filePath);
      }
    }

    try {
      if (configData == null) {
        configData = new Properties();
      }
      if (filePath.endsWith(".xml")) {
        configData.loadFromXML(is);
      } else {
        configData.load(is);
      }
    } catch (IOException ioe) {
      throw new RuntimeException(
              "PropertiesConfigData: loadConfigData(String): error: cannot read " + filePath, ioe);
    }
  }

  /**
   * Obtains all keys associated to this configuration data file.
   *
   * @return the String keys
   */
  public Collection<String> getKeys() {
    Set<String> result = new HashSet<String>();
    for (Object o : configData.keySet()) {
      result.add((String) o);
    }
    return result;
  }

  /**
   * Obtains the value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the value associated to a key
   */
  @Override
  public final String getProperty(String key, String... subkeys) {
    return configData
            .getProperty(StringUtils.concatWith(KEYWORD_HIERARCHY_SEPARATOR, key, subkeys));
  }

}

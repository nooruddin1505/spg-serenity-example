package dependencies.adapter.data;

import dependencies.adapter.abstractstuff.ConfigData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


public class AllPropertiesLoader extends ConfigData {

  /**
   * List of configuration objects containing properties information.
   */
  private List<ConfigData> configDataList;

  /**
   * Loads the data from the configuration objects to a single list.
   *
   * @param configData the configuration data
   */
  public AllPropertiesLoader(ConfigData... configData) {
    configDataList = new ArrayList<ConfigData>(Arrays.asList(configData));
  }

  /**
   * Obtain all keys from every configuration object in list.
   *
   * @return the String keys
   */
  @Override
  public Collection<String> getKeys() {
    Collection<String> keys = new ArrayList<String>();
    for (ConfigData data : configDataList) {
      keys.addAll(data.getKeys());
    }
    return keys;
  }

  /**
   * Obtains the value associated to a key. If a duplicate key is found, its value will overwrite
   * the previous one.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the value associated to a key
   */
  @Override
  public String getProperty(String key, String... subkeys) {
    String property = null;
    for (ConfigData data : configDataList) {
      if (data.getProperty(key, subkeys) != null) {
        property = data.getProperty(key, subkeys);
      }
    }
    return property;
  }

  /**
   * Unused method.
   */
  @Override
  protected void loadConfigData(String filePath) {}

}

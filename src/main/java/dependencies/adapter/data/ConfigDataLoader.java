package dependencies.adapter.data;

import dependencies.adapter.abstractstuff.ConfigData;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Utilities to load configuration data from default location and determine the specific subclass to
 * use based on the extension of the files to be loaded.
 *
 * @see ConfigData
 */
public final class ConfigDataLoader {

  /**
   * The default configuration data file.
   */
  private static final String PROPERTY_CONFIG_DATA_FILE_PATH    = "CONFIG_DATA_FILE_PATH";
  private static final String DEFAULT_CONFIG_DATA_FILE_PATH     = "config.properties";

  private static final String PROPERTY_ENV_DATA_FILE_PATH       = "ENVIRONMENT_DATA_FILE_PATH";
  private static final String DEFAULT_ENV_DATA_FILE_PATH        = "environment.properties";

  private static final String PROPERTY_ADDITIONAL_CONFIG_FILES  = "ADDITIONAL_CONFIG_FILES";

  private static final String ADDITIONAL_CONFIG_FILES_SEPARATOR = ",";

  /**
   * List to store file paths of configuration files
   */
  private static List<String> configFiles;

  /**
   * Create a new configuration files list instance if one does not already exists
   */
  private static List<String> getConfigFilesList() {
    if (configFiles == null) {
      configFiles = new ArrayList<String>();
    }
    return configFiles;
  }

  /**
   * Utility to load configuration data from a given file.
   * <p>
   * Determines the actual implementation of {@link ConfigData} to be used, based on the extension
   * of the given file.
   *
   * @param files the configuration file to load
   * @return the {@link ConfigData} instance with loaded configuration data from the given file
   */

  public static ConfigData load(List<String> files) {
    if (areAllFilesPropertyFiles(files)) {
      return new PropertiesConfigData(files);
    } else {
      throw new IllegalArgumentException(
              "ConfigDataLoader: load(boolean,String,String...): error: unknown file type exists in list: "
                      + files.toString());
    }
  }

  /**
   * Check all files to make sure they are legal property files
   *
   * @param files list of file paths of configuration files to load
   * @return boolean
   */
  private static boolean areAllFilesPropertyFiles(List<String> files) {
    for (String file : files) {
      if (!file.endsWith(".properties") && !file.endsWith(".xml")) {
        return false;
      }
    }
    return true;
  }

  /**
   * Utility to load configuration data from a file specified in the CONFIG_DATA_FILE_PATH
   * environment variable, or a default location if such variable has not been defined.
   *
   * @return the {@link ConfigData} instance with loaded configuration data
   */
  public static ConfigData load() {
    String config       = getPropertyValue(PROPERTY_CONFIG_DATA_FILE_PATH, DEFAULT_CONFIG_DATA_FILE_PATH);
    String environment  = getPropertyValue(PROPERTY_ENV_DATA_FILE_PATH,    DEFAULT_ENV_DATA_FILE_PATH);

    addConfigFilesToList(config, environment);

    String additionalConfigFiles = System.getProperty(PROPERTY_ADDITIONAL_CONFIG_FILES);
    if (additionalConfigFiles != null) {
      addConfigFilesToList(additionalConfigFiles.split(ADDITIONAL_CONFIG_FILES_SEPARATOR));
    }

    return new AllPropertiesLoader(ConfigDataLoader.load(getConfigFilesList()), new SystemPropertiesData());
  }

  /**
   * Get the value for a property or a default value, if no property is defined
   *
   * @param property        The property value key
   * @param defaultValue    The default value for the property, if no specific value is defined
   * @return
   */
  private static String getPropertyValue(String property, String defaultValue) {
    String propertyValue = System.getProperty(property);
    return (propertyValue != null) ? propertyValue : defaultValue;
  }

  /**
   * Add all configuration file paths to a single list
   *
   * @param filePaths list of file paths of configuration files to load
   */
  private static void addConfigFilesToList(String... filePaths) {
    for (String file : filePaths) {
      if (ConfigDataLoader.class.getClassLoader().getResource(file) != null || new File(file).exists()) {
        getConfigFilesList().add(file);
      }
    }
  }

  /**
   * Prevents instantiation of this class.
   */
  private ConfigDataLoader() {}

}

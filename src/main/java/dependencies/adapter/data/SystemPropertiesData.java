package dependencies.adapter.data;

import dependencies.adapter.abstractstuff.ConfigData;
import dependencies.adapter.utils.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class SystemPropertiesData extends ConfigData {

  /**
   * The instance linked to the properties keeping the configuration data.
   */
  protected Properties configData;

  /**
   * Base constructor that delegates initialisation to the superclass. Will then load system
   * properties configuration data.
   */
  public SystemPropertiesData() {
    super();
    configData = System.getProperties();
  }

  /**
   * Obtains all keys associated to system properties.
   *
   * @return the String keys
   */
  @Override
  public Collection<String> getKeys() {
    Set<String> result = new HashSet<String>();
    for (Object key : configData.keySet()) {
      result.add(key.toString());
    }
    return result;
  }

  /**
   * Obtains the value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the value associated to a key
   */
  @Override
  public String getProperty(String key, String... subkeys) {
    return configData
            .getProperty(StringUtils.concatWith(KEYWORD_HIERARCHY_SEPARATOR, key, subkeys));
  }

  /**
   * Unused method.
   */
  @Override
  protected void loadConfigData(String filePath) {}

}

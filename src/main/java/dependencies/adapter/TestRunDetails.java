package dependencies.adapter;

import dependencies.adapter.abstractstuff.TestResult;

/**
 * Details of the execution of a test.
 */
public class TestRunDetails {

  /**
   * The description of this test run.
   */
  String id;

  /**
   * The start timestamp of this test run.
   */
  long startTimestamp;

  /**
   * The end timestamp of this test run.
   */
  long endTimestamp;

  /**
   * The result of this test run.
   */
  TestResult result;

  /**
   * The tracer associated to this test run.
   */
  BaseTracer tracer;

  /**
   * Base constructor.
   * <p>
   * Sets the start timestamp based on the system clock (in milliseconds) and defaults the test
   * result to {@link TestResult#PASS}.
   *
   * @param id the description to associate with this test run
   */
  public TestRunDetails(String id) {
    this.id = id;
    this.startTimestamp = System.currentTimeMillis();
    this.endTimestamp = 0L;
    this.result = TestResult.PASS;
    this.tracer = null;
  }

  /**
   * Sets the result type for this test run.
   *
   * @param result the result to associate with this test run
   */
  public void setResult(TestResult result) {
    this.result = result;
  }

  /**
   * Closes the trace record of this test run.
   * <p>
   * Sets the end timestamp and the tracer for the test run.
   *
   * @param tracer the tracer to be associated with this test run
   */
  public void setFinalTestTrace(BaseTracer tracer) {
    this.tracer = tracer;
    this.endTimestamp = System.currentTimeMillis();
  }

  /**
   * Obtains the description of the test run.
   *
   * @return the description associated to this test run
   */
  public String getId() {
    return id;
  }


  /**
   * Sets the description of the test run.
   *
   * @param id the description to associate to this test run
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * Obtains the start timestamp of the test run.
   *
   * @return the start timestamp (in milliseconds) associated to this test run
   */
  public long getStartTime() {
    return startTimestamp;
  }

  /**
   * Obtains the end timestamp of the test run.
   *
   * @return the end timestamp (in milliseconds) associated to this test run
   */
  public long getEndTime() {
    return endTimestamp;
  }

  /**
   * Obtains the tracer used in the test run.
   *
   * @return the tracer associated to this test run
   */
  public BaseTracer getTracer() {
    return tracer;
  }

  /**
   * Obtains the result of the test run.
   *
   * @return the result of this test run
   */
  public String getTestResult() {
    return result.toString();
  }

}

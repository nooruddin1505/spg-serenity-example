package dependencies.adapter;

import dependencies.adapter.abstractstuff.AssertorLevel;
import dependencies.adapter.abstractstuff.BaseAssertor;
import dependencies.adapter.actions.AssertionAction;
import dependencies.adapter.driver.AssertionElement;
import dependencies.adapter.driver.ExecutionContext;

public class TracerAssertor extends BaseAssertor {

  private static final AssertorLevel DEFAULT_REPORTING_LEVEL = AssertorLevel.ASSERT;

  protected BaseTracer tracer = null;

  protected AssertorLevel reportingLevel = null;

  public TracerAssertor() {
    ExecutionContext context = ExecutionContext.getInstance();
    String reportingLevelString =
            context.getConfigData().getProperty("Assertor", "Reporting", "Level");
    this.reportingLevel =
            (reportingLevelString == null || reportingLevelString.equals("")) ? DEFAULT_REPORTING_LEVEL
                    : AssertorLevel.valueOf(reportingLevelString);
    this.tracer = context.getTracer();
  }

  @Override
  public void processAssertionResult(AssertorLevel level, String message, boolean result) {
    if (reportingLevel.compareTo(level) >= 0) {
      tracer.trace(new AssertionAction(level, message), new AssertionElement(result));
    }
  }

}

package dependencies.adapter.actions;

import dependencies.adapter.driver.PageElement;
import dependencies.adapter.abstractstuff.Pilot;
import dependencies.adapter.abstractstuff.BaseAction;

/**
 * Representation of web page actions.
 * <p>
 * Initial set of constants to associate with actions from the {@link Pilot} interface.
 *
 * @see Pilot
 */
public class PageAction extends BaseAction {

  /**
   * To include in String representations.
   */
  protected static final String DESCRIPTION = "ui action";

  /**
   * Constant for the {@link Pilot#close()} action.
   */
  public static final PageAction CLOSE = new PageAction("close");

  /**
   * Constant for the {@link Pilot#getCurrentUrl()} action.
   */
  public static final PageAction GET_CURRENT_URL = new PageAction("get current url");

  /**
   * Constant for the {@link Pilot#getPageSource()} action.
   */
  public static final PageAction GET_PAGE_SOURCE = new PageAction("get source");

  /**
   * Constant for the {@link Pilot#getTitle()} action.
   */
  public static final PageAction GET_TITLE = new PageAction("get title");

  /**
   * Constant for the {@link Pilot#getScreenshot()} action.
   */
  public static final PageAction GET_SCREENSHOT = new PageAction("capture screenshot");

  /**
   * Constant for the {@link Pilot#navigateBack()} action.
   */
  public static final PageAction NAVIGATE_BACK = new PageAction("navigate back");

  /**
   * Constant for the {@link Pilot#navigateForward()} action.
   */
  public static final PageAction NAVIGATE_FORWARD = new PageAction("navigate forward");

  /**
   * Constant for the {@link Pilot#navigateRefresh()} action.
   */
  public static final PageAction NAVIGATE_REFRESH = new PageAction("refresh");

  /**
   * Constant for the {@link Pilot#navigateTo(String)} and {@link Pilot#navigateTo(java.net.URL)}
   * actions.
   */
  public static final PageAction NAVIGATE_TO = new PageAction("navigate to");

  /**
   * Constant for the {@link Pilot#clear(PageElement)} action.
   */
  public static final PageAction CLEAR = new PageAction("clear");

  /**
   * Constant for the {@link Pilot#click(PageElement)} action.
   */
  public static final PageAction CLICK = new PageAction("click");

  /**
   * Constant for the {@link Pilot#getText(PageElement)} action.
   */
  public static final PageAction GET_TEXT = new PageAction("get text");

  /**
   * Constant for the {@link Pilot#isDisplayed(PageElement)} action.
   */
  public static final PageAction IS_DISPLAYED = new PageAction("check if displayed");

  /**
   * Constant for the {@link Pilot#isEnabled(PageElement)} action.
   */
  public static final PageAction IS_ENABLED = new PageAction("check if enabled");

  /**
   * Constant for the {@link Pilot#isSelected(PageElement)} action.
   */
  public static final PageAction IS_SELECTED = new PageAction("check if selected");

  /**
   * Constant for the {@link Pilot#sendKeys(PageElement, String)} action.
   */
  public static final PageAction SEND_KEYS = new PageAction("set text");

  /**
   * Constant for the {@link Pilot#switchToActiveElement()} action.
   */
  public static final PageAction SWITCH_TO_ACTIVE_ELEMENT = new PageAction(
          "switch to active element");

  /**
   * Constant for the {@link Pilot#switchToAlert()} action.
   */
  public static final PageAction SWITCH_TO_ALERT = new PageAction("switch to alert");

  /**
   * Constant for the {@link Pilot#switchToDefaultContent()} action.
   */
  public static final PageAction SWITCH_TO_DEFAULT_CONTENT = new PageAction(
          "switch to default content");

  /**
   * Constant for the {@link Pilot#switchToFrame(int)}, {@link Pilot#switchToFrame(String)} and
   * {@link Pilot#switchToFrame(PageElement)} actions.
   */
  public static final PageAction SWITCH_TO_FRAME = new PageAction("switch to frame");

  /**
   * Constant for the {@link Pilot#selectValueFromField(PageElement, String)} action.
   */
  public static final PageAction SELECT_VALUE_FROM_FIELD = new PageAction(
          "select option from dropdown");

  /**
   * Constant for the {@link Pilot#selectFromVisibleText(PageElement, String)} action.
   */
  public static final PageAction SELECT_FROM_VISIBLE_TEXT = new PageAction(
          "select option from dropdown based on visible text");

  /**
   * Constant for the {@link Pilot#getAttribute(PageElement,String)} action.
   */
  public static final PageAction GET_ATTRIBUTE = new PageAction("get element attribute");

  /**
   * Constant for the {@link Pilot#getCookie(String)} action.
   */
  public static final PageAction GET_COOKIE = new PageAction("get named cookie");

  /**
   * Constant for the {@link Pilot#executeJavaScript(String,Object...)} action.
   */
  public static final PageAction JAVASCRIPT = new PageAction("execute javascript");

  /**
   * Base constructor to set the description associated to this action.
   *
   * @param description the description to associate to this action
   */
  public PageAction(String description) {
    super(description);
  }

  /**
   * Returns a string representation of this action.
   * <p>
   * A page action is represented by a set identifier (page action) followed by its associated
   * description.
   *
   * @return the string representation of this action
   */
  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append(PageAction.DESCRIPTION);
    result.append(BaseAction.SEPARATOR);
    result.append((description == null) ? BaseAction.UNKNOWN_DESCRIPTION : description);
    return result.toString();
  }

  /**
   * Constant for the {@link Pilot#doubleClick(PageElement)} action.
   */
  public static final PageAction DOUBLE_CLICK = new PageAction("double click");

  /**
   * Constant for the {@link Pilot#getValue(PageElement)} action.
   */
  public static final PageAction GET_VALUE = new PageAction("get value");

  /**
   * Constant for the {@link Pilot#getMatches(PageElement)} action.
   */
  public static final PageAction GET_MATCHES = new PageAction("get matches");

  // /**
  // * Constant for the {@link Pilot#appendText(PageElement, String)} action.
  // */
  // public static final PageAction APPEND_TEXT = new PageAction("append text");

  /**
   * Constant for the {@link Pilot#elementExists(PageElement)} action.
   */
  public static final PageAction ELEMENT_EXISTS = new PageAction("check if element exists");

  /**
   * Constant for the {@link Pilot#deleteAllCookies()} action.
   */
  public static final PageAction DELETE_ALL_COOKIES = new PageAction("delete all cookies");

  /**
   * Constant for the {@link Pilot#dismissAlert()} action.
   */
  public static final PageAction DISMISS_ALERT = new PageAction("dismiss alert");

  /**
   * Constant for the {@link Pilot#acceptAlert()} action.
   */
  public static final PageAction ACCEPT_ALERT = new PageAction("accept alert");

  /**
   * Constant for the {@link Pilot#clearAndSendKeys(PageElement, String)} action.
   */
  public static final PageAction SET_VALUE = new PageAction("set text");

  /**
   * Constant
   */
  public static final PageAction SWITCH_WINDOW_HANDLE = new PageAction("Switch Window Handle");

  /**
   * Constant
   */
  public static final PageAction WINDOW_HANDLE = new PageAction("Window Handle");

  /**
   * Constant
   */
  public static final PageAction WAIT_FOR_PAGE_TO_LOAD = new PageAction("Wait for page to load");

  /**
   * Constant
   */
  public static final PageAction WAIT_FOR_ELEMENT_PRESENT = new PageAction(
          "Wait for element present");

  /**
   * Constant
   */
  public static final PageAction WAIT_FOR_ELEMENT_NOT_PRESENT = new PageAction(
          "Wait for element not present");

  /**
   * Constant for the {@link Pilot#authenticateUsing(Credentials)} action.
   */
  public static final PageAction AUTHENTICATE_WITH_CREDENTIALS = new PageAction(
          "Authenticate with credentials");

  /**
   * Constant
   */
  public static final PageAction MOVE_TO_ELEMENT = new PageAction("Move to centre of element");

  /**
   * Constant
   */
  public static final PageAction MOVE_TO_LOCATION_IN_ELEMENT = new PageAction("Move to location in element");

  /**
   * Constant
   */
  public static final PageAction CLICK_AT_MOUSE_LOCATION = new PageAction("Click at current mouse location");

  /**
   * Constant
   */
  public static final PageAction MAXIMIZE_WINDOW = new PageAction("Maximize the current window");

}

package dependencies.adapter.actions;

import dependencies.adapter.abstractstuff.BaseAction;

public class ReportLinkAction extends BaseAction {

  /**
   * To include in String representations.
   */
  protected static final String DESCRIPTION = "Report Link Action";

  /**
   * Base constructor to set the description associated to this action.
   *
   * @param description the description to associate to this action
   */
  public ReportLinkAction(String description) {
    super(description);
  }

  /**
   * Returns a string representation of this action.
   * <p>
   *
   * @return the string representation of this action
   */
  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append(ReportLinkAction.DESCRIPTION);
    result.append(BaseAction.SEPARATOR);
    result.append((description == null) ? BaseAction.UNKNOWN_DESCRIPTION : description);
    return result.toString();
  }

}

package dependencies.adapter.actions;

import dependencies.adapter.driver.ExecutionContext;
import dependencies.adapter.abstractstuff.Pilot;
import dependencies.adapter.reporting.SuiteReportGenerator;
import dependencies.adapter.TestDataLoader;
import dependencies.adapter.TestRunDetails;
import dependencies.adapter.abstractstuff.ConfigData;
import dependencies.adapter.abstractstuff.TestData;
import dependencies.adapter.runner.SuiteRunner;

import java.io.File;
import java.io.IOException;

/***
 * Class with static methods that group together actions on the single ExecutionContext instance.
 * <p>
 * The methods defined here are used from various runners (e.g. JUnit, Cucumber-JVM).
 * They ensure that the same initialisation and finalisation actions take place.
 */
public class ExecutionContextActions {

  /***
   * Initialises a test, by loading the necessary test data files,
   * initialising the report generation and starting the screen recorder.
   *
   * @param className The class name of the test class
   * @param methodName The method name of the test that is being executed
   * @param key The key that is used to select test data, using the set that matches the key-value pair.
   * @param value The value that is used to select test data, using the set that matches the key-value pair.
   */
  public static void onTestMethodSetup(String className, String methodName, String key, String value) {
    ExecutionContext context = ExecutionContext.getInstance();
    ConfigData configData = context.getConfigData();

    if (methodName != null) {
      TestData testData = context.getTestData();
      if (testData != null) {
        testData.add(TestDataLoader.load(methodName, key, value));
        testData.add(TestDataLoader.load(configData.getProperty("Data", methodName, "Specific"), key, value));
      }
    }

    if (context.getReportGenerator() == null) {
      context.setReportingFromSuite(false);
      context.setReportGenerator(new SuiteReportGenerator());
      if (methodName != null) {
        SuiteRunner.registerRunID(className + "." + methodName);
      } else {
        SuiteRunner.registerRunID(className);
      }
    }

    if (methodName == null) {
      methodName = className;
    }
    //ExecutionContextActions.startScreenRecorder(methodName);
    context.setTestRunDetails(new TestRunDetails(methodName));
  }

  /***
   * Capture a screenshot from the web browser.
   * <p>
   * This will only have an effect if a Selenium-driven test is running.
   * Otherwise, the pilot will be null and the method will return.
   */
  public static void captureScreenshot() {
    ExecutionContext context = ExecutionContext.getInstance();
    Pilot pilot = context.getPilot();
    if (pilot != null) {
      ConfigData configData = context.getConfigData();
      if (configData.getBooleanProperty("Screenshots")) {
        try {
          pilot.saveScreenshot(configData.getStringProperty("Screenshots", "Dir")
                  + File.separatorChar + System.currentTimeMillis() + ".png");
          // Check config if user wants to scroll for two screenshots
          if (configData.getStringProperty("Screenshots.Scroll") != null) {
            pilot.executeJavaScript("scroll(0," + configData.getStringProperty("Screenshots.Scroll") + ");");
            pilot.saveScreenshot(configData.getStringProperty("Screenshots", "Dir")
                    + File.separatorChar + System.currentTimeMillis() + ".png");
          }
        } catch (IOException ioe) {
          ioe.printStackTrace(System.err);
        }
      }
    }
  }


  /***
   * Performs finalisations and reporting actions, when a test is finished.
   * <p>
   * Stops tracing a test and adds the trace to the log of results to be output.
   */
  public static void onTestFinished() {
    ExecutionContext context = ExecutionContext.getInstance();
    TestRunDetails testRunDetails = context.getTestRunDetails();
    testRunDetails.setFinalTestTrace(context.getTracer());
    context.getReportGenerator().addResult(testRunDetails);
    if (!context.getReportingFromSuite()) {
      context.getReportGenerator().outputReport();
      context.setReportGenerator(null);
    }
  }

  /***
   * Quits the Selenium driver.
   * <p>
   * This will only have an effect if a Selenium-driven test is running.
   * Otherwise, the pilot will be null and the method will return.
   */
  public static void quitPilot() {
    Pilot pilot = ExecutionContext.getInstance().getPilot();
    if (pilot != null) {
      pilot.quit();
      ExecutionContext.getInstance().setPilot(null);
    }
  }

  /***
   * Starts the screen recorder.
   * <p>
   * This will only have an effect if the ScreenRecording parameter is set to true.
   *
   * @param methodName The name of the test method executed, which will be used in the naming of the video recording.
   */
//  public static void startScreenRecorder(String methodName) {
//    ExecutionContext context = ExecutionContext.getInstance();
//    if (context.getConfigData().getBooleanProperty("ScreenRecording")) {
//      try {
//        ScreenRecorder screenRecorder = VideoUtils.createScreenRecorder(methodName);
//        screenRecorder.start();
//        context.setScreenRecorder(screenRecorder);
//      } catch (IOException | AWTException e) {
//        e.printStackTrace();
//      }
//    }
//  }
//
//  /***
//   * Stops the screen recorder.
//   * <p>
//   * This will only have an effect if the screenrecorder single instance was initialised before.
//   */
//  public static void stopScreenRecorder() {
//    ScreenRecorder screenRecorder = ExecutionContext.getInstance().getScreenRecorder();
//    if (screenRecorder != null) {
//      try {
//        screenRecorder.stop();
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
//  }

}

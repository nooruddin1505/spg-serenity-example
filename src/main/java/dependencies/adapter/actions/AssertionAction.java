package dependencies.adapter.actions;


        import dependencies.adapter.abstractstuff.AssertorLevel;
import dependencies.adapter.abstractstuff.BaseAction;

public class AssertionAction extends BaseAction {

  private AssertorLevel level;

  public AssertionAction(String reason) {
    this(AssertorLevel.ASSERT, reason);
  }

  public AssertionAction(AssertorLevel level, String reason) {
    super(reason);
    this.level = level;
  }

  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append(level.toString().toLowerCase());
    result.append(BaseAction.SEPARATOR);
    result.append((description == null) ? "unknown" : description);
    return result.toString();
  }

}

package dependencies.adapter;

import dependencies.adapter.abstractstuff.TestData;
import dependencies.adapter.reporting.XLSTestData;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

import java.io.File;

/**
 * Utilities to load test data from default location and determine the specific subclass to use
 * based on the extension of the files to be loaded.
 *
 * @see TestData
 */
public class TestDataLoader {

  /**
   * Obtains the full path of a test data source file, based on the Data.Dir configuration data
   * property.
   *
   * @param testDataSource the relative path to a test data file
   * @return the processed path to a test data file
   */
  protected static String getDataSourceFilename(String testDataSource) {

    EnvironmentVariables env = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
    String dataDir = env.getProperty("Data.Dir");
    if (dataDir == null) {
      dataDir = ".";
    }
    return dataDir + File.separatorChar + testDataSource;
  }

  /**
   * Adds data from a given file to a test data instance.
   *
   * @param testData the test data to be augmented
   * @param testDataSource the files to load
   * @param key the field to use as criteria for data load
   * @param value the value to use as criteria for data load
   * @return the augmented {@link TestData}
   */
  protected static TestData loadDataFromFile(TestData testData, String testDataSource, String key,
                                             String value) {

    //String dataType = ExecutionContext.getInstance().getConfigData().getProperty("Data", "Format");
    TestData newTestData = null;
    if (testDataSource.contains("xls") || testDataSource.contains("xlsx")
            || testDataSource.contains("Excel")) {
      newTestData = new XLSTestData(getDataSourceFilename(testDataSource), key, value);
    }

    if (testData == null) {
      testData = newTestData;
    } else {
      testData.add(newTestData);
    }

    return testData;
  }

  /**
   * Adds test data to an existing instance.
   *
   * @param testData the test data to be augmented
   * @param testDataSources the files to load
   * @param key the field to use as criteria for data load
   * @param value the value to use as criteria for data load
   * @return the augmented {@link TestData}
   */
  public static TestData load(TestData testData, String testDataSources, String key, String value) {
    String[] testDataSourcesArray = testDataSources.split(",");
    if (testDataSourcesArray.length == 1) {
      return loadDataFromFile(testData, testDataSources, key, value);
    } else {
      for (String testDataSource : testDataSourcesArray) {
        testData = loadDataFromFile(testData, testDataSource, key, value);
      }
    }
    return testData;

  }

  /**
   * Creates a new test data instance from the given files.
   *
   * @param testDataSources the files to load
   * @param key the field to use as criteria for data load
   * @param value the value to use as criteria for data load
   * @return the {@link TestData} containing the information in the given files
   */
  public static TestData load(String testDataSources, String key, String value) {
    if (testDataSources != null) {
      return load(null, testDataSources, key, value);
    } else {
      return null;
    }

  }

  /**
   * Prevents instantiation.
   */
  private TestDataLoader() {

  }

}

package dependencies.adapter;

import dependencies.adapter.abstractstuff.BaseAction;
import dependencies.adapter.utils.DateUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Details of the features.steps (actions) associated to a test execution.
 * <p>
 * Provides the information to be included in the test report.
 *
 * @see BaseTracer
 */
public class TraceEntry {

  /**
   * The timestamp of the action being tracked.
   */
  private long timestamp;

  /**
   * The action being tracked.
   */
  private BaseAction action;

  /**
   * The elements the action is performed on, if any.
   */
  private List<BaseElement> parameters;

  /**
   * Base constructor to set all details.
   * <p>
   * The timestamp is set to the system clock.
   *
   * @param action the action to be trackedelements
   * @param element the element the action being tracked is performed on
   * @param parameter the string associated to the action being tracked
   */
  public TraceEntry(BaseAction action, BaseElement element, String parameter) {
    this(action, element, new BaseElement(parameter));
  }

  /**
   * Constructor with an action and its associated string parameter.
   *
   * @param action the action to be tracked
   * @param parameter the string associated to the action being tracked
   */
  public TraceEntry(BaseAction action, String parameter) {
    this(action, new BaseElement(parameter));
  }

  /**
   * Constructor with an action and the elements it is performed on.
   *
   * @param action the action to be tracked
   * @param parameters the elements the action being tracked is performed on
   */
  public TraceEntry(BaseAction action, BaseElement... parameters) {
    this.timestamp = System.currentTimeMillis();
    this.action = action;
    this.parameters = Arrays.asList(parameters);
  }

  /**
   * Dumps the string representation of the step to the standard output.
   *
   * @see System#out
   */
  public void dump() {
    System.out.println(toString());
  }

  /**
   * Returns the string representation of the step.
   *
   * @return the string representation of the step
   */
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("[");
    result.append(DateUtils.format(timestamp));
    result.append("]: ");
    result.append(action.toString());
    result.append("(");

    int parameterCount = parameters.size();
    if (parameterCount > 0) {
      for (int i = 0; i < parameterCount; i++) {
        String str = ((parameters.get(i) == null) ? "" : parameters.get(i).toString());
        result.append(str);
        if (i != (parameterCount - 1)) result.append(",");
      }
    }

    result.append(")");
    return result.toString();
  }

  /**
   * Obtains the timestamp of the step.
   *
   * @return the timestamp associated to the step
   */
  public long getTimestamp() {
    return timestamp;
  }

  /**
   * Obtains the action of the step.
   *
   * @return the action associated to the step
   */
  public BaseAction getAction() {
    return action;
  }

  /**
   * Obtains the element of the step.
   *
   * @return the element associated to the step
   * @deprecated Use {@link #getParameters()}.
   */
  @Deprecated
  public BaseElement getElement() {
    return parameters.get(0);
  }

  /**
   * Obtains the string parameter of the step.
   *
   * @return the string parameter associated to the step
   * @deprecated Use {@link #getParameters()}.
   */
  @Deprecated
  public String getParameter() {
    return parameters.get(0).toString();
  }

  /**
   * Obtains the elements associated to this step.
   *
   * @return the elements associated to the step
   */
  public List<BaseElement> getParameters() {
    return parameters;
  }

}

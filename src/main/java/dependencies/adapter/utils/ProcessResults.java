package dependencies.adapter.utils;


public class ProcessResults {

  private int exitValue;
  private String stderr;
  private String stdout;

  public ProcessResults(int exitValue, String stderr, String stdout) {
    this.exitValue = exitValue;
    this.stderr = stderr;
    this.stdout = stdout;
  }

  public int getExitValue() {
    return exitValue;
  }

  public String getStderr() {
    return stderr;
  }

  public String getStdout() {
    return stdout;
  }

}

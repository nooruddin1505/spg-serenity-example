package dependencies.adapter.utils;

/**
 * Utilities to manipulate strings.
 */
public class StringUtils {

  /**
   * Concatenates several strings, interposing a separator in between them.
   *
   * @param separator the string to interpose in between each concatenated element
   * @param first the first string to be concatenated
   * @param rest the sequence of remaining strings to be concatenated
   * @return the concatenated strings with the separator interposed in between them
   */
  public static String concatWith(String separator, String first, String... rest) {
    StringBuffer result = new StringBuffer(first);
    for (String element : rest) {
      result.append(separator);
      result.append(element);
    }
    return result.toString();
  }

  /**
   * Prevents this class from being instantiated.
   */
  private StringUtils() {

  }

}

package dependencies.adapter.utils;

/**
 * Created by InternetUser on 10/01/2018.
 */
public class Pair<FIRST, SECOND> {

  private FIRST first = null;

  private SECOND second = null;

  public Pair() {}

  public Pair(FIRST first, SECOND second) {
    this.first = first;
    this.second = second;
  }

  public FIRST getFirst() {
    return first;
  }

  public SECOND getSecond() {
    return second;
  }

  public void setFirst(FIRST first) {
    this.first = first;
  }

  public void setSecond(SECOND second) {
    this.second = second;
  }

}

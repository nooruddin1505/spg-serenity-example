package dependencies.adapter.utils;


import java.net.URI;

public class FileCopyResults extends ProcessResults {

  private URI uri;

  public FileCopyResults(int exitValue, String stderr, String stdout, URI uri) {
    super(exitValue, stderr, stdout);
    this.uri = uri;
  }

  public URI getUri() {
    return uri;
  }

}

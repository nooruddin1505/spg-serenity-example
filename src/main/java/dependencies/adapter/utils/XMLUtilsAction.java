package dependencies.adapter.utils;

import dependencies.adapter.abstractstuff.BaseAction;
import dependencies.adapter.others.NodeNotFoundException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

public class XMLUtilsAction extends BaseAction {

  public XMLUtilsAction(String description) {
    super("action: " + description);

    if (configData.getPropertyAsBoolean("Print.Actions", false)) {
      System.out.println(description);
    }
  }

  public static XMLUtilsAction reportParserConfigurationExceptionCaught(ParserConfigurationException e) {
    return new XMLUtilsAction("XMLUtils ParserConfigurationException caught. Message: " + e.getMessage());
  }

  public static XMLUtilsAction reportSAXExceptionCaught(SAXException e) {
    return new XMLUtilsAction("XMLUtils SAXException caught. Message: " + e.getMessage());
  }

  public static XMLUtilsAction reportIOExceptionCaught(IOException e) {
    return new XMLUtilsAction("XMLUtils IOException caught. Message: " + e.getMessage());
  }

  public static XMLUtilsAction reportXPathExpressionExceptionCaught(XPathExpressionException e) {
    return new XMLUtilsAction("XMLUtils XPathExpressionException caught. Message: " + e.getMessage());
  }

  public static XMLUtilsAction reportTransformerExceptionCaught(TransformerException e) {
    return new XMLUtilsAction("XMLUtils TransformerException caught. Message: " + e.getMessage());
  }

  public static XMLUtilsAction reportNodeNotFoundExceptionCaught(NodeNotFoundException e) {
    return new XMLUtilsAction("XMLUtils NodeNotFoundException caught. Message: " + e.getMessage());
  }
}

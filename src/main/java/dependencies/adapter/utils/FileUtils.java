package dependencies.adapter.utils;

import dependencies.adapter.driver.ExecutionContext;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FileUtils {

  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();

  public static FileCopyResults remoteFileDownload(String remoteHost, String remotePath, String localName, String... options) {
    int exitValue = -1;
    String stderr = null;
    String stdout = null;
    URI uri = null;

    // String resultsDir = "test-results";
    String resultsDir = configData.getProperty("Results.Dir");
    File directory = new File(resultsDir);
    if (! directory.exists()) {
      directory.mkdir();
    }

    // String scpExec = "scp";
    // String scpExec = "c:\\Program Files (x86)\\PuTTY\\pscp.exe";
    String scpExec = configData.getProperty("SCP.Executable");

    // String scpPem = "/home/alvaro/.ssh/ebsa-rebona.decrypted.pem";
    // String scpPem = "c:\\Users\\Alvaro\\.ssh\\ebsa-rebona.decrypted.ppk";
    String scpPem = configData.getProperty("SCP.IdentityFile");
    if (scpPem != null && !scpPem.equals("")) {
      scpPem = " -i " + scpPem + " ";
    } else {
      scpPem = " ";
    }

    // String scpUser = "rebona";
    String scpUser = configData.getProperty("SCP.User");
    if (scpUser != null && !scpUser.equals("")) {
      remoteHost = scpUser + "@" + remoteHost;
    }

    String scpOptions = String.join(" ", options) + " ";

    String localPath = resultsDir + File.separator + localName;

    // scp -i identity_file.pem username@<remoteHost>:<remotePath> localPath
    try {
      Process proc = Runtime.getRuntime().exec(scpExec + scpPem + scpOptions + remoteHost + ":" + remotePath + " " + localPath);
      proc.waitFor();
      exitValue = proc.exitValue();
      uri = new File(localPath).toURI();
      StringWriter swo = new StringWriter();
      IOUtils.copy(proc.getInputStream(), swo);
      stdout = swo.toString();
      StringWriter swe = new StringWriter();
      IOUtils.copy(proc.getErrorStream(), swe);
      stderr = swe.toString();
    } catch (IOException | InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return new FileCopyResults(exitValue, stderr, stdout, uri);
  }

  public static ArrayList<FileCopyResults> getMatchingTxFiles(String host, String searchPath, String searchTerm, String localName) {
    String searchCommand = String.format("find %s -mmin -60 -name '*.xml' -exec grep -l %s {} +", searchPath, searchTerm);

    ProcessResults processResults = RemoteUtils.remoteExec(host, searchCommand);

    String[] remotePaths = processResults.getStderr().split("\n");

    ArrayList<FileCopyResults> results = new ArrayList<>();

    int i = 1;
    for (String remotePath : remotePaths) {
      results.add(FileUtils.remoteFileDownload(host, remotePath, i + "_" + localName));
      i++;
    }

    return results;
  }

  public static ArrayList<FileCopyResults> getMatchingRefDataTxFiles(String host, String searchPath, String searchDate, String localName, String txName) {

    String searchCommand = String.format("find %s -name *%s* -exec grep -l %s {} +", searchPath, searchDate, txName);

    ProcessResults processResults = RemoteUtils.remoteExec(host, searchCommand);

    String[] remotePaths = processResults.getStderr().split("\n");

    ArrayList<FileCopyResults> results = new ArrayList<>();

    int i = 1;
    for (String remotePath : remotePaths) {
      results.add(FileUtils.remoteFileDownload(host, remotePath, i + "_" + localName));
      i++;
    }

    return results;
  }

  public static ArrayList<FileCopyResults> getDocummentID(String host, String searchPath, String caseReferenceNumber, String localName) {
    String udJarFile = "crc-dr-test-1.1.0.271-jar-with-dependencies-SPG-1.1-r9-hotfix-r3.jar";
    String fileName = host + "/containsDocID " + caseReferenceNumber;
    String documentIDSearchCommand = String.format("java –jar %s search %s 2> %s", searchPath + udJarFile, caseReferenceNumber, fileName);
    String searchCommand = String.format("find %s -mmin -60 -name '*.txt' -exec grep -l %s {} +", searchPath, caseReferenceNumber);

    RemoteUtils.remoteExec(host, documentIDSearchCommand);

    ProcessResults processResults = RemoteUtils.remoteExec(host, searchCommand);

    String[] remotePaths = processResults.getStderr().split("\n");

    ArrayList<FileCopyResults> results = new ArrayList<>();

    int i = 1;
    for (String remotePath : remotePaths) {
      results.add(FileUtils.remoteFileDownload(host, remotePath, i + "_" + localName));
      i++;
    }

    return results;

  }

  public static ArrayList<FileCopyResults> getMatchingMetaDataFiles(String host, String searchPath, String searchTerm, String localName) {
    String udJarFile = "crc-dr-test-1.1.0.271-jar-with-dependencies-SPG-1.1-r9-hotfix-r3.jar";
    String fileName = host + "/containsDocMetaData " + searchTerm;
    String searchCommand = String.format("java –jar %s details %s 2> %s", searchPath + udJarFile, searchTerm, fileName);

    ProcessResults processResults = RemoteUtils.remoteExec(host, searchCommand);

    String[] remotePaths = processResults.getStderr().split("\n");

    ArrayList<FileCopyResults> results = new ArrayList<>();

    int i = 1;
    for (String remotePath : remotePaths) {
      results.add(FileUtils.remoteFileDownload(host, remotePath, i + "_" + localName));
      i++;
    }

    return results;

  }

  public static ArrayList<FileCopyResults> getResponses(String host, String searchPath, String controlRef, String localName) {
    String searchCommand = String.format("find %s -name *receiverRef-%s*", searchPath, controlRef);

    ProcessResults processResults = RemoteUtils.remoteExec(host, searchCommand);

    String[] remotePaths = processResults.getStderr().split("\n");

    ArrayList<FileCopyResults> results = new ArrayList<>();

    int i = 1;
    for (String remotePath : remotePaths) {
      if (remotePath.startsWith("/opt")) {
        results.add(FileUtils.remoteFileDownload(host, remotePath, i + "_" + localName));
        i++;
      }
    }

    return results;
  }

  public static void saveScreenshot(String... params) {
    try {
      ExecutionContext.getInstance().getPilot().saveScreenshot(
              configData.getProperty("Screenshots.Dir") + File.separator + FileUtils
                      .getFileName(FileUtils.FileType.SCREENSHOT, params));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static String getFileName(FileType fileType, String... params) {
    StringBuilder sb = new StringBuilder();

    sb.append(fileType.getName());
    sb.append("_");
    sb.append(new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss").format(new Date()));

    for (String param : params) {
      sb.append("_");
      sb.append(param);
    }

    switch (fileType) {
      case TRANSACTION:
        sb.append(".xml");
        break;
      case RESPONSE:
        sb.append(".xml");
        break;
      case SCREENSHOT:
        sb.append(".png");
        break;
      case REFDATA:
        sb.append(".xml");
        break;
    }

    return sb.toString();
  }

  public enum FileType {
    TRANSACTION("TXN"),
    RESPONSE("RES"),
    SCREENSHOT("SCN"),
    REFDATA("REF");

    private String name;

    FileType(String name) {
      this.name = name;
    }

    public String getName() {
      return this.name;
    }
  }


  /**
   * Uploads a file from the test data directory to a remote location.
   *
   * @param testDataPath the path to the file to be uploaded, relative to the test data folder
   * @param remoteHost   the remote host where the file will be uploaded
   * @param remotePath   the path on the remote host where the file will be uploaded
   * @param options      any additional options to the SCP command of choice
   * @return the exit status, standard error, and standard output associated to the SCP process used to upload the file
   */
  public static ProcessResults dataFileUpload(String testDataPath, String remoteHost, String remotePath, String... options) {

    int exitValue = -1;
    String stderr = null;
    String stdout = null;

    String testDataDir = configData.getProperty("Data.Dir");

    String scpExec = configData.getProperty("SCP.Executable");

    String scpPem = configData.getProperty("SCP.IdentityFile");
    if (scpPem != null && !scpPem.equals("")) {
      scpPem = " -i " + scpPem + " ";
    } else {
      scpPem = " ";
    }

    String scpUser = configData.getProperty("SCP.User");
    if (scpUser != null && !scpUser.equals("")) {
      remoteHost = scpUser + "@" + remoteHost;
    }

    String scpOptions = String.join(" ", options) + " ";

    String localPath = testDataDir + File.separator + testDataPath + " ";
    //localPath = testDataPath;

    // scp -i identity_file.pem <test data dir>/testDataPath username@<remoteHost>:<remotePath>
    try {
      Process proc =
              Runtime.getRuntime().exec(
                      scpExec + scpPem + scpOptions + localPath + remoteHost + ":" + remotePath);
      proc.waitFor();
      exitValue = proc.exitValue();
      StringWriter swo = new StringWriter();
      IOUtils.copy(proc.getInputStream(), swo);
      stdout = swo.toString();
      StringWriter swe = new StringWriter();
      IOUtils.copy(proc.getErrorStream(), swe);
      stderr = swe.toString();
    } catch (IOException e) {
      e.printStackTrace(System.err);
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return new ProcessResults(exitValue, stderr, stdout);
  }

  /**
   * Uploads a file from the test data directory to a remote location.
   *
   * @param testDataPath the path to the file to be uploaded, relative to the test data folder
   * @param remoteHost   the remote host where the file will be uploaded
   * @param remotePath   the path on the remote host where the file will be uploaded
   * @return the exit status of the SCP process used to upload the file
   * @deprecated See {@link #dataFileUpload(String, String, String, String...)},
   */
  @Deprecated
  public static int uploadDataFile(String testDataPath, String remoteHost, String remotePath) {
    return dataFileUpload(testDataPath, remoteHost, remotePath).getExitValue();
  }

  /**
   * Download a file from a remote location to the test execution workstation.
   *
   * @param remoteHost  the remote host where the file to be downloaded is located
   * @param remotePath  the path to on the remote host where the file to be downloaded is located
   * @param localPrefix a prefix to add to the downloaded file name on the local destination
   * @param options     additional options to the SCP command of choice
   * @return the exit status, standard error, and standard output of the SCP process used to downloaded the file, as well as the URI of the local copy
   */
//  public static FileCopyResults remoteFileDownload(String remoteHost, String remotePath, String localPrefix, String... options) {
//    int exitValue = -1;
//    String stderr = null;
//    String stdout = null;
//    URI uri = null;
//    ConfigData configData = ExecutionContext.getInstance().getConfigData();
//
//    // String resultsDir = "test-results";
//    String resultsDir = configData.getProperty("Results", "Dir");
//
//    // String scpExec = "scp";
//    // String scpExec = "c:\\Program Files (x86)\\PuTTY\\pscp.exe";
//    String scpExec = configData.getProperty("SCP", "Executable");
//
//    // String scpPem = "/home/alvaro/.ssh/ebsa-rebona.decrypted.pem";
//    // String scpPem = "c:\\Users\\Alvaro\\.ssh\\ebsa-rebona.decrypted.ppk";
//    String scpPem = configData.getProperty("SCP", "IdentityFile");
//    if (scpPem != null && !scpPem.equals("")) {
//      scpPem = " -i " + scpPem + " ";
//    } else {
//      scpPem = " ";
//    }
//
//    // String scpUser = "rebona";
//    String scpUser = configData.getProperty("SCP", "User");
//    if (scpUser != null && !scpUser.equals("")) {
//      remoteHost = scpUser + "@" + remoteHost;
//    }
//
//    String scpOptions = String.join(" ", options) + " ";
//
//    String localPath = resultsDir + File.separator + localPrefix + remotePath.replace('/', '_');
//
//    // scp -i identity_file.pem username@<remoteHost>:<remotePath> localPath
//    try {
//      Process proc =
//              Runtime.getRuntime().exec(
//                      scpExec + scpPem + scpOptions + remoteHost + ":" + remotePath + " " + localPath);
//      proc.waitFor();
//      exitValue = proc.exitValue();
//      uri = new File(localPath).toURI();
//      StringWriter swo = new StringWriter();
//      IOUtils.copy(proc.getInputStream(), swo);
//      stdout = swo.toString();
//      StringWriter swe = new StringWriter();
//      IOUtils.copy(proc.getErrorStream(), swe);
//      stderr = swe.toString();
//    } catch (IOException e) {
//      e.printStackTrace(System.err);
//    } catch (InterruptedException e) {
//      e.printStackTrace(System.err);
//    }
//
//    return new FileCopyResults(exitValue, stderr, stdout, uri);
//  }

  /**
   * Download a file from a remote location to the test execution workstation.
   *
   * @param remoteHost  the remote host where the file to be downloaded is located
   * @param remotePath  the path to on the remote host where the file to be downloaded is located
   * @param localPrefix a prefix to add to the downloaded file name on the local destination
   * @return the URI of the local copy
   * @deprecated See {@link #remoteFileDownload(String, String, String, String...)}.
   */
  @Deprecated
  public static URI fetchRemoteFile(String remoteHost, String remotePath, String localPrefix) {
    return remoteFileDownload(remoteHost, remotePath, localPrefix).getUri();
  }


}

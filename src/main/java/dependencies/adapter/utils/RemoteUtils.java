package dependencies.adapter.utils;

import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RemoteUtils {

  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
  private static final String LOG_NEW_REQUEST = "virtuoso: debug: remote command: ";
  private static final SimpleDateFormat LOG_DATE_FORMAT = new SimpleDateFormat(
          "MMM dd, YYYY HH:mm:ss aa");

  /**
   * ssh host command [args...]
   *
   * @param host host to ssh to
   * @param command command to execution on host
   * @param args arguments to command to execute on host
   * @return return code, stdout, and stderr from command executed on host
   */
  public static ProcessResults remoteExec(String host, String command, String... args) {
    int exitValue = -1;
    String stdout = null;
    String stderr = null;

    String sshExec = configData.getProperty("SSH.Executable");

    String sshPem = configData.getProperty("SSH.IdentityFile");
    if (sshPem != null && !sshPem.equals("")) {
      sshPem = " -i " + sshPem;
    } else {
      sshPem = "";
    }

    String sshUser = configData.getProperty("SSH.User");
    if (sshUser != null && !sshUser.equals("")) {
      host = sshUser + "@" + host;
    }

    String sshOptions = String.join(" ", args);

    // ssh -i identity_file.pem args username@host command ...
    try {
      String theCommand = String.format("%s %s %s %s %s", sshExec, sshPem, sshOptions, host, command);
      System.out.println(LOG_DATE_FORMAT.format(new Date()) + " " + LOG_NEW_REQUEST + theCommand);
      Process proc = Runtime.getRuntime().exec(theCommand);
      proc.waitFor();
      exitValue = proc.exitValue();
      StringWriter swo = new StringWriter();
      IOUtils.copy(proc.getInputStream(), swo);
      stdout = swo.toString();
      StringWriter swe = new StringWriter();
      IOUtils.copy(proc.getErrorStream(), swe);
      stderr = swe.toString();
    } catch (IOException e) {
      e.printStackTrace(System.err);
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return new ProcessResults(exitValue, stdout, stderr);
  }

  /**
   * ssh host command [args...]
   *
   * @param host host to ssh to
   * @param command command to execution on host
   * @param args arguments to command to execute on host
   * @return return code and stdout from command executed on host
   * @deprecated {@link RemoteUtils#remoteExec(String, String, String...)}
   */
//  @Deprecated
//  public static Pair<Integer, String> sshWithOutput(String host, String command, String... args) {
//    ProcessResults pr = remoteExec(host, command, args);
//    return new Pair<>(pr.getExitValue(), pr.getStdout());
//  }

  /**
   * ssh host command [args...]
   *
   * @param host host to ssh to
   * @param command command to execution on host
   * @param args arguments to command to execute on host
   * @return return code from command executed on host
   * @deprecated {@link RemoteUtils#remoteExec(String, String, String...)}
   */
  @Deprecated
  public static int ssh(String host, String command, String... args) {
    return remoteExec(host, command, args).getExitValue();
  }

}

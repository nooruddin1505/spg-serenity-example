package dependencies.adapter.utils;

import java.util.Map;

/**
 * Utilities to manipulate data mappings.
 */
public class MapUtils {

  /**
   * Obtains the list of keys in a data mapping.
   *
   * @param map the map to analyse
   * @return the set of keys in the given map
   */
  public static String[] getKeys(Map<String, String> map) {
    int index = 0;
    String[] keys = new String[map.size()];
    for (Map.Entry<String, String> mapEntry : map.entrySet()) {
      keys[index] = mapEntry.getKey();
      index++;
    }
    return keys;
  }
}

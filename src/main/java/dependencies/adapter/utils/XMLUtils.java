package dependencies.adapter.utils;

import dependencies.adapter.abstractstuff.TestData;
import dependencies.adapter.others.NodeNotFoundException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;

/**
 * XMLUtils inspects/updates .XML files
 * <p>
 * Required - file path, xpath reference node in the document, update value
 * Throws - ParserConfigurationException, IOException, SAXException
 * Return - Details stored in the node
 */
public class XMLUtils {

  private String path;
  private Document document;

  public XMLUtils(String path) throws ParserConfigurationException, IOException, SAXException {
    System.setProperty("line.separator", "\n"); // Ensures use of unix line endings.

    this.path = path;

    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    this.document = documentBuilder.parse(this.path);
  }

  public String getNodeTextContent(String xPathString) throws NodeNotFoundException {
    XPath xPath = XPathFactory.newInstance().newXPath();

    Node n;
    try {
      n = (Node) xPath.compile(xPathString).evaluate(this.document, XPathConstants.NODE);

      if (n == null) {
        throw new NodeNotFoundException("No node found at xpath: " + xPathString);
      }
    } catch (XPathExpressionException e) {
      XMLUtilsAction.reportXPathExpressionExceptionCaught(e);
      return "";
    }

    return n.getTextContent();
  }

  public String getNodeName(String xPathString) {
    XPath xPath = XPathFactory.newInstance().newXPath();

    Node n;
    try {
      n = (Node) xPath.compile(xPathString).evaluate(this.document, XPathConstants.NODE);
    } catch (XPathExpressionException e) {
      XMLUtilsAction.reportXPathExpressionExceptionCaught(e);
      return "";
    }

    return n.getNodeName();
  }

  public void updateNode(String xPathString, String updateValue) throws XPathExpressionException {
    XPath xPath = XPathFactory.newInstance().newXPath();

    Node n = (Node) xPath.compile(xPathString).evaluate(this.document, XPathConstants.NODE);

    if (n == null) {
      throw new NodeNotFoundException("No node found at xpath: " + xPathString);
    }

    n.setTextContent(String.valueOf(updateValue));
  }

  public void saveDocument() throws TransformerException {
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    DOMSource source = new DOMSource(document);
    StreamResult result = new StreamResult(new File(this.path));
    transformer.transform(source, result);
  }

  public int contactFindNumber(TestData testData) {
    NodeList nList = document.getElementsByTagName("Contact");
    String expectedContactType = testData.getExpectedResult("AllocateEvent.Contact.ContactType");
    int temp = 0;
    for (temp = 0; temp < nList.getLength(); temp++) {
      Node nNode = nList.item(temp);
      if (nNode.getNodeType() == Node.ELEMENT_NODE) {
        Element eElement = (Element) nNode;
        String contactType = eElement.getElementsByTagName("ContactType").item(0).getTextContent();
        if (contactType.equalsIgnoreCase(expectedContactType)) {
          temp=Math.incrementExact(temp/2);
          return temp;
        }

      }
    }
    return temp;

  }
}

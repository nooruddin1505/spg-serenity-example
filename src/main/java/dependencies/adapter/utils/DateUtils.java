package dependencies.adapter.utils;

import dependencies.adapter.driver.ExecutionContext;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Utilities to format the dates in the test reports.
 */
public class DateUtils {

  /**
   * Default date format, to be used if none is defined in the configuration data.
   */
  private static final String DEFAULT_REPORT_DATE_FORMAT = "yyyy.MM.dd HH:mm:ss";

  /**
   * Formats a given date.
   * <p>
   * The default format can be overridden in the configuration data using the Report.DateFormat
   * property.
   *
   * @param timestamp the timestamp in milliseconds for the date to be formated
   * @return the formated timestamp
   *
   * @see dependencies.adapter.abstractstuff.ConfigData
   * @see SimpleDateFormat#format(Date)
   * @see System#currentTimeMillis()
   */
  public static String format(long timestamp) {
    String configReportDateFormat =
            ExecutionContext.getInstance().getConfigData().getStringProperty("Report", "DateFormat");
    return new SimpleDateFormat((configReportDateFormat == null) ? DEFAULT_REPORT_DATE_FORMAT
            : configReportDateFormat).format(new Date(timestamp));
  }

  /**
   * Prevents this class from being instantiated.
   */
  private DateUtils() {

  }

}

package dependencies.adapter.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataUtils {
  /**
   * Creates a new file which contains all the contents of the given file but with the placeholder
   * replaced with the update value.
   *
   * @param fileName    The name of the file for which the placeholder exists within.
   * @param placeholder The Placeholder to replace.
   * @param updateValue The value to replace the placeholder with.
   * @return A Path to a newly created file.
   * @throws IOException
   */
  public static Path replaceInFile(String filePath, String fileName, String placeholder,
                                   String updateValue, String newFilePath) throws IOException {
    // Splits the filename into name and extension.
    String[] split = fileName.split("\\.(?=[^\\.]+$)");

    Path pathOldFile = Paths.get(filePath + File.separator + fileName);
    Path pathNewFile = Paths.get(newFilePath + File.separator + split[0] + "_tmp." + split[1]);
    Charset charset = StandardCharsets.UTF_8;

    String content = new String(Files.readAllBytes(pathOldFile), charset);
    content = content.replaceAll(placeholder, updateValue);

    File tempDir = new File(newFilePath);
    if (!tempDir.exists()) {
      tempDir.mkdir();
    }

    return Files.write(pathNewFile, content.getBytes());
  }

  /**
   * Returns a String containing the contents of a given XML file.
   *
   * @param xmlFile the XML file to read.
   * @return contents of the XML file.
   * @throws IOException if the file cannot be read.
   */
  public static String xmlToString(File xmlFile) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(xmlFile));
    StringBuilder sb = new StringBuilder();

    String line;
    while ((line = reader.readLine()) != null) {
      sb.append(line);
    }

    reader.close();

    return sb.toString();
  }
}

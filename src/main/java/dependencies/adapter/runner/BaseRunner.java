package dependencies.adapter.runner;

import dependencies.adapter.data.ConfigDataLoader;
import dependencies.adapter.driver.ExecutionContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * Root for test runners.
 * <p>
 * Runners provide the entry point for test execution.
 */
public class BaseRunner {

  /**
   * Setup routine carried out prior to the execution of a runner.
   * <p>
   * Loads the configuration data to control an execution.
   *
   * @see ConfigDataLoader#load()
   */
  @BeforeClass
  public final static void baseRunnerSetup() {
    ExecutionContext.getInstance().setConfigData(ConfigDataLoader.load());
  }

  /**
   * Placeholder for any actions to take place upon completion of a runner's execution.
   */
  @AfterClass
  public final static void baseRunnerTearDown() {

  }

}

package dependencies.adapter.runner;

import dependencies.adapter.driver.ExecutionContext;
import dependencies.adapter.reporting.SuiteReportGenerator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Root for test suites.
 * <p>
 * Test suites group individual tests in logical units and provide the single point of entry for all
 * test execution, i.e. a test must be part of a suite for it to be executed.
 * <p>
 * For a new test suite:
 * <ol>
 * <li>Create a sub-class of
 *
 * uk.gov.homeoffice.virtuoso.framework.runner.SuiteRunner.</li>
 * <li>Mark the class with the
 *
 * <pre>
 * org.junit.runners.Suite.SuiteClasses({Test_1.class,Test_2.class,...,Test_n.class})
 * </pre>
 *
 * annotation, listing all the classes containing the desired tests. Note: all methods marked with
 * the {@link Test} annotation within each SampleTest_i.class will be attempted in the execution.</li>
 * <li>Create a static method
 *
 * <pre>
 * void mySuiteSetup()
 * </pre>
 *
 * mark it with the {@link BeforeClass} annotation, and call the registerRunID ("&lt;SuiteID&gt;")
 * method, where &lt;SuiteID&gt; is the identifier for the suite in the generated reports. Note: the
 * identifier mySuiteSetup cannot be one of: suiteRunnerSetup or baseRunnerSetup.</li>
 * </ol>
 * <p>
 * Refer to the uk.gov.homeoffice.virtuoso .sample.runner.* for examples.
 *
 */
@RunWith(Suite.class)
public class SuiteRunner extends BaseRunner {

  private static int count = 0;

  /**
   * Initialises the report generator to be used by the test suite.
   */
  @BeforeClass
  public final static void suiteRunnerSetup() {
    ExecutionContext.getInstance().setReportGenerator(new SuiteReportGenerator());
  }

  /**
   * Records the identifier to be used in the report for the test suite's execution.
   * <p>
   * The "Results.File.Prefix" configuration property overrides the ID passed as parameter. The
   * "Results.File.Timestamp" determines whether to append the start timestamp to the chosen ID.
   *
   * @param id the identifier associated to the test suite
   */
  public final static void registerRunID(String id) {
    ExecutionContext executionContext = ExecutionContext.getInstance();
    String idFromConfig = executionContext.getConfigData().getProperty("Results", "File", "Prefix");
    Boolean useTimestamp =
            executionContext.getConfigData().getBooleanProperty("Results", "File", "Timestamp");
    String idToUse = (idFromConfig != null) ? idFromConfig : id;
    if (useTimestamp) {
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
      idToUse = idToUse + "_" + formatter.format(new Date());
    }
    idToUse = idToUse + "_" + count++;
    executionContext.setRunID(idToUse);
    executionContext.getReportGenerator().startRunRecord(idToUse);
  }

  /**
   * Outputs the results collected throughout the execution of the test suite.
   *
   * @see BaseReportGenerator#outputReport()
   */
  @AfterClass
  public final static void suiteRunnerTearDown() {
    ExecutionContext.getInstance().getReportGenerator().outputReport();
  }

}

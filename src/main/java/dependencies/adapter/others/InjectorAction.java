package dependencies.adapter.others;

import dependencies.adapter.abstractstuff.BaseAction;
import dependencies.adapter.abstractstuff.ConfigData;
import dependencies.adapter.utils.ProcessResults;

import java.io.File;


public class InjectorAction extends BaseAction {

  public InjectorAction(String description) {
    super("action: " + description);

    if (configData.getPropertyAsBoolean("Print.Actions", false)) {
      System.out.println(description);
    }
  }

  public static InjectorAction reportControlReference(String controlReference) {
    return new InjectorAction("controlReference to be used: " + controlReference);
  }

  public static InjectorAction reportTimeOfTest(String timeString) {
    return new InjectorAction("Time of injection: " + timeString);
  }

  public static InjectorAction reportFilePath(String filePath) {
    return new InjectorAction("Path of data file to be injected: " + filePath);
  }

  public static InjectorAction reportInjectionLocation(String host, String tempDir, String injectorDir) {
    return new InjectorAction(String.format("Injecting to host: %s\nTemp Dir: %s\nInjector Dir: %s", host, tempDir, injectorDir));
  }

  public static InjectorAction reportProcessResults(ProcessResults result) {
    return new InjectorAction(
            String.format("Injection result: %s\nOutput: %s\nError: %s", result.getExitValue(), result.getStderr(), result.getStderr()));
  }

  public static InjectorAction reportProcessCommand(String command) {
    return new InjectorAction("Process command to be executed: " + command);
  }

  public static InjectorAction reportIOExceptionCaughtDuringReplacement(String placeholder, String replacement, String dataFileName, String message) {
    return new InjectorAction(
            String.format("IOException caught while replacing '%s' with '%s' in '%s'. Message: %s", placeholder, replacement, dataFileName, message));
  }

  // TODO - Not sure if we should keep this or not. Might be better to have the framework report the command.
  public static InjectorAction reportSCPCommandToBeUsed(ConfigData configData, String testDataPath, String remoteHost, String remotePath,
                                                        String... options) {
    String testDataDir = configData.getProperty("Data", "Dir");
    String scpExec = configData.getProperty("SCP", "Executable");
    String scpPem = configData.getProperty("SCP", "IdentityFile");
    if (!scpPem.equals(""))
      scpPem = " -i " + scpPem + " ";
    else
      scpPem = " ";

    String scpUser = configData.getProperty("SCP", "User");
    if (scpUser != null && !scpUser.equals(""))
      remoteHost = scpUser + "@" + remoteHost;

    String scpOptions = String.join(" ", options) + " ";
    String localPath = testDataDir + File.separator + testDataPath + " ";

    return new InjectorAction(String.format("Putting file to server with the following commang: %s %s %s %s %s:%s", scpExec, scpPem, scpOptions,
            localPath, remoteHost, remotePath));
  }
}

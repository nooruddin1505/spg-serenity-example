package dependencies.adapter.others;

import dependencies.adapter.BaseTracer;
import dependencies.adapter.abstractstuff.CRCID;
import dependencies.adapter.driver.ExecutionContext;
import dependencies.adapter.utils.DataUtils;
import dependencies.adapter.utils.XMLUtils;
import dependencies.adapter.utils.XMLUtilsAction;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;

/**
 * Class allowing abstraction of message injection into the SPG for various routes. For each
 * injection point of the SPG there can be a child class derived from this one which allows
 * injection to that point.
 * <p>
 * This class also handles the preparation of data files prior to injection. For a data file
 * to be ready for injection, a unique control reference (timestamp) needs to be set. The
 * unique control reference will allow the message to be tracked throughout the system logs.
 *
 * @author Rob Livermore
 */
public abstract class SPGAgent {
  private static final String PH_CONTROL_REF = "placeholder_control_ref";
  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();

  protected String dataFileName;
  protected XMLUtils xmlUtils;
  protected String controlReference;
  protected BaseTracer tracer;
  public String subDir;

  /**
   * Constructor for SPGAgent. Takes a control reference and the name of the file to be
   * injected. The data file must be contained within the test-data directory.
   *
   * @param controlReference a unique control reference (recommended to be a timestamp).
   * @param dataFileName     the name of the file within the test-data directory.
   */
  public SPGAgent(String controlReference, String subDir, String dataFileName, String schemaVersion, CRCID crcID) {
    this.controlReference = controlReference;
    this.subDir = subDir;
    this.dataFileName = dataFileName;

    this.tracer = ExecutionContext.getInstance().getTracer();

    try {
      String addDirectory = File.separator + subDir;
      if(subDir == null){
        addDirectory = "";
      }
      xmlUtils = new XMLUtils(configData.getProperty("Data.Dir") +  addDirectory + File.separator + dataFileName);

      xmlUtils.updateNode("/SPGInterchange/SPGInterchangeHeader/SenderControlReference", this.controlReference);
      xmlUtils.updateNode("/SPGInterchange/SPGInterchangeTrailer/SenderControlReference", this.controlReference);
      xmlUtils.updateNode("/SPGInterchange/SPGInterchangeHeader/TestControlData", configData.getProperty("Test.Control.Data.Input") + this.dataFileName);
      xmlUtils.updateNode("/SPGInterchange/SPGInterchangeHeader/SenderIdentity", crcID.toString().replace("RC", ""));
      xmlUtils.updateNode("/SPGInterchange/SPGInterchangeTrailer/SenderIdentity", crcID.toString().replace("RC", ""));
      xmlUtils.updateNode("/SPGInterchange/SPGMessage/SPGMessageHeader/MessageVersionNumber", schemaVersion);
      xmlUtils.updateNode("/*/@SchemaVersion", schemaVersion);

      if (schemaVersion.equals("0-9-9")) {
        xmlUtils.updateNode("/*/@SchemaDate", configData.getProperty("Root.Date.099"));
      }
      if (schemaVersion.equals("0-9-10")) {
        xmlUtils.updateNode("/*/@SchemaDate", configData.getProperty("Root.Date.010"));
      }
      if (schemaVersion.equals("0-9-12")) {
        xmlUtils.updateNode("/*/@SchemaDate", configData.getProperty("Root.Date.012"));
      }

      xmlUtils.saveDocument();
    } catch (ParserConfigurationException e) {
      XMLUtilsAction.reportParserConfigurationExceptionCaught(e);
    } catch (IOException e) {
      XMLUtilsAction.reportIOExceptionCaught(e);
    } catch (SAXException e) {
      XMLUtilsAction.reportSAXExceptionCaught(e);
    } catch (XPathExpressionException e) {
      XMLUtilsAction.reportXPathExpressionExceptionCaught(e);
    } catch (TransformerException e) {
      XMLUtilsAction.reportTransformerExceptionCaught(e);
    } catch (NodeNotFoundException e) {
      ExecutionContext.getInstance().getAssertor().fail(e.getMessage());
    }

    //tracer.trace(InjectorAction.reportControlReference(controlReference));
    //tracer.trace(InjectorAction.reportTimeOfTest(new Date().toString()));

    InjectorAction.reportControlReference(controlReference);
    InjectorAction.reportTimeOfTest(new Date().toString());
  }


  /**
   * Abstract method allowing injection methods to be defined by child classes for each
   * injection point into the SPG.
   *
   * @return a boolean indicating whether or not the injection was successful.
   */
  @Deprecated
  public abstract boolean inject();

  /**
   * There are two entry points for messages into the SPG; from a Delius perspective, via a JMS
   * queue, or from a CRC perspective, via a SOAP connection. The method of putting requests
   * to the entry point can be defined in any classes extending this.
   *
   * @return a boolean indicating whether or not the injection was successful.
   */
  public abstract boolean putMessage();

  /**
   * Abstract method for retrieving messages from either the SPG's SOAP service or the JMS queue.
   *
   * @return
   */
  public abstract <E> E retrieveMessage();

  /**
   * Prepares a new data file for injection. The new data file will be identical to the original
   * with the exception being the control reference placeholder will be replaced with the unique
   * control reference provided to this class.
   * <p>
   * Note: The new data file created should be considered temporary and not retained by any VCS.
   *
   * @return a relative {@link Path} to the newly created data file.
   */
  @Deprecated
  private Path prepareDataFile() {
    Path path = null;
    try {
      String tmpFileDir = configData.getProperty("Data.Dir") + File.separator + configData.getProperty("Temp.Msg.Dir");
      path = DataUtils.replaceInFile(configData.getProperty("Data.Dir"), dataFileName, PH_CONTROL_REF, controlReference, tmpFileDir);

      tracer.trace(InjectorAction.reportFilePath(path.toString()));

      // Need to remove the Data.Dir part of the Path as this being present will cause a failure
      // when we send with FileUtils.
      path = path.subpath(4, path.getNameCount());
    } catch (IOException e) {
      tracer.trace(InjectorAction.reportIOExceptionCaughtDuringReplacement(PH_CONTROL_REF, controlReference, dataFileName, e.getMessage()));
    }

    return path;
  }

  public void updateDataFileXML(String xPath, String updateValue) throws XPathExpressionException, TransformerException {
    xmlUtils.updateNode(xPath, updateValue);
    xmlUtils.saveDocument();
  }

  public static String generateControlReference(){
    return Long.toString(new Date().getTime());
  }
}

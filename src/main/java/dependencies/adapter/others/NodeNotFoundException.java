package dependencies.adapter.others;

public class NodeNotFoundException extends RuntimeException {
  public NodeNotFoundException(String message) {
    super(message);
  }

  public NodeNotFoundException(String message, Throwable throwable) {
    super(message, throwable);
  }
}

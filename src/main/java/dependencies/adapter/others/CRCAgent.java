package dependencies.adapter.others;

import dependencies.adapter.abstractstuff.CRCID;
import dependencies.adapter.utils.DataUtils;
import dependencies.adapter.utils.FileUtils;
import dependencies.adapter.utils.ProcessResults;
import dependencies.adapter.utils.RemoteUtils;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

/**
 * Class allowing data files to be injected into the CRCInbound directory.
 */
public class CRCAgent extends SPGAgent {

  private String crcServerName;

  public CRCAgent(String controlReference, String dataFileName, String crcServerName, String schemaVersion, CRCID crcID) {
    super(controlReference, "", dataFileName, schemaVersion, crcID);
    this.crcServerName = crcServerName;
  }

  public CRCAgent(String controlReference, String subDir, String dataFileName, String crcServerName, String schemaVersion, CRCID crcID) {
    super(controlReference, subDir, dataFileName, schemaVersion, crcID);
    this.crcServerName = crcServerName;
  }

  /**
   * Method implementing the abstract method within the {@code SPGAgent} class. Injects a data
   * file into the SPG's CRCInbound injector.
   *
   * @return a boolean indicating whether the injection was successful.
   */
  public boolean inject() {
    String remotePathTempDirectory = configData.getProperty("Remote.Path.Message.Tmp");
    String remotePathCRCStubInboundQueue = configData.getProperty("Remote.Path.CRCStub.Outbound");

    String noKeyCheck = configData.getProperty("SSH.Params.NoKeyCheck");
    String scpPort = configData.getProperty("SCP.Params.Port");
    String sshPort = configData.getProperty("SSH.Params.Port");

    ProcessResults putFileResult = FileUtils
            .dataFileUpload(subDir + File.separator + this.dataFileName, crcServerName, remotePathTempDirectory, noKeyCheck,
                    scpPort);

    System.out.println("putFileResult.getStderr()" + putFileResult.getStderr());
    System.out.println("putFileResult.getStdout()" + putFileResult.getStdout());
    System.out.println("putFileResult.getExitValue()" + putFileResult.getExitValue());

    //tracer.trace(InjectorAction.reportProcessResults(putFileResult));
    InjectorAction.reportProcessResults(putFileResult);

    String mkdirCommand = "mkdir -p " + remotePathTempDirectory;

    String mvFileCommand =
            "mv -f " + remotePathTempDirectory + this.dataFileName
                    + " " + remotePathCRCStubInboundQueue;

    //tracer.trace(InjectorAction.reportProcessCommand(mkdirCommand));
    InjectorAction.reportProcessCommand(mkdirCommand);

    //tracer.trace(InjectorAction.reportProcessCommand(mvFileCommand));
    InjectorAction.reportProcessCommand(mvFileCommand);

    ProcessResults moveFileResult =
            RemoteUtils.remoteExec(crcServerName, mvFileCommand, noKeyCheck, sshPort);

    System.out.println("moveFileResult.getStderr()" + moveFileResult.getStderr());
    System.out.println("moveFileResult.getStdout()" + moveFileResult.getStdout());
    System.out.println("moveFileResult.getExitValue()" + moveFileResult.getExitValue());

    //tracer.trace(InjectorAction.reportProcessResults(moveFileResult));
    InjectorAction.reportProcessResults(moveFileResult);

    return putFileResult.getExitValue() == 0 && moveFileResult.getExitValue() == 0;
  }

  @Override
  public boolean putMessage() {
    String url = configData.getProperty("SOAP.CRC.URL");

    SOAPConnection conn = null;
    try {
      String message = DataUtils.xmlToString(new File(configData.getProperty("Data.Dir") + File.separator + dataFileName.toString()));

      MessageFactory messageFactory = MessageFactory.newInstance();
      SOAPMessage soapMessage = messageFactory.createMessage(new MimeHeaders(), new ByteArrayInputStream(message.getBytes()));

      conn = getConnection();

      SOAPMessage response = conn.call(soapMessage, url);

      System.out.println("We received a response...");
      response.writeTo(System.out);
      return true;
    } catch (IOException e) {
      e.printStackTrace();
    } catch (SOAPException e) {
      e.printStackTrace();
    } finally {
      try {
        conn.close();
      } catch (SOAPException e) {
        e.printStackTrace();
      }
    }

    return false;
  }

  @Override
  public <E> E retrieveMessage() {
    throw new NotImplementedException();
  }

  private SOAPConnection getConnection() throws SOAPException {
    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
    SOAPConnection connection = soapConnectionFactory.createConnection();

    return connection;
  }
}

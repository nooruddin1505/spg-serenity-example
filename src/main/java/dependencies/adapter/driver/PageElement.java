package dependencies.adapter.driver;

import dependencies.adapter.BaseElement;
import org.openqa.selenium.By;

// TODO: update javadocs
// TODO: review toString()
/**
 * Representation of traceable web page elements.
 */
public class PageElement extends BaseElement {

  /**
   * Different ways to identify an element on a web page.
   */
  public enum Type {

    CLASS_NAME("className"), CSS_SELECTOR("cssSelector"), ID("id"), LINK_TEXT("linkText"), NAME(
            "name"), PARTIAL_LINK_TEXT("partialLinkText"), TAG_NAME("tagName"), XPATH("xpath");

    /**
     * To use in String representations of this type.
     */
    private String description;

    /**
     * Base constructor to set the String representation of this type.
     *
     * @param description the String representation to associate to this instance
     */
    private Type(String description) {
      this.description = description;
    }

    /**
     * Generates the String representation of this instance.
     *
     * @return the String representation of this instance
     */
    @Override
    public String toString() {
      return description;
    }

  }

  /**
   * The chosen way to refer to this element.
   */
  private Type type;

  /**
   * The string parameter associated to this element, e.g. the XPath, ID, Name, etc.
   */
  private String selector;

  /**
   * Backwards compatibility with v1.0 pre-constructed {@link By} instances.
   */
  @Deprecated
  private By v1Selector;

  /**
   * Base constructor to set the description and Selenium locator associated to this element.
   * <p>
   * For backwards compatibility with v1.0 pre-constructed {@link By} instances.
   *
   * @param selector the Selenium locator to associate with this element
   * @param description the description to associate with this element
   */
  @Deprecated
  public PageElement(By selector, String description) {
    super(description);
    this.v1Selector = selector;
    this.type = null;
    this.selector = null;
  }

  /**
   * Base constructor to set the description and selector information associated to this element.
   * <p>
   * The additional, optional parameters are used to format the selector prior to being set.
   *
   * @param type the chosen way to refer to this element
   * @param selector the string parameter to associate to this element
   * @param description the description to associate with this element
   * @param parameters the additional parameters required to format the
   */
  public PageElement(Type type, String selector, String description, Object... parameters) {
    super(String.format(description, parameters));
    this.type = type;
    this.selector = String.format(selector, parameters);
    this.v1Selector = null;
  }

  /**
   * Obtains the Selenium locator associated to this element.
   * <p>
   * Creates the appropriate instance depending on the type of selector specified during instance
   * creation time.
   *
   * @param parameters parameters to construct the locator
   * @return the Selenium locator associated to this element
   */
  public By getBy(Object... parameters) {
    /* for backwards compatibility */
    if (v1Selector != null) {
      return v1Selector;
    }

    By result = null;

    switch (type) {
      case CLASS_NAME:
        result = By.className(String.format(selector, parameters));
        break;
      case CSS_SELECTOR:
        result = By.cssSelector(String.format(selector, parameters));
        break;
      case ID:
        result = By.id(String.format(selector, parameters));
        break;
      case LINK_TEXT:
        result = By.linkText(String.format(selector, parameters));
        break;
      case NAME:
        result = By.name(String.format(selector, parameters));
        break;
      case PARTIAL_LINK_TEXT:
        result = By.partialLinkText(String.format(selector, parameters));
        break;
      case TAG_NAME:
        result = By.tagName(String.format(selector, parameters));
        break;
      case XPATH:
        result = By.xpath(String.format(selector, parameters));
        break;
    }

    return result;
  }

  /**
   * Returns a string representation of this element.
   * <p>
   * The element is represented by its associated description and the string representation of its
   * Selenium locator, if any, or a default keyword (unknown) if no description or locator have been
   * set.
   *
   * @return the string representation of this element
   */
  @Override
  public String toString() {
    if (description == null && type == null && v1Selector == null) {
      return BaseElement.UNKNOWN_DESCRIPTION;
    }

    StringBuffer result = new StringBuffer();
    if (description != null) {
      result.append(description);
      // result.append(BaseElement.SEPARATOR);
    }
    // result.append("(");
    // if (type != null) {
    // result.append(type);
    // result.append("=\"");
    // result.append(selector);
    // result.append("\"");
    // } else if (v1Selector != null) {
    // result.append(v1Selector.toString());
    // }
    // result.append(")");
    return result.toString();
  }

  public static PageElement byClassName(String selector, String description, Object... parameters) {
    return new PageElement(Type.CLASS_NAME, selector, description, parameters);
  }

  public static PageElement byCssSelector(String selector, String description, Object... parameters) {
    return new PageElement(Type.CSS_SELECTOR, selector, description, parameters);
  }

  public static PageElement byId(String selector, String description, Object... parameters) {
    return new PageElement(Type.ID, selector, description, parameters);
  }

  public static PageElement byLinkText(String selector, String description, Object... parameters) {
    return new PageElement(Type.LINK_TEXT, selector, description, parameters);
  }

  public static PageElement byName(String selector, String description, Object... parameters) {
    return new PageElement(Type.NAME, selector, description, parameters);
  }

  public static PageElement byPartialLinkText(String selector, String description,
                                              Object... parameters) {
    return new PageElement(Type.PARTIAL_LINK_TEXT, selector, description, parameters);
  }

  public static PageElement byTagName(String selector, String description, Object... parameters) {
    return new PageElement(Type.TAG_NAME, selector, description, parameters);
  }

  public static PageElement byXpath(String selector, String description, Object... parameters) {
    return new PageElement(Type.XPATH, selector, description, parameters);
  }

}

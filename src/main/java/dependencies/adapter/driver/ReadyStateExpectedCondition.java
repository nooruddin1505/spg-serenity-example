package dependencies.adapter.driver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 * Document State ready expected condition Returns true when the document ready state is complete
 * i.e. fully loaded
 */
public class ReadyStateExpectedCondition implements ExpectedCondition<Boolean> {

  public ReadyStateExpectedCondition() {
    super();
  }

  @Override
  public Boolean apply(WebDriver driver) {
    try {
      return Boolean.valueOf(((JavascriptExecutor) driver).executeScript(
              "return document.readyState").equals("complete"));
    } catch (WebDriverException wee) {
      return false;
    }
  }

  @Override
  public String toString() {
    return "document.readyState must equal complete";
  }

}

package dependencies.adapter.driver;

import dependencies.adapter.BaseElement;
import dependencies.adapter.BaseTracer;
import dependencies.adapter.abstractstuff.Pilot;
import dependencies.adapter.actions.PageAction;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.security.Credentials;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Set;

/**
 * A {@link Pilot} that forwards calls to a designated {@link Pilot} and generates tracing entries
 * in the generated report as it goes along.
 *
 * @see BaseTracer
 * @see Pilot
 */
public class TracingPilot implements Pilot {

  /**
   * The {@link Pilot} this instance forwards calls to.
   */
  private Pilot doer;

  /**
   * The {@link BaseTracer} this instance uses to introduce tracing statements.
   */
  private BaseTracer tracer;

  /**
   * Constructor with {@link Pilot} to wrap up.
   * <p>
   * The {@link BaseTracer} to use for the introduction of tracing features.steps is obtained from the
   * execution context.
   *
   * @param doer the {@link Pilot} to be wrapped up.
   * @see BaseTracer
   * @see ExecutionContext
   */
  public TracingPilot(Pilot doer) {
    this.tracer = ExecutionContext.getInstance().getTracer();
    this.doer = doer;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getValue(PageElement element) {
    String result = doer.getValue(element);
    tracer.trace(PageAction.GET_VALUE, element, new BaseElement(result));
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<WebElement> getMatches(PageElement element) {
    List<WebElement> result = doer.getMatches(element);
    tracer.trace(PageAction.GET_MATCHES, element);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void close() {
    tracer.trace(PageAction.CLOSE);
    doer.close();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getCurrentUrl() {
    // TODO: add result from doer to trace statement
    tracer.trace(PageAction.GET_CURRENT_URL);
    return doer.getCurrentUrl();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getPageSource() {
    // TODO: add result from doer to trace statement
    tracer.trace(PageAction.GET_PAGE_SOURCE);
    return doer.getPageSource();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getScreenshot() {
    String result = doer.getScreenshot();
    tracer.trace(PageAction.GET_SCREENSHOT, new BaseElement(result));
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getTitle() {
    // TODO: add result from doer to trace statement
    tracer.trace(PageAction.GET_TITLE);
    return doer.getTitle();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void quit() {
    doer.quit();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void navigateBack() {
    tracer.trace(PageAction.NAVIGATE_BACK);
    doer.navigateBack();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void navigateForward() {
    tracer.trace(PageAction.NAVIGATE_FORWARD);
    doer.navigateForward();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void navigateRefresh() {
    tracer.trace(PageAction.NAVIGATE_REFRESH);
    doer.navigateRefresh();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void navigateTo(String url) {
    doer.navigateTo(url);
    tracer.trace(PageAction.NAVIGATE_TO, new BaseElement(url));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void navigateTo(URL url) {
    doer.navigateTo(url);
    tracer.trace(PageAction.NAVIGATE_TO, new BaseElement(url.toExternalForm()));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clear(PageElement element) {
    tracer.trace(PageAction.CLEAR, element);
    doer.clear(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void click(PageElement element) {
    tracer.trace(PageAction.CLICK, element);
    doer.click(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getText(PageElement element) {
    // TODO: add result from doer to trace statement
    tracer.trace(PageAction.GET_TEXT, element);
    return doer.getText(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isDisplayed(PageElement element) {
    // TODO: add result from doer to trace statement
    tracer.trace(PageAction.IS_DISPLAYED, element);
    return doer.isDisplayed(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isEnabled(PageElement element) {
    // TODO: add result from doer to trace statement
    tracer.trace(PageAction.IS_ENABLED, element);
    return doer.isEnabled(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isSelected(PageElement element) {
    // TODO: add result from doer to trace statement
    tracer.trace(PageAction.IS_SELECTED, element);
    return doer.isSelected(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendKeys(PageElement element, String value) {
    doer.sendKeys(element, value);
    tracer.trace(PageAction.SEND_KEYS, element, new BaseElement(value));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void switchToActiveElement() {
    tracer.trace(PageAction.SWITCH_TO_ACTIVE_ELEMENT);
    doer.switchToActiveElement();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void switchToAlert() {
    tracer.trace(PageAction.SWITCH_TO_ALERT);
    doer.switchToAlert();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void switchToDefaultContent() {
    tracer.trace(PageAction.SWITCH_TO_DEFAULT_CONTENT);
    doer.switchToDefaultContent();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void switchToFrame(int id) {
    doer.switchToFrame(id);
    tracer.trace(PageAction.SWITCH_TO_FRAME, new BaseElement(Integer.toString(id)));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void switchToFrame(String id) {
    doer.switchToFrame(id);
    tracer.trace(PageAction.SWITCH_TO_FRAME, new BaseElement(id));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void switchToFrame(PageElement element) {
    tracer.trace(PageAction.SWITCH_TO_FRAME, element);
    doer.switchToFrame(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void selectValueFromField(PageElement element, String value) {
    tracer.trace(PageAction.SELECT_VALUE_FROM_FIELD, element, new BaseElement(value));
    doer.selectValueFromField(element, value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getAttribute(PageElement element, String attribute) {
    // TODO: add result from doer to trace statement
    tracer.trace(PageAction.GET_ATTRIBUTE, element);
    return doer.getAttribute(element, attribute);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Cookie getCookie(String name) {
    tracer.trace(PageAction.GET_COOKIE, new BaseElement(name));
    return doer.getCookie(name);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeJavaScript(String js, Object... args1) {
    tracer.trace(PageAction.JAVASCRIPT, new BaseElement(js));
    doer.executeJavaScript(js, args1);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public WebDriver getWebDriver() {
    return doer.getWebDriver();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public WebDriverWait getWebDriverWait() {
    return doer.getWebDriverWait();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void waitUntil(ExpectedCondition<?> condition) {
    doer.waitUntil(condition);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public File saveScreenshot(String screenshotFileName) throws IOException {
    File result = doer.saveScreenshot(screenshotFileName);
    tracer.trace(PageAction.GET_SCREENSHOT, new BaseElement(result.getAbsolutePath()));
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteAllCookies() {
    tracer.trace(PageAction.DELETE_ALL_COOKIES);
    doer.deleteAllCookies();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void selectValue(PageElement element, String value) {
    tracer.trace(PageAction.SELECT_VALUE_FROM_FIELD, element, new BaseElement(value));
    doer.selectValue(element, value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void selectFromVisibleText(PageElement element, String text) {
    tracer.trace(PageAction.SELECT_FROM_VISIBLE_TEXT, element, new BaseElement(text));
    doer.selectFromVisibleText(element, text);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean elementExists(PageElement element) {
    tracer.trace(PageAction.ELEMENT_EXISTS, element);
    return doer.elementExists(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void acceptAlert() {
    tracer.trace(PageAction.ACCEPT_ALERT);
    doer.acceptAlert();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void dismissAlert() {
    tracer.trace(PageAction.DISMISS_ALERT);
    doer.dismissAlert();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void waitForPageToLoad() {
    doer.waitForPageToLoad();
    tracer.trace(PageAction.WAIT_FOR_PAGE_TO_LOAD);
  }

  /**
   * {@inheritDoc}
   */
  public void waitForElementPresent(PageElement element) {
    doer.waitForElementPresent(element);
    tracer.trace(PageAction.WAIT_FOR_ELEMENT_PRESENT);
  }

  /**
   * {@inheritDoc}
   */
  public void waitForElementNotPresent(PageElement element) {
    doer.waitForElementPresent(element);
    tracer.trace(PageAction.WAIT_FOR_ELEMENT_NOT_PRESENT);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setImplicitWait(int waitInSeconds) {
    doer.setImplicitWait(waitInSeconds);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void doubleClick(PageElement element) {
    doer.doubleClick(element);
    tracer.trace(PageAction.DOUBLE_CLICK);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getCurrentWindowHandle() {
    String result = doer.getCurrentWindowHandle();
    tracer.trace(PageAction.WINDOW_HANDLE);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<String> getAllWindowHandles() {
    Set<String> results = doer.getAllWindowHandles();
    tracer.trace(PageAction.WINDOW_HANDLE);
    return results;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clickAndWait(PageElement element) {
    doer.clickAndWait(element);
    tracer.trace(PageAction.CLICK, element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendKeys(CharSequence... keysToSend) {
    tracer.trace(PageAction.SEND_KEYS, new BaseElement(keysToSend.toString()));
    doer.sendKeys(keysToSend);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Pilot switchToWindowHandle(String windowHandleName) {
    Pilot result = doer.switchToWindowHandle(windowHandleName);
    tracer.trace(PageAction.SWITCH_WINDOW_HANDLE);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clearAndSendKeys(PageElement element, String value) {
    tracer.trace(PageAction.SET_VALUE, element, new BaseElement(value));
    doer.clearAndSendKeys(element, value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void authenticateUsing(Credentials credentials) {
    tracer.trace(
            PageAction.AUTHENTICATE_WITH_CREDENTIALS,
            new BaseElement(credentials.toString())
    );
    doer.authenticateUsing(credentials);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void moveToElement(PageElement element) {
    tracer.trace(PageAction.MOVE_TO_ELEMENT, element);
    doer.moveToElement(element);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void moveToElement(PageElement element, int xOffset, int yOffset) {
    tracer.trace(PageAction.MOVE_TO_LOCATION_IN_ELEMENT, element, new BaseElement(xOffset + "," + yOffset));
    doer.moveToElement(element, xOffset, yOffset);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clickAtMouseLocation() {
    tracer.trace(PageAction.CLICK_AT_MOUSE_LOCATION);
    doer.clickAtMouseLocation();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void maximize() {
    tracer.trace(PageAction.MAXIMIZE_WINDOW);
    doer.maximize();
  }

}

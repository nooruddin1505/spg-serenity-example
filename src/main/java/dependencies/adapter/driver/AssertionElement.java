package dependencies.adapter.driver;


import dependencies.adapter.BaseElement;

public class AssertionElement extends BaseElement {

  private static final String PASS = "pass";

  private static final String FAIL = "fail";

  public AssertionElement(boolean result) {
    super(result ? PASS : FAIL);
  }

}

package dependencies.adapter.driver;

import dependencies.adapter.BaseTracer;
import dependencies.adapter.TestRunDetails;
import dependencies.adapter.abstractstuff.Assertor;
import dependencies.adapter.abstractstuff.BaseReportGenerator;
import dependencies.adapter.abstractstuff.ConfigData;
import dependencies.adapter.abstractstuff.Pilot;
import dependencies.adapter.abstractstuff.TestData;

/**
 * Container for components of global scope within an individual test execution.
 * <p>
 * Singleton class to keep track of framework components accessed from different hierarchies. Avoids
 * the need to obscure the descriptive nature of the test scripts with non-related parameters.
 * <p>
 * Works as a reference container. Each reference is set at the point they are created. The object
 * requesting access to a reference is in charge of verifying whether it is null or not.
 *
 * @see BaseReportGenerator
 * @see BaseTracer
 * @see ConfigData
 * @see Pilot
 */
public final class ExecutionContext {

  /**
   * Reference to the single instance of this class.
   */
  private static ExecutionContext singleton = null;

  /**
   * Access to the single instance of this class.
   * <p>
   * Initialises the single instance of this class upon first invocation of the method.
   *
   * @return the single instance of this class
   */
  public static ExecutionContext getInstance() {
    if (singleton == null) {
      singleton = new ExecutionContext();
    }
    return singleton;
  }

  /**
   * Enforces the singleton nature of this class.
   */
  private ExecutionContext() {
  }

  /**
   * Reference to the configuration data tracked by this context.
   */
  private ConfigData configData = null;

  /**
   * Obtains the configuration data tracked by this context.
   *
   * @return the configuration data tracked by this context
   */
  public ConfigData getConfigData() {
    return configData;
  }

  /**
   * Sets the configuration data to be tracked by this context.
   *
   * @param configData the configuration data to keep track of
   */
  public void setConfigData(ConfigData configData) {
    this.configData = configData;
  }

  /**
   * Reference to the report generator tracked by this context.
   */
  private BaseReportGenerator resultGenerator = null;

  /**
   * Obtains the report generator tracked by this context.
   *
   * @return the report generator tracked by this context
   */
  public BaseReportGenerator getReportGenerator() {
    return resultGenerator;
  }

  /**
   * Sets the report generator to be tracked by this context.
   *
   * @param resultGenerator the report generator to be tracked by this context.
   */
  public void setReportGenerator(BaseReportGenerator resultGenerator) {
    this.resultGenerator = resultGenerator;
  }

  /**
   * Reference to the web pilot tracked by this context.
   */
  private Pilot pilot = null;

  /**
   * Obtains the web pilot tracked by this context.
   *
   * @return the web pilot tracked by this context
   */
  public Pilot getPilot() {
    return pilot;
  }

  /**
   * Sets the web pilot tracked by this context.
   *
   * @param pilot the web pilot to be tracked by this context
   */
  public void setPilot(Pilot pilot) {
    this.pilot = pilot;
  }

  /**
   * Reference to the tracer tracked by this context.
   */
  private BaseTracer tracer = null;

//  /**
//   * Screen Recorder.
//   */
//  private ScreenRecorder screenRecorder = null;
//
//  /**
//   * Obtains the screen recorder for this context.
//   *
//   * @return the screen recorder for this context
//   */
//  public ScreenRecorder getScreenRecorder() {
//    return screenRecorder;
//  }
//
//  /**
//   * Sets the screen recorder for this context.
//   *
//   * @param screenRecorder the screen recorder for this context
//   */
//  public void setScreenRecorder(ScreenRecorder screenRecorder) {
//    this.screenRecorder = screenRecorder;
//  }

  /**
   * Obtains the tracer tracked by this context.
   *
   * @return the tracer tracked by this context
   */
  public BaseTracer getTracer() {
    return tracer;
  }

  /**
   * Sets the tracker to be tracked by this context.
   *
   * @param tracer the tracker to be tracked by this context
   */
  public void setTracer(BaseTracer tracer) {
    this.tracer = tracer;
  }

  /**
   * Reference to the test data tracked by this context.
   */
  private TestData testData = null;

  /**
   * Obtains the test data tracked by this context.
   *
   * @return the test data tracked by this context
   */
  public TestData getTestData() {
    return testData;
  }

  /**
   * Sets the test data to be tracked by this context.
   *
   * @param testData the test data to be tracked by this context
   */
  public void setTestData(TestData testData) {
    this.testData = testData;
  }

  /**
   * Reference to the run ID tracked by this context.
   */
  private String runID = null;

  /**
   * Obtains the run ID tracked by this context.
   *
   * @return the run ID tracked by this context
   */
  public String getRunID() {
    return runID;
  }

  /**
   * Sets the run ID to be tracked by this context.
   *
   * @param runID the run ID to be tracked by this context
   */
  public void setRunID(String runID) {
    this.runID = runID;
  }

  private TestRunDetails testRunDetails = null;

  public TestRunDetails getTestRunDetails() {
    return testRunDetails;
  }

  public void setTestRunDetails(TestRunDetails testRunDetails) {
    this.testRunDetails = testRunDetails;
  }

  private boolean reportingFromSuite = true;

  public boolean getReportingFromSuite() {
    return reportingFromSuite;
  }

  public void setReportingFromSuite(boolean reportingFromSuite) {
    this.reportingFromSuite = reportingFromSuite;
  }

  private Assertor assertor = null;

  public Assertor getAssertor() {
    return assertor;
  }

  public void setAssertor(Assertor assertor) {
    this.assertor = assertor;
  }

}

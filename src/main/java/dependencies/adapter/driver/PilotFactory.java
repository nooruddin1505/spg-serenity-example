package dependencies.adapter.driver;


import dependencies.adapter.abstractstuff.Pilot;

/**
 * Centralised location for the creation of web pilots.
 * <p>
 * A configuration data property determines the type of pilot to be created.
 *
 * @see ConfigData
 * @see Pilot
 */
public class PilotFactory {

  /**
   * Default constructor.
   * <p>
   * Prevents instantiation of this factory class.
   */
  private PilotFactory() {

  }

  /**
   * Creates a new {@link Pilot} instance.
   * <p>
   * The type of pilot to use will be determined by the "Run" configuration property.
   *
   * @return a new web pilot
   *
   * @see ConfigData
   * @see Pilot
   */
  public static Pilot createTestPilot() {
    String pilotType = ExecutionContext.getInstance().getConfigData().getStringProperty("Tracing");
    if (pilotType != null) {
      if (pilotType.equalsIgnoreCase("no_log")) {
        return new SeleniumPilot();
      }
    }
    return new TracingPilot(new SeleniumPilot());
  }
}

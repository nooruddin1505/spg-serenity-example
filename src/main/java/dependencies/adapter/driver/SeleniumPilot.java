package dependencies.adapter.driver;

import dependencies.adapter.abstractstuff.ConfigData;
import dependencies.adapter.abstractstuff.Pilot;
import dependencies.adapter.abstractstuff.TestData;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.security.Credentials;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * A {@link Pilot} implementation for the Selenium {@link WebDriver}.
 * <p>
 * The underlying Selenium {@link WebDriver} instance is created within the constructor.
 *
 * @see WebDriver
 */
public class SeleniumPilot implements Pilot {


//  static{
//    System.setProperty("webdriver.ie.driver", "C:\\Users\\InternetUser\\Downloads\\Selenium\\ie\\IE32\\IEDriverServer.exe");
//    System.setProperty("webdriver.gecko.driver", "C:\\Users\\InternetUser\\Downloads\\Selenium\\ff\\geckodriver.exe");
//    System.setProperty("webdriver.chrome.driver", "C:\\Users\\InternetUser\\Downloads\\Selenium\\chrome\\chromedriver.exe");
//  }

  /**
   * Default implicit timeout. Overriden by configuration property Timeout.Implicit.
   *
   * @see ConfigData
   */
  private static final long DEFAULT_IMPLICIT_TIMEOUT = 5;

  /**
   * Default explicit timeout. Overriden by configuration property Timeout.Explicit.
   *
   * @see ConfigData
   */
  private static final long DEFAULT_EXPLICIT_TIMEOUT = 120;

  /**
   * The underlying Selenium WebDriver.
   */
  protected WebDriver driver;

  /**
   * The underlying Selenium WebDriverWait.
   */
  protected WebDriverWait wait;

  /**
   * wait for element to be displayed.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element the element to wait for before returning.
   */
  public void waitForElementPresent(PageElement element) {

    // Timeout in 30 seconds
    Wait<WebDriver> wait = new WebDriverWait(driver, 30);

    wait.until(ExpectedConditions.visibilityOfElementLocated(element.getBy()));
  }

  /**
   * Double click on a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be clicked
   * @see WebDriver#findElement(By)
   * @see Actions#doubleClick(WebElement)
   */
  public void doubleClick(PageElement element) {
    Actions action = new Actions(driver);
    action.doubleClick(driver.findElement(element.getBy()));
    action.perform();
  }

  /**
   * Switch to Window with the supplied handle name
   *
   * @param windowHandleName the name of the window to switch to.
   * @return new pilot for interacting with Window.
   */
  @Override
  public Pilot switchToWindowHandle(String windowHandleName) {
    WebDriver windowDriver = driver.switchTo().window(windowHandleName);
    // Wait for Window
    waitForPageToLoad(windowDriver);

    Pilot pilotforNewWindow = new SeleniumPilot(windowDriver);

    return pilotforNewWindow;
  }

  /**
   * Default constructor.
   * <p>
   * The configuration data is queried for a specific type of browser to drive, defaulting to
   * Firefox.
   *
   * @see ChromeDriver
   * @see ConfigData
   * @see ExecutionContext
   * @see FirefoxDriver
   * @see InternetExplorerDriver
   * @see WebDriver
   */
  public SeleniumPilot() {

    ConfigData configData = ExecutionContext.getInstance().getConfigData();
    TestData testData = ExecutionContext.getInstance().getTestData();
    String webBrowser = configData.getStringProperty("WebBrowser");
    if (webBrowser.equals("IE")) {
      driver = new InternetExplorerDriver(parseDesiredCapabilities());
    } else if (webBrowser.equals("Chrome")) {
      driver = new ChromeDriver(parseDesiredCapabilities());
    } else if (webBrowser.equals("HTMLUnit")) {
      driver = new HtmlUnitDriver();
    } else if (webBrowser.equals("SauceLabs") || webBrowser.equals("SeleniumGrid")) {
      String url = "";
      if (webBrowser.equals("SauceLabs")) {
        url = "http://" + configData.getStringProperty("SauceLabs", "Username") + ":"
                + configData.getStringProperty("SauceLabs", "Password")
                + configData.getStringProperty("SauceLabs", "Path");
      } else {
        url = configData.getStringProperty("SeleniumGrid", "URL");
      }
      try {
        DesiredCapabilities caps = parseDesiredCapabilities();
        driver = new RemoteWebDriver(new URL(url), caps);
        String width = testData.getInputValue("Configuration", "Browser", "Width");
        String height = testData.getInputValue("Configuration", "Browser", "Height");
        if (width != null && !width.equals("") && height != null && !height.equals("")) {
          driver.manage().window()
                  .setSize(new Dimension(Integer.parseInt(width), Integer.parseInt(height)));
        }
        //Output the driver information including sessionID
        Capabilities capabilities = ((RemoteWebDriver) getWebDriver()).getCapabilities();
        System.out.println("\n" + "RemoteWebDriver: " + capabilities.getBrowserName() + " "
                + capabilities.getVersion() + " on " + capabilities.getPlatform() + " (" +
                ((RemoteWebDriver) getWebDriver()).getSessionId() + ")" + "\n");
      } catch (Throwable e) {
        // Output the driver error
        e.printStackTrace();
      }
    } else {
      // Use custom Firefox Profile if specified in config, otherwise use default
      String browserProfile = configData.getStringProperty("WebBrowser", "Profile");
      FirefoxOptions fo = new FirefoxOptions();
      fo.setLogLevel(Level.OFF);
      fo.addPreference("log", "{level: error}");
      DesiredCapabilities fc = DesiredCapabilities.firefox();
      fc.setCapability("mos:firefoxOptions", fo);
      if (browserProfile != null && !browserProfile.isEmpty()) {
        FirefoxProfile profile = (new ProfilesIni()).getProfile(browserProfile);
        driver = new FirefoxDriver();
      } else {
        driver = new FirefoxDriver(fc);
      }
    }
    if (driver != null) {
      initWaits(driver);
    } else {
      //web driver not initalised so exit
      System.out.println("\n" + "[ERROR] Failed to initialise the WebDriver");
      System.exit(-1);
    }
  }

  /**
   * Allow Webdriver to be passed to Constructor, used when a new Window is opened
   *
   * @param driver
   */
  private SeleniumPilot(WebDriver driver) {
    this.driver = driver;
    initWaits(driver);
  }

  private void initWaits(WebDriver driver) {
    ConfigData configData = ExecutionContext.getInstance().getConfigData();

    this.wait =
            new WebDriverWait(driver, configData.getLongProperty(DEFAULT_EXPLICIT_TIMEOUT, "Timeout",
                    "Explicit"));

    String implicitTimeoutString = configData.getProperty("Timeout", "Implicit");
    if (implicitTimeoutString == null) {
      implicitTimeoutString = configData.getProperty("Timeout");
    }
    driver
            .manage()
            .timeouts()
            .implicitlyWait(
                    (implicitTimeoutString == null) ? DEFAULT_IMPLICIT_TIMEOUT
                            : Long.parseLong(implicitTimeoutString), TimeUnit.SECONDS);
  }

  private DesiredCapabilities parseDesiredCapabilities() {
    DesiredCapabilities caps = null;
    TestData testData = ExecutionContext.getInstance().getTestData();
    String browserType = testData.getInputValue("Configuration", "Browser", "Type");
    if (browserType == null || browserType.isEmpty()) {
      String webBrowser = ExecutionContext.getInstance().getConfigData().getStringProperty("WebBrowser");
      if (!(webBrowser.equals("SauceLabs") || webBrowser.equals("SeleniumGrid"))) {
        browserType = webBrowser;
      }
    }
    String HTTPProxy = ExecutionContext.getInstance().getConfigData().getStringProperty("WebBrowser.HttpProxy");
    String SSLProxy = ExecutionContext.getInstance().getConfigData().getStringProperty("WebBrowser.SslProxy");

    if (browserType.equals("Chrome")) {
      caps = DesiredCapabilities.chrome();
    } else if (browserType.equals("IE") || browserType.equals("Internet Explorer")) {
      caps = DesiredCapabilities.internetExplorer();
      caps.setCapability("requireWindowFocus", true);
      caps.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
    } else if (browserType.equals("Safari")) {
      caps = DesiredCapabilities.safari();
    } else if (browserType.equals("Android")) {
      caps = DesiredCapabilities.android();
    } else if (browserType.equals("iPhone")) {
      caps = DesiredCapabilities.iphone();
    } else if (browserType.equals("iPad")) {
      caps = DesiredCapabilities.ipad();
    } else /* (browserType.equals("Firefox")) */{
      caps = DesiredCapabilities.firefox();
    }

    if (browserType.equals("SauceLabs") || browserType.equals("SeleniumGrid")) {
      caps.setCapability("version", testData.getInputValue("Configuration", "Browser", "Version"));
      caps.setCapability("platform", testData.getInputValue("Configuration", "OS"));
      caps.setCapability("device-orientation",
              testData.getInputValue("Configuration", "Device", "Orientation"));
      // caps.setCapability("avoid-proxy", true);
      caps.setCapability("name", ExecutionContext.getInstance().getRunID());
    }

    // Set the httpProxy or sslProxy for Internet Explorer or Chrome
    // Firefox can be set via the profile
    if (browserType.equals("IE") || browserType.equals("Chrome")) {
      if ((HTTPProxy != null && !HTTPProxy.isEmpty()) || (SSLProxy !=null && !SSLProxy.isEmpty())) {
        Proxy proxy = new Proxy();
        if (!HTTPProxy.isEmpty()) {
          proxy.setHttpProxy(HTTPProxy);
        }
        if (!SSLProxy.isEmpty()) {
          proxy.setSslProxy(SSLProxy);
        }
        caps.setCapability("proxy", proxy);
      }
    }

    return caps;
  }

  /**
   * Close the current browser page.
   *
   * @see WebDriver#close()
   */
  @Override
  public void close() {
    driver.close();
  }

  /**
   * Obtain the URL of the current browser page.
   *
   * @return a {@link String} representation of the current browser page's URL
   * @see WebDriver#getCurrentUrl()
   */
  @Override
  public String getCurrentUrl() {
    return driver.getCurrentUrl();
  }

  /**
   * Obtain the source (HTML) code of the current browser page.
   *
   * @return a {@link String} representation of the current browser page's source code
   * @see WebDriver#getPageSource()
   */
  @Override
  public String getPageSource() {
    return driver.getPageSource();
  }

  /**
   * Capture a screenshot of the current browser page.
   *
   * @return the path to the file containing the captured screenshot
   * @see TakesScreenshot#getScreenshotAs(OutputType)
   */
  @Override
  public String getScreenshot() {
    if (driver instanceof TakesScreenshot) {
      File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
      String screenshotFilename =
              ExecutionContext.getInstance().getConfigData().getStringProperty("Screenshots", "Dir")
                      + File.separatorChar + System.currentTimeMillis() + ".png";
      try {
        FileUtils.copyFile(screenshotFile, new File(screenshotFilename));
      } catch (IOException e) {
        throw new RuntimeException("Failed to output screenshot to " + screenshotFilename, e);
      }
      return screenshotFilename;
    } else {
      return null;
    }
  }

  /**
   * Obtain the (HTML) title of the current browser page.
   *
   * @return a {@link String} containing the current browser page's title
   * @see WebDriver#getTitle()
   */
  @Override
  public String getTitle() {
    return driver.getTitle();
  }

  /**
   * Quit the underlying Selenium driver.
   *
   * @see WebDriver#quit()
   */
  @Override
  public void quit() {
    driver.quit();
  }

  /**
   * Navigate back to the immediately preceding page in browsing history, if any.
   *
   * @see WebDriver#navigate()
   * @see WebDriver.Navigation#back()
   */
  @Override
  public void navigateBack() {
    driver.navigate().back();
  }

  /**
   * Navigate forward to the immediately following page in browsing history, if any.
   *
   * @see WebDriver#navigate()
   * @see WebDriver.Navigation#forward()
   */
  @Override
  public void navigateForward() {
    driver.navigate().forward();
  }

  /**
   * Refresh the page currently displayed in the browser.
   *
   * @see WebDriver#navigate()
   * @see WebDriver.Navigation#refresh()
   */
  @Override
  public void navigateRefresh() {
    driver.navigate().refresh();
  }

  /**
   * Navigate to a specified URL.
   *
   * @param url a String representing the URL to navigate to
   *
   * @see WebDriver#navigate()
   * @see WebDriver.Navigation#to(String)
   */
  @Override
  public void navigateTo(String url) {
    driver.navigate().to(url);
  }

  /**
   * Navigate to a specified URL.
   *
   * @param url a URL identifying the page to navigate to
   * @see WebDriver#navigate()
   * @see WebDriver.Navigation#to(URL)
   */
  @Override
  public void navigateTo(URL url) {
    driver.navigate().to(url);
  }

  /**
   * Clear a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be cleared
   * @see WebDriver#findElement(By)
   * @see WebElement#clear()
   */
  @Override
  public void clear(PageElement element) {
    driver.findElement(element.getBy()).clear();
  }

  /**
   * Click on a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be clicked
   * @see WebDriver#findElement(By)
   * @see WebElement#click()
   */
  @Override
  public void click(PageElement element) {
    driver.findElement(element.getBy()).click();
  }

  /**
   * Obtain the text associated to a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element whose text is required
   * @see WebDriver#findElement(By)
   * @see WebElement#getText()
   */
  @Override
  public String getText(PageElement element) {
    return driver.findElement(element.getBy()).getText();
  }

  /**
   * Verify whether a specified page's element is displayed on the current page or not.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be verified
   * @return true if the specified element is displayed on the current page, false otherwise
   * @see WebDriver#findElement(By)
   * @see WebElement#isDisplayed()
   */
  @Override
  public boolean isDisplayed(PageElement element) {
    return driver.findElement(element.getBy()).isDisplayed();
  }

  /**
   * Verify whether a specified page's element is enabled on the current page or not.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be verified
   * @return true if the specified element is enabled on the current page, false otherwise
   * @see WebDriver#findElement(By)
   * @see WebElement#isEnabled()
   */
  @Override
  public boolean isEnabled(PageElement element) {
    return driver.findElement(element.getBy()).isEnabled();
  }

  /**
   * Verify whether a specified page's element is selected on the current page or not.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be verified
   * @return true if the specified element is selected on the current page, false otherwise
   * @see WebDriver#findElement(By)
   * @see WebElement#isSelected()
   */
  @Override
  public boolean isSelected(PageElement element) {
    return driver.findElement(element.getBy()).isSelected();
  }

  /**
   * Send a sequence of characters to a specified page's element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element to be modified
   * @param value the string to be sent
   * @see WebDriver#findElement(By)
   * @see WebElement#sendKeys(CharSequence...)
   */
  @Override
  public void sendKeys(PageElement element, String value) {
    driver.findElement(element.getBy()).sendKeys(value);
  }

  /**
   * Switch focus to the active element on the browser.
   *
   * @see WebDriver#switchTo()
   * @see WebDriver.TargetLocator#activeElement()
   */
  @Override
  public void switchToActiveElement() {
    driver.switchTo().activeElement();
  }

  /**
   * Switch focus to the active alert on the browser.
   *
   * @see WebDriver#switchTo()
   * @see WebDriver.TargetLocator#alert()
   */
  @Override
  public void switchToAlert() {
    driver.switchTo().alert();
  }

  /**
   * Switch focus to the default content on the browser.
   *
   * @see WebDriver#switchTo()
   * @see WebDriver.TargetLocator#defaultContent()
   */
  @Override
  public void switchToDefaultContent() {
    driver.switchTo().defaultContent();
  }

  /**
   * Switch focus to a specified frame.
   * <p>
   * The frame is identified by its (zero-based) index.
   *
   * @param id the (zero-based) index identifying the target frame
   * @see WebDriver#findElement(By)
   * @see WebDriver.TargetLocator#frame(int)
   */
  @Override
  public void switchToFrame(int id) {
    driver.switchTo().frame(id);
  }

  /**
   * Switch focus to a specified frame.
   * <p>
   * The frame is identified by its name or ID.
   *
   * @param id the name or ID identifying the target frame
   * @see WebDriver#findElement(By)
   * @see WebDriver.TargetLocator#frame(String)
   */
  @Override
  public void switchToFrame(String id) {
    driver.switchTo().frame(id);
  }

  /**
   * Switch focus to a specified frame.
   * <p>
   * The frame is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference identifying the target frame
   * @see WebDriver#findElement(By)
   * @see WebDriver.TargetLocator#frame(WebElement)
   */
  @Override
  public void switchToFrame(PageElement element) {
    driver.switchTo().frame(driver.findElement(element.getBy()));
  }

  /**
   * Selects a specified option element from within a select element
   * <p>
   * The select is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference identifying the target frame
   * @param value the textual value of the option to be select
   * @see WebDriver#findElement(By)
   * @see Select#selectByVisibleText(String)
   */
  @Override
  public void selectValueFromField(PageElement element, String value) {
    new Select(driver.findElement(element.getBy())).selectByVisibleText(value);
  }

  /**
   * Obtain the textual value of the specified attribute of an element.
   * <p>
   * The element is defined by the {@link By} entity within the {@link PageElement} argument.
   *
   * @param element a unique reference to the element where an attribute value is required
   * @param attribute the name of the attribute to be obtained
   * @see WebDriver#findElement(By)
   * @see WebElement#getAttribute(String)
   */
  @Override
  public String getAttribute(PageElement element, String attribute) {
    return driver.findElement(element.getBy()).getAttribute(attribute);
  }

  /**
   * Obtain a cookie that is set in the current driver
   * <p>
   *
   * @param name the name of the desired cookie object to be retrieved
   */
  @Override
  public Cookie getCookie(String name) {
    return driver.manage().getCookieNamed(name);

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void executeJavaScript(String js, Object... parameters) {
    if (!(driver instanceof JavascriptExecutor)) {
      throw new RuntimeException(
              "SeleniumPilot.executeJavaScript(String,Object...): error: driver not Javascript-enabled");
    }
    ((JavascriptExecutor) driver).executeScript(js, parameters);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public WebDriver getWebDriver() {
    return this.driver;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public WebDriverWait getWebDriverWait() {
    return wait;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void waitUntil(ExpectedCondition<?> condition) {
    wait.until(condition);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public File saveScreenshot(String screenshotFileName) throws IOException {
    if (!(driver instanceof TakesScreenshot)) {
      throw new IllegalArgumentException(
              "error: SeleniumPilot#saveScreenshot(String): not instance of TakesScreenshot");
    }
    File tmpResult = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    File result = new File(screenshotFileName);
    FileUtils.copyFile(tmpResult, result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendKeys(CharSequence... keysToSend) {
    Actions actions = new Actions(driver);
    actions.sendKeys(keysToSend).build().perform();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clearAndSendKeys(PageElement element, String value) {
    clear(element);
    sendKeys(element, value);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteAllCookies() {
    driver.manage().deleteAllCookies();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void selectValue(PageElement element, String value) {
    By selector = element.getBy();
    wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
    // TODO: wait for value to be present?
    new Select(driver.findElement(selector)).selectByValue(value);
  }

  public void selectFromVisibleText(PageElement element, String visibleText) {
    By selector = element.getBy();
    wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
    new Select(driver.findElement(selector)).selectByVisibleText(visibleText);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean elementExists(PageElement element) {
    List<WebElement> matchingElements = driver.findElements(element.getBy());
    return matchingElements.size() > 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void acceptAlert() {
    driver.switchTo().alert().accept();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void dismissAlert() {
    driver.switchTo().alert().dismiss();
  }

  /**
   * Wait for given element to not be visible.
   *
   * @param element to wait for.
   */
  public void waitForElementNotPresent(PageElement element) {
    WebDriverWait wait = new WebDriverWait(driver, 15);

    wait.until(ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(element.getBy())));
  }

  /**
   * Get the Current Window Handle, this allows for switching windows
   *
   * @return name of the current window handle
   *
   */
  @Override
  public String getCurrentWindowHandle() {
    return driver.getWindowHandle();
  }

  /**
   * Return all names of Windows Handles
   *
   * @return set of window handle names
   */
  @Override
  public Set<String> getAllWindowHandles() {
    return driver.getWindowHandles();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setImplicitWait(int waitInSeconds) {
    driver.manage().timeouts().implicitlyWait(waitInSeconds, TimeUnit.SECONDS);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clickAndWait(PageElement element) {
    click(element);
    waitForPageToLoad();
  }

  /**
   * Wait for page to finish loading.
   *
   * Uses Default driver
   */
  public void waitForPageToLoad() {
    waitForPageToLoad(driver);
  }

  /**
   * Wait for page to finish loading before releasing control back to test classes
   *
   * @param driverToUse to use
   */
  private void waitForPageToLoad(WebDriver driverToUse) {
    // See if Javascript is supported and wait for page to refresh
    if (driver instanceof JavascriptExecutor) {
      // Timeout in 30 seconds
      Wait<WebDriver> wait = new WebDriverWait(driver, 30);

      ReadyStateExpectedCondition readyStateCondition = new ReadyStateExpectedCondition();
      wait.until(readyStateCondition);
    }
  }

  /**
   * Obtain 'value' attribute text associated to a specified page's element.
   * <p>
   * Returns <b>null</b> if value attribute not set.
   *
   * @param element the page's element with a 'value' attribute.
   * @return text value.
   */
  @Override
  public String getValue(PageElement element) {
    return driver.findElement(element.getBy()).getAttribute("value");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<WebElement> getMatches(PageElement element) {
    return driver.findElements(element.getBy());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void authenticateUsing(Credentials credentials) {
    driver.switchTo().alert().authenticateUsing(credentials);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void moveToElement(PageElement element) {
    Actions action = new Actions(driver);
    action.moveToElement(driver.findElement(element.getBy())).build().perform();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void moveToElement(PageElement element, int xOffset, int yOffset) {
    Actions action = new Actions(driver);
    action.moveToElement(driver.findElement(element.getBy()), xOffset, yOffset).build().perform();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clickAtMouseLocation() {
    Actions action = new Actions(driver);
    action.click().build().perform();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void maximize() {
    driver.manage().window().maximize();
  }

}

package dependencies.adapter.reporting;

import dependencies.adapter.BaseElement;
import dependencies.adapter.abstractstuff.BaseReportGenerator;
import dependencies.adapter.TestRunDetails;
import dependencies.adapter.TraceEntry;
import dependencies.adapter.abstractstuff.ReportWriterInterface;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Supporting class for the report generation component.
 *
 * @see BaseReportGenerator
 */
public class XmlWriter implements ReportWriterInterface {

  protected Document document;
  protected Element rootElement;
  protected String reportFilename;

  public XmlWriter(String reportFilename) {
    this.reportFilename = reportFilename;
    try {
      this.document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      this.rootElement = document.createElement("testResults");

      document.appendChild(rootElement);
      flush();
    } catch (ParserConfigurationException e) {
      throw new Error("Could not create results XML file!");
    }
  }

  protected void addTextNodeTo(Element element, String title, Object value) {
    if (value == null) {
      value = "";
    }

    Element newElement = document.createElement(title);
    newElement.appendChild(document.createTextNode(value.toString()));
    element.appendChild(newElement);
  }

  @Override
  public void appendToReport(TestRunDetails testResult) {
    Element testResultElement = document.createElement("testResult");

    testResultElement.setAttribute("id", testResult.getId());
    testResultElement.setAttribute("result", testResult.getTestResult());
    testResultElement.setAttribute("startTime", String.valueOf(testResult.getStartTime()));
    testResultElement.setAttribute("endTime", String.valueOf(testResult.getEndTime()));

    List<TraceEntry> traces = testResult.getTracer().getTraces();

    Element traceElement = document.createElement("trace");
    Integer count = 1;
    for (TraceEntry trace : traces) {
      Element stepElement = document.createElement("step");
      stepElement.setAttribute("id", (count++).toString());
      stepElement.setAttribute("timestamp", String.valueOf(trace.getTimestamp()));

      addTextNodeTo(stepElement, "action", trace.getAction());
      for (BaseElement parameter : trace.getParameters()) {
        addTextNodeTo(stepElement, "parameter", parameter);
      }

      traceElement.appendChild(stepElement);
    }
    testResultElement.appendChild(traceElement);

    rootElement.appendChild(testResultElement);

    flush();
  }

  private void flush() {
    try {
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      DOMSource source = new DOMSource(document);
      StreamResult result = new StreamResult(new PrintWriter(reportFilename));
      transformer.transform(source, result);
    } catch (TransformerException tfe) {
      tfe.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void finalise() {
    flush();
  }

}

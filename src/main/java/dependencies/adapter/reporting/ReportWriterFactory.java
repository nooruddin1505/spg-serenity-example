package dependencies.adapter.reporting;

import dependencies.adapter.abstractstuff.BaseReportGenerator;
import dependencies.adapter.abstractstuff.ReportWriterInterface;
import dependencies.adapter.driver.ExecutionContext;

import java.io.File;

/**
 * Supporting class for the report generation component.
 *
 * @see BaseReportGenerator
 */
public class ReportWriterFactory {

  protected static ReportWriterInterface getWriterOfType(String reportType, String reportFilename) {
    if (reportType.equalsIgnoreCase("XML")) {
      reportFilename += ".xml";
      return new XmlWriter(reportFilename);
    } else if (reportType.equalsIgnoreCase("TXT")) {
      reportFilename += ".txt";
      return new TxtWriter(reportFilename);
    } else {
      reportFilename += ".xls";
      return new XlsWriter(reportFilename);
    }
  }

  public static ReportWriterInterface getWriter(String runID) {
    runID = runID.replace(' ', '_');
    String resultsFormat =
            ExecutionContext.getInstance().getConfigData().getStringProperty("Results", "Format");

    String resultsDir =
            ExecutionContext.getInstance().getConfigData().getStringProperty("Results", "Dir")
                    .replace(' ', '_');

    if (!new File(resultsDir).exists()) {
      if (!new File(resultsDir).mkdirs()) {
        throw new RuntimeException("Failed to create results directory: " + resultsDir);
      }
    }

    String reportFilename = resultsDir + File.separatorChar + runID;

    String[] writerTypes = resultsFormat.split(",");
    if (writerTypes.length == 1) {
      return getWriterOfType(resultsFormat, reportFilename);
    } else {
      CompositeWriter compositeWriter = new CompositeWriter();
      for (String writerType : writerTypes) {
        compositeWriter.addWriter(getWriterOfType(writerType, reportFilename));
      }
      return compositeWriter;
    }

  }

}

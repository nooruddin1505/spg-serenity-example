package dependencies.adapter.reporting;

import dependencies.adapter.TestRunDetails;
import dependencies.adapter.abstractstuff.BaseReportGenerator;
import dependencies.adapter.abstractstuff.ReportWriterInterface;

import java.util.LinkedList;
import java.util.List;
/**
 * Supporting class for the report generation component.
 *
 * @see BaseReportGenerator
 */
public class CompositeWriter implements ReportWriterInterface {

  protected List<ReportWriterInterface> writers;

  public CompositeWriter() {
    writers = new LinkedList<ReportWriterInterface>();
  }

  public void addWriter(ReportWriterInterface writer) {
    writers.add(writer);
  }

  @Override
  public void appendToReport(TestRunDetails testResult) {
    for (ReportWriterInterface writer : writers) {
      writer.appendToReport(testResult);
    }
  }

  @Override
  public void finalise() {
    for (ReportWriterInterface writer : writers) {
      writer.finalise();
    }
  }

}

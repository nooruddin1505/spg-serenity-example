package dependencies.adapter.reporting;

import dependencies.adapter.abstractstuff.TestData;
import dependencies.adapter.utils.MapUtils;
import dependencies.adapter.utils.StringUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * A test data implementation based on the Excel (XLSX) file format.
 */
public class XLSTestData extends TestData {

  /*
   * The tab where expected results are located. Defined by the Data.ExpectedWorksheet configuration
   * data property.
   */
  private static final String SHEET_EXPECTED_RESULTS = "Expected Results";
//  ExecutionContext.getInstance()
//          .getConfigData().getProperty("Data", "ExpectedWorksheet");

  /*
   * The tab where input values are located. Defined by the Data.InputWorksheet configuration data
   * property.
   */
  private static final String SHEET_INPUT_VALUES = "Input Values";
//  ExecutionContext.getInstance().getConfigData()
//          .getProperty("Data", "InputWorksheet");

  /*
   * The expected results tracked by this test data instance.
   */
  protected Map<String, String> expectedResults;

  /*
   * The input values tracked by this test data instance.
   */
  protected Map<String, String> inputValues;

  /**
   * Base constructor.
   *
   * @param filename the file to be loaded
   * @param key the field to use as criteria to determine the data to be loaded
   * @param value the value to use as criteria to determine the data to be loaded
   */
  public XLSTestData(String filename, String key, String value) {
    expectedResults = new HashMap<String, String>();
    inputValues = new HashMap<String, String>();

    filename = filename.split("\\.")[0];
    File xlsFile = new File(filename + ".xls");
    if (!xlsFile.isFile()) {
      xlsFile = new File(filename + ".xlsx");
    }

    if (xlsFile.isFile()) {
      load(xlsFile, key, value);
    }
  }

  /**
   * Obtains the input value associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the input value associated to a key
   */
  public String getInputValue(String key, String... subkeys) {
    return inputValues.get(StringUtils.concatWith(HIERARCHY_SEPARATOR, key, subkeys));
  }

  /**
   * Obtains the expected result associated to a key.
   *
   * @param key the base element of the key
   * @param subkeys the sub-elements of the key
   * @return the expected result associated to a key
   */
  public String getExpectedResult(String key, String... subkeys) {
    return expectedResults.get(StringUtils.concatWith(HIERARCHY_SEPARATOR, key, subkeys));
  }

  /**
   * Loads the test data from a given file.
   *
   * @param testDataFile the location of the file containing the configuration data to be loaded
   * @param key the field to use as criteria to determine the data to be loaded
   * @param value the value to use as criteria to determine the data to be loaded
   *
   * @see XLSReader
   */
  @Override
  protected void load(File testDataFile, String key, String value) {
    filePaths.add(testDataFile.getAbsolutePath());
    XLSReader xlsReader = new XLSReader(testDataFile);
    xlsReader.load(SHEET_INPUT_VALUES, inputValues, key, value);
    xlsReader.load(SHEET_EXPECTED_RESULTS, expectedResults, key, value);
  }

  /**
   * Obtains the keys in the set of input values tracked by this test data instance.
   *
   * @return the keys in this test data instance's input values
   */
  @Override
  public String[] getInputValueKeys() {
    return MapUtils.getKeys(inputValues);
  }

  /**
   * Obtains the keys in the set of expected results tracked by this test data instance.
   *
   * @return the keys in this test data instance's expected results
   */
  @Override
  public String[] getExpectedResultKeys() {
    return MapUtils.getKeys(expectedResults);
  }

  /**
   * Add the test data in the given object to this one.
   *
   * @param newTestData the object whose data will be added to this one
   */
  @Override
  public void add(TestData newTestData) {
    if (newTestData != null) {
      super.add(newTestData);

      String[] newInputValues = newTestData.getInputValueKeys();
      for (String newInputValue : newInputValues) {
        inputValues.put(newInputValue, newTestData.getInputValue(newInputValue));
      }

      String[] newExpectedResults = newTestData.getExpectedResultKeys();
      for (String newExpectedResult : newExpectedResults) {
        expectedResults.put(newExpectedResult, newTestData.getExpectedResult(newExpectedResult));
      }
    }
  }

  /**
   * String representation of this test data instance.
   *
   * @return the string representation of this test data instance
   */
  @Override
  public String toString() {
    return super.toString(SHEET_INPUT_VALUES, inputValues.toString(), SHEET_EXPECTED_RESULTS,
            expectedResults.toString());
  }

  // New Multiple Line Loading Code Start
  public XLSTestData(String filename) {
    expectedResults = new HashMap<String, String>();
    inputValues = new HashMap<String, String>();

    File xlsFile = new File(filename + ".xls");
    if (!xlsFile.isFile()) {
      xlsFile = new File(filename + ".xlsx");
    }

    if (xlsFile.isFile()) {
      load(xlsFile);
    }
  }

  protected void load(File testDataFile) {
    filePaths.add(testDataFile.getAbsolutePath());
    XLSReader xlsReader = new XLSReader(testDataFile);
    xlsReader.loadMultipleLines(SHEET_INPUT_VALUES, inputValues, null, null);
    xlsReader.loadMultipleLines(SHEET_EXPECTED_RESULTS, expectedResults, null, null);
  }

  // New Multiple Line Loading Code End

}

package dependencies.adapter.reporting;

import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Supporting routines for loading test data in the Excel file format.
 */
public class XLSReader {

  private static final int COLUMN_START = 0;
  private static final int ROW_START = 0;
  private static final int ROW_HEADERS = 0;
  private static final int COL_HEADERS = 0;
  private static final int FIRST_VALUE_COLUMN = COL_HEADERS + 1;
  private static final String DATA_AS_COLUMNS = "Column";
  private File file = null;

  public XLSReader(File file) {
    this.file = file;
  }

  public void load(String dataSheetName, Map<String, String> dataMap, String key, String value) {
    try {
      Workbook testDataWorkbook = WorkbookFactory.create(file);
      /** New Formula Handling code Start **/
      // TODO: perform this re-calculation based on some config.properties
      // value
      HSSFFormulaEvaluator.evaluateAllFormulaCells(testDataWorkbook);
      EnvironmentVariables env = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
      String dataAsColumns = env.getProperty("Data.Orientation");
      /** New Formula Handling code End **/
      if (DATA_AS_COLUMNS.equalsIgnoreCase(dataAsColumns)) {
        loadByColumn(testDataWorkbook, dataSheetName, dataMap, key, value);
      } else {
        loadByRow(testDataWorkbook, dataSheetName, dataMap, key, value);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  protected Map<String, String> getRowInfo(Sheet testDataSheet, Row headers, int row) {
    Map<String, String> rowMap = new HashMap<String, String>();
    Cell headerCell = null;
    String header = null;
    Row data = testDataSheet.getRow(row);
    for (int p = COLUMN_START; ((headerCell = headers.getCell(p)) != null)
            && ((header = getCellValueAsString(headerCell)) != null) && !header.equals(""); p++) {
      try {
        data.getCell(p);
      } catch (Exception e) {
        rowMap.put(header, "");
        continue;
      }
      Cell datumCell = data.getCell(p);
      String datum;
      if ((datumCell == null) || ((datum = getCellValueAsString(datumCell)) == null)) {
        datum = "";
      }
      rowMap.put(header, datum);
    }
    return rowMap;
  }

  public void loadByRow(Workbook testDataWorkbook, String dataSheetName,
                        Map<String, String> dataMap, String key, String value) {
    Sheet testDataSheet = testDataWorkbook.getSheet(dataSheetName);
    Row headers = testDataSheet.getRow(ROW_HEADERS);
    int lastRow = 1;  //testDataSheet.getLastRowNum();
    Map<String, String> lastRowMap = null;
    for (int row = 1; row <= lastRow; row++) {
      lastRowMap = getRowInfo(testDataSheet, headers, row);
      if (lastRowMap != null) {
        if (key == null || (lastRowMap.containsKey(key) && lastRowMap.get(key).equals(value))) {
          dataMap.putAll(lastRowMap);
          return;
        }
      }
    }
    dataMap.putAll(lastRowMap);
  }

  public void loadByColumn(Workbook testDataWorkbook, String dataSheetName,
                           Map<String, String> dataMap, String key, String value) {
    Sheet testDataSheet = testDataWorkbook.getSheet(dataSheetName);
    int lastRow = testDataSheet.getLastRowNum();
    int column = FIRST_VALUE_COLUMN;
    Cell currentHeaderCell = null;
    String currentHeader = null;
    Row currentRow = testDataSheet.getRow(ROW_START);
    // if key is null, then load the first the column
    // otherwise, search for the column with the right key-value pair
    if (key != null) {
      for (int row = ROW_START; ((currentHeaderCell = currentRow.getCell(COL_HEADERS)) != null)
              && ((currentHeader = getCellValueAsString(currentHeaderCell)) != null) && row <= lastRow; row++) {
        currentRow = testDataSheet.getRow(row);
        currentHeader = getCellValueAsString(currentRow.getCell(COL_HEADERS));
        if (currentHeader.equals(key)) {
          Cell currentCell = null;
          String current = null;
          for (; ((currentCell = currentRow.getCell(column)) != null)
                  && ((current = getCellValueAsString(currentCell)) != null) && (!current.equals(""))
                  && !currentCell.equals(value); column++);
          if (currentCell == null)
            column--; // Use last column or first?
          // we found the the key, so no need to keep iterating
          break;
        }
      }
    }
    dataMap.putAll(getColumnInfo(testDataSheet, column));
  }

  protected Map<String, String> getColumnInfo(Sheet testDataSheet, int column) {
    int lastRow = testDataSheet.getLastRowNum();
    Map<String, String> dataMap = new HashMap<String, String>();
    String currentHeader = null;
    String currentValue = null;
    Cell currentHeaderCell = null;
    Cell currentValueCell = null;
    Row currentRow = null;
    for (int row = ROW_START; ((currentRow = testDataSheet.getRow(row)) != null)
            && ((currentHeaderCell = currentRow.getCell(COL_HEADERS)) != null)
            && ((currentHeader = getCellValueAsString(currentHeaderCell)) != null)
            && ((currentValueCell = currentRow.getCell(column)) != null)
            && ((currentValue = getCellValueAsString(currentValueCell)) != null) && row <= lastRow; row++) {
      dataMap.put(currentHeader, currentValue);
    }
    return dataMap;
  }

  private String getCellValueAsString(Cell cell) {
    cell.setCellType(Cell.CELL_TYPE_STRING);
    return cell.getStringCellValue();
  }

  // New Multiple Line Loading Code Start

  public void loadMultipleLines(String dataSheetName, Map<String, String> dataMap, String key,
                                String value) {
    try {
      Workbook testDataWorkbook = WorkbookFactory.create(file);
      /** New Formula Handling code Start **/
      HSSFFormulaEvaluator.evaluateAllFormulaCells(testDataWorkbook);
      /** New Formula Handling code End **/
      loadLines(testDataWorkbook, dataSheetName, dataMap, key, value);
      loadLines(testDataWorkbook, dataSheetName, dataMap, key, value);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public void loadLines(Workbook testDataWorkbook, String dataSheetName,
                        Map<String, String> dataMap, String key, String value) {
    Sheet testDataSheet = testDataWorkbook.getSheet(dataSheetName);
    Row headers = testDataSheet.getRow(ROW_HEADERS);
    int lastRow = testDataSheet.getLastRowNum();
    Map<String, String> lastRowMap = null;
    for (int row = 1; row <= lastRow; row++) {
      lastRowMap = getRowInfo(testDataSheet, headers, row);
      if (lastRowMap != null) {
        if (key == null || (lastRowMap.containsKey(key) && lastRowMap.get(key).equals(value))) {
          dataMap.putAll(transformToMultiLineTestData(lastRowMap, row));
        }
      }
    }
  }

  private Map<String, String> transformToMultiLineTestData(Map<String, String> lastRowMap, int row) {
    Map<String, String> returnValue = new HashMap<String, String>();

    for (String key : lastRowMap.keySet()) {
      String value = lastRowMap.get(key);
      returnValue.put(key + "" + row, value);
    }
    return returnValue;
  }

  /** New Multiple Line Loading Code End **/

}

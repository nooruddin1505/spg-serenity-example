package dependencies.adapter.reporting;

import dependencies.adapter.TestRunDetails;
import dependencies.adapter.abstractstuff.ReportWriterInterface;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class XlsWriter implements ReportWriterInterface {

  private static final int SUMMARY_NAME_COLUMN = 2;
  private static final int SUMMARY_PASS_COLUMN = 3;
  private static final int SUMMARY_FAIL_COLUMN = 4;
  private static final int SUMMARY_TOTAL_COLUMN = 5;
  private static final int SUMMARY_PASS_RATE_COLUMN = 6;

  private static final int DETAIL_NAME_COLUMN = 2;
  private static final int DETAIL_TEST_COLUMN = 3;
  private static final int DETAIL_STATUS_COLUMN = 4;
  private static final int DETAIL_LINK_COLUMN = 5;

  private static final int SUMMARY_START_ROW = 2;
  private static final int DETAIL_START_ROW = 2;

  private static final String SHEET_NAME_SUMMARY = "Summary";
  private static final String SHEET_NAME_DETAIL = "Details";

  private static final String TOTALS_GROUP_NAME = "Totals";

  private HSSFWorkbook results;
  private HSSFSheet summary;
  private HSSFSheet detail;

  final String reportFilename;

  private Map<String, Integer> testGroupMappingsPass = new HashMap<String, Integer>();
  private Map<String, Integer> testGroupMappingsFail = new HashMap<String, Integer>();
  private Set<String> tests = new HashSet<String>();

  public XlsWriter(String reportFilename) {
    // Set global variables
    this.reportFilename = reportFilename;

    // Create the excel file
    createBaseXlsREportFile();
  }

  @Override
  public void appendToReport(TestRunDetails testResult) {
    String id = testResult.getId();
    String testGroup = id.substring(id.lastIndexOf(".") + 1, id.length() - 1);

    readReportFile();
    appendTestDetails(testResult, testGroup);
    appendTestToSummary(testResult, testGroup);
    autoSizeColumns();
    saveReportFile();
  }

  @Override
  public void finalise() {}

  private void appendTestToSummary(TestRunDetails testResult, String testGroup) {
    testGroupMappingsPass.clear();
    testGroupMappingsFail.clear();
    tests.clear();

    readSummary();
    appendTestToSummaryData(testResult, testGroup);
    // resetting summary
    results.removeSheetAt(results.getSheetIndex(SHEET_NAME_SUMMARY));
    summary = results.createSheet(SHEET_NAME_SUMMARY);
    results.setSheetOrder(SHEET_NAME_SUMMARY, 0);

    writeSummary();
  }

  private void writeSummary() {
    HSSFCellStyle style = getFontStyle();

    setSummaryHeader(summary.createRow(SUMMARY_START_ROW), style);

    int index = SUMMARY_START_ROW + 1, totalPassed = 0, totalFailed = 0;

    for (String testName : tests) {
      int totalTestPassed =
              (testGroupMappingsPass.get(testName) == null) ? 0 : testGroupMappingsPass.get(testName);
      int totalTestFailed =
              (testGroupMappingsFail.get(testName) == null) ? 0 : testGroupMappingsFail.get(testName);

      Row row = summary.createRow(index);
      row.createCell(SUMMARY_NAME_COLUMN).setCellValue(testName);
      row.createCell(SUMMARY_PASS_COLUMN).setCellValue(totalTestPassed);
      row.createCell(SUMMARY_FAIL_COLUMN).setCellValue(totalTestFailed);
      row.createCell(SUMMARY_TOTAL_COLUMN).setCellValue(totalTestPassed + totalTestFailed);
      totalPassed += totalTestPassed;
      totalFailed += totalTestFailed;
      int percent = ((int) ((totalTestPassed * 100.0f) / (totalTestPassed + totalTestFailed)));
      row.createCell(SUMMARY_PASS_RATE_COLUMN).setCellValue(percent + "%");
      index++;
    }


    writeSummaryTotals(index, totalPassed, totalFailed);
  }

  private HSSFCellStyle getFontStyle() {
    HSSFFont font = results.createFont();
    font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
    HSSFCellStyle style = results.createCellStyle();
    style.setFont(font);
    return style;
  }

  private void writeSummaryTotals(int rowIndex, int totalPassed, int totalFailed) {
    Row row = summary.createRow(rowIndex);
    row.createCell(SUMMARY_NAME_COLUMN).setCellValue(TOTALS_GROUP_NAME);
    row.createCell(SUMMARY_PASS_COLUMN).setCellValue(totalPassed);
    row.createCell(SUMMARY_FAIL_COLUMN).setCellValue(totalFailed);
    row.createCell(SUMMARY_TOTAL_COLUMN).setCellValue(totalPassed + totalFailed);
    int percent = ((int) ((totalPassed * 100.0f) / (totalPassed + totalFailed)));
    row.createCell(SUMMARY_PASS_RATE_COLUMN).setCellValue(percent + "%");
  }

  private void appendTestToSummaryData(TestRunDetails testResult, String testGroup) {
    tests.add(testGroup);

    if (testResult.getTestResult().equalsIgnoreCase("PASS")) {
      if (testGroupMappingsPass.containsKey(testGroup)) {
        int currentCount = testGroupMappingsPass.get(testGroup);
        testGroupMappingsPass.put(testGroup, ++currentCount);
      } else {
        testGroupMappingsPass.put(testGroup, 1);
      }
    } else {
      if (testGroupMappingsFail.containsKey(testGroup)) {
        int currentCount = testGroupMappingsFail.get(testGroup);
        testGroupMappingsFail.put(testGroup, ++currentCount);
      } else {
        testGroupMappingsFail.put(testGroup, 1);
      }
    }
  }

  private void readSummary() {
    int lastRowIndex = summary.getLastRowNum();
    if (SUMMARY_START_ROW < lastRowIndex) {
      int currentRowIndex = SUMMARY_START_ROW + 1;

      for (; currentRowIndex < lastRowIndex; currentRowIndex++) {
        HSSFRow summaryRow = summary.getRow(currentRowIndex);
        HSSFCell nameCell = summaryRow.getCell(SUMMARY_NAME_COLUMN);

        if (!nameCell.getStringCellValue().equalsIgnoreCase(TOTALS_GROUP_NAME)) {
          tests.add(nameCell.getStringCellValue());

          HSSFCell passCell = summaryRow.getCell(SUMMARY_PASS_COLUMN);
          HSSFCell failCell = summaryRow.getCell(SUMMARY_FAIL_COLUMN);

          testGroupMappingsPass.put(nameCell.getStringCellValue(),
                  (int) passCell.getNumericCellValue());
          testGroupMappingsFail.put(nameCell.getStringCellValue(),
                  (int) failCell.getNumericCellValue());

        }
      }
    }
  }

  private void readReportFile() {
    try {
      results = new HSSFWorkbook(new FileInputStream(reportFilename));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    summary = results.getSheet(SHEET_NAME_SUMMARY);
    detail = results.getSheet(SHEET_NAME_DETAIL);
  }

  private void appendTestDetails(TestRunDetails testResult, String testGroup) {
    int noOfRows = detail.getLastRowNum() + 1;
    Row newTestDetailsRow = detail.createRow(noOfRows);
    newTestDetailsRow.createCell(DETAIL_NAME_COLUMN).setCellValue(testGroup);
    newTestDetailsRow.createCell(DETAIL_TEST_COLUMN).setCellValue(testResult.getId());
    newTestDetailsRow.createCell(DETAIL_STATUS_COLUMN).setCellValue(testResult.getTestResult());
    //HSSFHyperlink fileLink = new HSSFHyperlink(HSSFHyperlink.LINK_FILE);
    // fileLink.setAddress(runId + ".txt");
    //newTestDetailsRow.createCell(DETAIL_LINK_COLUMN).setHyperlink(fileLink);
    // row.createCell(DETAIL_LINK_COLUMN).setCellValue(runId + ".txt");
    CellStyle hLinkStyle = results.createCellStyle();
    final Font hLinkFont = results.createFont();
    hLinkFont.setFontName("Ariel");
    hLinkFont.setUnderline(Font.U_SINGLE);
    hLinkFont.setColor(IndexedColors.BLUE.getIndex());
    hLinkStyle.setFont(hLinkFont);
  }

  private void saveReportFile() {
    try {
      FileOutputStream out = new FileOutputStream(new File(reportFilename));
      results.write(out);
      out.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void autoSizeColumns() {
    summary.autoSizeColumn(SUMMARY_NAME_COLUMN);
    summary.autoSizeColumn(SUMMARY_PASS_COLUMN);
    summary.autoSizeColumn(SUMMARY_FAIL_COLUMN);
    summary.autoSizeColumn(SUMMARY_TOTAL_COLUMN);

    detail.autoSizeColumn(DETAIL_NAME_COLUMN);
    detail.autoSizeColumn(DETAIL_TEST_COLUMN);
    detail.autoSizeColumn(DETAIL_STATUS_COLUMN);
    detail.autoSizeColumn(DETAIL_LINK_COLUMN);
  }

  private void createBaseXlsREportFile() {
    results = new HSSFWorkbook();
    // Create the sheets for the results
    summary = results.createSheet(SHEET_NAME_SUMMARY);
    detail = results.createSheet(SHEET_NAME_DETAIL);
    HSSFCellStyle style = getFontStyle();
    //
    CellStyle cs = results.createCellStyle();
    // Font f = results.createFont();
    // f.setBoldweight(Font.BOLDWEIGHT_BOLD);
    // cs.setFont(f);
    HSSFColor myColor2 = setColor(results, (byte) 0xE5, (byte) 0xE0, (byte) 0xEC);
    // get the palette index of that color
    short palIndex2 = myColor2.getIndex();
    cs.setFillForegroundColor(palIndex2);
    cs.setFillBackgroundColor(palIndex2);
    cs.setFillPattern(CellStyle.BIG_SPOTS);

    for (int i = 0; i < 50; i++) {
      detail.setDefaultColumnStyle(i, cs);
    }

    // Create the header cells using the header styling
    // Summary Header
    setSummaryHeader(summary.createRow(SUMMARY_START_ROW), style);

    // Detail Header
    setDetailHeader(detail.createRow(DETAIL_START_ROW), style);
    autoSizeColumns();
    saveReportFile();
  }

  private HSSFColor setColor(HSSFWorkbook workbook, byte r, byte g, byte b) {
    HSSFPalette palette = workbook.getCustomPalette();
    HSSFColor hssfColor = null;
    try {
      hssfColor = palette.findColor(r, g, b);
      if (hssfColor == null) {
        palette.setColorAtIndex(HSSFColor.LAVENDER.index, r, g, b);
        hssfColor = palette.getColor(HSSFColor.LAVENDER.index);
      }
    } catch (Exception e) {
    }

    return hssfColor;
  }

  private void setSummaryHeader(Row summaryHeaderRow, HSSFCellStyle style) {
    Cell testNameCell = summaryHeaderRow.createCell(SUMMARY_NAME_COLUMN);
    testNameCell.setCellValue("Test Name");
    testNameCell.setCellStyle(style);
    Cell passCell = summaryHeaderRow.createCell(SUMMARY_PASS_COLUMN);
    passCell.setCellValue("Pass");
    passCell.setCellStyle(style);
    Cell failCell = summaryHeaderRow.createCell(SUMMARY_FAIL_COLUMN);
    failCell.setCellValue("Fail");
    failCell.setCellStyle(style);
    Cell totalCell = summaryHeaderRow.createCell(SUMMARY_TOTAL_COLUMN);
    totalCell.setCellValue("Total");
    totalCell.setCellStyle(style);
    Cell passRateCell = summaryHeaderRow.createCell(SUMMARY_PASS_RATE_COLUMN);
    passRateCell.setCellValue("Percent");
    passRateCell.setCellStyle(style);
  }

  private void setDetailHeader(Row detailHeaderRow, HSSFCellStyle style) {
    Cell nameCell = detailHeaderRow.createCell(DETAIL_NAME_COLUMN);
    nameCell.setCellValue("Test Name");
    nameCell.setCellStyle(style);
    Cell testCell = detailHeaderRow.createCell(DETAIL_TEST_COLUMN);
    testCell.setCellValue("Test ID");
    testCell.setCellStyle(style);
    Cell statusCell = detailHeaderRow.createCell(DETAIL_STATUS_COLUMN);
    statusCell.setCellValue("Status");
    statusCell.setCellStyle(style);
    Cell logCell = detailHeaderRow.createCell(DETAIL_LINK_COLUMN);
    logCell.setCellValue("Log");
    logCell.setCellStyle(style);
  }
}

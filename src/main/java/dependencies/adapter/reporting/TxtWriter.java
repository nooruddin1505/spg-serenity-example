package dependencies.adapter.reporting;

import dependencies.adapter.abstractstuff.BaseReportGenerator;
import dependencies.adapter.TestRunDetails;
import dependencies.adapter.TraceEntry;
import dependencies.adapter.abstractstuff.ReportWriterInterface;
import dependencies.adapter.utils.DateUtils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Supporting class for the report generation component.
 *
 * @see BaseReportGenerator
 */
public class TxtWriter implements ReportWriterInterface {

  protected PrintWriter textOutput;

  public TxtWriter(String reportFilename) {
    try {
      textOutput = new PrintWriter(reportFilename);
    } catch (FileNotFoundException e) {
      throw new Error("Could not create results TXT file!");
    }
  }

  @Override
  public void appendToReport(TestRunDetails testResult) {
    textOutput
            .println("================================================================================");
    textOutput.println("Test:     " + testResult.getId());
    textOutput.println("Status:   " + testResult.getTestResult());
    textOutput.println("Started:  " + DateUtils.format(testResult.getStartTime()));
    textOutput.println("Ended:    " + DateUtils.format(testResult.getEndTime()));

    List<TraceEntry> traces = testResult.getTracer().getTraces();
    int tracesSize = traces.size();

    textOutput.println("Trace:    " + tracesSize + " step(s)");
    Integer count = 1;
    for (TraceEntry trace : traces) {
      textOutput.printf("  %" + String.valueOf(tracesSize).length() + "d) %s\r\n", count++,
              trace.toString());
    }
    textOutput.println("");
    textOutput.flush();
  }

  @Override
  public void finalise() {
    textOutput.close();
  }

}

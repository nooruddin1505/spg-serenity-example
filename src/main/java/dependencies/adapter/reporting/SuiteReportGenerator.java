package dependencies.adapter.reporting;

import dependencies.adapter.TestRunDetails;
import dependencies.adapter.abstractstuff.BaseReportGenerator;
import dependencies.adapter.abstractstuff.ReportWriterInterface;

import java.util.LinkedList;
import java.util.List;

/**
 * A report generator for test suites.
 */
public class SuiteReportGenerator implements BaseReportGenerator {

  /**
   * The description to include in the report.
   */
  private String runID;

  /**
   * The report write used to generate the report.
   */
  private ReportWriterInterface reportWriter;

  /**
   * The details of the test runs to include in the report.
   */
  private List<TestRunDetails> testResults;

  /**
   * Default constructor.
   */
  public SuiteReportGenerator() {

  }

  /**
   * Initiates a new run record with the given description.
   *
   * @param id the description of the started run
   */
  @Override
  public void startRunRecord(String id) {
    runID = id;
    testResults = new LinkedList<TestRunDetails>();
    reportWriter = ReportWriterFactory.getWriter(runID);
  }

  /**
   * Adds details of a test run to the report.
   *
   * @param currentTestResults the details of the test run to add
   */
  public void addResult(TestRunDetails currentTestResults) {
    testResults.add(currentTestResults);
  }

  /**
   * Outputs the report.
   */
  @Override
  public void outputReport() {
    for (TestRunDetails testResult : testResults) {
      reportWriter.appendToReport(testResult);
    }
    reportWriter.finalise();
  }

}

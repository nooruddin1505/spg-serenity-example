Feature: Inject at CRC
  I want to be able to directly insert into CRC
  So that the updates are same across multiple systems


  @SPG-15194
  Scenario: Perform a CRC insert of community requirement
    Given I am logged into nDelius with following data:
      | location  | crcinsert                                              |
      | fileName1 | OffenderEngagedCRCInsert_Community_Requirement_001.xls |
    And I add a new Offender to nDelius with an Event
    And I transfer the Offender number 1
    When I perform an interchange at the specified CRC with following xml file:
      | xmlInterchangeFile1 | OffenderEngagedCRCInsert_Community_Requirement_001.xml |
    Then I should see the correct response messages
    When I view event by row 1
    Then Verify correct requirements details displayed
Feature: ND Insert at CRC
  I want to be able to perform ND Insert operations
  So that the updates are same across multiple systems


  @todo @SPG-9421
  Scenario: Perform a ND Insert of registration review
    Given I am logged into nDelius with following data:
      | location  | ndinsert       |
      | fileName1 | OffenderEngagedNDInsert_RegistrationReview_003.xls |
    And I add a new Offender to nDelius with an Event
    And I transfer the Offender number 1
    When I add new registration details
    When I perform an interchange at the specified CRC with following xml file:
      | xmlInterchangeFile1  | OffenderEngagedNDInsert_RegistrationReview_003.xml        |
    Then I should see the correct response messages
    And Verify transactions sent to the correct CRC Stub and contain the expected data:
      | Registration |
      | RegistrationReview    |

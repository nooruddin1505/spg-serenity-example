Feature: ND Update at CRC
  I want to be able to perform ND Update operations
  So that the updates are same across multiple systems


  @todo @SPG-9421
  Scenario: Perform a ND Update by adding Unpaid Work
    Given I am logged into nDelius with following data:
      | location  | ndupdate       |
      | fileName1 | OffenderEngagedNDUpdate30.xls |
    And I add a new Offender to nDelius with an Event
    And I add requirement
    And I transfer the Offender number 1
    When I view event by row 1
    And update UPW details
    And Verify transactions sent to the correct CRC Stub and contain the expected data:
      | UPWDetails |

Feature: As an nDelius user
  I want to transfer an Offender to a CRC
  So that the CRC is responsible for that Offender


  @SPG-3302
  Scenario: Transfer newly created event with an event 1
    Given I am logged into nDelius
    And I have added an Offender to nDelius with following data:
      | location  | allocateoffender        |
      | fileName1 | AllocateOffender001.xls |
    And I add an Event to the newly created offender
    When I transfer the Offender number 1
    Then Verify correct transactions sent to the CRC Stub "C17" and contain the expected data:
      | AllocateOffender |


  @SPG-3302
  Scenario: Transfer a newly created offender with an event 2
    Given I am logged into nDelius with following data:
      | location  | allocateoffender        |
      | fileName1 | AllocateOffender001.xls |
    And I add a new Offender to nDelius with an Event
    When I transfer the Offender number 1
    Then Verify correct transaction AllocateOffender sent to the CRC Stub CRC17 and contain the expected data


  @SPG-3303
  Scenario: Transfer a newly created offender with diversity
    Given I am logged into nDelius with following data:
      | location  | allocateoffender        |
      | fileName1 | AllocateOffender002.xls |
    And I add a new Offender to nDelius with an Event
    And Add diversity details
    When I transfer the Offender number 1
    Then Verify transactions sent to the correct CRC Stub and contain the expected data:
      | AllocateOffender |
      | AllocateEvent    |
      | Offender         |


  @SPG-3306
  Scenario: Transfer a newly created event with address and address assessment details
    Given I am logged into nDelius with following data:
      | location  | allocateoffender        |
      | fileName1 | AllocateOffender006.xls |
    And I add a new Offender to nDelius with an Event
    And Add address details
    And Add address assessment details
    When I transfer the Offender number 1
    Then Verify transactions sent to the correct CRC Stub and contain the expected data:
      | AllocateOffender |
      | AllocateEvent    |

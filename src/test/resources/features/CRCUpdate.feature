Feature: Inject at CRC
  I want to be able to directly insert into CRC
  So that the updates are same across multiple systems


  @todo @SPG-9421
  Scenario: Perform a CRC update of registration review
    Given I am logged into nDelius with following data:
      | location  | crcupdate        |
      | fileName1 | OffenderEngagedCRCUpdate_RegistrationReview_001.xls |
    And I add a new Offender to nDelius with an Event
    And I transfer the Offender number 1
    When I add new registration details
    And I perform an interchange at the specified CRC with following xml file:
      | xmlInterchangeFile1  | OffenderEngagedCRCUpdate_RegistrationReview_001.xml        |
    Then I should see the correct response messages
    When I view registraton details by row 1
    Then Verify correct registration details displayed
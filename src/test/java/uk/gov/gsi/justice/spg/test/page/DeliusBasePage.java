package uk.gov.gsi.justice.spg.test.page;

import dependencies.adapter.abstractstuff.Assertor;
import dependencies.adapter.abstractstuff.BasePage;
import dependencies.adapter.abstractstuff.ConfigData;
import dependencies.adapter.abstractstuff.TestData;
import dependencies.adapter.driver.ExecutionContext;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import uk.gov.gsi.justice.spg.test.actions.DeliusBasePageAction;
import uk.gov.gsi.justice.spg.test.session.SessionKey;
import uk.gov.gsi.justice.spg.test.utils.OffenderNameUtils;

public class DeliusBasePage extends BasePage {
  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();

  protected Assertor assertor;

  protected ExecutionContext context;
  protected TestData testData;
  protected String offenderFirstName;
  protected String offenderLastName;

  public DeliusBasePage() {
    this.context = ExecutionContext.getInstance();
    this.assertor = context.getAssertor();

    if (testData == null)
      testData = (TestData) Serenity.getCurrentSession().get(SessionKey.testData);

    //Set firstName and Surname
    offenderFirstName = OffenderNameUtils.generateName();
    offenderLastName = OffenderNameUtils.generateName();
  }

  public void checkAndAcceptAlert() {
    WebDriver webDriver = getDriver();
    try {
      Alert alert = webDriver.switchTo().alert();
      alert.accept();
    } catch (NoAlertPresentException ex) {
      DeliusBasePageAction.reportAlertNotPresent();
    }

  }

// TODO: An issue/enhancement (#29) has raised against Virtuoso on GitHub requesting the implementation of a method which detects alerts. If/when this is implemented in Virtuoso we can remove this method. This method can also be uncoupled. -  BN

  public String getInputValuesIndex(int index) {
    return index < 1 ? "" : Integer.toString(index);
  }
}

package uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails;

import dependencies.adapter.driver.PageElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.TablePageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class UPWDetailsPage extends EventDetailsCommon implements TablePageAssertionInterface {
  private static final String TEST_DATA_PREFIX = "UPWDetails";

  @FindBy(xpath = "//*[@value='Worksheet Summary']")
  WebElement btnWorksheetSummary;
  @FindBy(xpath = "//*[@value='Adjustment']")
  WebElement btnAdjustment;
  @FindBy(xpath = "//*[@value='Update']")
  WebElement btnUpdate;

  //Dynamically find elements
  private static final String XPATH_BUTTON_UPDATE_BY_ROW = "//*[@id='UnpaidWorkDetailsForm:requirementsTable:tbody_element']/tr[%s]/td[8]/a";


  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("UPWDetails.AgreedTravelFare", "//*[@id='UnpaidWorkDetailsForm:AgreedTravelFare']"));
    add(new AssertionPair("UPWDetails.Notes",  "//*[@id='UnpaidWorkDetailsForm:Notes']"));
  }};

//  public UPWWorksheetSummaryPage clickWorksheetSummary() {
//    pilot.click(WORKSHEET_SUMMARY);
//    return new UPWWorksheetSummaryPage();
//  }
//
//  public UPWAdjustmentPage clickAdjustment() {
//    pilot.click(ADJUSTMENT);
//    return new UPWAdjustmentPage();
//  }

//  public UpdateUPWRequirementPage clickUpdateByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_BUTTON_UPDATE_BY_ROW, UPDATE_BY_ROW_DESCRIPTION, Integer.toString(row)));
//    return new UpdateUPWRequirementPage();
//  }
//
  public UpdateUPWDetailsPage clickUpdate() {
    clickOn(btnUpdate);
    return switchToPage(UpdateUPWDetailsPage.class);
  }

  @Override
  public boolean tableExists() {
    return false;
  }

  @Override
  public String getValueAtLocation(int row, int column) {
    return null;
  }

  @Override
  public String getValueAtLocation(int row, String columnName) {
    return null;
  }

  @Override
  public void assertTableRowNotExists(int rowLocation) {

  }

  @Override
  public void assertTableRowExists(int rowLocation) {

  }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "OutboundMessageDetailsPage get value by description"));

      if (expected == null || expected.isEmpty())
        continue;

      DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "OutboundMessageDetailsPage");
      assertor.assertEquals(String.format("%s value is correct. Expected: %s, Actual: %s", pair.getDataHeading(), expected, actual), expected, actual);
    }

  }

}

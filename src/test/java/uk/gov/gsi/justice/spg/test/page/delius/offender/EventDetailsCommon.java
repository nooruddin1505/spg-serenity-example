package uk.gov.gsi.justice.spg.test.page.delius.offender;

import dependencies.adapter.driver.PageElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.RequirementsListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.UPWDetailsPage;

public class EventDetailsCommon extends OffenderCommon {

  @FindBy(id="linkNavigation3KeyData")
  WebElement linkNavigation3KeyData;
  @FindBy(id="linkNavigation3CaseAllocation")
  WebElement linkNavigation3CaseAllocation;
  @FindBy(id="linkNavigation3CourtAppearance")
  WebElement linkNavigation3CourtAppearance;
  @FindBy(id="linkNavigation3SentenceComponent")
  WebElement linkRequirement;
  @FindBy(id="linkNavigation3PSSRqmnts")
  WebElement linkNavigation3PSSRqmnts;
  @FindBy(id="linkNavigation3UnpaidWork")
  WebElement linkNavigation3UnpaidWork;


  private static final String XPATH_LINK_EVENT_DETAILS = "//*[@id='linkNavigation3KeyData']";
  private static final String XPATH_LINK_CASE_ALLOCATION = "//*[@id='linkNavigation3CaseAllocation']";
  private static final String XPATH_LINK_COURT_APPEARANCES = "//*[@id='linkNavigation3CourtAppearance']";
  private static final String XPATH_LINK_REQUIREMENTS = "//*[@id='linkNavigation3SentenceComponent']";
  private static final String XPATH_LINK_SUPERVISION_REQUIREMENTS = "//*[@id='linkNavigation3PSSRqmnts']";
  private static final String XPATH_LINK_LICENCE_CONDITIONS = "//*[@id='linkNavigation3SentenceComponent']";
  private static final String XPATH_LINK_COURT_AND_INSTITUTIONAL_REPORTS = "//*[@id='linkNavigation3Reports']";
  private static final String XPATH_LINK_THROUGHCARE = "//*[@id='linkNavigation3Throughcare']";
  private static final String XPATH_LINK_ADDITIONAL_OFFENCES = "//*[@id='linkNavigation3AdditionalOffence']";
  private static final String XPATH_LINK_ADDITIONAL_SENTENCES = "//*[@id='linkNavigation3AdditionalSentence']";
  private static final String XPATH_LINK_OGRS_CALCULATION = "//*[@id='linkNavigation3OGRSCalculation']";
  private static final String XPATH_LINK_OASYS_ASSESSMENTS = "//*[@id='linkNavigation3OasysAssessments']";
  private static final String XPATH_LINK_TRANSFER_ORDER_SUPERVISOR = "//*[@id='linkNavigation3OrderTransfer']";
  private static final String XPATH_LINK_ORDER_SUPERVISOR_HISTORY = "//*[@id='linkNavigation3OrderHistory']";
  private static final String XPATH_LINK_APPROVED_PREMISES_REFERRALS = "//*[@id='linkNavigation3APReferrals']";
  private static final String XPATH_LINK_REFERRALS = "//*[@id='linkNavigation3Referrals']";
  private static final String XPATH_LINK_DRUG_TESTING_REFERRALS = "//*[@id='linkNavigation3DRRReferrals']";
  private static final String XPATH_LINK_UNPAID_WORK = "//*[@id='linkNavigation3UnpaidWork']";
  private static final String XPATH_LINK_CONTACT_EXTRACT = "//*[@id='linkNavigation3ContactExtract']";
  private static final String XPATH_LINK_NON_STATUTORY_INTERVENTIONS = "//*[@id='linkNavigation3EventNsi']";
  private static final String XPATH_LINK_COHORT = "//*[@id='linkNavigation3Cohort']";

  private static final PageElement LINK_EVENT_DETAILS = new PageElement(PageElement.Type.XPATH, XPATH_LINK_EVENT_DETAILS, "Event Details Link");
  private static final PageElement LINK_CASE_ALLOCATION =
          new PageElement(PageElement.Type.XPATH, XPATH_LINK_CASE_ALLOCATION, "Event Details Court Appearances Link");

  private static final PageElement LINK_COURT_APPEARANCES =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_COURT_APPEARANCES, "Event Details Court Appearances Link");
  private static final PageElement LINK_REQUIREMENTS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_REQUIREMENTS, "Event Details Requirements Link");
  private static final PageElement LINK_SUPERVISION_REQUIREMENTS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_SUPERVISION_REQUIREMENTS, "Event Details Supervision Requirements Link");
  private static final PageElement LINK_LICENCE_CONDITIONS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_LICENCE_CONDITIONS, "Event Details Licence Conditions Link");
  private static final PageElement LINK_COURT_AND_INSTITUTIONAL_REPORTS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_COURT_AND_INSTITUTIONAL_REPORTS, "Event Details Corut and Institutional Reports Link");
  private static final PageElement LINK_THROUGHCARE =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_THROUGHCARE, "Event Details Throughcare Link");
  private static final PageElement LINK_ADDITIONAL_OFFENCES =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_ADDITIONAL_OFFENCES, "Event Details Additional Offences Link");
  private static final PageElement LINK_ADDITIONAL_SENTENCES =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_ADDITIONAL_SENTENCES, "Event Details Additional Sentences Link");
  private static final PageElement LINK_OGRS_CALCULATION =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_OGRS_CALCULATION, "Event Details OGRS Calculation Link");
  private static final PageElement LINK_OASYS_ASSESSMENTS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_OASYS_ASSESSMENTS, "Event Details OASYS Assessments Link");
  private static final PageElement LINK_TRANSFER_ORDER_SUPERVISOR =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_TRANSFER_ORDER_SUPERVISOR, "Event Details Transfer Order Supervisor Link");
  private static final PageElement LINK_ORDER_SUPERVISOR_HISTORY =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_ORDER_SUPERVISOR_HISTORY, "Event Details Order Supervisor History Link");
  private static final PageElement LINK_APPROVED_PREMISES_REFERRALS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_APPROVED_PREMISES_REFERRALS, "Event Details Approved Premises Referrals Link");
  private static final PageElement LINK_REFERRALS = new PageElement(PageElement.Type.XPATH, XPATH_LINK_REFERRALS, "Event Details Referrals Link");
  private static final PageElement LINK_DRUG_TESTING_REFERRALS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_DRUG_TESTING_REFERRALS, "Event Details Drug Testing Referrals Link");
  private static final PageElement LINK_UNPAID_WORK =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_UNPAID_WORK, "Event Details Unpaid Work Link");
  private static final PageElement LINK_CONTACT_EXTRACT =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_CONTACT_EXTRACT, "Event Details Contact Extract Link");
  private static final PageElement LINK_NON_STATUTORY_INTERVENTIONS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_NON_STATUTORY_INTERVENTIONS, "Event Details Non-Statutory Interventions Link");
  private static final PageElement LINK_COHORT = new PageElement(PageElement.Type.XPATH, XPATH_LINK_COHORT, "Event Details Cohort Link");


//    public EventDetailsPage clickEventDetailsLink() {
//    pilot.click(LINK_EVENT_DETAILS);
//    return new EventDetailsPage();
//  }
//
//  public CaseAllocationListPage clickCaseAllocationLink() {
//    pilot.click(LINK_CASE_ALLOCATION);
//    return new CaseAllocationListPage();
//  }
//
//  public CourtAppearanceListPage clickCourtAppearancesLink() {
//    pilot.click(LINK_COURT_APPEARANCES);
//    return new CourtAppearanceListPage();
//  }
//
  public RequirementsListPage clickRequirementsLink() {
    clickOn(linkRequirement);
    return switchToPage(RequirementsListPage.class);
  }

//
//  public SupervisionRequirementsListPage clickSupervisionRequirementLink() {
//    pilot.click(LINK_SUPERVISION_REQUIREMENTS);
//    return new SupervisionRequirementsListPage();
//  }
//
//  public LicenceConditionListPage clickLicenceConditions() {
//    pilot.click(LINK_LICENCE_CONDITIONS);
//    return new LicenceConditionListPage();
//  }
//
//  public CourtAndInstitutionalReportsListPage clickCourtAndInstitutionalReportsLink() {
//    pilot.click(LINK_COURT_AND_INSTITUTIONAL_REPORTS);
//    return new CourtAndInstitutionalReportsListPage();
//  }
//
//  public ThroughcareDetailsPage clickThroughcare() {
//    pilot.click(LINK_THROUGHCARE);
//    return new ThroughcareDetailsPage();
//  }
//
//  public UpdateAdditionalOffencesPage clickAdditionalOffencesLink() {
//    pilot.click(LINK_ADDITIONAL_OFFENCES);
//    return new UpdateAdditionalOffencesPage();
//  }
//
//  public UpdateAdditionalSentencesPage clickAdditionalSentencesLink() {
//    pilot.click(LINK_ADDITIONAL_SENTENCES);
//    return new UpdateAdditionalSentencesPage();
//  }
//
//  public OGRSCalculationPage clickOGRSCalculationLink() {
//    pilot.click(LINK_OGRS_CALCULATION);
//    return new OGRSCalculationPage();
//  }
//
//  public OASysAssessmentsPage clickOASysAssessmentsLink() {
//    pilot.click(LINK_OASYS_ASSESSMENTS);
//    return new OASysAssessmentsPage();
//  }
//
//  public TransferOrderSupervisorPage clickTransferOrderSupervisorLink() {
//    pilot.click(LINK_TRANSFER_ORDER_SUPERVISOR);
//    return new TransferOrderSupervisorPage();
//  }
//
//  public TransferHistoryPage clickOrderSupervisorHistoryLink() {
//    pilot.click(LINK_ORDER_SUPERVISOR_HISTORY);
//    return new TransferHistoryPage();
//  }
//
//  public ApprovedPremisesReferralsListPage clickApprovedPremisesReferralsLink() {
//    pilot.click(LINK_APPROVED_PREMISES_REFERRALS);
//    return new ApprovedPremisesReferralsListPage();
//  }
//
//  public ReferralsListPage clickReferralsLink() {
//    pilot.click(LINK_REFERRALS);
//    return new ReferralsListPage();
//  }

  public UPWDetailsPage clickUnpaidWorkLink() {
    clickOn(linkNavigation3UnpaidWork);
    return switchToPage(UPWDetailsPage.class);
  }
//
//  public DrugTestingReferralsListPage clickDrugTestingReferralsLink() {
//    pilot.click(LINK_DRUG_TESTING_REFERRALS);
//    return new DrugTestingReferralsListPage();
//  }
//
//  public ContactPrintPreviewListPage clickContactExtractLink() {
//    pilot.click(LINK_CONTACT_EXTRACT);
//    return new ContactPrintPreviewListPage();
//  }
//
//  public NonStatutoryInterventionListPage clickNonStatutoryInterventionsLink() {
//    pilot.click(LINK_NON_STATUTORY_INTERVENTIONS);
//    return new NonStatutoryInterventionListPage();
//  }
//
//  public CohortDetailsPage clickCohortLink() {
//    pilot.click(LINK_COHORT);
//    return new CohortDetailsPage();
//  }
}

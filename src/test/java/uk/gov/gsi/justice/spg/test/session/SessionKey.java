package uk.gov.gsi.justice.spg.test.session;

/**
 * Created by InternetUser on 16/11/2017.
 */
public class SessionKey {

  public static String testName = "Current Test Name";
  public static String testDataFileLocation = "Test data location";
  public static String testDataFile = "List of data file names";
  public static String listOfTestDataFiles = "List of data file names";
  public static String mapOfDataFiles = "Map of data file names and its location in test-data folder";
  public static String testData = "Test Data for current test";
  public static String crn = "Customer Reference Number";
  public static String listOfProviders = "List of providers";
  public static String xmlInterchangeFileNames = "XML files to interchange messages with";
  public static String controlReference = "Control reference number";
  public static String crcID = "CRCID C17, C01 etc";
}

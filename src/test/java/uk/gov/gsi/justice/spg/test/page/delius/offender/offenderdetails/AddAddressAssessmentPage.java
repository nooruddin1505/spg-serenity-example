package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;

public class AddAddressAssessmentPage extends OffenderDetailsCommon implements FormPageInterface {
  private static final String TEST_DATA_PREFIX = "AddAddressAssessment";

  @FindBy(id = "assessmentForm:Trust")
  WebElementFacade selectProvider;
  @FindBy(id = "assessmentForm:Team")
  WebElementFacade selectTeam;
  @FindBy(id = "assessmentForm:Officer")
  WebElementFacade selectOfficer;
  @FindBy(id = "assessmentForm:AssessmentDate")
  WebElementFacade inputDate;
  @FindBy(id = "assessmentForm:Notes")
  WebElementFacade inputNotes;


  @FindBy(xpath = "//*[@value='Save']")
  WebElementFacade btnSave;
  @FindBy(xpath = "//*[@value='Update']")
  WebElementFacade btnUpdate;
  @FindBy(xpath = "//*[@value='Document']")
  WebElementFacade btnDocument;
  @FindBy(xpath = "//*[@value='Cancel']")
  WebElementFacade btnCancel;
  @FindBy(xpath = "//*[@value='Confirm']")
  WebElementFacade btnConfirm;

  public void selectProvider(String provider) {
    if (provider == null || provider.isEmpty())
      return;

    WaitUtils.isDropDownReadyForSelection(By.id("assessmentForm:Trust"), provider);
    selectFromDropdown(selectProvider, provider);
  }

  public void selectTeam(String team) {
    if (team == null || team.isEmpty())
      return;

    WaitUtils.isDropDownReadyForSelection(By.id("assessmentForm:Team"), team);
    selectFromDropdown(selectTeam, team);
  }

  public void selectOfficer(String officer) {
    if (officer == null || officer.isEmpty())
      return;

    WaitUtils.sleepSpecificTimeDontUseMeOverSeleniumWaits(this, 300);
    WaitUtils.isDropDownReadyForSelection(By.id("assessmentForm:Officer"), officer);
    selectFromDropdown(selectOfficer, officer);
  }

  public void inputDate(String date) {
    typeInto(inputDate, date);
  }

  public void inputNotes(String notes) {
    typeInto(inputNotes, notes);
  }

  public AddressAssessmentListPage clickSave() {
    clickOn(btnSave);
    return new AddressAssessmentListPage();
  }

  public AddressAssessmentListPage clickCancel() {
    clickOn(btnCancel);
    return new AddressAssessmentListPage();
  }

  public AddressAssessmentListPage clickConfirm() {
    clickOn(btnConfirm);
    return new AddressAssessmentListPage();
  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
		String value = getInputValuesIndex(inputValuesIndex);

    this.selectProvider(testData.getInputValue(TEST_DATA_PREFIX + value, "Provider"));
    this.selectTeam(testData.getInputValue(TEST_DATA_PREFIX + value, "Team"));
    this.selectOfficer(testData.getInputValue(TEST_DATA_PREFIX + value, "Officer"));
    this.inputDate(testData.getInputValue(TEST_DATA_PREFIX + value, "Date"));
    this.inputNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "Notes"));

    //FileUtils.saveScreenshot(this.getClass().getSimpleName());

    AddressAssessmentListPage addressAssessmentListPage = this.clickSave();

    // A warning screen may appear depending on the date inputted - user must confirm to save
    if (containsElements(By.xpath("//*[@value='Confirm']")))
      this.clickConfirm();

    return type.cast(addressAssessmentListPage);
  }
}

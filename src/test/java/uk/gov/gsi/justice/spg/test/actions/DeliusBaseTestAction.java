package uk.gov.gsi.justice.spg.test.actions;

import dependencies.adapter.abstractstuff.BaseAction;
import uk.gov.gsi.justice.spg.test.utils.cukes.ConfigUtils;

/**
 * This class defines 'Actions' for events which occur from within DeliusBaseTest or any of its child test objects.
 * <p>
 * Actions are used for outputting traces of events into the Virtuoso report. Each Action called will add a new row to the
 * report containing the given description text.
 *
 * @see BaseAction
 */
public class DeliusBaseTestAction extends BaseAction {

  /**
   * Constructor which adds the given description to the Virtuoso report.
   *
   * @param description text to display in the Virtuoso report.
   */
  public DeliusBaseTestAction(String description) {
    super(description);

    if (ConfigUtils.configData.getPropertyAsBoolean("Print.Actions", false)){
      System.out.println(description);
    }
  }

  /**
   * Should be called when a new AssertionPair is about to be checked. This allows the user to see each assertion
   * as it takes place rather than just those which fail which is the default Virtuoso behaviour.
   *
   * @param xPath the XPath field of the AssertionPair object.
   * @param dataHeading the Data Heading field of the AssertionPair object.
   * @param transactionType the type of transaction being checked.
   * @return a new action describing the event.
   */
  public static DeliusBaseTestAction reportNextAssertionPairToCheck(String xPath, String dataHeading, String transactionType) {
    return new DeliusBaseTestAction(String.format("Checking %s for data with heading %s in type %s", xPath, dataHeading, transactionType));
  }

  /**
   * Should be called when it is relevant to have the Case Reference Number be displayed in the report. This is
   * particularly useful for debugging failing tests.
   *
   * @param caseReferenceNumber the CRN to report.
   * @return a new action describing the event.
   */
  public static DeliusBaseTestAction reportCaseReferenceNumber(String caseReferenceNumber) {
    return new DeliusBaseTestAction(String.format("Case Reference Number: %s", caseReferenceNumber));
  }

  /**
   * Should be called when a new transaction message has been found at the CRC server and has been parsed. This is
   * particularly useful for debugging.
   *
   * @param transactionName the name of the type of transaction.
   * @param currentNumber the current number of that transaction type found so far when this method is called.
   * @return a new action describing the event.
   */
  public static DeliusBaseTestAction reportTransactionTypeFound(String transactionName, Integer currentNumber) {
    return new DeliusBaseTestAction(String.format("Transaction type found: %s (%s)", transactionName, currentNumber));
  }
}

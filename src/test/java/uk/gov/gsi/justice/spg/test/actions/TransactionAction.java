package uk.gov.gsi.justice.spg.test.actions;

import dependencies.adapter.abstractstuff.BaseAction;
import dependencies.adapter.driver.ExecutionContext;
import org.xml.sax.SAXException;
import uk.gov.gsi.justice.spg.test.exceptions.UnknownTransactionTypeException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * This class defines 'Actions' for events which occur from within BaseTransaction or any of its child test objects.
 * <p>
 * Actions are used for outputting traces of events into the Virtuoso report. Each Action called will add a new row to the
 * report containing the given description text.
 *
 * @see BaseAction
 */
public class TransactionAction extends BaseAction {

  /**
   * Constructor which adds the given description to the Virtuoso report.
   *
   * @param description text to display in the Virtuoso report.
   */
  public TransactionAction(String description) {
    super(description);

    if (configData.getPropertyAsBoolean("Print.Actions", false)) {
      System.out.println(description);
    }
  }

  /**
   * Should be called when a FileNotFoundException has been caught after a transaction file is expected to have been
   * downloaded from a CRC server.
   *
   * @param host the host name of the CRC server.
   * @param path the path the file should have been copied from.
   * @param searchTerm the search term used to locate the file.
   * @param localName the local name of the file once it has been copied.
   * @param e the Exception object.
   * @return a new action describing the event.
   */
  public static TransactionAction reportFileNotFoundExceptionCaught(String host, String path, String searchTerm, String localName,
                                                                    FileNotFoundException e) {
    return new TransactionAction(String.format("FileNotFoundException caught. Host: %s; Path: %s; searchTerm: %s; localName: %s; message: %s", host,
                                               path, searchTerm, localName, e.getMessage()));
  }

  /**
   * Should be called when a SAXException has been caught while trying to parse a transaction file.
   *
   * @param host the host name of the CRC server.
   * @param path the path the file should have been copied from.
   * @param searchTerm the search term used to locate the file.
   * @param localName the local name of the file once it has been copied.
   * @param e the Exception object.
   * @return a new action describing the event.
   */
  public static TransactionAction reportSAXExceptionCaught(String host, String path, String searchTerm, String localName, SAXException e) {
    return new TransactionAction(String.format("SAXException caught. Host: %s; Path: %s; searchTerm: %s; localName: %s; message: %s", host, path,
                                               searchTerm, localName, e.getMessage()));
  }

  /**
   * should be called when a ParserConfigurationException has been caught while trying to parse a transaction file.
   *
   * @param host the host name of the CRC server.
   * @param path the path the file should have been copied from.
   * @param searchTerm the search term used to locate the file.
   * @param localName the local name of the file once it has been copied.
   * @param e the Exception object.
   * @return a new action describing the event.
   */
  public static TransactionAction reportParserConfigurationExceptionCaught(String host, String path, String searchTerm, String localName,
                                                                           ParserConfigurationException e) {
    return new TransactionAction(String.format("ParserConfigurationException caught. Host: %s; Path: %s; searchTerm: %s; localName: %s; message: %s",
                                               host, path, searchTerm, localName, e.getMessage()));
  }

  /**
   * Should be called when an IOException has been caught while trying to parse a transaction file.
   *
   * @param host the host name of the CRC server.
   * @param path the path the file should have been copied from.
   * @param searchTerm the search term used to locate the file.
   * @param localName the local name of the file once it has been copied.
   * @param e the Exception object.
   * @return a new action describing the event.
   */
  public static TransactionAction reportIOExceptionCaught(String host, String path, String searchTerm, String localName, IOException e) {
    return new TransactionAction(String.format("IOException caught. Host: %s; Path: %s; searchTerm: %s; localName: %s; message: %s", host, path,
                                               searchTerm, localName, e.getMessage()));
  }

  /**
   * Should be called when an XPathExpressionException has been caught while trying to parse or operate on a transaction file.
   *
   * @param host the host name of the CRC server.
   * @param path the path the file should have been copied from.
   * @param searchTerm the search term used to locate the file.
   * @param localName the local name of the file once it has been copied.
   * @param e the Exception object.
   * @return a new action describing the event.
   */
  public static TransactionAction reportXPathExpressionExceptionCaught(String host, String path, String searchTerm, String localName,
                                                                       XPathExpressionException e) {
    return new TransactionAction(String
                                     .format("XPathExpressionException caught. Host: %s; Path: %s; searchTerm: %s; localName: %s; message: %s", host,
                                             path, searchTerm, localName, e.getMessage()));
  }

  /**
   * Should be called when a transaction has been copied from the CRC server but the type doesn't match any of the sub-classes of BaseTransaction.
   *
   * @param localPath the path of the transaction file of unknown type.
   * @param e the Exception object.
   * @return a new action describing the event.
   */
  public static TransactionAction reportUnknownTransactionTypeExceptionCaught(String localPath, UnknownTransactionTypeException e) {
    return new TransactionAction(
        String.format("UnknownTransactionTypeException caught. Local transaction file: %s; message: %s", localPath, e.getMessage()));
  }
}

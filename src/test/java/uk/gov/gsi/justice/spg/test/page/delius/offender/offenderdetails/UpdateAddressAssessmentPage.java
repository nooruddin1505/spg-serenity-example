package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import net.serenitybdd.core.Serenity;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;

public class UpdateAddressAssessmentPage extends OffenderDetailsCommon implements FormPageInterface {
  private static final String TEST_DATA_PREFIX = "UpdateAddressAssessment";

  @FindBy(id = "assessmentForm:Trust")
  WebElement selectProvider;
  @FindBy(xpath = "assessmentForm:Team")
  WebElement selectTeam;
  @FindBy(xpath = "assessmentForm:Officer")
  WebElement selectOfficer;

  @FindBy(xpath = "assessmentForm:AssessmentDate")
  WebElement inputDate;
  @FindBy(xpath = "assessmentForm:Notes")
  WebElement inputNotes;

  @FindBy(xpath = "//*[@value='Save']")
  WebElement btnSave;
  @FindBy(xpath = "//*[@value='Document']")
  WebElement btnDocument;
  @FindBy(xpath = "//*[@value='Cancel']")
  WebElement btnCancel;
  @FindBy(xpath = "//*[@value='Confirm']")
  WebElement btnConfirm;

  //Dynamic
  private static final String XPATH_TEMPLATE_TEAM_OPTION = "//*[@id='assessmentForm:Team']/option[contains(.,'%s')]";
  private static final String XPATH_TEMPLATE_OFFICER_OPTION = "//*[@id='assessmentForm:Officer']/option[contains(.,'%s')]";


  private static final String XPATH_TEMPLATE_TEAM_DESCRIPTION = "Add AddressTransaction Assessment Team Dropdown Option";
  private static final String XPATH_TEMPLATE_OFFICER_DESCRIPTION = "Add AddressTransaction Assignment Officer Dropdown Option";

  // There are no required fields as this is an update page therefore null/empty logic is required for each field
  public void selectProvider(String provider) {
    if (provider == null || provider.isEmpty())
      return;

    selectFromDropdown(selectProvider, provider);
  }

  public void selectTeam(String team) {
    if (team == null || team.isEmpty())
      return;

    WaitUtils.isDropDownReadyForSelection(By.xpath("//*[@id='assessmentForm:Team']"), team);
    selectFromDropdown(selectTeam, team);
  }

  public void selectOfficer(String officer) {
    if (officer == null || officer.isEmpty())
      return;

    WaitUtils.isDropDownReadyForSelection(By.xpath("//*[@id='assessmentForm:Officer']"), officer);
    selectFromDropdown(selectTeam, officer);
  }

  public void inputDate(String date) {
    if (date == null || date.isEmpty())
      return;

    typeInto(inputDate, date);
  }

  public void inputNotes(String notes) {
    if (notes == null || notes.isEmpty())
      return;

    typeInto(inputNotes, notes);
  }

  public AddressAssessmentListPage clickSave() {
    clickOn(btnSave);
    return switchToPage(AddressAssessmentListPage.class);
  }

  public void clickDocument() {
    clickOn(btnDocument);
  }

  public AddressAssessmentListPage clickCancel() {
    clickOn(btnCancel);
    return switchToPage(AddressAssessmentListPage.class);
  }

  public AddressAssessmentListPage clickConfirm() {
    clickOn(btnConfirm);
    return switchToPage(AddressAssessmentListPage.class);
  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
		String value = getInputValuesIndex(inputValuesIndex);

    this.selectProvider(testData.getInputValue(TEST_DATA_PREFIX + value, "Provider"));
    this.selectTeam(testData.getInputValue(TEST_DATA_PREFIX + value, "Team"));
    this.selectOfficer(testData.getInputValue(TEST_DATA_PREFIX + value, "Officer"));
    this.inputDate(testData.getInputValue(TEST_DATA_PREFIX + value, "Date"));
    this.inputNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "Notes"));

    //FileUtils.saveScreenshot(this.getClass().getSimpleName());
    Serenity.takeScreenshot();

    AddressAssessmentListPage addressAssessmentListPage = this.clickSave();

    // A warning screen may appear depending on the date inputted - user must confirm to save
    if (containsElements(By.xpath("//*[@value='Confirm']")))
      this.clickConfirm();

    return type.cast(addressAssessmentListPage);
  }
}

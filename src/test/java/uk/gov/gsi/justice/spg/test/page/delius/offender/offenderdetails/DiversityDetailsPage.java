package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.DetailsPageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class DiversityDetailsPage extends OffenderDetailsCommon implements DetailsPageAssertionInterface {

  @FindBy(id="diversityForm:updateDiversity")
  WebElementFacade btnUpdateDiversity;
  @FindBy(id="diversityForm:goToOffenderDetails")
  WebElementFacade btnClose;

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("EqualityMonitoring.SexualOrientation", "//label[contains(text(),'Sexual Orientation:')]/../div/span"));
    add(new AssertionPair("EqualityMonitoring.GenderReassignment", "//label[contains(text(),'Gender Reassignment:')]/../div/span"));
    add(new AssertionPair("EqualityMonitoring.ConsentToDisclose", "//label[contains(text(),'Consent To Disclose:')]/../div"));
    add(new AssertionPair("EqualityMonitoring.InterpreterRequired", "//label[contains(text(),'Interpreter Required:')]/../div"));
    add(new AssertionPair("EqualityMonitoring.ImmigrationNumber", "//*[@id='diversityForm:immigrationNumber']"));
  }};

  public UpdateDiversityDetailsPage clickButtonUpdate() {
    clickOn(btnUpdateDiversity);
    return switchToPage(UpdateDiversityDetailsPage.class);
  }

  public OffenderDetailsPage clickButtonClose() {
    clickOn(btnClose);
    return switchToPage(OffenderDetailsPage.class);
  }

  @Override
  public String getValueByLabel(String label) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public String getLabelByValue(String value) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean labelExists(String label) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean valueExists(String value) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "OffenderDetailsPage get value by description"));

      if (expected == null || expected.isEmpty())
        actual = expected;

      DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "OffenderDetailsPage");

      assertor.assertEquals(String.format("%s is correct. Expected: '%s', Actual: '%s'", pair.getDataHeading(), expected, actual), expected, actual);
    }
  }
}

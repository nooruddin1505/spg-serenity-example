package uk.gov.gsi.justice.spg.test.page;

import dependencies.adapter.BaseElement;
import dependencies.adapter.BaseTracer;
import dependencies.adapter.actions.ReportLinkAction;
import dependencies.adapter.driver.ExecutionContext;
import dependencies.adapter.driver.PageElement;

public class DeliusCommon extends DeliusBasePage {
  private static final String XPATH_NAV_HOME_LINK = "//*[@id='linkNavigation1Home']";
  private static final String XPATH_NAV_NATIONAL_SEARCH_LINK = "//*[@id='linkNavigation1Search']";
  private static final String XPATH_NAV_RECENTLY_VIEWED_LINK = "//*[@id='linkNavigation1RecentlyViewedOffenders']";
  private static final String XPATH_NAV_CASE_MANAGEMENT_LINK = "//*[@id='linkNavigation1CaseManagement']/a";
  private static final String XPATH_NAV_OFFICER_DIARY_LINK = "//*[@id='linkNavigation1OfficerDiary']";
  private static final String XPATH_NAV_COURT_DIARY_LINK = "//*[@id='linkNavigation1CourtDiary']";
  private static final String XPATH_NAV_APPROVED_PREMISES_DIARY_LINK = "//*[@id='linkNavigation1HostelDiary']";
  private static final String XPATH_NAV_UPW_PROJECT_DIARY_LINK = "//*[@id='linkNavigation1UnpaidWorkProjectDiary']/a";
  private static final String XPATH_NAV_UPW_SEARCH_LINK = "//*[@id='linkNavigation1UnpaidWorkSearch']/a";
  private static final String XPATH_NAV_UPW_PROJECTS_LINK = "//*[@id='linkNavigation1UnpaidWorkProjectList']/a";
  private static final String XPATH_NAV_DATA_MAINTENANCE_LINK = "//*[@id='linkNavigation1UnpaidWorkProjectList']/a";
  private static final String XPATH_NAV_REFERENCE_DATA_LINK = "//*[@id='linkNavigation1ReferenceData']";
  private static final String XPATH_NAV_USER_ADMINISTRATION_LINK = "//*[@id='linkNavigation1UserAdministration']/a";
  private static final String XPATH_NAV_USER_PREFERENCES_LINK = "//*[@id='linkNavigation1Preferences']";
  private static final String XPATH_NAV_SPG_ADMINISTRATION = "//*[@id='linkNavigation1SPGAdministration']";
  private static final String XPATH_NAV_EXIT_LINK = "//*[@id='linkNavigation1Exit']";

  private static final PageElement NAV_HOME_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_HOME_LINK, "NDelius Page Navigation Pane Home Link");
  private static final PageElement NAV_NATIONAL_SEARCH_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_NATIONAL_SEARCH_LINK, "NDelius Page Navigation Pane National Search Link");
  private static final PageElement NAV_RECENTLY_VIEWED_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_RECENTLY_VIEWED_LINK, "NDelius Page Navigation Pane Recently Viewed Link");
  private static final PageElement NAV_CASE_MANAGEMENT_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_CASE_MANAGEMENT_LINK, "NDelius Page Navigation Pane Case Management Link");
  private static final PageElement NAV_OFFICER_DIARY_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_OFFICER_DIARY_LINK, "NDelius Page Navigation Pane Officer Diary Link");
  private static final PageElement NAV_COURT_DIARY_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_COURT_DIARY_LINK, "NDelius Page Navigation Pane Court Diary Link");
  private static final PageElement NAV_APPROVED_PREMISES_DIARY_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_APPROVED_PREMISES_DIARY_LINK, "NDelius Page Navigation Pane Approved Premises Diary Link");
  private static final PageElement NAV_UPW_PROJECT_DIARY_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_UPW_PROJECT_DIARY_LINK, "NDelius Page Navigation Pane Project Diary Link");
  private static final PageElement NAV_UPW_SEARCH_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_UPW_SEARCH_LINK, "NDelius Page Navigation Pane UPW Search Link");
  private static final PageElement NAV_UPW_PROJECTS_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_UPW_PROJECTS_LINK, "NDelius Page Navigation Pane UPW Projects Link");
  private static final PageElement NAV_DATA_MAINTENANCE_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_DATA_MAINTENANCE_LINK, "NDelius Page Navigation Pane Data Maintenance Link");
  private static final PageElement NAV_REFERENCE_DATA_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_REFERENCE_DATA_LINK, "NDelius Page Navigation Pane Reference Data Link");
  private static final PageElement NAV_USER_ADMINISTRATION_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_USER_ADMINISTRATION_LINK, "NDelius Page Navigation Pane User Administration Link");
  private static final PageElement NAV_USER_PREFERENCES_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_USER_PREFERENCES_LINK, "NDelius Page Navigation Pane User Preferences Link");
  private static final PageElement NAV_SPG_ADMINISTRATION_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_SPG_ADMINISTRATION, "NDelius Page Navigation Pane SPG Administration Link");
  private static final PageElement NAV_EXIT_LINK =
          new PageElement(PageElement.Type.XPATH, XPATH_NAV_EXIT_LINK, "NDelius Page Navigation Pane Exit Link");


  public HomePage clickHome() {
    pilot.click(NAV_HOME_LINK);
    return new HomePage();
  }
//
//  public NationalSearchPage clickNationalSearch() {
//    pilot.click(NAV_NATIONAL_SEARCH_LINK);
//    return new NationalSearchPage();
//  }
//
//  public RecentlyViewedPage clickRecentlyViewed() {
//    pilot.click(NAV_RECENTLY_VIEWED_LINK);
//    return new RecentlyViewedPage();
//  }
//
//  public OffenderSummaryPage clickCaseManagement() {
//    pilot.click(NAV_CASE_MANAGEMENT_LINK);
//    return new OffenderSummaryPage();
//  }
//
//  public CaseLoadDiaryPage clickOfficerDiary() {
//    pilot.click(NAV_OFFICER_DIARY_LINK);
//    return new CaseLoadDiaryPage();
//  }
//
//  public CourtDiaryPage clickCourtDiary() {
//    pilot.click(NAV_COURT_DIARY_LINK);
//    return new CourtDiaryPage();
//  }
//
//  public CurrentResidentsPage clickApprovedPremisesDiary() {
//    pilot.click(NAV_APPROVED_PREMISES_DIARY_LINK);
//    return new CurrentResidentsPage();
//  }
//
//  public UPWProjectDiaryPage clickUPWProjectDiary() {
//    pilot.click(NAV_UPW_PROJECT_DIARY_LINK);
//    return new UPWProjectDiaryPage();
//  }
//
//  public UPWSearchPage clickUPWSearch() {
//    pilot.click(NAV_UPW_SEARCH_LINK);
//    return new UPWSearchPage();
//  }
//
//  public UPWProjectsListPage clickUPWProjects() {
//    pilot.click(NAV_UPW_PROJECTS_LINK);
//    return new UPWProjectsListPage();
//  }
//
//  public DataMaintenancePage clickDataMaintenance() {
//    pilot.click(NAV_DATA_MAINTENANCE_LINK);
//    return new DataMaintenancePage();
//  }
//
//  public ReferenceDataPage clickReferenceData() {
//    pilot.click(NAV_REFERENCE_DATA_LINK);
//    return new ReferenceDataPage();
//  }
//
//  public FindUserPage clickUserAdministration() {
//    pilot.click(NAV_USER_ADMINISTRATION_LINK);
//    return new FindUserPage();
//  }
//
//  public UserPreferencesPage clickUserPreferences() {
//    pilot.click(NAV_USER_PREFERENCES_LINK);
//    return new UserPreferencesPage();
//  }
//
//  public SPGAdministrationPage clickSPGAdministration() {
//    pilot.click(NAV_SPG_ADMINISTRATION_LINK);
//    return new SPGAdministrationPage();
//  }

  public void clickExit() {
    pilot.click(NAV_EXIT_LINK);
  }

  public void printOut(String display) {
    System.out.println(display);
    BaseTracer tracer = null;
    tracer = ExecutionContext.getInstance().getTracer();
    ReportLinkAction myReportLinkAction = new ReportLinkAction("PRINT Message");
    tracer.trace(myReportLinkAction, new BaseElement(display));
  }

}

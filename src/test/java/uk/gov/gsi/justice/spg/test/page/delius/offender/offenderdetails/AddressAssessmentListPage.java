package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import dependencies.adapter.utils.FileUtils;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.interfaces.TablePageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class AddressAssessmentListPage extends OffenderDetailsCommon implements FormPageInterface, TablePageAssertionInterface {
  private static final String TEST_DATA_PREFIX = "";


  @FindBy(id = "addressDetailsForm:j_id_id181")
  WebElementFacade btnAddAddressAssessment;
  @FindBy(xpath = "//*[@value='Close']")
  WebElementFacade btnClose;
  @FindBy(xpath = "//*[@id='assessmentListForm:addressAssessmentTable:tbody_element']/tr/td[6]/a")
  WebElementFacade linkFirstAssessmentUpdate;
  @FindBy(xpath = "//*[@id='assessmentListForm:addressAssessmentTable:tbody_element']/tr/td[5]/a")
  WebElementFacade linkFirstAssessmentView;

  //Dynamic lookup
  private static final String XPATH_LINK_DELETE_ASSESSMENT_BY_ROW = "//*[@id=\"assessmentListForm:addressAssessmentTable:tbody_element\"]/tr[%s]/td[7]/a";


  public AddAddressAssessmentPage clickAddAddressAssessmentButton() {
    clickOn(btnAddAddressAssessment);
    return switchToPage(AddAddressAssessmentPage.class);
  }

  private static final String XPATH_ADDESS_ASSESSMENT_LIST_TABLE = "//table";

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("AddressAssessment.Date", XPATH_ADDESS_ASSESSMENT_LIST_TABLE + "/tbody/tr%s/td[1]"));
    add(new AssertionPair("AddressAssessment.Team", XPATH_ADDESS_ASSESSMENT_LIST_TABLE + "/tbody/tr%s/td[2]"));
    add(new AssertionPair("AddressAssessment.Officer", XPATH_ADDESS_ASSESSMENT_LIST_TABLE + "/tbody/tr%s/td[3]"));
    add(new AssertionPair("AddressAssessment.Notes", XPATH_ADDESS_ASSESSMENT_LIST_TABLE + "/tbody/tr%s/td[4]"));
  }};

  public OffenderDetailsPage clickCloseButton() {
    clickOn(btnClose);
    return switchToPage(OffenderDetailsPage.class);
  }

//  public AddressAssessmentDetailsPage clickViewAssessmentByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_LINK_FIRST_ASSESSMENT_VIEW, "AddAddressAssessmentPage view address assessment by row", row));
//    return new AddressAssessmentDetailsPage();
//  }

//  public UpdateAddressAssessmentPage clickFirstAssessmentUpdateLink() {
//    pilot.click(LINK_FIRST_ASSESSMENT_UPDATE);
//    return new UpdateAddressAssessmentPage();
//  }
//
//  public DeleteAddressAssessmentPage clickDeleteAssessmentByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_LINK_DELETE_ASSESSMENT_BY_ROW, "AddAddressAssessmentPage delete address assessment by row", row));
//    return new DeleteAddressAssessmentPage();
//  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
    String value = getInputValuesIndex(inputValuesIndex);

    FileUtils.saveScreenshot(this.getClass().getSimpleName());

    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean tableExists() {
    return pilot.elementExists(PageElement.byXpath(XPATH_ADDESS_ASSESSMENT_LIST_TABLE, "AddressAssessmentPage table"));
  }

  @Override
  public void assertTableRowNotExists(int rowLocation) {
    assertor.assertFalse("Results table does not exist on this page suggesting no Registration Summary Entries exist.", tableExists());

    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      PageElement targetElement = new PageElement(PageElement.Type.XPATH, pair.getXPath(rowLocation), "Asserting nothing exists at following xPath: " + pair.getXPath(), rowLocation);

      boolean elementExists = pilot.elementExists(targetElement);

      // If there's something at the rowLocation, need to check whether it's the row we're expecting not to see.
      if (elementExists) {
        String actual = pilot.getText(targetElement);
        assertor.assertNotEquals(String.format("Expected value at row %s does not exist. Expected: '%s', actual: '%s'", rowLocation, expected, actual), expected, actual);
      } else {
        assertor.assertFalse(String.format("Value at row '%s' is null, as expected.", rowLocation), elementExists);
      }
    }
  }

  @Override
  public void assertTableRowExists(int rowLocation) {
    assertor.assertTrue("Results table exists.", tableExists());

    for (AssertionPair pair : ASSERTION_PAIRS) {
      PageElement targetElement = new PageElement(PageElement.Type.XPATH, pair.getXPath(rowLocation), "Asserting correct value at following xPath: " + pair.getXPath());
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = pilot.getText(targetElement);

      DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(rowLocation), pair.getDataHeading(), "RegistrationListPage");

      assertor.assertEquals(String.format("Expected value at row %s exists. Expected: '%s', actual: '%s'", rowLocation, expected, actual), expected, actual);
    }
  }

  @Override
  public String getValueAtLocation(int row, int column) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public String getValueAtLocation(int row, String columnName) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "AddressAssessmentListPage");
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "AddressAssessmentListPage get value by description"));

      assertor.assertEquals(String.format("Value is correct. Expected: %s, Actual: %s", expected, actual), expected, actual);
    }
  }
}


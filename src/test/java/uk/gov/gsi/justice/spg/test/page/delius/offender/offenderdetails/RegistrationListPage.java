package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.TablePageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class RegistrationListPage extends OffenderDetailsCommon implements TablePageAssertionInterface {

  @FindBy(xpath = "//*[@value='Add Registration']")
  WebElement btnAddRegistration;
  @FindBy(xpath = "//*[@id='registrationListForm:IncludeInactive']")
  WebElement selectShowInactiveRegistration;
  @FindBy(xpath = "//*[@value='Refresh']")
  WebElement btnRefresh;

  //Dynamic lookup
  private static final String XPATH_UPDATE_BY_ROW = "//table/tbody/tr[%s]/td[7]/a";
  private static final String XPATH_VIEW_BY_ROW = "//table/tbody/tr[%s]/td[8]/a";
  private static final String XPATH_DELETE_BY_ROW = "//table/tbody/tr[%s]/td[9]/a";
  private static final String XPATH_DEREGISTER_BY_ROW = "//table/tbody/tr[%s]/td[10]/a";

  private static final String XPATH_TABLE = "//table";

  private static final String XPATH_REGISTRATION_LIST_TABLE = "//table";

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("RegistrationList.Type", XPATH_REGISTRATION_LIST_TABLE + "/tbody/tr%s/td[3]"));
    add(new AssertionPair("RegistrationList.Date", XPATH_REGISTRATION_LIST_TABLE + "/tbody/tr%s/td[4]"));
    add(new AssertionPair("RegistrationList.NextReview", XPATH_REGISTRATION_LIST_TABLE + "/tbody/tr%s/td[5]"));

  }};

  public AddRegistrationPage clickAddRegistration() {
    clickOn(btnAddRegistration);
    return switchToPage(AddRegistrationPage.class);
  }

//  public UpdateRegistrationPage clickUpdateByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_UPDATE_BY_ROW, "RegistrationListPage update by row", row));
//    return new UpdateRegistrationPage();
//  }
//
  public RegistrationDetailsPage clickViewByRow(int row) {
    By viewByRow = By.xpath(String.format(XPATH_VIEW_BY_ROW, row));
    clickOn(find(viewByRow));
    return switchToPage(RegistrationDetailsPage.class);
  }
//
//  public DeleteRegistrationPage clickDeleteByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_DELETE_BY_ROW, "RegistrationListPage delete by row", row));
//    return new DeleteRegistrationPage();
//  }
//
//  public AddDeregistrationPage clickDeregisterByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_DEREGISTER_BY_ROW, "RegistrationListPage deregister by row", row));
//    return new AddDeregistrationPage();
//  }

  public void ShowInactiveRegistrations(String choice) {
    //pilot.selectFromVisibleText(PageElement.byXpath(XPATH_SHOW_INACTIVE_REGISTRATIONS, "RegistrationListPage show inactive Registrations"), choice);
    selectFromDropdown(selectShowInactiveRegistration, choice);
    clickOn(btnRefresh);
  }

  @Override
  public boolean tableExists() {
    return pilot.elementExists(PageElement.byXpath(XPATH_TABLE, "RegistrationListPage table"));
  }

  @Override
  public void assertTableRowNotExists(int rowLocation) {
    assertor.assertFalse("Results table does not exist on this page suggesting no Registration Summary Entries exist.", tableExists());

    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      PageElement targetElement = new PageElement(PageElement.Type.XPATH, pair.getXPath(rowLocation), "Asserting nothing exists at following xPath: " + pair.getXPath(), rowLocation);

      boolean elementExists = pilot.elementExists(targetElement);

      // If there's something at the rowLocation, need to check whether it's the row we're expecting not to see.
      if (elementExists) {
        String actual = pilot.getText(targetElement);
        assertor.assertNotEquals(String.format("Expected value at row %s does not exist. Expected: '%s', actual: '%s'", rowLocation, expected, actual), expected, actual);
      } else {
        assertor.assertFalse(String.format("Value at row '%s' is null, as expected.", rowLocation), elementExists);
      }
    }
  }

  @Override
  public void assertTableRowExists(int rowLocation) {
    assertor.assertTrue("Results table exists.", tableExists());

    for (AssertionPair pair : ASSERTION_PAIRS) {
      PageElement targetElement = new PageElement(PageElement.Type.XPATH, pair.getXPath(rowLocation), "Asserting correct value at following xPath: " + pair.getXPath());
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = pilot.getText(targetElement);

      DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(rowLocation), pair.getDataHeading(), "RegistrationListPage");

      assertor.assertEquals(String.format("Expected value at row %s exists. Expected: '%s', actual: '%s'", rowLocation, expected, actual), expected, actual);
    }
  }

  @Override
  public String getValueAtLocation(int row, int column) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public String getValueAtLocation(int row, String columnName) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public void assertPageValues() { throw new UnsupportedOperationException("Not implemented, yet"); }
}

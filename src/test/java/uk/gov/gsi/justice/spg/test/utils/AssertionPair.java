package uk.gov.gsi.justice.spg.test.utils;

/**
 * This class acts as a link between data items in the test data and data items in the transactions obtained during the test. The idea is
 * that each transaction type will have an assertion pair associated with it for each element which can be present in that transaction.
 */
public class AssertionPair {
  private String XPath;
  private String dataHeading;
  private int[] element = null;

  /**
   * Constructor for AssertionPair.
   *
   * @param dataHeading the name of the column heading in the test data spreadsheet.
   * @param xPath the XPath to the data item in the XML transaction.
   * @param element a list of any element indices to check within the XML transaction when performing assertions using this AssertionPair.
   */
  public AssertionPair(String dataHeading, String xPath, int... element) {
    this.dataHeading = dataHeading;
    this.XPath = xPath;
    if (element.length > 0) {
      this.element = element;
    }
  }

  /**
   * Getter for XPath. Any '%s' in the XPath will be replaced with an empty string.
   *
   * @return the XPath for this AssertionPair.
   */
  public String getXPath() {
    return this.XPath.replace("%s", ""); // Ensures %s characters are removed from xPath if no parameters are passed in.
  }

  /**
   * Getter for XPath. Any '%s' in the XPath will be replaced with the given element number.
   *
   * @param element the nth element to check.
   * @return the XPath for this AssertionPair.
   */
  public String getXPath(int element) {
    return String.format(this.XPath, "[" + element + "]");
  }

  /**
   * Getter for the data heading.
   *
   * @return the data heading for this AssertionPair.
   */
  public String getDataHeading() {
    return dataHeading;
  }

  /**
   * Getter for data heading with transaction and element indexes replacing first and second '%s' respectively.
   *
   * @param transactionParam the nth transaction to check.
   * @param elementParam the nth element to check.
   * @return the data heading for this AssertionPair.
   */
  public String getDataHeading(String transactionParam, String elementParam) {
    return String.format(getDataHeading(), transactionParam, elementParam);
  }

  /**
   * Getter for the array of element indices.
   *
   * @return the array of element indecies for this AssertionPair.
   */
  public int[] getElement() {
    return this.element;
  }
}

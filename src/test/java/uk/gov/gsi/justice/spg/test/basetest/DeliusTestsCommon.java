package uk.gov.gsi.justice.spg.test.basetest;

import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.page.HomePage;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.OffenderDetailsPage;

/**
 * Class for implementation of methods common to the vast majority, if not all, of the tests within this project.
 */
public class DeliusTestsCommon extends DeliusEvents {

  /**
   * The first step in every test is always to 1) create an offender, 2) add an event to that offender's record. This
   * method initialises the test by performing those two initial features.steps.
   *
   * @return the last page shown after the event is added.
   */
  protected EventDetailsCommon initTest() {

    HomePage homePage = login();

    OffenderDetailsPage offenderDetailsPage = addOffender(homePage);

    return addEvent(offenderDetailsPage);
  }

  /**
   * Given a common Delius page (which isn't a home page) this method will obtain the CRN number for the
   * selected offender and return it.
   *
   * @param offenderCommon a page on which the CRN is visible.
   * @return the CRN number for the selected offender.
   */
  protected String getCRN(OffenderCommon offenderCommon) {
    String caseReferenceNumber = offenderCommon.getOffenderCRN();

    DeliusBaseTestAction.reportCaseReferenceNumber(caseReferenceNumber);

    return caseReferenceNumber;
  }

  protected String getOffenderProvider(OffenderCommon offenderCommon) {
    return offenderCommon.getOffenderProvider();
  }

  protected String getEventProvider(OffenderCommon offenderCommon) {
    return offenderCommon.getEventProvider();
  }


}

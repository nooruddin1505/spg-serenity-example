package uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails;

import dependencies.adapter.driver.PageElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;

public class UpdateUPWDetailsPage extends EventDetailsCommon implements FormPageInterface {
  private static final String TEST_DATA_PREFIX = "UPWDetails";

  @FindBy(xpath = "//*[@value='Update']")
  WebElement btnUpdate;
  @FindBy(id = "UnpaidWorkDetailsForm:AgreedTravelFareField")
  WebElement inputTravelFare;
  @FindBy(id = "UnpaidWorkDetailsForm:NewNotes")
  WebElement inputNotes;
  @FindBy(xpath = "//*[@value='Save']")
  WebElement btnSave;

//  public AddUPWAppointmentPage clickUpdate() {
//    pilot.click(UPDATE);
//    return new AddUPWAppointmentPage();
//  }

  public UPWDetailsPage clickSave() {
    clickOn(btnSave);
    return switchToPage(UPWDetailsPage.class);
  }

  public void inputTravelFare(String travelFare) {
    if(travelFare==null || travelFare.isEmpty())
      return;
    typeInto(inputTravelFare, travelFare);
  }

  public void inputNotes(String notes) {
    if(notes==null || notes.isEmpty())
      return;
    typeInto(inputNotes, notes);
  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
    String value = getInputValuesIndex(inputValuesIndex);

    this.inputTravelFare(testData.getInputValue(TEST_DATA_PREFIX + value, "AgreedTravelFare"));
    this.inputNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "Notes"));

    //FileUtils.saveScreenshot(this.getClass().getSimpleName());

    return type.cast(this.clickSave());
  }
}

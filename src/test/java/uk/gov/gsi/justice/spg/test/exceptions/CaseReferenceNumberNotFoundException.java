package uk.gov.gsi.justice.spg.test.exceptions;

public class CaseReferenceNumberNotFoundException extends Exception{

  public CaseReferenceNumberNotFoundException(String description) {
    super(description);
  }
}


package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;


public class AddRegistrationPage extends OffenderCommon implements FormPageInterface {
    private static final String TEST_DATA_PREFIX = "AddRegistration";

    @FindBy(id="addRegistrationForm:RegisterType")
    WebElement selectRegistrationType;
    @FindBy(id="RegistrationDate")
    WebElement txtRegistrationDate;
    @FindBy(id="addRegistrationForm:Trust")
    WebElement selectProvider;
    @FindBy(id="addRegistrationForm:Team")
    WebElement selectOfficerTeam;
    @FindBy(id="addRegistrationForm:Level")
    WebElement selectLevel;
    @FindBy(id="addRegistrationForm:Category")
    WebElement selectCategory;
    @FindBy(id="addRegistrationForm:Staff")
    WebElement selectRegistrationOfficer;

    @FindBy(id="addRegistrationForm:Notes")
    WebElement txtRegistrationNotes;

    @FindBy(id="addRegistrationForm:j_id_id132")
    WebElement btnSave;
    @FindBy(xpath="//*[@value='Close']")
    WebElement btnClose;
    @FindBy(id="ReviewPeriod")
    WebElement txtReviewPeriod;
    @FindBy(id="NextReviewDate")
    WebElement txtReviewDate;

    //Required Field
    public void selectProvider(String provider) {  // Required field
        if (provider == null || provider.isEmpty())
            return;
        WaitUtils.isDropDownReadyForSelection(By.id("addRegistrationForm:Trust"), provider);
        selectFromDropdown(selectProvider, provider);
    }

    //Required Field
    public void selectRegisterType(String registerType) {  // Required field
        if (registerType == null || registerType.isEmpty())
            return;
        WaitUtils.isDropDownReadyForSelection(By.id("addRegistrationForm:RegisterType"), registerType);
        selectFromDropdown(selectRegistrationType, registerType);
    }

    //Required Field
    public void inputRegistrationDate(String registrationDate) {  // Required field
        if (registrationDate == null || registrationDate.isEmpty())
            return;
        clickOn(txtRegistrationDate);
        typeInto(txtRegistrationDate, registrationDate);
    }

    //Required Field
    public void selectTeam(String team) {  // Required field
        if (team == null || team.isEmpty())
            return;
        WaitUtils.isDropDownReadyForSelection(By.id("addRegistrationForm:Team"), team);
        selectFromDropdown(selectOfficerTeam, team);
    }

    //Required Field
    public void selectOfficer(String officer) {  // Required field
        if (officer == null || officer.isEmpty())
            return;
        WaitUtils.isDropDownReadyForSelection(By.id("addRegistrationForm:Staff"), officer);
        selectFromDropdown(selectRegistrationOfficer, officer);
    }

    public void selectLevel(String level) {
        if (level == null || level.isEmpty())
            return;

        WaitUtils.isDropDownReadyForSelection(By.id("addRegistrationForm:Level"), level);
        selectFromDropdown(selectLevel, level);
    }

    public void selectCategoryType(String categoryType) {
        if (categoryType == null || categoryType.isEmpty())
            return;

        WaitUtils.isDropDownReadyForSelection(By.id("addRegistrationForm:Category"), categoryType);
        selectFromDropdown(selectCategory, categoryType);
    }

    public void inputNotes(String registrationNotes) {
        if (registrationNotes == null || registrationNotes.isEmpty())
            return;

        typeInto(txtRegistrationNotes, registrationNotes);
    }

    public void inputReviewPeriod(String reviewPeriod) {
        if (reviewPeriod == null || reviewPeriod.isEmpty())
            return;

        typeInto(txtReviewPeriod, reviewPeriod);
    }

    public void inputNextReviewDate(String reviewDate) {
        if (reviewDate == null || reviewDate.isEmpty())
            return;

        typeInto(txtReviewDate, reviewDate);
    }

    public RegistrationListPage clickSave() {
        clickOn(btnSave);
        return switchToPage(RegistrationListPage.class);
    }


    @Override
    public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
        String value = getInputValuesIndex(inputValuesIndex);

        this.selectProvider(testData.getInputValue(TEST_DATA_PREFIX + value, "RegistrationProvider"));
        this.selectRegisterType(testData.getInputValue(TEST_DATA_PREFIX + value, "RegisterType"));
        this.inputRegistrationDate(testData.getInputValue(TEST_DATA_PREFIX + value, "RegistrationDate"));
        this.inputReviewPeriod(testData.getInputValue(TEST_DATA_PREFIX + value, "ReviewPeriod"));
        this.inputNextReviewDate(testData.getInputValue(TEST_DATA_PREFIX + value, "NextReviewDate"));
        this.selectTeam(testData.getInputValue(TEST_DATA_PREFIX + value, "RegisteringTeam"));
        this.selectOfficer(testData.getInputValue(TEST_DATA_PREFIX + value, "RegisteringOfficer"));
        this.selectLevel(testData.getInputValue(TEST_DATA_PREFIX + value, "Level"));
        this.selectCategoryType(testData.getInputValue(TEST_DATA_PREFIX + value, "Category"));
        this.inputNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "RegistrationNotes"));

        //FileUtils.saveScreenshot(this.getClass().getSimpleName());
        this.selectRegisterType(testData.getInputValue(TEST_DATA_PREFIX + value, "RegisterType"));

        return type.cast(this.clickSave());
    }


}

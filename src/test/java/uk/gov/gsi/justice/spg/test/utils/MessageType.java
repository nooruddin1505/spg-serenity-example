package uk.gov.gsi.justice.spg.test.utils;

public enum MessageType {
  INS("INS"),
  UPD("UPD"),
  DEL("DEL"),
  RSP("RSP");

  private final String name;

  MessageType(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }
}

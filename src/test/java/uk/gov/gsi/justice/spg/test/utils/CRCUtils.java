package uk.gov.gsi.justice.spg.test.utils;

import dependencies.adapter.abstractstuff.CRCID;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import uk.gov.gsi.justice.spg.test.session.SessionKey;

import java.util.List;

/**
 * Class which defines static methods to support interactions with the CRC servers.
 */
public class CRCUtils {


  /**
   * Gets the host name for a given CRC ID.
   *
   * @param crcID the CRC ID for which the host name should be returned.
   * @return the host name of the given CRC ID.
   */

  /**
   * Gets the host name for a given CRC ID.
   *
   * @param crcID the CRC ID for which the host name should be returned.
   * @return the host name of the given CRC ID.
   */
  public static String getHostName(CRCID crcID) {
    EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
    String schemaVersion = getSchemaVersion(crcID);
    return configData.getProperty("Host." + schemaVersion + ".Server");
  }

  public static String getHostName(String schemaVersion) {
    EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
    return configData.getProperty("Host." + schemaVersion + ".Server");
  }

  /**
   * Maps CRC to correct Schema
   *
   * @param crcID
   * @return
   */
  public static String getSchemaVersion(CRCID crcID) {
    String schemaVersion = "0-9-11";
    switch (crcID) {
      case CRC01:
      case CRC02:
      case CRC03:
      case CRC04:
      case CRC05:
        schemaVersion = "0-9-9";
        break;
      case CRC06:
      case CRC07:
      case CRC08:
      case CRC09:
      case CRC10:
      case CRC11:
        schemaVersion = "0-9-10";
        break;
      case CRC12:
      case CRC13:
      case CRC14:
      case CRC15:
      case CRC16:
      case CRC17:
      case CRC18:
      case CRC19:
      case CRC20:
      case CRC21:
        schemaVersion = "0-9-12";
        break;
    }
    return schemaVersion;
  }

  /**
   * Map crc to CRCID
   * C01 -> CRC01
   * @param crcID
   * @return
   */
  public static CRCID getCRCID(String crcID) {
    CRCID crc = CRCID.CRC17;

    switch (crcID) {
      case "CRC01":
        crc = CRCID.CRC01;
        break;
      case "CRC07":
        crc = CRCID.CRC07 ;
        break;
      case "CRC17":
      case "C17":
        crc = CRCID.CRC17 ;
        break;
    }
    return crc;
  }

  /**
   * Get CRC for provider number X
   *
   * example Maps CPS London -> CRC17
   *
   * @param i
   * @return
   */
  public static CRCID getCRC(int index) {
    List<String> listOfProviders = (List<String>) Serenity.getCurrentSession().get(SessionKey.listOfProviders);
    String provider = listOfProviders.get(index-1);
    CRCID crc = getCRCFromProvder(provider);
    return crc;
  }


  public static CRCID getCRCFromProvder(String provider) {
    CRCID crc = null;
    switch (provider) {
      case "aNPS London":
        crc = CRCID.CRC01;
        break;
      case "bNPS London":
        crc = CRCID.CRC07 ;
        break;
      case "CRC17":
      case "NPS London":
        crc = CRCID.CRC17 ;
        break;
      case "CPA London":
        crc = CRCID.CRC17 ;
        break;
    }
    return crc;

  }
}

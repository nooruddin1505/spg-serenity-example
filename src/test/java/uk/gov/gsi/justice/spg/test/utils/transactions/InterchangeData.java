package uk.gov.gsi.justice.spg.test.utils.transactions;

import dependencies.adapter.utils.Pair;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import uk.gov.gsi.justice.spg.test.txtypes.AllocateEventTransaction;
import uk.gov.gsi.justice.spg.test.txtypes.RegistrationReviewTransaction;
import uk.gov.gsi.justice.spg.test.utils.MessageType;
import uk.gov.gsi.justice.spg.test.utils.TransactionInterchange;

import java.util.HashMap;

/**
 * Created by InternetUser on 10/01/2018.
 *
 * TransactionInterchange data is different for each tests
 */
public class InterchangeData {
  private static EnvironmentVariables env = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();

  public static TransactionInterchange getInterchangeData(String xmlInterchangeFile, int version) {
    TransactionInterchange txInterchange = null;
    String typeXPath = env.getProperty("XPath.TransactionType");

    //OffenderEngagedCRCInsert_Community_Requirement_001
    if(xmlInterchangeFile.contains("OffenderEngagedCRCInsert_Community_Requirement_001")){
      if(version == 1){
        txInterchange = new TransactionInterchange(new HashMap<Pair<Class, MessageType>, HashMap<String, String>>() {{
          put(new Pair<>(AllocateEventTransaction.class, MessageType.INS),
                  new HashMap<String, String>() {{
                    put(typeXPath + "/Event/EventDetails/Offender/CaseReferenceNumber", typeXPath + "/CommunityRequirementDetails/Offender/CaseReferenceNumber");
                    put(typeXPath + "/Event/EventDetails/Event/OffenderID", typeXPath + "/CommunityRequirementDetails/CommunityRequirement/OffenderID");
                    put(typeXPath + "/Event/EventDetails/Event/EventID", typeXPath + "/CommunityRequirementDetails/CommunityRequirement/EventID");
                  }});
        }});
      }
    }

    if(xmlInterchangeFile.contains("OffenderEngagedCRCUpdate_RegistrationReview_001")){
      if(version == 1){
        txInterchange = new TransactionInterchange(new HashMap<Pair<Class, MessageType>, HashMap<String, String>>() {{
          put(new Pair<>(RegistrationReviewTransaction.class, MessageType.INS),
                  new HashMap<String, String>() {{
                    put(typeXPath + "/RegistrationReviewDetails/Offender/CaseReferenceNumber", typeXPath + "/RegistrationReviewDetails/Offender/CaseReferenceNumber");
                    put(typeXPath + "/RegistrationReviewDetails/SPGVersion/SPGVersion", typeXPath + "/RegistrationReviewDetails/SPGVersion/SPGVersion");
                    put(typeXPath + "/RegistrationReviewDetails/RegistrationReview/OffenderID", typeXPath + "/RegistrationReviewDetails/RegistrationReview/OffenderID");
                    put(typeXPath + "/RegistrationReviewDetails/RegistrationReview/RegistrationID", typeXPath + "/RegistrationReviewDetails/RegistrationReview/RegistrationID");
                    put(typeXPath + "/RegistrationReviewDetails/RegistrationReview/RegistrationReviewID", typeXPath + "/RegistrationReviewDetails/RegistrationReview/RegistrationReviewID");
                  }});
        }});

      }
    }

    //OffenderEngagedNDInsert_RegistrationReview_003
    if(xmlInterchangeFile.contains("OffenderEngagedNDInsert_RegistrationReview_003")){
      if(version == 1){
        txInterchange = new TransactionInterchange(new HashMap<Pair<Class, MessageType>, HashMap<String, String>>() {{
          put(new Pair<>(RegistrationReviewTransaction.class, MessageType.INS),
                  new HashMap<String, String>() {{
                    put(typeXPath + "/RegistrationReviewDetails/Offender/CaseReferenceNumber", typeXPath + "/RegistrationReviewDetails/Offender/CaseReferenceNumber");
                    put(typeXPath + "/RegistrationReviewDetails/SPGVersion/SPGVersion", typeXPath + "/RegistrationReviewDetails/SPGVersion/SPGVersion");
                    put(typeXPath + "/RegistrationReviewDetails/RegistrationReview/OffenderID", typeXPath + "/RegistrationReviewDetails/RegistrationReview/OffenderID");
                    put(typeXPath + "/RegistrationReviewDetails/RegistrationReview/RegistrationID", typeXPath + "/RegistrationReviewDetails/RegistrationReview/RegistrationID");
                    put(typeXPath + "/RegistrationReviewDetails/RegistrationReview/RegistrationReviewID", typeXPath + "/RegistrationReviewDetails/RegistrationReview/RegistrationReviewID");
                  }});
        }});
      }
    }


    return txInterchange;
  }
}

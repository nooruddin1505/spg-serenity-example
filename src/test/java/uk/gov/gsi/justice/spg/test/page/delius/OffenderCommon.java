package uk.gov.gsi.justice.spg.test.page.delius;

import dependencies.adapter.driver.PageElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import uk.gov.gsi.justice.spg.test.page.DeliusCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.OffenderDetailsPage;

public class OffenderCommon extends DeliusCommon {

  @FindBy(xpath = "//*[@id='offender-overview']/div[1]/span[1]/a")
  WebElementFacade overViewCRN;

  @FindBy(id = "linkNavigation2OffenderIndex")
  WebElementFacade linkNavigation2OffenderIndex;
  @FindBy(id = "linkNavigation2EventList")
  WebElementFacade linkNavigation2EventList;
  @FindBy(id = "linkNavigation2OffenderSummary")
  WebElementFacade linkNavigation2OffenderSummary;

  public OffenderDetailsPage clickOffenderIndexLink() {
    clickOn(linkNavigation2OffenderIndex);
    return switchToPage(OffenderDetailsPage.class);
  }

  public EventListPage clickEventListLink() {
    clickOn(linkNavigation2EventList);
    return switchToPage(EventListPage.class);
  }

  public OffenderCommon clickOffenderSummaryLink() {
    clickOn(linkNavigation2OffenderSummary);
    return switchToPage(OffenderCommon.class);
  }

  private static final String XPATH_NAV_OFFENDER_SUMMARY_LINK = "//*[@id='linkNavigation2OffenderSummary']";
  private static final String XPATH_NAV_OFFENDER_INDEX_LINK = "//*[@id='linkNavigation2OffenderIndex']";
  private static final String XPATH_NAV_EVENT_LIST = "//*[@id='linkNavigation2EventList']";
  private static final String XPATH_NAV_CONTACT_LIST = "//*[@id='linkNavigation1ContactList']";
  private static final String XPATH_NAV_SUBJECT_ACCESS_REPORTS = "//*[@id='linkNavigation2SubjectAccessReportList']";

  private static final String XPATH_OVERVIEW_CRN = "//*[@id='offender-overview']/div[1]/span[1]/a";
  private static final String XPATH_OVERVIEW_OFFENDER_PROVIDER = "//*[@id='offender-overview']/div[1]/span[4]";
  private static final String XPATH_OVERVIEW_EVENT_PROVIDER = "//*[@id='event-overview']/div[1]/span[3]";

  private static final PageElement NAV_OFFENDER_SUMMARY_LINK =
      new PageElement(PageElement.Type.XPATH, XPATH_NAV_OFFENDER_SUMMARY_LINK, "Offender Summary Navigation Pane Offender Summary Link");
  private static final PageElement NAV_OFFENDER_INDEX_LINK =
      new PageElement(PageElement.Type.XPATH, XPATH_NAV_OFFENDER_INDEX_LINK, "Offender Summary Navigation Pane Offender Index Link");
  private static final PageElement NAV_EVENT_LIST_LINK =
      new PageElement(PageElement.Type.XPATH, XPATH_NAV_EVENT_LIST, "Offender Summary Navigation Pane Event List Link");
  private static final PageElement NAV_CONTACT_LIST_LINK =
      new PageElement(PageElement.Type.XPATH, XPATH_NAV_CONTACT_LIST, "Offender Summary Navigation Pane Contact List Link");
  private static final PageElement NAV_SUBJECT_ACCESS_REPORTS =
      new PageElement(PageElement.Type.XPATH, XPATH_NAV_SUBJECT_ACCESS_REPORTS, "Offender Summary Navigation Pane Event List Link");

  private static final PageElement OVERVIEW_CRN = new PageElement(PageElement.Type.XPATH, XPATH_OVERVIEW_CRN, "Offender Common Overview CRN Number");
  private static final PageElement OVERVIEW_OFFENDER_PROVIDER = new PageElement(PageElement.Type.XPATH, XPATH_OVERVIEW_OFFENDER_PROVIDER, "Offender Common Overview Offender Provider");
  private static final PageElement OVERVIEW_EVENT_PROVIDER = new PageElement(PageElement.Type.XPATH, XPATH_OVERVIEW_EVENT_PROVIDER, "Event Details Common Overview Event Provider");

//  public OffenderSummaryPage clickOffenderSummaryLink() {
//    pilot.click(NAV_OFFENDER_SUMMARY_LINK);
//    return new OffenderSummaryPage();
//  }

//  public OffenderDetailsPage clickOffenderIndexLink() {
//    pilot.click(NAV_OFFENDER_INDEX_LINK);
//    return new OffenderDetailsPage();
//  }

//  public EventListPage clickEventListLink() {
//    WaitUtils.waitForElementToBeClickable(By.xpath(XPATH_NAV_EVENT_LIST), 5);
//    pilot.click(NAV_EVENT_LIST_LINK);
//    return new EventListPage();
//  }

//  public ContactListPage clickContactListLink() {
//    pilot.click(NAV_CONTACT_LIST_LINK);
//    return new ContactListPage();
//  }
//
//  public SubjectAccessReportsListPage clickSubjectAccessReportsLink() {
//    pilot.click(NAV_SUBJECT_ACCESS_REPORTS);
//    return new SubjectAccessReportsListPage();
//  }

  public String getOffenderCRN() {
    return overViewCRN.getText();
  }

  public String getOffenderProvider() {
    return pilot.getText(OVERVIEW_OFFENDER_PROVIDER);
  }

  public String getEventProvider() {
    return pilot.getText(OVERVIEW_EVENT_PROVIDER);
  }


}

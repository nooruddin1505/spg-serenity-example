package uk.gov.gsi.justice.spg.test.exceptions;

/**
 * Exception class for when a transaction file has been found but it does not match any defined as a child of BaseTransaction
 * and therefore will not be able to be parsed.
 */
public class UnknownTransactionTypeException extends Exception {

  public String getLocalPath() {
    return localPath;
  }

  private String localPath;

  public UnknownTransactionTypeException(String localPath, String description) {
    super(description);
    this.localPath = localPath;
  }
}

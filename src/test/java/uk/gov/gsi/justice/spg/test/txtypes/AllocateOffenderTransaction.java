package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class AllocateOffenderTransaction extends BaseTransaction {

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    // Offender Details
    add(new AssertionPair("OffenderSteps%s.Offender%s.CaseReferenceNumber", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/Offender/CaseReferenceNumber"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.OffenderID", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/OffenderID"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.Title", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/Title"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.FirstName", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/FirstName"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.SecondName", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/SecondName"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.ThirdName", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/ThirdName"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.FamilyName", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/FamilyName"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.PreviousName", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/PreviousName"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.DateOfBirth", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/DateOfBirth"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.DateDied", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/DateDied"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/Notes"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.Gender", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/Gender"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.PNCNumber", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/PNCNumber"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.CRONumber", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/CRONumber"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.NOMSNumber", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/NOMSNumber"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.NINumber", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/NINumber"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.TelephoneNumber", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/TelephoneNumber"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.MobileNumber", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/MobileNumber"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.AllowSMS", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/AllowSMS"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.EmailAddress", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/EmailAddress"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.OMProvider", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/OMProvider"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.OMResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/OMResponsibleTeam"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.OMResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/OMResponsibleOfficer"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.REMMainCategory", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/REMMainCategory"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.Nationality", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/Nationality"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.SecondNationality", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/SecondNationality"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.ImmigrationStatus", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/ImmigrationStatus"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.ImmigrationNumber", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/ImmigrationNumber"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.Language", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/Language"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.InterpreterRequired", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/InterpreterRequired"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.ReligionOrFaith", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/ReligionOrFaith"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.SexualOrientation", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/SexualOrientation"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.RemandStatus", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/RemandStatus"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.TransGenderProcess", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/TransGenderProcess"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.TransGenderDiscloseConsent", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/TransGenderDiscloseConsent"));
    add(new AssertionPair("OffenderSteps%s.Offender%s.EqualityMonitoringNotes", "/SPGInterchange/SPGMessage/*[2]/Offender%s/OffenderDetails/OffenderDetails/EqualityMonitoringNotes"));
    // Contact Details
    add(new AssertionPair("OffenderSteps%s.Contact%s.Alert", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Alert", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.ContactType", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactType", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.ContactDate", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactDate", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.ContactStartTime", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactStartTime", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.ContactEndTime", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactEndTime", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.Provider", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Provider", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.Team", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Team", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.ContactOfficer", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactOfficer", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.Location", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Location", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.ContactOutcome", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactOutcome", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.EnforcementAction", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/EnforcementAction", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.SensitiveContact", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/SensitiveContact", 1, 2));
    add(new AssertionPair("OffenderSteps%s.Contact%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Notes", 1, 2));
    // Process Contact
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessType", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessType"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessSubType", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessSubType"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessRefDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessRefDate"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessExpStartDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessExpStartDate"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessStartDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessStartDate"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessExpEndDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessExpEndDate"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessEndDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessEndDate"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessStage", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessStage"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessStageDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessStageDate"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessNotes", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessNotes"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessOutcome", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessOutcome"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessEndAttCount", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessEndAttCount"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.IntendedProvider", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/IntendedProvider"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessManagerProvider", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessManagerProvider"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessManagerTeam", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessManagerTeam"));
    add(new AssertionPair("OffenderSteps%s.ProcessContact%s.ProcessManagerOfficer", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessManagerOfficer"));
    // Address Details
    add(new AssertionPair("OffenderSteps%s.Address%s.NoFixedAbode", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/NoFixedAbode"));
    add(new AssertionPair("OffenderSteps%s.Address%s.BuildingName", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/BuildingName"));
    add(new AssertionPair("OffenderSteps%s.Address%s.HouseNumber", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/HouseNumber"));
    add(new AssertionPair("OffenderSteps%s.Address%s.StreetName", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/StreetName"));
    add(new AssertionPair("OffenderSteps%s.Address%s.District", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/District"));
    add(new AssertionPair("OffenderSteps%s.Address%s.TownCity", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/TownCity"));
    add(new AssertionPair("OffenderSteps%s.Address%s.County", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/County"));
    add(new AssertionPair("OffenderSteps%s.Address%s.TelephoneNumber", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/TelephoneNumber"));
    add(new AssertionPair("OffenderSteps%s.Address%s.StartDate", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/StartDate"));
    add(new AssertionPair("OffenderSteps%s.Address%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/EndDate"));
    add(new AssertionPair("OffenderSteps%s.Address%s.Status", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/Status"));
    add(new AssertionPair("OffenderSteps%s.Address%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Address%s/AddressDetails/OffenderAddress/Notes"));
    // Address Assessment Details
    add(new AssertionPair("OffenderSteps%s.AddressAssessment%s.AddressAssessmentProvider", "/SPGInterchange/SPGMessage/*[2]/AddressAssessment%s/AddressAssessmentDetails/AddressAssessment/AddressAssessmentProvider"));
    add(new AssertionPair("OffenderSteps%s.AddressAssessment%s.Date", "/SPGInterchange/SPGMessage/*[2]/AddressAssessment%s/AddressAssessmentDetails/AddressAssessment/Date"));
    add(new AssertionPair("OffenderSteps%s.AddressAssessment%s.Notes", "/SPGInterchange/SPGMessage/*[2]/AddressAssessment%s/AddressAssessmentDetails/AddressAssessment/Notes"));
    // Alias Details
    add(new AssertionPair("OffenderSteps%s.Alias%s.FirstName", "/SPGInterchange/SPGMessage/*[2]/Alias%s/AliasDetails/Alias/FirstName"));
    add(new AssertionPair("OffenderSteps%s.Alias%s.SecondName", "/SPGInterchange/SPGMessage/*[2]/Alias%s/AliasDetails/Alias/SecondName"));
    add(new AssertionPair("OffenderSteps%s.Alias%s.ThirdName", "/SPGInterchange/SPGMessage/*[2]/Alias%s/AliasDetails/Alias/ThirdName"));
    add(new AssertionPair("OffenderSteps%s.Alias%s.Surname", "/SPGInterchange/SPGMessage/*[2]/Alias%s/AliasDetails/Alias/FamilyName"));
    add(new AssertionPair("OffenderSteps%s.Alias%s.DateOfBirth", "/SPGInterchange/SPGMessage/*[2]/Alias%s/AliasDetails/Alias/DateOfBirth"));
    add(new AssertionPair("OffenderSteps%s.Alias%s.Gender", "/SPGInterchange/SPGMessage/*[2]/Alias%s/AliasDetails/Alias/Gender"));
    // Personal Contact Details
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.RelationshipType", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/RelationshipType"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.Title", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/Title"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.FirstName", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/FirstName"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.OtherNames", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/OtherNames"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.Surname", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/FamilyName"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.PreviousSurname", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/PreviousSurname"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.RelationshipToOffender", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/RelationshipToOffender"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.Gender", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/Gender"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.BuildingName", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/BuildingName"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.HouseNumber", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/HouseNumber"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.StreetName", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/StreetName"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.District", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/District"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.TownCity", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/TownCity"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.County", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/County"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.Postcode", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/Postcode"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.MobileNumber", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/MobileNumber"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.TelephoneNumber", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/TelephoneNumber"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.EmailAddress", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/EmailAddress"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.StartDate", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/StartDate"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/EndDate"));
    add(new AssertionPair("OffenderSteps%s.PersonalContact%s.Notes", "/SPGInterchange/SPGMessage/*[2]/PersonalContact%s/PersonalContactDetails/PersonalContact/Notes"));
    // Disability Details
    add(new AssertionPair("OffenderSteps%s.Disability%s.Disability", "/SPGInterchange/SPGMessage/*[2]/Disability%s/DisabilityDetails/OffenderDisability/Disability"));
    add(new AssertionPair("OffenderSteps%s.Disability%s.StartDate", "/SPGInterchange/SPGMessage/*[2]/Disability%s/DisabilityDetails/OffenderDisability/StartDate"));
    add(new AssertionPair("OffenderSteps%s.Disability%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/Disability%s/DisabilityDetails/OffenderDisability/EndDate"));
    add(new AssertionPair("OffenderSteps%s.Disability%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Disability%s/DisabilityDetails/OffenderDisability/Notes"));
    // Provision Details
    add(new AssertionPair("OffenderSteps%s.Provision%s.Adjustment", "/SPGInterchange/SPGMessage/*[2]/Provision%s/ProvisionDetails/Provision/Adjustment"));
    add(new AssertionPair("OffenderSteps%s.Provision%s.StartDate", "/SPGInterchange/SPGMessage/*[2]/Provision%s/ProvisionDetails/Provision/StartDate"));
    add(new AssertionPair("OffenderSteps%s.Provision%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/Provision%s/ProvisionDetails/Provision/EndDate"));
    add(new AssertionPair("OffenderSteps%s.Provision%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Provision%s/ProvisionDetails/Provision/Notes"));
    // Registration Details
    add(new AssertionPair("OffenderSteps%s.Registration%s.Type", "/SPGInterchange/SPGMessage/*[2]/Registration%s/RegistrationDetails/Registration/RegisterType"));
    add(new AssertionPair("OffenderSteps%s.Registration%s.Date", "/SPGInterchange/SPGMessage/*[2]/Registration%s/RegistrationDetails/Registration/RegistrationDate"));
    add(new AssertionPair("OffenderSteps%s.Registration%s.NextReviewDate", "/SPGInterchange/SPGMessage/*[2]/Registration%s/RegistrationDetailsRegistration/NextReviewDate"));
    add(new AssertionPair("OffenderSteps%s.Registration%s.Provider", "/SPGInterchange/SPGMessage/*[2]/Registration%s/RegistrationDetails/Registration/RegistrationProvider"));
    add(new AssertionPair("OffenderSteps%s.Registration%s.Team", "/SPGInterchange/SPGMessage/*[2]/Registration%s/RegistrationDetails/Registration/RegisteringTeam"));
    add(new AssertionPair("OffenderSteps%s.Registration%s.Officer", "/SPGInterchange/SPGMessage/*[2]/Registration%s/RegistrationDetails/Registration/RegisteringOfficer"));
    add(new AssertionPair("OffenderSteps%s.Registration%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Registration%s/RegistrationDetails/Registration/RegistrationNotes"));
    // Personal Circumstances
    add(new AssertionPair("OffenderSteps%s.PersonalCircumstance%s.StartDate", "/SPGInterchange/SPGMessage/*[2]/PersonalCircumstance%s/PersonalCircumstanceDetails/PersonalCircumstance/StartDate"));
    add(new AssertionPair("OffenderSteps%s.PersonalCircumstance%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/PersonalCircumstance%s/PersonalCircumstanceDetails/PersonalCircumstance/EndDate"));
    add(new AssertionPair("OffenderSteps%s.PersonalCircumstance%s.CircumstanceType", "/SPGInterchange/SPGMessage/*[2]/PersonalCircumstance%s/PersonalCircumstanceDetails/PersonalCircumstance/CircumstanceType"));
    add(new AssertionPair("OffenderSteps%s.PersonalCircumstance%s.CircumstanceSubType", "/SPGInterchange/SPGMessage/*[2]/PersonalCircumstance%s/PersonalCircumstanceDetails/PersonalCircumstance/CircumstanceSubType"));
    add(new AssertionPair("OffenderSteps%s.PersonalCircumstance%s.Notes", "/SPGInterchange/SPGMessage/*[2]/PersonalCircumstance%s/PersonalCircumstanceDetails/PersonalCircumstance/Notes"));
    // Additional Identifier
    add(new AssertionPair("OffenderSteps%s.AdditionalIdentifier%s.Type", "/SPGInterchange/SPGMessage/*[2]/AdditionalIdentifier%s/AdditionalIdentifierDetails/AdditionalIdentifier/IdentifierType"));
    add(new AssertionPair("OffenderSteps%s.AdditionalIdentifier%s.Identifier", "/SPGInterchange/SPGMessage/*[2]/AdditionalIdentifier%s/AdditionalIdentifierDetails/AdditionalIdentifier/Identifier"));
    // Offender Manager
    add(new AssertionPair("OffenderSteps%s.OffenderManager%s.Provider", "/SPGInterchange/SPGMessage/*[2]/OffenderManager%s/OffenderManagerDetails/OffenderManager/Provider"));
    add(new AssertionPair("OffenderSteps%s.OffenderManager%s.ResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/OffenderManager%s/OffenderManagerDetails/OffenderManager/ResponsibleTeam"));
    add(new AssertionPair("OffenderSteps%s.OffenderManager%s.ResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/OffenderManager%s/OffenderManagerDetails/OffenderManager/ResponsibleOfficer"));
    add(new AssertionPair("OffenderSteps%s.OffenderManager%s.AllocationReason", "/SPGInterchange/SPGMessage/*[2]/OffenderManager%s/OffenderManagerDetails/OffenderManager/AllocationReason"));
    add(new AssertionPair("OffenderSteps%s.OffenderManager%s.AllocationDate", "/SPGInterchange/SPGMessage/*[2]/OffenderManager%s/OffenderManagerDetails/OffenderManager/AllocationDate"));
    add(new AssertionPair("OffenderSteps%s.OffenderManager%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/OffenderManager%s/OffenderManagerDetails/OffenderManager/EndDate"));
    // Rate Card Intervention
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.InterventionTypeMainCategory", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionTypeMainCategory"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.InterventionTypeSubCategory", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionTypeSubCategory"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.ReferralDate", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/ReferralDate"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.ExpectedStartDate", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/ExpectedStartDate"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.ActualStartDate", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/ActualStartDate"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.Notes", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/Notes"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.InterventionStatus", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionStatus"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.InterventionStatusDateTime", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionStatusDateTime"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.InterventionProvider", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionProvider"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.InterventionResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionResponsibleTeam"));
    add(new AssertionPair("OffenderSteps%s.RateCardIntervention%s.InterventionResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionResponsibleOfficer"));
    // Registration Review
    add(new AssertionPair("OffenderSteps%s.RegistrationReview%s.ReviewingOfficer", "/SPGInterchange/SPGMessage/*[2]/RegistrationReview%s/RegistrationReviewDetails/RegistrationReview/ReviewingOfficer"));
    add(new AssertionPair("OffenderSteps%s.RegistrationReview%s.ReviewCompleted", "/SPGInterchange/SPGMessage/*[2]/RegistrationReview%s/RegistrationReviewDetails/RegistrationReview/ReviewCompleted"));
  }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

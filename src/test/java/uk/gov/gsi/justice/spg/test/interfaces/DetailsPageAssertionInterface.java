package uk.gov.gsi.justice.spg.test.interfaces;

/**
 * All Delius page can be considered either a form page (where data is input against an entity), a details page (where data against one entity
 * is shown) or a table page (where data against multiple entities is shown), or a combination of two or more of the above. To represent this,
 * all Delius page representations in this project must implement one or more of these PageInterfaces wherever relevant to each individual page.
 *
 * This interface defines methods which must be implemented on any Delius 'details' pages, such as the offender details page. Details pages can be
 * said to be arranged in a table format of label/value pairs, and this interface defines a comprehensive set of methods for obtaining page values
 * in a flexible manner.
 */
public interface DetailsPageAssertionInterface {
  /**
   * Given a page label, this method should return the value associated with it.
   *
   * @param label the label for the value to be returned.
   * @return the value of the given label. Null if not present.
   */
  String getValueByLabel(String label);

  /**
   * Given a page value, this method should return the label associated with it. If there are multiple labels which match the search, these should
   * be separated in some intuitive way, such as with a comma (,) or pipe (|).
   *
   * @param value the value for the label(s) returned.
   * @return the label(s) for the given value. Null if not present.
   */
  String getLabelByValue(String value);

  /**
   * Checks whether a label exists on the page.
   *
   * @param label the label to check if it exists.
   * @return true if the label exists, false otherwise.
   */
  boolean labelExists(String label);

  /**
   * Checks whether a value exists on the page.
   *
   * @param value the value to check if it exists.
   * @return true if the value exists, false otherwise.
   */
  boolean valueExists(String value);

  /**
   * This method should be used to assert, in a manner relevant to each individual page, the page values against expected values obtained from test
   * data.
   */
  void assertPageValues();
}

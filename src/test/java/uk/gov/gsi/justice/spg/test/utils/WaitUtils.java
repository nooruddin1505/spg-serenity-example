package uk.gov.gsi.justice.spg.test.utils;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import uk.gov.gsi.justice.spg.test.page.DeliusCommon;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * WaitUtils: Remove the use of Thread.sleep() or reduce it
 * <p>
 * Only use WebDriverWaits, don't mix with Implicit waits
 */
public class WaitUtils {

  static boolean showDebugMessages = false;

  /**
   * This should NEVER be used over WebDriverWait
   * <p>
   * It can be used if we are doing messaging between different systems
   * - Never set a very large sleep value
   * - Try to use it with small value and use polling to check if messages are ready
   *
   * @param timeInMiliseconds
   */
  public static void sleepSpecificTimeDontUseMeOverSeleniumWaits(long timeInMiliseconds) {
    try {
      Thread.sleep(timeInMiliseconds);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  public static void sleepSpecificTimeDontUseMeOverSeleniumWaits(DeliusCommon page, long timeInMiliseconds) {
    System.out.println("Please find a better solution : " + page);
    try {
      Thread.sleep(timeInMiliseconds);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  /**
   * When waiting for dropdown options to dynamically populate we should use
   * POLLING instead of Thread.sleep()
   *
   * @param by    Locator XPath, ID of the drop down
   * @param value Option we are waiting for
   */
  public static boolean isDropDownReadyForSelection(By by, final String value) {
    return isDropDownReadyForSelection(by, value, false);
  }

  /**
   * Use this if you want to disable implicitlyWait and only use explicitWait
   * - When you mix and match implicitlyWait with explicitWait you may get some undesired behaviour
   * - If implicitWait=10 and explicitWait=30 than you may get 10+30=40 second wait sometimes
   * <p>
   * When waiting for dropdown options to dynamically populate we should use
   * POLLING instead of Thread.sleep()
   *
   * @param by    Locator XPath, ID of the drop down
   * @param value Option we are waiting for
   */
  public static boolean isDropDownReadyForSelection(By by, final String value, boolean disableImplicitWaits) {
    WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();

    //disable implicitlyWaits
    disableImplicitlyWaits(driver, disableImplicitWaits);

    int timeout = 1;
    int maxAttempts = 5;
    int attemptNumber = 1;
    boolean found = false;
    do {
      WebElement sb = driver.findElement(by);
      try {
        final Select dropDownList = new Select(sb);
        if (!dropDownList.getOptions().isEmpty() && dropDownContains(dropDownList, value)) {
          if (showDebugMessages)
            System.out.format("Option found : %s ", value);
          found = true;
          break;
        } else {
          //Try again in a second
          new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfNestedElementLocatedBy(sb, By.xpath("option[text()='" + value + "']")));
        }
      } catch (Exception e) {
        if (showDebugMessages) {
          String date = new Date().toString();
          System.out.format("\nDrop down data loading for : %s, Time=%s ", by, date.substring(date.indexOf(":") - 2, date.indexOf(":") + 6));
        }
      }

      attemptNumber++;
    } while (!found && attemptNumber < maxAttempts);

    //Enable if disabled implicitlyWaits: As the framework depends on implicitlyWaits
    if (disableImplicitWaits)
      disableImplicitlyWaits(driver, false);

    //Try one last time, if not found it should timeout
    if (!found) {
      WebElement sb = driver.findElement(by);
      new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfNestedElementLocatedBy(sb, By.xpath("option[text()='" + value + "']")));
    }

    return found;
  }

  private static void disableImplicitlyWaits(WebDriver driver, boolean disableImplicitWaits) {
    if (disableImplicitWaits) {
      driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    } else {
      EnvironmentVariables env = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
      long timeToWait = env.getPropertyAsInteger("webdriver.timeouts.implicitlywait", 5000);
      driver.manage().timeouts().implicitlyWait(timeToWait, TimeUnit.MILLISECONDS);
    }
  }

  /**
   * If its not empty than check drop down contains the expected value
   *
   * @param dropDown
   * @param value
   * @return
   */
  public static boolean dropDownContains(Select dropDown, String value) {
    boolean contains = false;
    for (WebElement el : dropDown.getOptions()) {
      String text = el.getText();
      if (text.contains(value)) {
        contains = true;
        break;
      }
    }
    return contains;
  }


//  public static void waitForElementToBeClickable(By by, int timeout) {
//    WebDriver driver = ExecutionContext.getInstance().getPilot().getWebDriver();
//    new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(by));
//  }
//
//  public static void waitForElementToBeClickable(WebElement element, int timeout) {
//    WebDriver driver = ExecutionContext.getInstance().getPilot().getWebDriver();
//    new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));
//  }

}

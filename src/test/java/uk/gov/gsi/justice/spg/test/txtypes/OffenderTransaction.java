package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class OffenderTransaction extends BaseTransaction {

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("Offender%s.OffenderDetails%s.Title",                       "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/Title"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.FirstName",                   "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/FirstName"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.SecondName",                  "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/SecondName"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.ThirdName",                   "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/ThirdName"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.FamilyName",                  "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/FamilyName"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.DateOfBirth",                 "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/DateOfBirth"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.Notes",                       "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/Notes"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.Gender",                      "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/Gender"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.PNCNumber",                   "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/PNCNumber"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.CRONumber",                   "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/CRONumber"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.NOMSNumber",                  "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/NOMSNumber"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.NINumber",                    "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/NINumber"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.TransGenderProcess",          "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/TransGenderProcess"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.TransGenderDiscloseConsent",  "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/TransGenderDiscloseConsent"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.EqualityMonitoringNotes",     "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/EqualityMonitoringNotes"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.DateDied",                    "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/DateDied"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.REMMainCategory",             "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/REMMainCategory"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.Nationality",                 "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/Nationality"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.SecondNationality",           "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/SecondNationality"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.ImmigrationStatus",           "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/ImmigrationStatus"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.ImmigrationNumber",           "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/ImmigrationNumber"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.Language",                    "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/Language"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.LanguageConcerns",            "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/LanguageConcerns"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.InterpreterRequired",         "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/InterpreterRequired"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.ReligionOrFaith",             "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/ReligionOrFaith"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.SecondNationality",           "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/SecondNationality"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.SexualOrientation",           "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/SexualOrientation"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.Tier",                        "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/Tier"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.RestrictionsExistYN",         "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/RestrictionsExistYN"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.ExclusionsExistYN",           "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/ExclusionsExistYN"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.ExclusionsExistYN",           "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/ExclusionsExistYN"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.OMResponsibleTeam",           "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/OMResponsibleTeam"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.OMResponsibleOfficer",        "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/OMResponsibleOfficer"));
    add(new AssertionPair("Offender%s.OffenderDetails%s.OMProvider",                  "/SPGInterchange/SPGMessage/*[2]/OffenderDetails%s/OffenderDetails/OMProvider"));
  }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

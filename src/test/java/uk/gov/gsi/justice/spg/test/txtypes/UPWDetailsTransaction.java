package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class UPWDetailsTransaction extends BaseTransaction {

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("UPWDetails%s.UPWDetailsDetails%s.UPWLengthMinutes",      "/SPGInterchange/SPGMessage/UPWDetails/UPWDetailsDetails/UPWDetails/UPWLengthMinutes"));
    add(new AssertionPair("UPWDetails%s.UPWDetailsDetails%s.UPWStatus",             "/SPGInterchange/SPGMessage/UPWDetails/UPWDetailsDetails/UPWDetails/UPWStatus"));
    add(new AssertionPair("UPWDetails%s.UPWDetailsDetails%s.WorkedIntensively",     "/SPGInterchange/SPGMessage/UPWDetails/UPWDetailsDetails/UPWDetails/WorkedIntensively"));
    add(new AssertionPair("UPWDetails%s.UPWDetailsDetails%s.AgreedTravelFare",      "/SPGInterchange/SPGMessage/UPWDetails/UPWDetailsDetails/UPWDetails/AgreedTravelFare"));
    add(new AssertionPair("UPWDetails%s.UPWDetailsDetails%s.Notes",                 "/SPGInterchange/SPGMessage/UPWDetails/UPWDetailsDetails/UPWDetails/Notes"));
  }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

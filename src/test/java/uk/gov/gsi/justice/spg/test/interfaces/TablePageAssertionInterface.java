package uk.gov.gsi.justice.spg.test.interfaces;

/**
 * All Delius page can be considered either a form page (where data is input against an entity), a details page (where data against one entity
 * is shown) or a table page (where data against multiple entities is shown), or a combination of two or more of the above. To represent this,
 * all Delius page representations in this project must implement one or more of these PageInterfaces wherever relevant to each individual page.
 *
 * This interface defines methods which must be implemented on any Delius 'table' pages, such as the event list page. This interface defines a
 * set of comprehensive methods for obtaining data from the table in a flexible manner.
 */
public interface TablePageAssertionInterface {
  /**
   * When there is no data to be displayed in a table in Delius, the table is not present. This method provides a simple way to
   * determine if there is any data to be accessed.
   *
   * @return true if a table exists on the page, false otherwise.
   */
  boolean tableExists();

  /**
   * Attempts to return the value in the cell location by row and column number.
   *
   * @param row the row location of the cell.
   * @param column the column location of the cell.
   * @return the value at the given cell location. Null if not present.
   */
  String getValueAtLocation(int row, int column);

  /**
   * Attempts to return the value in the cell location by row number and column name.
   *
   * @param row the row location of the cell.
   * @param columnName the name of the column header the cell is under.
   * @return the value at the given cell location. Null if not present.
   */
  String getValueAtLocation(int row, String columnName);

  /**
   * Asserts expected values from test data do not exist at a given row in the table.
   *
   * @param rowLocation the location of the row to assert against.
   */
  void assertTableRowNotExists(int rowLocation);

  /**
   * Asserts expected values from test data exist at a given row in the table.
   *
   * @param rowLocation the location of the row to assert against.
   */
  void assertTableRowExists(int rowLocation);

  /**
   * This method should be used to assert, in a manner relevant to each individual page, the table values against expected values
   * obtained from the test data.
   */
  void assertPageValues();
}

package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import org.openqa.selenium.By;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.DetailsPageAssertionInterface;
import uk.gov.gsi.justice.spg.test.interfaces.TablePageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class RegistrationDetailsPage extends OffenderDetailsCommon implements DetailsPageAssertionInterface, TablePageAssertionInterface {

  private static final String EXPECTED_VALUES_PREFIX = "RegistrationDetailsPage.";

  //Dynamic lookups
  public static final String XPATH_UPDATE_REGISTRATION_REVIEW_HISTORY_BY_ROW = "//table/tbody/tr[%s]/td[6]/a";
  public static final String XPATH_VIEW_REGISTRATION_REVIEW_HISTORY_BY_ROW = "//table/tbody/tr[%s]/td[7]/a";
  public static final String XPATH_VIEW_DEREGISTRATION_BY_ROW = "//table/tbody/tr[%s]/td[7]/a";
  public static final String XPATH_REGISTRATION_REVIEW_DATE_TIME_BY_ROW = "//table/tbody/tr[%s]/td[1]";

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "RegisterType", "//div/label[contains(text(), 'Register Type')]/../div/span"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "RegistrationDate", "//div/label[contains(text(), 'Registration Date')]/../div/span"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "ReviewPeriod", "//div/label[contains(text(), 'Review Period')]/../div/span"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "NextReviewDate", "//div/label[contains(text(), 'Next Review Date')]/../div/span"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "RegisteringOfficersProvider", "//div/label[contains(text(), 'Provider')]/../div/span"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "RegisteringOfficersTeam", "//div/label[contains(text(), 'Team')]/../div/span"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "RegisteringOfficer", "//div/label[contains(text(), 'Registering Officer:')]/../div/span"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "RegistrationNotes",    "//*[@id='registrationForm:Notes']"));
    //add(new AssertionPair(EXPECTED_VALUES_PREFIX + "InfoBarFlagTitle", "//[@id='offender-overview']/div[3]/div[7]/a"));
  }};
  //*[@id="registrationForm:Notes"]
  public RegistrationReviewDetailsPage clickViewByRow(int row) {
    By viewRegistrationReviewHistoryByRow = By.xpath(String.format(XPATH_VIEW_REGISTRATION_REVIEW_HISTORY_BY_ROW, row));
    clickOn(find(viewRegistrationReviewHistoryByRow));
    return switchToPage(RegistrationReviewDetailsPage.class);
  }

//  public DeRegistrationDetailsPage clickViewDeRegistrationdByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_VIEW_DEREGISTRATION_BY_ROW, "RegistrationListPage view registration history by row", row));
//    return new DeRegistrationDetailsPage();
//  }
//
//  public UpdateRegistrationReviewPage clickUpdateByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_UPDATE_REGISTRATION_REVIEW_HISTORY_BY_ROW, "RegistrationListPage update registration history by row", row));
//    return new UpdateRegistrationReviewPage();
//  }

  public String getRegistrationReviewDateTimeByRow(int row) {
    return pilot.getText(PageElement.byXpath(XPATH_REGISTRATION_REVIEW_DATE_TIME_BY_ROW, "RegistrationDetailsPage review date time by row " + row, row));
  }

  @Override
  public String getValueByLabel(String label) {
    return null;
  }

  @Override
  public String getLabelByValue(String value) {
    return null;
  }

  @Override
  public boolean labelExists(String label) {
    return false;
  }

  @Override
  public boolean valueExists(String value) {
    return false;
  }

  @Override
  public boolean tableExists() {
    return false;
  }

  @Override
  public String getValueAtLocation(int row, int column) {
    return null;
  }

  @Override
  public String getValueAtLocation(int row, String columnName) {
    return null;
  }

  @Override
  public void assertTableRowNotExists(int rowLocation) {

  }

  @Override
  public void assertTableRowExists(int rowLocation) {

  }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "RegistrationDetailsPage get value by description"));

      if (expected == null || expected.isEmpty())
        continue;

      DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "");
      assertor.assertEquals(String.format("Value is correct. Expected: '%s', Actual: '%s'", expected, actual), expected, actual);
    }
  }
}

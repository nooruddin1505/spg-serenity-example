package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

/**
 * Created by rizwan.sulaman on 10/02/2017.
 */

import dependencies.adapter.driver.PageElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.DetailsPageAssertionInterface;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;


public class AddressAssessmentDetailsPage extends AddressAssessmentListPage implements DetailsPageAssertionInterface {

    @FindBy(xpath = "//*[@value='Close']")
    WebElement btnClose;

   private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
        add(new AssertionPair("AddressAssessmentDetails.Team", "//*[@id='j_id_id12:Team']"));
        add(new AssertionPair("AddressAssessmentDetails.Officer", "//*[@id='j_id_id12:Staff']"));
        add(new AssertionPair("AddressAssessmentDetails.Date", "//*[@id='j_id_id12:Date']"));
        add(new AssertionPair("AddressAssessmentDetails.Notes", "//*[@id='j_id_id12:Notes']"));
    }};

    public AddressAssessmentListPage clickClose() {
        clickOn(btnClose);
        return switchToPage(AddressAssessmentListPage.class);
    }

    @Override
    public String getValueByLabel(String label) {
        return null;
    }

    @Override
    public String getLabelByValue(String value) {
        return null;
    }

    @Override
    public boolean labelExists(String label) {
        return false;
    }

    @Override
    public boolean valueExists(String value) {
        return false;
    }

    @Override
    public void assertPageValues() {
        for (AssertionPair pair : ASSERTION_PAIRS) {
            DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "AddressAssessmentDetailsPage");
            String expected = testData.getExpectedResult(pair.getDataHeading());
            String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "AddressAssessmentDetailsPage get value by description"));

            assertor.assertEquals(String.format("Value is correct. Expected: %s, Actual: %s", expected, actual), expected, actual);
        }
    }

}


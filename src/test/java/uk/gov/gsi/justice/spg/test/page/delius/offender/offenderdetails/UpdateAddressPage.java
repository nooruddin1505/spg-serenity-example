package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;

public class UpdateAddressPage extends OffenderDetailsCommon implements FormPageInterface {
  private static final String TEST_DATA_PREFIX = "UpdateAddress";

  @FindBy(id = "updateAddressForm:NoFixedAbode")
  WebElementFacade selectNoFixedAbode;
  @FindBy(id = "updateAddressForm:BuildingName")
  WebElementFacade inputBuildingName;
  @FindBy(id = "updateAddressForm:HouseNumber")
  WebElementFacade inputHouseNumber;
  @FindBy(id = "updateAddressForm:StreetName")
  WebElementFacade inputStreetName;
  @FindBy(id = "updateAddressForm:District")
  WebElementFacade inputDistrict;
  @FindBy(id = "updateAddressForm:TownCity")
  WebElementFacade inputTownCity;
  @FindBy(id = "updateAddressForm:County")
  WebElementFacade inputCounty;
  @FindBy(id = "updateAddressForm:Postcode")
  WebElementFacade inputPostcode;
  @FindBy(id = "updateAddressForm:TelephoneNumber")
  WebElementFacade inputTelephoneNumber;
  @FindBy(id = "StartDate")
  WebElementFacade inputStartDate;
  @FindBy(id = "updateAddressForm:EndDate")
  WebElementFacade inputEnddate;
  @FindBy(id = "updateAddressForm:AddressStatus")
  WebElementFacade selectStatus;
  @FindBy(id = "updateAddressForm:newNotes")
  WebElementFacade inputNotes;

  @FindBy(xpath = "//*[@value='Save']")
  WebElementFacade btnSave;
  @FindBy(xpath = "//*[@value='Cancel']")
  WebElementFacade btnCancel;
  @FindBy(xpath = "//*[@value='Confirm']")
  WebElementFacade btnConfirm;



  // There are no required fields as this is an update page therefore null/empty logic is required for each field
  public void selectNoFixedAbode(String noFixedAbode) {
    if (noFixedAbode == null || noFixedAbode.isEmpty())
      return;

    WaitUtils.isDropDownReadyForSelection(By.id("updateAddressForm:NoFixedAbode"), noFixedAbode);
    selectFromDropdown(selectNoFixedAbode, noFixedAbode);
  }

  public void inputBuildingName(String buildingName) {
    if (buildingName == null || buildingName.isEmpty())
      return;

    typeInto(inputBuildingName, buildingName);
  }

  public void inputHouseNumber(String houseNumber) {
    if (houseNumber == null || houseNumber.isEmpty())
      return;

    typeInto(inputHouseNumber, houseNumber);
  }

  public void inputStreetName(String streetName) {
    if (streetName == null || streetName.isEmpty())
      return;
    typeInto(inputStreetName, streetName);
  }

  public void inputDistrict(String district) {
    if (district == null || district.isEmpty())
      return;

    typeInto(inputDistrict, district);
  }

  public void inputTownCity(String townCity) {
    if (townCity == null || townCity.isEmpty())
      return;

    typeInto(inputTownCity, townCity);
  }

  public void inputCounty(String county) {
    if (county == null || county.isEmpty())
      return;

    typeInto(inputCounty, county);
  }

  public void inputPostcode(String postcode) {
    if (postcode == null || postcode.isEmpty())
      return;

    typeInto(inputPostcode, postcode);
  }

  public void inputTelephoneNumber(String telephoneNumber) {
    if (telephoneNumber == null || telephoneNumber.isEmpty())
      return;

    typeInto(inputTelephoneNumber, telephoneNumber);
  }

  public void inputStartDate(String startDate) {
    if (startDate == null || startDate.isEmpty())
      return;

    typeInto(inputStartDate, startDate);
  }

  public void inputEndDate(String endDate) {
    if (endDate == null || endDate.isEmpty())
      return;

    typeInto(inputEnddate, endDate);
  }

  public void selectStatus(String status) {
    if (status == null || status.isEmpty())
      return;

    selectFromDropdown(selectStatus, status);
  }

  public void inputNotes(String notes) {
    if (notes == null || notes.isEmpty())
      return;

    typeInto(inputBuildingName, notes);
  }


  public AddressListPage clickSave() {
    clickOn(btnSave);
    return switchToPage(AddressListPage.class);
  }

  public AddressListPage clickCancel() {
    clickOn(btnCancel);
    return switchToPage(AddressListPage.class);
  }

  public AddressListPage clickConfirm() {
    clickOn(btnConfirm);
    return switchToPage(AddressListPage.class);
  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
		String value = getInputValuesIndex(inputValuesIndex);

    this.selectNoFixedAbode(testData.getInputValue(TEST_DATA_PREFIX + value, "NoFixedAbode"));
    this.inputBuildingName(testData.getInputValue(TEST_DATA_PREFIX + value, "BuildingName"));
    this.inputHouseNumber(testData.getInputValue(TEST_DATA_PREFIX + value, "HouseNumber"));
    this.inputStreetName(testData.getInputValue(TEST_DATA_PREFIX + value, "StreetName"));
    this.inputDistrict(testData.getInputValue(TEST_DATA_PREFIX + value, "District"));
    this.inputTownCity(testData.getInputValue(TEST_DATA_PREFIX + value, "TownCity"));
    this.inputCounty(testData.getInputValue(TEST_DATA_PREFIX + value, "County"));
    this.inputPostcode(testData.getInputValue(TEST_DATA_PREFIX + value, "Postcode"));
    this.inputTelephoneNumber(testData.getInputValue(TEST_DATA_PREFIX + value, "TelephoneNumber"));
    this.inputStartDate(testData.getInputValue(TEST_DATA_PREFIX + value, "StartDate"));
    this.inputEndDate(testData.getInputValue(TEST_DATA_PREFIX + value, "EndDate"));
    this.selectStatus(testData.getInputValue(TEST_DATA_PREFIX + value, "Status"));
    this.inputNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "Notes"));

    //FileUtils.saveScreenshot(this.getClass().getSimpleName());

    AddressListPage addressListPage = this.clickSave();

    // A warning screen may appear if some of the details (e.g. telephone number) already exist on the system
    if (containsElements(By.xpath("//*[@value='Confirm']")))
      this.clickConfirm();

    return type.cast(addressListPage);
  }
}

package uk.gov.gsi.justice.spg.test.tests;

import dependencies.adapter.abstractstuff.CRCID;
import org.junit.Test;
import uk.gov.gsi.justice.spg.test.basetest.DeliusTestsCommon;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddressAssessmentListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddressListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.DiversityDetailsPage;
import uk.gov.gsi.justice.spg.test.txtypes.AllocateEventTransaction;
import uk.gov.gsi.justice.spg.test.txtypes.AllocateOffenderTransaction;
import uk.gov.gsi.justice.spg.test.txtypes.OffenderTransaction;

import java.util.ArrayList;
import java.util.List;

/**
 * First attempt at running tests on Selenium3 and FF52+
 */
public class AllocateOffender001_009 extends DeliusTestsCommon {

  /**
   * <a href="https://spgqa.acentria-it.com/browse/SPG-3302">SPG-3302 - AllocateOffender001</a>
   */
  @Test
  public void AllocateOffender001() {
    //Transfer a new offender with an event
    EventDetailsCommon eventDetailsCommon = initTest();
    OffenderCommon offenderCommon = updateOffender(eventDetailsCommon);
    doTransfer(offenderCommon);

    List<Class> expectedTransactions = new ArrayList<Class>() {{
      add(AllocateOffenderTransaction.class);
    }};

    assertExpectedResults(getCRN(eventDetailsCommon), CRCID.CRC17, expectedTransactions);
    System.out.println("Done");

  }

  /**
   * <a href="https://spgqa.acentria-it.com/browse/SPG-3303">SPG-3303 - AllocateOffender002</a>
   */
  @Test
  public void AllocateOffender002() {
    EventDetailsCommon eventDetailsCommon = initTest();

    DiversityDetailsPage diversityDetailsPage = updateDiversityDetails(eventDetailsCommon);

    doTransfer(diversityDetailsPage);

    List<Class> expectedTransactions = new ArrayList<Class>() {{
      add(AllocateOffenderTransaction.class);
      add(AllocateEventTransaction.class);
      add(OffenderTransaction.class);
    }};

    assertExpectedResults(getCRN(eventDetailsCommon), CRCID.CRC17, expectedTransactions);
  }

  /**
   * <a href="https://spgqa.acentria-it.com/browse/SPG-3306">SPG-3306 - AllocateOffender006</a>
   */
  @Test
  public void AllocateOffender006() {

    EventDetailsCommon eventDetailsCommon = initTest();

    AddressListPage addressListPage = addAddress(eventDetailsCommon);

    AddressAssessmentListPage addressAssessmentListPage = addAddressAssessment(addressListPage);

    doTransfer(addressAssessmentListPage);

    List<Class> expectedTransactions = new ArrayList<Class>() {{
      add(AllocateOffenderTransaction.class);
      add(AllocateEventTransaction.class);
    }};

    assertExpectedResults(getCRN(eventDetailsCommon), CRCID.CRC17, expectedTransactions);
  }
}

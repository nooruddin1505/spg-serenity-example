package uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails;

import dependencies.adapter.driver.PageElement;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.interfaces.DetailsPageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class RequirementDetailsPage extends EventDetailsCommon implements DetailsPageAssertionInterface {
  private static final String EXPECTED_VALUES_PREFIX = "ViewRequirement";

  @FindBy(xpath = "//*[@value='View Manager History']")
  WebElement btnViewManagerHistory;

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {
    {
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement", "//label[contains(text(), 'Requirement:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".RequirementSubType", "//label[contains(text(), 'Requirement Subtype:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement.ImposedDate1", "//label[contains(text(), 'Imposed (Sentence) Date:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement.Length1", "//label[contains(text(), 'Length:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement.ExpectedStartDate1", "//label[contains(text(), 'Expected Start Date:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement.ActualStartDate1", "//label[contains(text(), 'Actual Start Date:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement.Provider1", "//label[contains(text(), 'Provider:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement.Team1", "//label[contains(text(), 'Team:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement.Officer1", "//label[contains(text(), 'Officer:')]/../../div[2]/span"));
      add(new AssertionPair(EXPECTED_VALUES_PREFIX + ".Requirement.Notes", "//textarea[@id='j_id_id11:requirement:Note']"));
    }
  };

//  public RequirementManagerHistoryListPage clickViewManagerHistory() {
//    pilot.click(BUTTON_VIEW_MANAGER_HISTORY);
//    return new RequirementManagerHistoryListPage();
//  }

  @Override
  public String getValueByLabel(String label) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public String getLabelByValue(String value) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean labelExists(String label) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean valueExists(String value) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());

      if (expected == null || expected.isEmpty())
        continue;

      String actual = getDriver().findElement(By.xpath(pair.getXPath())).getText();

      Assert.assertEquals(String.format("Value is correct. Expected: %s, Actual: %s", expected, actual), expected, actual);
    }
  }
}

package uk.gov.gsi.justice.spg.test.utils.cukes;

import uk.gov.gsi.justice.spg.test.txtypes.AllocateEventTransaction;
import uk.gov.gsi.justice.spg.test.txtypes.AllocateOffenderTransaction;
import uk.gov.gsi.justice.spg.test.txtypes.OffenderTransaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by InternetUser on 24/11/2017.
 */
public class TransactionNameUtils {

  /**
   * Get a list of expected transaction class
   *
   * @param transactions
   * @return
   */
  public static List<Class> getExpectedTransactions(List<String> transactions) {
    List<Class> expectedTransactions = new ArrayList<>();

    for (String transaction : transactions) {
      Class cl = getTransactionClass(transaction);
      if (cl != null) {
        expectedTransactions.add(cl);
      }
    }
    return expectedTransactions;
  }

  /**
   * Get a single transaction class
   *
   * @param transaction
   * @return
   */
  public static List<Class> getExpectedTransactionClass(String transaction) {
    List<Class> expectedTransactions = new ArrayList<>();
    Class cl = getTransactionClass(transaction);

    if (cl != null) {
      expectedTransactions.add(cl);
    }
    return expectedTransactions;
  }

  private static Class getTransactionClass(String transaction) {
    Class expected = null;
    //transaction = transaction.replace("Transaction", "");
    if (transaction.contains("AllocateOffender")) {
      expected = AllocateOffenderTransaction.class;
    } else if (transaction.contains("AllocateEvent")) {
      expected = AllocateEventTransaction.class;
    } else if (transaction.equals("Offender")) {
      expected = OffenderTransaction.class;
    }
    return expected;
  }


}

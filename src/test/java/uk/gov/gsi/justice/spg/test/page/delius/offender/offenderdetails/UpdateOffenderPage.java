package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import dependencies.adapter.utils.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.interfaces.TablePageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.NationalSearchPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;

public class UpdateOffenderPage extends OffenderDetailsCommon implements FormPageInterface, TablePageAssertionInterface {
  private static final String TEST_DATA_PREFIX = "UpdateOffender";

  public int personaIDCount = 0;

  private static final String XPATH_SELECT_OWNING_PROVIDER = "//*[@id='updateOffenderForm:Trust']";
  private static final String XPATH_SELECT_TITLE = "//*[@id='updateOffenderForm:Title']";
  private static final String XPATH_INPUT_FIRST_NAME = "//*[@id='updateOffenderForm:FirstName']";
  private static final String XPATH_INPUT_SECOND_NAME = "//*[@id='updateOffenderForm:SecondName']";
  private static final String XPATH_INPUT_THIRD_NAME = "//*[@id='updateOffenderForm:ThirdName']";
  private static final String XPATH_INPUT_SURNAME = "//*[@id='updateOffenderForm:Surname']";
  private static final String XPATH_INPUT_PREVIOUS_NAME = "//*[@id='updateOffenderForm:PreviousSurname']";
  private static final String XPATH_SELECT_GENDER = "//*[@id='updateOffenderForm:Gender']";
  private static final String XPATH_INPUT_DATE_OF_BIRTH = "//*[@id='updateOffenderForm:DateOfBirth']";
  private static final String XPATH_INPUT_NOTES = "//*[@id='updateOffenderForm:Notes']";
  private static final String XPATH_INPUT_MOBILE_NUMBER = "//*[@id='updateOffenderForm:Mobile']";
  private static final String XPATH_SELECT_SMS_CONTACT = "//*[@id='updateOffenderForm:SMS']";
  private static final String XPATH_INPUT_TELEPHONE_NUMBER = "//*[@id='updateOffenderForm:Telephone']";
  private static final String XPATH_INPUT_EMAIL_ADDRESS = "//*[@id='updateOffenderForm:Email']";
  private static final String XPATH_INPUT_DATE_DIED = "//*[@id='DateOfDeath']";
  private static final String XPATH_INPUT_PNC_NUMBER = "//*[@id='updateOffenderForm:PNC']";
  private static final String XPATH_INPUT_CRO_NUMBER = "//*[@id='updateOffenderForm:CRO']";
  private static final String XPATH_INPUT_NOMS_NUMBER = "//*[@id='updateOffenderForm:NOMS']";
  private static final String XPATH_INPUT_NI_NUMBER = "//*[@id='updateOffenderForm:NI']";
  private static final String XPATH_BTN_ADD = "//*[@value='Add/Update']";
  private static final String XPATH_BTN_SAVE = "//input[@value='Save']";
  private static final String XPATH_BTN_CONFIRM = "//input[@value='Confirm']";
  private static final String XPATH_BTN_CANCEL = "//input[@value='Cancel']";
  private static final String XPATH_BTN_CANCEL_2 = "//*[@id='updateOffenderForm:j_id_id174']";
  private static final String XPATH_PERSONAL_ID_TYPE = "//*[@id='updateOffenderForm:identifierType']";
  private static final String XPATH_PERSONAL_ID_VALUE = "//*[@id='updateOffenderForm:identifierValue']";

  private static final String XPATH_DELETE_BY_ROW = "//table/tbody/tr[%s]/td[5]/a";

  private static final PageElement SELECT_OWNING_PROVIDER =
      new PageElement(PageElement.Type.XPATH, XPATH_SELECT_OWNING_PROVIDER, "Add Offender Owning Provider Select Field");
  private static final PageElement SELECT_TITLE = new PageElement(PageElement.Type.XPATH, XPATH_SELECT_TITLE, "Add Offender Title Select Field");
  private static final PageElement INPUT_FIRST_NAME =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_FIRST_NAME, "Add Offender First Name Input Field");
  private static final PageElement INPUT_SECOND_NAME =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_SECOND_NAME, "Add Offender Second Name Input Field");
  private static final PageElement INPUT_THIRD_NAME =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_THIRD_NAME, "Add Offender Third Name Input Field");
  private static final PageElement INPUT_SURNAME = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_SURNAME, "Add Offender Surname Input Field");
  private static final PageElement INPUT_PREVIOUS_NAME =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_PREVIOUS_NAME, "Add Offender Previous Name Input Field");
  private static final PageElement SELECT_GENDER = new PageElement(PageElement.Type.XPATH, XPATH_SELECT_GENDER, "Add Offender Gender Select Field");
  private static final PageElement INPUT_DATE_OF_BIRTH =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_DATE_OF_BIRTH, "Add Offender Date Of Birth Input Field");
  private static final PageElement INPUT_NOTES = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_NOTES, "Add Offender Notes Input Field");
  private static final PageElement INPUT_MOBILE_NUMBER =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_MOBILE_NUMBER, "Add Offender Mobile Number Input Field");
  private static final PageElement SELECT_SMS_CONTACT =
      new PageElement(PageElement.Type.XPATH, XPATH_SELECT_SMS_CONTACT, "Add Offender SMS Contact Select Field");
  private static final PageElement INPUT_TELEPHONE_NUMBER =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_TELEPHONE_NUMBER, "Add Offender Telephone Number Input Field");
  private static final PageElement INPUT_EMAIL_ADDRESS =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_EMAIL_ADDRESS, "Add Offender Email AddressTransaction Input Field");
  private static final PageElement INPUT_DATE_DIED =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_DATE_DIED, "Add Offender Date Died Input Field");
  private static final PageElement INPUT_PERSONAL_ID_TYPE =
          new PageElement(PageElement.Type.XPATH, XPATH_PERSONAL_ID_TYPE, "Add Offender Personal Identification Type Input Field");
  private static final PageElement INPUT_PERSONAL_ID_VALUE =
          new PageElement(PageElement.Type.XPATH, XPATH_PERSONAL_ID_VALUE, "Add Offender Personal Identification Type Input Field");
  private static final PageElement INPUT_PNC_NUMBER =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_PNC_NUMBER, "Add Offender PNC Number Input Field");
  private static final PageElement INPUT_CRO_NUMBER =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_CRO_NUMBER, "Add Offender CRO Number Input Field");
  private static final PageElement INPUT_NOMS_NUMBER =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_NOMS_NUMBER, "Add Offender NOMS Number Input Field");
  private static final PageElement INPUT_NI_NUMBER =
      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_NI_NUMBER, "Add Offender NI Number Input Field");
  private static final PageElement BTN_ADD = new PageElement(PageElement.Type.XPATH, XPATH_BTN_ADD, "Input Personal Identifier Add Button");
  private static final PageElement BTN_SAVE = new PageElement(PageElement.Type.XPATH, XPATH_BTN_SAVE, "Add Offender Save Button");
  private static final PageElement BTN_CONFIRM = new PageElement(PageElement.Type.XPATH, XPATH_BTN_CONFIRM, "Add Offender Confirm Button");
  private static final PageElement BTN_CANCEL = new PageElement(PageElement.Type.XPATH, XPATH_BTN_CANCEL, "Add Offender Cancel Button");
  private static final PageElement BTN_CANCEL_2 = new PageElement(PageElement.Type.XPATH, XPATH_BTN_CANCEL_2, "Add Offender Second Cancel Button");

  public void selectOwningProvider(String provider) {
    pilot.selectFromVisibleText(SELECT_OWNING_PROVIDER, provider);
  }

  public void selectTitle(String title) {
    pilot.selectFromVisibleText(SELECT_TITLE, title);
  }

  public void inputFirstName(String firstName) {
    pilot.sendKeys(INPUT_FIRST_NAME, firstName.toString());
  }

  public void inputSecondName(String secondName) {
    pilot.sendKeys(INPUT_SECOND_NAME, secondName.toString());
  }

  public void inputThirdName(String thirdName) {
    pilot.sendKeys(INPUT_THIRD_NAME, thirdName);
  }

  public void inputSurname(String surname) {
    pilot.sendKeys(INPUT_SURNAME, surname);
  }

  public void inputPreviousName(String previousName) {
    pilot.sendKeys(INPUT_PREVIOUS_NAME, previousName);
  }

  public void selectGender(String gender) {
    pilot.selectFromVisibleText(SELECT_GENDER, gender);
  }

  public void inputDateOfBirth(String dateOfBirth) {
    pilot.sendKeys(INPUT_DATE_OF_BIRTH, dateOfBirth);
  }

  public void inputNotes(String notes) {
    pilot.sendKeys(INPUT_NOTES, notes);
  }

  public void inputMobileNumber(String mobileNumber) {
    pilot.sendKeys(INPUT_MOBILE_NUMBER, mobileNumber);
  }

  public void selectSMSContact(String smsContact) {
    pilot.selectFromVisibleText(SELECT_SMS_CONTACT, smsContact);
  }

  public void inputTelephoneNumber(String telephoneNumber) {
    pilot.sendKeys(INPUT_TELEPHONE_NUMBER, telephoneNumber);
  }

  public void inputEmailAddress(String emailAddress) {
    pilot.sendKeys(INPUT_EMAIL_ADDRESS, emailAddress);
  }

  public void inputDateDied(String dateDied) {
    pilot.sendKeys(INPUT_DATE_DIED, dateDied);
  }

  public void inputPNCNumber(String pncNumber) {
    if (!pncNumber.isEmpty()) {
      pilot.sendKeys(INPUT_PNC_NUMBER, pncNumber);
    }
  }

  public void inputCRONumber(String croNumber) {
    if (!croNumber.isEmpty()) {

      pilot.sendKeys(INPUT_CRO_NUMBER, croNumber);
    }
  }

  public void inputNOMSNumber(String nomsNumber) {
    if (!nomsNumber.isEmpty()) {
      WaitUtils.isDropDownReadyForSelection(By.xpath(XPATH_PERSONAL_ID_TYPE), nomsNumber);
      pilot.selectFromVisibleText(INPUT_PERSONAL_ID_TYPE, "NOMS");
      pilot.clearAndSendKeys(INPUT_PERSONAL_ID_VALUE, nomsNumber);
      personaIDCount += 1;
      this.clickAdd();
    }
  }

  public void inputNINumber(String niNumber) {
    if (!niNumber.isEmpty()) {
      pilot.sendKeys(INPUT_NI_NUMBER, niNumber);
    }
  }

  public OffenderDetailsPage clickAdd() {
    pilot.click(BTN_ADD);
    return new OffenderDetailsPage();
  }

  public UpdateOffenderPage clickDeleteByRow(int row) {
    pilot.click(PageElement.byXpath(XPATH_DELETE_BY_ROW, "delete by row", row));
    return new UpdateOffenderPage();
  }

  public OffenderDetailsPage clickSave() {
    while (pilot.elementExists(BTN_SAVE)) {
      pilot.click(BTN_SAVE);
    }
    return new OffenderDetailsPage();
  }

  public OffenderDetailsPage clickConfirm() {
    pilot.click(BTN_CONFIRM);
    return new OffenderDetailsPage();
  }

  public NationalSearchPage clickCancel() {
    pilot.click(BTN_CANCEL);

    WebDriver driver = pilot.getWebDriver();
    driver.switchTo().alert().accept();

    return new NationalSearchPage();
  }

  public NationalSearchPage clickCancel2() {
    pilot.click(BTN_CANCEL_2);

    WebDriver driver = pilot.getWebDriver();
    driver.switchTo().alert().accept();

    return new NationalSearchPage();
  }

  public boolean isConfirmBtnVisible() {
    return pilot.isDisplayed(BTN_CONFIRM);
  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
		String value = getInputValuesIndex(inputValuesIndex);

    this.inputDateDied(testData.getInputValue(TEST_DATA_PREFIX + value, "DateDied"));
//    this.inputPNCNumber(testData.getInputValue(TEST_DATA_PREFIX + value, "PNCNo"));
//    this.inputCRONumber(testData.getInputValue(TEST_DATA_PREFIX + value, "CRONo"));
//    this.inputNOMSNumber(testData.getInputValue(TEST_DATA_PREFIX + value, "NOMSNo"));
//    this.inputNINumber(testData.getInputValue(TEST_DATA_PREFIX + value, "NINumber"));

    FileUtils.saveScreenshot(this.getClass().getSimpleName());

    return type.cast(this.clickSave());
  }

  @Override
  public boolean tableExists() {
    return false;
  }

  @Override
  public String getValueAtLocation(int row, int column) {
    return null;
  }

  @Override
  public String getValueAtLocation(int row, String columnName) {
    return null;
  }

  @Override
  public void assertTableRowNotExists(int rowLocation) {

  }

  @Override
  public void assertTableRowExists(int rowLocation) {

  }

  @Override
  public void assertPageValues() {

  }
}

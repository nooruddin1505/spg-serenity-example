package uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;

public class AddRequirementPage extends EventDetailsCommon implements FormPageInterface {
  private static final String TEST_DATA_PREFIX = "AddRequirement";

  @FindBy(id="RequirementMainCategory")
  WebElement selectRequirementMainCategory;
  @FindBy(id="RequirementSubCategory")
  WebElement selectRequirementSubCategory;
  @FindBy(id="Length")
  WebElement txtLength;
  @FindBy(id="AddSentenceComponentsForm:requirement:RequirementSentenceDate")
  WebElement txtImposedSentenceDate;
  @FindBy(id="AddSentenceComponentsForm:requirement:ExpectedStartDate")
  WebElement txtExpectedStartDate;
  @FindBy(id="AddSentenceComponentsForm:requirement:ActualStartDate")
  WebElement txtActualStartDate;
  @FindBy(id="Area")
  WebElement selectProvider;
  @FindBy(id="AddSentenceComponentsForm:requirement:Team")
  WebElement selectTeam;
  @FindBy(id="AddSentenceComponentsForm:requirement:Officer")
  WebElement selectOfficer;
  @FindBy(id="AddSentenceComponentsForm:requirement:Note")
  WebElement txtNotes;
  @FindBy(id="AddSentenceComponentsForm:requirement:ExpectedEndDate")
  WebElement txtExpectedEndDate;
  @FindBy(id="AddSentenceComponentsForm:requirement:EndDate")
  WebElement txtEndDate;
  @FindBy(id="AddSentenceComponentsForm:requirement:ComponentTermimationReason")
  WebElement selectTerminationReason;
  @FindBy(xpath="//*[@value='Add']")
  WebElement btnAdd;
  @FindBy(xpath="//*[@value='Cancel']")
  WebElement btnCancel;
  @FindBy(xpath="//*[@value='Save']")
  WebElement btnSave;

  //Dynamically select from drop down
  private static final String XPATH_TEMPLATE_SELECT_REQUIREMENT_SUBTYPE_OPTION =  "//*[@id='RequirementSubCategory']/option[contains(.,'%s')]";

  private void selectRequirement(String requirement) {
    if(requirement==null || requirement.isEmpty())
      return;
    selectFromDropdown(selectRequirementMainCategory, requirement);
  }

  private void selectRequirementSubtype(String requirementSubtype) {
    if (requirementSubtype != null && !requirementSubtype.isEmpty()) {
      WaitUtils.isDropDownReadyForSelection(By.id("RequirementSubCategory"), requirementSubtype);
      selectFromDropdown(selectRequirementSubCategory, requirementSubtype);
    }
  }

  private void inputLength(String length) {
    if (length != null || !length.isEmpty())
      typeInto(txtLength, length);
  }

  private void inputImposedSentenceDate(String imposedSentenceDate) {
    if(imposedSentenceDate==null || imposedSentenceDate.isEmpty())
      return;

    typeInto(txtImposedSentenceDate, imposedSentenceDate);
  }

  private void inputExpectedStartDate(String expectedStartDate) {
    if(expectedStartDate==null || expectedStartDate.isEmpty())
      return;
    typeInto(txtExpectedStartDate, expectedStartDate);
  }

  private void inputActualStartDate(String actualStartDate) {

    if(actualStartDate==null || actualStartDate.isEmpty())
      return;

    typeInto(txtActualStartDate, actualStartDate);
  }

  private void selectProvider(String provider) {

    if(provider==null || provider.isEmpty())
      return;
    selectFromDropdown(selectProvider, provider);
  }

  private void selectTeam(String team) {

    if(team==null || team.isEmpty())
      return;

    selectFromDropdown(selectTeam, team);
  }

  private void selectOfficer(String officer) {
    if(officer==null || officer.isEmpty())
      return;
    selectFromDropdown(selectOfficer, officer);
  }

  private void inputNotes(String notes) {
    if(notes==null || notes.isEmpty())
      return;
    typeInto(txtNotes, notes);
  }

  private void inputExpectedEndDate(String expectedEndDate) {

    if(expectedEndDate==null || expectedEndDate.isEmpty())
      return;
    typeInto(txtExpectedEndDate, expectedEndDate);
  }

  private void inputActualEndDate(String actualEndDate) {
    if(actualEndDate==null || actualEndDate.isEmpty())
      return;
    typeInto(txtExpectedEndDate, actualEndDate);
  }

  private void selectTerminationReason(String terminationReason) {
    if (terminationReason != null && !terminationReason.isEmpty())
      selectFromDropdown(selectTerminationReason, terminationReason);
  }

  private AddRequirementPage clickAdd() {
    clickOn(btnAdd);
    return switchToPage(AddRequirementPage.class);
  }

  private RequirementsListPage clickCancel() {
    clickOn(btnCancel);
    return switchToPage(RequirementsListPage.class);
  }

  private RequirementsListPage clickSave() {
    clickOn(btnSave);
    return switchToPage(RequirementsListPage.class);
  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
		String value = getInputValuesIndex(inputValuesIndex);

    this.selectRequirement(testData.getInputValue(TEST_DATA_PREFIX + value, "Requirement"));
    this.selectRequirementSubtype(testData.getInputValue(TEST_DATA_PREFIX + value, "Subtype"));

    this.inputLength(testData.getInputValue(TEST_DATA_PREFIX + value, "Length"));
    this.inputImposedSentenceDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ImposedDate"));
    this.inputExpectedStartDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ExpectedStartDate"));
    this.inputActualStartDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ActualStartDate"));
    this.selectProvider(testData.getInputValue(TEST_DATA_PREFIX + value, "Provider"));
    this.selectTeam(testData.getInputValue(TEST_DATA_PREFIX + value, "Team"));
    this.selectOfficer(testData.getInputValue(TEST_DATA_PREFIX + value, "Officer"));
    this.inputNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "RequirementNotes"));
    this.inputExpectedEndDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ExpectedEndDate"));
    this.inputActualEndDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ActualEndDate"));
    this.selectTerminationReason(testData.getInputValue(TEST_DATA_PREFIX + value, "TerminationReason"));
    this.clickAdd();

    //FileUtils.saveScreenshot(this.getClass().getSimpleName());

    return type.cast(this.clickSave());
  }
}

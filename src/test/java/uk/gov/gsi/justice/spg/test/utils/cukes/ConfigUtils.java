package uk.gov.gsi.justice.spg.test.utils.cukes;

import dependencies.adapter.TestDataLoader;
import dependencies.adapter.abstractstuff.TestData;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import uk.gov.gsi.justice.spg.test.session.SessionKey;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * Created by InternetUser on 27/11/2017.
 */
public class ConfigUtils {
  public static final String RESOURCE_LOCATION = "src/test/resources/";
  public static final String TYPE_PROPERTIES_FILE = "properties";
  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();

  /**
   * Reads all the properties from specified file
   * @param file
   * @return
   */
  public static Properties load(String file) {
    if(!file.contains(TYPE_PROPERTIES_FILE)){
      file = file + "." + TYPE_PROPERTIES_FILE;
    }
    Properties prop = new Properties();
    InputStream input = null;

    try {
      input = new FileInputStream(RESOURCE_LOCATION + file);

      // load a properties file
      prop.load(input);

      // get the property value and print it out
      //System.out.println(prop.getProperty("XPath.SenderIdentity"));

    } catch (IOException ex) {
      ex.printStackTrace();
    }

    return prop;
  }

  /**
   * Update with our own configuration file
   * @param prop
   */
  public static void updateGlobalConfigProperties(Properties prop) {
    if(prop!=null){
      for(Map.Entry entry: prop.entrySet()){
        String k = entry.getKey().toString();
        String v = entry.getValue().toString();

        if(configData.getProperty(k) == null)
          configData.setProperty(k,v);
        //else
        //  System.out.println("Config already loaded from another file : " + k);
      }
    }
  }

  public static TestData loadTestDataFiles() {
    TestData testData = null;
    try {
      Map<String, String> mapOfDataFiles = (Map<String, String>) Serenity.getCurrentSession().get(SessionKey.mapOfDataFiles);
      if(mapOfDataFiles!=null && testData==null) {
        String fileName = mapOfDataFiles.get("fileName1");
        String filePath = mapOfDataFiles.get("location") + File.separator + fileName;
        System.out.println("Using datasheet : " + filePath);
        testData = TestDataLoader.load(filePath, "", "");
        Serenity.getCurrentSession().put(SessionKey.testData, testData);

        fileName = fileName.split("\\.")[0];
        Serenity.getCurrentSession().put(SessionKey.testDataFileLocation, fileName);
      }
    }catch (Exception e){
      //We didn't supply the test data
    }
    return testData;
  }

  public static void loadAdditionalPropertyFiles() {
    try {
      //Load any additional properties files
      String configFiles = configData.getProperty("config.files");
      if(!TextUtils.isEmptyOrNull(configFiles)){
        String[] listOfConfigFiles = configFiles.split(",");

        for(String file: listOfConfigFiles) {
          Properties prop = ConfigUtils.load(file);
          ConfigUtils.updateGlobalConfigProperties(prop);
        }

      }

    }catch (Exception e){
      //We didn't supply the test data
    }
  }
}

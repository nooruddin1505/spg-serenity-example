package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import org.junit.Assert;
import org.openqa.selenium.By;
import uk.gov.gsi.justice.spg.test.interfaces.DetailsPageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class RegistrationReviewDetailsPage extends OffenderDetailsCommon implements DetailsPageAssertionInterface {
  private static final String XPATH_GET_VALUE_BY_LABEL = "//main[@id='content']/section/div/label[contains(.,'%s')]/following-sibling::div/span";

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("RegistrationReviewDetails.RegisterType", "//main[@id='content']/section/div/label[contains(.,'Register Type')]/following-sibling::div/span"));
    add(new AssertionPair("RegistrationReviewDetails.ReviewingOfficerProvider", "//main[@id='content']/section/div/label[contains(.,'Provider')]/following-sibling::div/span"));
    add(new AssertionPair("RegistrationReviewDetails.ReviewingOfficerTeam", "//main[@id='content']/section/div/label[contains(.,'Team')]/following-sibling::div/span"));
    add(new AssertionPair("RegistrationReviewDetails.ReviewedBy", "//main[@id='content']/section/div/label[contains(.,'Reviewed By')]/following-sibling::div/span"));
    add(new AssertionPair("RegistrationReviewDetails.DateOfReview", "//main[@id='content']/section/div/label[contains(.,'Date of Review')]/following-sibling::div/span"));
    add(new AssertionPair("RegistrationReviewDetails.TimeOfReview", "//main[@id='content']/section/div/label[contains(.,'Time of Review')]/following-sibling::div/span"));
    add(new AssertionPair("RegistrationReviewDetails.NextReviewDate", "//main[@id='content']/section/div/label[contains(.,'Next Review Date')]/following-sibling::div/span"));
    add(new AssertionPair("RegistrationReviewDetails.ReviewNotes", "//*[@id='registrationReviewForm:Notes']"));
    add(new AssertionPair("RegistrationReviewDetails.Completed", "//main[@id='content']/section/div/label[contains(.,'Completed?')]/following-sibling::div/span"));
  }};

  @Override
  public String getValueByLabel(String label) {
    return pilot.getText(PageElement.byXpath(XPATH_GET_VALUE_BY_LABEL, "RegistrationReviewDetailsPage get value by description", label));
  }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = getDriver().findElement(By.xpath(pair.getXPath())).getText();
      //String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "RegistrationReviewDetailsPage get value by description"));

      if (expected == null || expected.isEmpty())
        continue;

      //DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "RegistrationReviewDetailsPage");
      Assert.assertEquals(String.format("Value is correct. Expected: %s, Actual: %s", expected, actual), expected, actual);
    }
  }

  @Override
  public String getLabelByValue(String value) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean labelExists(String label) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean valueExists(String value) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }
}


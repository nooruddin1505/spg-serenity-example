package uk.gov.gsi.justice.spg.test.utils;

import dependencies.adapter.abstractstuff.CRCID;
import dependencies.adapter.utils.FileCopyResults;
import dependencies.adapter.utils.FileUtils;
import dependencies.adapter.utils.SleepUtils;
import dependencies.adapter.utils.XMLUtils;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.xml.sax.SAXException;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.actions.TransactionAction;
import uk.gov.gsi.justice.spg.test.exceptions.NoTransactionFoundException;
import uk.gov.gsi.justice.spg.test.exceptions.UnknownTransactionTypeException;
import uk.gov.gsi.justice.spg.test.txtypes.BaseTransaction;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to provide static methods for obtaining and parsing XML messages (transactions) from the CRCs.
 */
public class TransactionUtils {

  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();

  /**
   * Obtains a list of outbound transactions from a CRC server using the given CRN and CRC ID.
   *
   * @param caseReferenceNumber the CRN to use when searching for the transactions.
   * @param crcID               the ID of the CRC to search for the transactions.
   * @return a list of transactions which match the given CRN.
   * @throws UnknownTransactionTypeException thrown when a transaction is found but a transaction type does not exist for it.
   * @throws NoTransactionFoundException     thrown when no transactions match the CRN searched for.
   */
  public static List<BaseTransaction> getTransactions(String caseReferenceNumber, CRCID crcID) throws UnknownTransactionTypeException, NoTransactionFoundException {
    SleepUtils.longSleep(); // Sleep here to allow messages to get through the SPG.
    String host = CRCUtils.getHostName(crcID);
    String path = configData.getProperty("Remote.TxAudit.Path") + crcID.toString().replace("RC", "");
    String localName = FileUtils.getFileName(FileUtils.FileType.TRANSACTION, caseReferenceNumber);

    ArrayList<FileCopyResults> fcrTransactions = FileUtils.getMatchingTxFiles(host, path, caseReferenceNumber, localName);

    System.out.println("getTransactions found " + fcrTransactions.size() + " message(s) sent to " + crcID.toString());

    List<BaseTransaction> transactions = new ArrayList<>();
    for (FileCopyResults fileCopyResult : fcrTransactions) {
      BaseTransaction tx = TransactionUtils.parseTransaction(fileCopyResult.getUri().toString());

      transactions.add(tx);

      DeliusBaseTestAction.reportTransactionTypeFound(tx.getTransactionName(), getAmount(tx.getClass(), transactions));
    }

    return transactions;
  }

  public static List<BaseTransaction> getTransactions(String caseReferenceNumber, CRCID crcID, String schemaVersion) throws UnknownTransactionTypeException, NoTransactionFoundException {
    SleepUtils.longSleep(); // Sleep here to allow messages to get through the SPG.
    String host = CRCUtils.getHostName(schemaVersion);
    String path = configData.getProperty("Remote.TxAudit.Path") + crcID.toString().replace("RC", "");
    String localName = FileUtils.getFileName(FileUtils.FileType.TRANSACTION, caseReferenceNumber);

    ArrayList<FileCopyResults> fcrTransactions = FileUtils.getMatchingTxFiles(host, path, caseReferenceNumber, localName);

    System.out.println("getTransactions found " + fcrTransactions.size() + " message(s) sent to " + crcID.toString());

    List<BaseTransaction> transactions = new ArrayList<>();
    for (FileCopyResults fileCopyResult : fcrTransactions) {
      BaseTransaction tx = TransactionUtils.parseTransaction(fileCopyResult.getUri().toString());

      transactions.add(tx);

      DeliusBaseTestAction.reportTransactionTypeFound(tx.getTransactionName(), getAmount(tx.getClass(), transactions));
    }

    return transactions;
  }

  /**
   * USE WITH POLLING: Only waits for the transactions we are interested in
   * Obtains a list of outbound transactions from a CRC server using the given CRN and CRC ID.
   *
   * @param caseReferenceNumber the CRN to use when searching for the transactions.
   * @param crcID               the ID of the CRC to search for the transactions.
   * @return a list of transactions which match the given CRN.
   * @throws UnknownTransactionTypeException thrown when a transaction is found but a transaction type does not exist for it.
   * @throws NoTransactionFoundException     thrown when no transactions match the CRN searched for.
   */
  public static List<BaseTransaction> getTransactionsUseWithPolling(String caseReferenceNumber, CRCID crcID) throws UnknownTransactionTypeException, NoTransactionFoundException {
    String host = CRCUtils.getHostName(crcID);
    String path = configData.getProperty("Remote.TxAudit.Path") + crcID.toString().replace("RC", "");
    String localName = FileUtils.getFileName(FileUtils.FileType.TRANSACTION, caseReferenceNumber);

    ArrayList<FileCopyResults> fcrTransactions = FileUtils.getMatchingTxFiles(host, path, caseReferenceNumber, localName);

    System.out.println("getTransactions found " + fcrTransactions.size() + " message(s) sent to " + crcID.toString());

    List<BaseTransaction> transactions = new ArrayList<>();
    for (FileCopyResults fileCopyResult : fcrTransactions) {
      BaseTransaction tx = TransactionUtils.parseTransaction(fileCopyResult.getUri().toString());

      transactions.add(tx);

      DeliusBaseTestAction.reportTransactionTypeFound(tx.getTransactionName(), getAmount(tx.getClass(), transactions));
    }

    return transactions;
  }

  /**
   * Obtains a list of responses from inbound messages for a CRC server using a given CRN and CRC ID.
   *
   * @param controlReference the CRN to use when searching for the responses.
   * @param crcID            the ID of the CRC to search for the responses.
   * @return a list of response transactions which match the given CRN.
   * @throws UnknownTransactionTypeException thrown when a transaction is found but a transaction type does not exist for it.
   * @throws NoTransactionFoundException     thrown when no transactions match the CRN searched for,
   */
  public static List<BaseTransaction> getResponses(String controlReference, CRCID crcID) throws UnknownTransactionTypeException, NoTransactionFoundException {
    SleepUtils.longSleep(); // Sleep here to allow messages to get through the SPG.
    String host = CRCUtils.getHostName(crcID);
    String path = configData.getProperty("Remote.TxAudit.Path") + crcID.toString().replace("RC", "");
    String localName = FileUtils.getFileName(FileUtils.FileType.RESPONSE, controlReference);

    ArrayList<FileCopyResults> fcrResponses = FileUtils.getResponses(host, path, controlReference, localName);

    System.out.println("getResponses found " + fcrResponses.size() + " message(s) sent to " + crcID.toString());

    List<BaseTransaction> responses = new ArrayList<>();
    for (FileCopyResults fileCopyResult : fcrResponses) {
      BaseTransaction res = TransactionUtils.parseTransaction(fileCopyResult.getUri().toString());

      responses.add(res);

      DeliusBaseTestAction.reportTransactionTypeFound(res.getTransactionName(), getAmount(res.getClass(), responses));
    }

    return responses;
  }

  public static List<BaseTransaction> getResponses(String controlReference, CRCID crcID, String schemaVersion) throws UnknownTransactionTypeException, NoTransactionFoundException {
    SleepUtils.sleep(); // Sleep here to allow messages to get through the SPG.
    String host = CRCUtils.getHostName(schemaVersion);
    String path = configData.getProperty("Remote.TxAudit.Path") + crcID.toString().replace("RC", "");
    String localName = FileUtils.getFileName(FileUtils.FileType.RESPONSE, controlReference);

    ArrayList<FileCopyResults> fcrResponses = FileUtils.getResponses(host, path, controlReference, localName);

    System.out.println("getResponses found " + fcrResponses.size() + " message(s) sent to " + crcID.toString());

    List<BaseTransaction> responses = new ArrayList<>();
    for (FileCopyResults fileCopyResult : fcrResponses) {
      BaseTransaction res = TransactionUtils.parseTransaction(fileCopyResult.getUri().toString());

      responses.add(res);

      DeliusBaseTestAction.reportTransactionTypeFound(res.getTransactionName(), getAmount(res.getClass(), responses));
    }

    return responses;
  }


  public static List<BaseTransaction> getResponsesWithPolling(String controlReference, CRCID crcID, String schemaVersion) throws UnknownTransactionTypeException, NoTransactionFoundException {
    String host = CRCUtils.getHostName(schemaVersion);
    String path = configData.getProperty("Remote.TxAudit.Path") + crcID.toString().replace("RC", "");
    String localName = FileUtils.getFileName(FileUtils.FileType.RESPONSE, controlReference);

    ArrayList<FileCopyResults> fcrResponses = FileUtils.getResponses(host, path, controlReference, localName);

    System.out.println("getResponses found " + fcrResponses.size() + " message(s) sent to " + crcID.toString());

    List<BaseTransaction> responses = new ArrayList<>();
    for (FileCopyResults fileCopyResult : fcrResponses) {
      BaseTransaction res = TransactionUtils.parseTransaction(fileCopyResult.getUri().toString());

      responses.add(res);

      DeliusBaseTestAction.reportTransactionTypeFound(res.getTransactionName(), getAmount(res.getClass(), responses));
    }

    return responses;
  }


  /**
   * Gets the number of currently found transactions in the given list which match the given transaction type.
   *
   * @param transactionClass the current transaction type.
   * @param transactions     the list of currently found transactions.
   * @return the number of transaction type within the list of transactions.
   */
  public static int getAmount(Class transactionClass, List<BaseTransaction> transactions) {
    int count = 0;

    for (BaseTransaction transaction : transactions) {
      if (transaction.getClass().equals(transactionClass)) {
        count++;
      }
    }

    return count;
  }

  /**
   * Parses an XML transaction at the given path to a Transaction object which allows data on the transaction to be accessed.
   *
   * @param transactionFilePath the path to the XML transaction file.
   * @return a BaseTransaction containing information on the transaction file.
   * @throws UnknownTransactionTypeException thrown when a transaction isn't recognised.
   * @throws NoTransactionFoundException     thrown when the parsing was unsuccessful.
   */
  private static BaseTransaction parseTransaction(String transactionFilePath) throws UnknownTransactionTypeException, NoTransactionFoundException {
    String transactionClassName = "";
    String xPath = "/SPGInterchange/SPGMessage/*[2]";

    try {
      XMLUtils xmlUtils = new XMLUtils(transactionFilePath);
      transactionClassName = "uk.gov.gsi.justice.spg.test.txtypes." + xmlUtils.getNodeName(xPath) + "Transaction";
      BaseTransaction tx = (BaseTransaction) Class.forName(transactionClassName).newInstance();
      tx.setTransactionFilePath(transactionFilePath);
      tx.setXMLUtils(xmlUtils);
      return tx;
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
      throw new UnknownTransactionTypeException(transactionFilePath, "Could not find transaction type with name: " + transactionClassName);
    } catch (SAXException e) {
      TransactionAction.reportSAXExceptionCaught("n/a", transactionFilePath, xPath, "n/a", e);
    } catch (ParserConfigurationException e) {
      TransactionAction.reportParserConfigurationExceptionCaught("n/a", transactionFilePath, xPath, "n/a", e);
    } catch (IOException e) {
      TransactionAction.reportIOExceptionCaught("n/a", transactionFilePath, xPath, "n/a", e);
    }

    throw new NoTransactionFoundException("Could not find transaction at path: " + transactionFilePath);
  }

  /**
   * SEARCH list of transaction and get correct file name in our local drive
   * <p>
   * So we can query it
   *
   * @param expectedTransactionFileName
   * @param transactions
   * @param messageType
   * @return
   */
  public static String getTransactionFileName(String expectedTransactionFileName, List<BaseTransaction> transactions, String messageType) {
    expectedTransactionFileName = expectedTransactionFileName.replace("Transaction", "");
    String fileName = null;
    String messagePath = configData.getProperty("XPath.MessageType");
    for (BaseTransaction sourceTransaction : transactions) {
      String v = sourceTransaction.getXMLUtils().getNodeTextContent(messagePath);
      if (v.equals(messageType)) {
        String txName = sourceTransaction.getTransactionName();
        if (txName.contains(expectedTransactionFileName)) {
          fileName = sourceTransaction.getTransactionFilePath();
          break;
        }
      }
    }

    return fileName;
  }

  /**
   * Check specified xpath contains the value
   * <p>
   * This way we can identify the correct Entity if transaction contains more than 1
   * <p>
   * Example Contact can be more than 1
   *
   * @param xpath
   * @param valueToFind
   * @param targetXMLPath
   * @return
   */
  public static boolean containsXPathWithSpecifiedValue(String xpath, String valueToFind, String targetXMLPath) {
    boolean found = false;
    try {
      XMLUtils targetXMLUtils = new XMLUtils(targetXMLPath);
      String nodeTextContent = targetXMLUtils.getNodeTextContent(xpath);
      if (nodeTextContent.contains(valueToFind)) {
        found = true;
      }
    } catch (Exception e) {
      //e.printStackTrace();
    }
    return found;
  }

}

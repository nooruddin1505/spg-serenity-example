package uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails;

import dependencies.adapter.driver.PageElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.interfaces.DetailsPageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class EventDetailsPage extends EventDetailsCommon implements DetailsPageAssertionInterface {
  private static final String EXPECTED_VALUES_PREFIX = "EventDetails.";

  @FindBy(xpath = ".//*[@value='Requirements']")
  WebElement btnRequirements;
  @FindBy(xpath = ".//*[@value='Document']")
  WebElement btnDocument;
  @FindBy(xpath = ".//*[@value='Search DSS']")
  WebElement btnSearchDSS;
  @FindBy(xpath = ".//*[@value='OGRS']")
  WebElement btnOGRS;
  @FindBy(xpath = ".//*[@value='Close']")
  WebElement btnClose;

  @FindBy(xpath = "//span[contains(text(), 'Recall Date')]/../div/span/text()")
  WebElement labelRecalDate;
  @FindBy(xpath = "//span[contains(text(), 'Recall Reason')]/../div/span/text()")
  WebElement labelRecalReason;

  //Dynamically find elements
  private static final String XPATH_LABEL_BY_NAME = "//span[contains(text(),'%s')]/../div/span";

  public RequirementsListPage clickRequirementsButton() {
    clickOn(btnRequirements);
    return switchToPage(RequirementsListPage.class);
  }

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "ReleaseDate", "//main/section/div[15]/div/span[contains(.,'Release Date')]/following-sibling::div"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "ReleaseReason", "//main/section/div[15]/div/span[contains(.,'Release Reason')]/following-sibling::div"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "ReturnToCustodyReason", "//*[@id='j_id_id11:RecallReason']"));
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "ReturnToCustodyDate", "//*[@id='j_id_id11:RecallReasonDate']"));
  }};


  private String getLabelByName(String name) {
    return pilot.getText(PageElement.byXpath(XPATH_LABEL_BY_NAME, "EventDetailsPage get label by name", name));
  }

  public void assertPage() {
    String actualReturnToCustodyDateValue = this.getLabelByName("Return To Custody Date:");
    String expectedRecallDateValue = testData.getExpectedResult(EXPECTED_VALUES_PREFIX + "RecallDate");
    assertor.assertEquals(String.format("NonStatutoryIntervention. Expected: '%s', Actual: '%s'", expectedRecallDateValue, actualReturnToCustodyDateValue),
                          expectedRecallDateValue, actualReturnToCustodyDateValue);

    String actualRecallReasonValue = this.getLabelByName("Recall Reason:");
    String expectedRecallReasonValue = testData.getExpectedResult(EXPECTED_VALUES_PREFIX + "RecallReason");
    assertor.assertEquals(
        String.format("NonStatutoryInterventionSubType. Expected: '%s', Actual: '%s'", expectedRecallReasonValue, actualRecallReasonValue),
        expectedRecallReasonValue, actualRecallReasonValue);

  }

  @Override
  public String getValueByLabel(String label) { throw new UnsupportedOperationException("Not implemented, yet"); }

  @Override
  public String getLabelByValue(String value) { throw new UnsupportedOperationException("Not implemented, yet"); }

  @Override
  public boolean labelExists(String label) { throw new UnsupportedOperationException("Not implemented, yet"); }

  @Override
  public boolean valueExists(String value) { throw new UnsupportedOperationException("Not implemented, yet"); }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());

      if (expected == null || expected.isEmpty()) {
        continue;
      }

      String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "EventDetailsPage get value by description"));

      assertor.assertEquals(String.format("Value '%s' is correct. Expected: %s, Actual: %s", pair.getDataHeading(), expected, actual), expected, actual);}
    }

}



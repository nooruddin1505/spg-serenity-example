package uk.gov.gsi.justice.spg.test.utils.cukes;

import org.apache.commons.lang.StringUtils;

/**
 * Created by InternetUser on 23/11/2017.
 */
public class TextUtils {

  public static boolean isEmpty(String str){
    return StringUtils.isEmpty(str);
  }

  public static boolean isEmptyOrNull(String str){
    return isEmpty(str) || str == null;
  }
}

package uk.gov.gsi.justice.spg.test.actions;

import dependencies.adapter.abstractstuff.BaseAction;
import dependencies.adapter.driver.ExecutionContext;

/**
 * This class defines 'Actions' for events which occur from within DeliusBasePage or any of its child page objects.
 * <p>
 * Actions are used for outputting traces of events into the Virtuoso report. Each Action called will add a new row to the
 * report containing the given description text.
 *
 * @see BaseAction
 */
public class DeliusBasePageAction extends BaseAction {

  /**
   * Constructor which adds the given description to the Virtuoso report.
   *
   * @param description text to display in the Virtuoso report.
   */
  public DeliusBasePageAction(String description) {
    super(description);

    if (configData.getPropertyAsBoolean("Print.Actions", false)) {
      System.out.println(description);
    }
  }

  /**
   * Should be called when a JavaScript alert is expected to be present at a certain point on the page but isn't.
   *
   * @return a new action describing the event.
   */
  public static DeliusBasePageAction reportAlertNotPresent() {
    return new DeliusBasePageAction("No alert present on this page");
  }
}

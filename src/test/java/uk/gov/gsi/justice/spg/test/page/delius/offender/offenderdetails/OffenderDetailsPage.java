package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import dependencies.adapter.utils.SleepUtils;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.DetailsPageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class OffenderDetailsPage extends OffenderDetailsCommon implements DetailsPageAssertionInterface {

    @FindBy(xpath="//input[@value='Document']")
    WebElementFacade btnDocument;
    @FindBy(xpath="//input[@value='Update']")
    WebElementFacade btnUpdate;
    @FindBy(xpath="//input[@value='Delete']")
    WebElementFacade btnDelete;
    @FindBy(xpath="//input[@value='Close']")
    WebElementFacade btnClose;
    @FindBy(xpath="//*[@id='offender-overview']/div[3]/div[1]/a/i")
    WebElementFacade flagOffenderLAOSummary;

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
      //Personal
      add(new AssertionPair("OffenderDetails.Title", "//span[contains(text(),'Title:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.FirstName", "//span[contains(text(),'First Name:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.SecondName", "//span[contains(text(),'Second Name:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.ThirdName", "//span[contains(text(),'Third Name:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.SurnameOrFamilyName", "//span[contains(text(),'Surname or Family Name:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.PreviousName", "//span[contains(text(),'Previous Name:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.Gender", "//span[contains(text(),'Gender:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.DateOfBirth", "//span[contains(text(),'Date of Birth:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.Notes", "//*[@id='SearchForm:Notes']"));
      //Contact
      add(new AssertionPair("OffenderDetails.MobileNumber", "//span[contains(text(),'Mobile Number:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.SMSContact", "//span[contains(text(),'SMS Contact:')]/../div"));
      add(new AssertionPair("OffenderDetails.TelephoneNumber", "//span[contains(text(),'Telephone Number:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.EmailAddress", "//span[contains(text(),'Email address:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.DateDied", "//span[contains(text(),'Date Died:')]/../div/span"));
      //Identifiers
      add(new AssertionPair("OffenderDetails.PNCNumber", "//span[contains(text(),'PNC Number:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.CRONumber", "//span[contains(text(),'CRO Number:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.NOMSNumber", "//span[contains(text(),'NOMS Number:')]/../div/span"));
      add(new AssertionPair("OffenderDetails.NINumber", "//span[contains(text(),'NI Number:')]/../div/span"));
    }};



//  public DocumentListPage clickDocument() {
//    pilot.click(BTN_DOCUMENT);
//    return new DocumentListPage();
//  }

  public UpdateOffenderPage clickUpdate() {
    clickOn(btnUpdate);
    return switchToPage(UpdateOffenderPage.class);
  }

//  public DeleteOffenderPage clickDelete() {
//    pilot.click(BTN_DELETE);
//    return new DeleteOffenderPage();
//  }
//
//  public OffenderSummaryPage clickClose() {
//    pilot.click(BTN_CLOSE);
//    return new OffenderSummaryPage();
//  }
//
//    public OffenderLAOSummaryPage FlagLAOSummary() {
//        pilot.click(FLAG_OFFENDERLAOSUMMARY);
//        return new OffenderLAOSummaryPage();
//    }
  public void refreshPage() {
    SleepUtils.shortSleep();
    clickOffenderIndexLink();
  }

  @Override
  public String getValueByLabel(String label) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public String getLabelByValue(String value) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean labelExists(String label) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public boolean valueExists(String value) {
    throw new UnsupportedOperationException("Not implemented, yet");
  }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "OffenderDetailsPage get value by description"));

      if (expected == null || expected.isEmpty())
        expected = actual;

      DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "OffenderDetailsPage");

      assertor.assertEquals(String.format("%s is correct. Expected: '%s', Actual: '%s'", pair.getDataHeading(), expected, actual), expected, actual);
    }
  }
}

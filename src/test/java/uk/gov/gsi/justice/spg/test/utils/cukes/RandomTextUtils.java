package uk.gov.gsi.justice.spg.test.utils.cukes;

import org.apache.commons.lang.StringUtils;

import java.util.Calendar;

/**
 * Created by InternetUser on 16/11/2017.
 */
public class RandomTextUtils {

  public static void main(String[] args){
    String randomText = getRandomText();
    System.out.println(randomText);
    randomText = getRandomText("Noor");
    System.out.println(randomText);
    randomText = getRandomText(null, true);
    System.out.println(randomText);
  }

  public static String getRandomText(){
    return getRandomText(null, false);
  }

  public static String getRandomText(String beginsWith){
    return getRandomText(beginsWith, false);
  }
  /**
   * Generates text based on date and long time
   * @return
   */
  public static String getRandomText(String beginsWith, boolean useDateValues){
    if(beginsWith == null){
      beginsWith = "";
    }

    //Create random name based on date
    String text = "";
    if(!useDateValues){
      text = String.valueOf(System.nanoTime());
    }else {
      Calendar instance = Calendar.getInstance();
      //User todays date
      text = instance.get(Calendar.MONTH) + 1 + "" + instance.get(Calendar.DAY_OF_MONTH) + "" + instance.get(Calendar.YEAR)
              + instance.get(Calendar.HOUR_OF_DAY) + 1 + "" + instance.get(Calendar.MINUTE) + "" + instance.get(Calendar.YEAR);
    }
    text = convertNumbersToString(text);

    return StringUtils.capitalize(beginsWith+text);
  }

  /**
   * Replaces each number to a random String
   *
   * 11/16/2017/13:130:2017 -> 11162017131302017 -> Aaafbagacacbag
   *
   * @param str
   * @return
   */
  public static String convertNumbersToString(String str) {
    System.out.println("Convert Date String to Name : " + str);
    String[] alphas = {
            "a","b","c","d","e","f","g","h","i","j",
            "j","k","l","m","n","o","p","q","r","s",
            "t","u","v","w","x","y","z","a","b","c",
    };
    String val = "";
    for(Character c: str.toCharArray()){
      String xx = String.valueOf(c);
      int x = Integer.parseInt(xx);
      if(x > 0){
        val = val + alphas[x-1];
      }
    }
    return  val;
  }
}

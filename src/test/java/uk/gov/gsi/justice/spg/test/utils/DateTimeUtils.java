package uk.gov.gsi.justice.spg.test.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class which defines static methods which support operations relating to dates and times when interacting with Delius and the SPG.
 */
public class DateTimeUtils {

  /**
   * Method to return today's date in the Delius-compatible format: dd/MM/yyyy.
   *
   * @return the current date in Delius format.
   */
  public static String getCurrentDate(Application app) {
    DateFormat dateFormat;

    if (app == Application.SPG) {
      dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    } else {
      dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    }

    Date date = new Date();
    return dateFormat.format(date);
  }

  /**
   * Method to return the current time in a Delius-compatible format: HH:mm.
   *
   * @return the current time in Delius format.
   */
  public static String getCurrentTime(Application app) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf;

    if (app == Application.SPG) {
      sdf = new SimpleDateFormat("HH:mm:ss");
    } else {
      sdf = new SimpleDateFormat("HH:mm");
    }
    return( sdf.format(cal.getTime()) );
  }

  public static void addDelay(long duration) throws InterruptedException {

    System.out.println("Waiting for " + duration / 60 + " minutes to allow Ref Data messages to reach the SPG");

    do {
      long minutes = duration / 60;
      long seconds = duration % 60;
      Thread.sleep(1000);
      duration = duration - 1;

      if (duration % 30 == 0) {
        System.out.println(minutes + " minute(s), " + seconds + " second(s)");
      }
    }

    while (duration != 0);
    System.out.println("Time's Up!");
  }

  public enum Application {
    SPG,
    DELIUS
  }
}

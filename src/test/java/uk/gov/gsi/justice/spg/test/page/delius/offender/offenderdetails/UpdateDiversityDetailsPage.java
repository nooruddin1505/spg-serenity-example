package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;

public class UpdateDiversityDetailsPage extends OffenderDetailsCommon implements FormPageInterface {
  private static final String TEST_DATA_PREFIX = "UpdateOffenderEquality";

  @FindBy(id = "diversityForm:sexualOrientation")
  WebElementFacade selectSexualOrientation;
  @FindBy(id = "diversityForm:remMainCategory")
  WebElementFacade selectEthinicity;
  @FindBy(id = "diversityForm:transgenderProcess")
  WebElementFacade selectTransgenderProcess;
  @FindBy(id = "diversityForm:consentToDisclose")
  WebElementFacade selectConsentToDisclose;
  @FindBy(id = "diversityForm:remMainCategory")
  WebElementFacade selectRemMainCategory;
  @FindBy(id = "diversityForm:nationality")
  WebElementFacade selectNationality;
  @FindBy(id = "diversityForm:secondNationality")
  WebElementFacade selectSecondNationality;
  @FindBy(id = "diversityForm:immigrationStatus")
  WebElementFacade selectImmigrationStatus;
  @FindBy(id = "diversityForm:immigrationNumber")
  WebElementFacade inputImmigrationNumber;
  @FindBy(id = "diversityForm:language")
  WebElementFacade selectLanguage;
  @FindBy(id = "diversityForm:interpreterRequired")
  WebElementFacade selectInterpreterRequired;
  @FindBy(id = "diversityForm:religion")
  WebElementFacade selectReligion;
  @FindBy(id = "diversityForm:newNotes")
  WebElementFacade inputNewNotes;

  //Buttons
  @FindBy(id = "diversityForm:saveButton")
  WebElementFacade btnSave;
  @FindBy(id = "diversityForm:confirmButton")
  WebElementFacade btnConfirm;
  @FindBy(id = "diversityForm:cancelButton")
  WebElementFacade btnCancel;

  private void selectSexualOrientation(String sexualOrientation) {
    if (sexualOrientation == null || sexualOrientation.isEmpty())
      return;
    selectFromDropdown(selectSexualOrientation, sexualOrientation);
  }

  private void selectEthnicity(String ethnicity) {
    if (ethnicity == null || ethnicity.isEmpty())
      return;
    selectFromDropdown(selectEthinicity, ethnicity);
  }

  private void selectTransGenderProcess(String transGenderProcess) {
    if (transGenderProcess == null || transGenderProcess.isEmpty())
      return;
    selectFromDropdown(selectTransgenderProcess, transGenderProcess);
  }

  private void selectConsentToDisclose(String consentToDisclose) {
    if (consentToDisclose == null || consentToDisclose.isEmpty())
      return;
    selectFromDropdown(selectConsentToDisclose, consentToDisclose);
  }

  public void selectREMMainCategory(String remMainCategory) {
    if (remMainCategory == null || remMainCategory.isEmpty())
      return;
    selectFromDropdown(selectRemMainCategory, remMainCategory);
  }

  public void selectNationality(String nationality) {
    if (nationality == null || nationality.isEmpty())
      return;
    selectFromDropdown(selectNationality, nationality);
  }

  public void selectSecondNationality(String secondNationality) {
    if (secondNationality == null || secondNationality.isEmpty())
      return;
    selectFromDropdown(selectSecondNationality, secondNationality);
  }

  public void selectImmigrationStatus(String immigrationStatus) {
    if (immigrationStatus == null || immigrationStatus.isEmpty())
      return;
    selectFromDropdown(selectImmigrationStatus, immigrationStatus);
  }

  public void inputImmigrationNumber(String immigrationNumber) {
    if (immigrationNumber == null || immigrationNumber.isEmpty())
      return;
    typeInto(inputImmigrationNumber, immigrationNumber);
  }

  public void selectLanguage(String language) {
    if (language == null || language.isEmpty())
      return;
    selectFromDropdown(selectLanguage, language);
  }

  public void selectInterpreterRequired(String interpreterRequired) {
    if (interpreterRequired == null || interpreterRequired.isEmpty())
      return;
    selectFromDropdown(selectInterpreterRequired, interpreterRequired);
  }

  public void selectReligion(String religion) {
    if (religion == null || religion.isEmpty())
      return;
    selectFromDropdown(selectReligion, religion);
  }

  public void inputNotes(String notes) {
    if (notes == null || notes.isEmpty())
      return;
    typeInto(inputNewNotes, notes);
  }

  public DiversityDetailsPage clickButtonSave() {
    clickOn(btnSave);
    return switchToPage(DiversityDetailsPage.class);
  }

  public DiversityDetailsPage clickButtonConfirm() {
    clickOn(btnConfirm);
    return switchToPage(DiversityDetailsPage.class);
  }

  public DiversityDetailsPage clickButtonClose() {
    clickOn(btnCancel);
    return switchToPage(DiversityDetailsPage.class);
  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
    String value = getInputValuesIndex(inputValuesIndex);

    this.selectSexualOrientation(testData.getInputValue(TEST_DATA_PREFIX + value, "SexualOrientation"));
    this.selectEthnicity(testData.getInputValue(TEST_DATA_PREFIX + value, "Ethnicity"));
    this.selectTransGenderProcess(testData.getInputValue(TEST_DATA_PREFIX + value, "TransGenderProcess"));
    this.selectConsentToDisclose(testData.getInputValue(TEST_DATA_PREFIX + value, "TransGenderDiscloseConsent"));
    this.selectREMMainCategory(testData.getInputValue(TEST_DATA_PREFIX + value, "REMMainCategory"));
    this.selectNationality(testData.getInputValue(TEST_DATA_PREFIX + value, "Nationality"));
    this.selectSecondNationality(testData.getInputValue(TEST_DATA_PREFIX + value, "SecondNationality"));
    this.selectImmigrationStatus(testData.getInputValue(TEST_DATA_PREFIX + value, "ImmigrationStatus"));
    this.inputImmigrationNumber(testData.getInputValue(TEST_DATA_PREFIX + value, "ImmigrationNumber"));
    this.selectLanguage(testData.getInputValue(TEST_DATA_PREFIX + value, "PreferredLanguage"));
    this.selectInterpreterRequired(testData.getInputValue(TEST_DATA_PREFIX + value, "InterpreterRequired"));
    this.selectReligion(testData.getInputValue(TEST_DATA_PREFIX + value, "ReligionOrFaith"));
    this.inputNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "Notes3"));

    //FileUtils.saveScreenshot(this.getClass().getSimpleName());

    DiversityDetailsPage diversityDetailsPage = this.clickButtonSave();

    if (containsElements(By.id("diversityForm:confirmButton"))) {
      this.clickButtonConfirm();
    }

    return type.cast(diversityDetailsPage);
  }
}

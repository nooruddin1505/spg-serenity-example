package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class ConsolidatedTransferRequestTransaction extends BaseTransaction {

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("ConsolidatedTransferRequest%s.OMTransferRequest%s.OMTransferReason",                 "/SPGInterchange/SPGMessage/*[2]/OMTransferRequest%s/OMTransferRequestDetails/OMTransferRequest/OMTransferReason"));
    add(new AssertionPair("ConsolidatedTransferRequest%s.OMTransferRequest%s.OMTransferStatus",                 "/SPGInterchange/SPGMessage/*[2]/OMTransferRequest%s/OMTransferRequestDetails/OMTransferRequest/OMTransferStatus"));
    add(new AssertionPair("ConsolidatedTransferRequest%s.OMTransferRequest%s.OMTransferFromProvider",           "/SPGInterchange/SPGMessage/*[2]/OMTransferRequest%s/OMTransferRequestDetails/OMTransferRequest/OMTransferFromProvider"));
    add(new AssertionPair("ConsolidatedTransferRequest%s.OMTransferRequest%s.OMTransferFromResponsibleTeam",    "/SPGInterchange/SPGMessage/*[2]/OMTransferRequest%s/OMTransferRequestDetails/OMTransferRequest/OMTransferFromResponsibleTeam"));
    add(new AssertionPair("ConsolidatedTransferRequest%s.OMTransferRequest%s.OMTransferFromResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/OMTransferRequest%s/OMTransferRequestDetails/OMTransferRequest/OMTransferFromResponsibleOfficer"));
    add(new AssertionPair("ConsolidatedTransferRequest%s.OMTransferRequest%s.OMTransferToProvider",             "/SPGInterchange/SPGMessage/*[2]/OMTransferRequest%s/OMTransferRequestDetails/OMTransferRequest/OMTransferToProvider"));
    add(new AssertionPair("ConsolidatedTransferRequest%s.OMTransferRequest%s.OMTransferToResponsibleTeam",      "/SPGInterchange/SPGMessage/*[2]/OMTransferRequest%s/OMTransferRequestDetails/OMTransferRequest/OMTransferToResponsibleTeam"));
    add(new AssertionPair("ConsolidatedTransferRequest%s.OMTransferRequest%s.OMTransferToResponsibleOfficer",   "/SPGInterchange/SPGMessage/*[2]/OMTransferRequest%s/OMTransferRequestDetails/OMTransferRequest/OMTransferToResponsibleOfficer"));
  }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

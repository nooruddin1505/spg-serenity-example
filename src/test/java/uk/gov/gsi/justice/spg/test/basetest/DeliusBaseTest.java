package uk.gov.gsi.justice.spg.test.basetest;

import dependencies.adapter.BaseElement;
import dependencies.adapter.BaseTracer;
import dependencies.adapter.abstractstuff.CRCID;
import dependencies.adapter.abstractstuff.ConfigData;
import dependencies.adapter.abstractstuff.Pilot;
import dependencies.adapter.abstractstuff.SeleniumTest;
import dependencies.adapter.actions.ReportLinkAction;
import dependencies.adapter.driver.ExecutionContext;
import dependencies.adapter.others.CRCAgent;
import dependencies.adapter.others.NodeNotFoundException;
import dependencies.adapter.others.SPGAgent;
import dependencies.adapter.utils.SleepUtils;
import dependencies.adapter.utils.XMLUtils;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.xml.sax.SAXException;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.actions.TransactionAction;
import uk.gov.gsi.justice.spg.test.exceptions.NoTransactionFoundException;
import uk.gov.gsi.justice.spg.test.exceptions.UnknownTransactionTypeException;
import uk.gov.gsi.justice.spg.test.txtypes.BaseTransaction;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;
import uk.gov.gsi.justice.spg.test.utils.CRCUtils;
import uk.gov.gsi.justice.spg.test.utils.OffenderNameUtils;
import uk.gov.gsi.justice.spg.test.utils.TransactionUtils;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import test.utils.TransactionUtils;

/**
 * This class provides custom methods specific to the SPG Automation effort, extending the capabilities of SeleniumTest.
 * <p>
 * The majority of the methods within this class are concerned with asserting various test cases and comparing the actual
 * results to those expected in the test data.
 * <p>
 * All tests within this suite interact with Delius and so will extend this class. Furthermore, all tests will involve the
 * creation of an Offender entity within Delius. The majority of the offender data is taken from the test data spreadsheets
 * provided for the tests, however, the offender's first name and last name are generated randomly and need to be accessible
 * at various points within the test execution thus they are made available here as public variables.
 *
 * @author Rob Livermore
 */
public class DeliusBaseTest extends SeleniumTest {

  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
  private static final String EMPTY = "";

  protected String offenderFirstName;
  protected String offenderLastName;
  private Boolean spaceRemoved = false;
  private String expectedNew;
  public String nomsNumber = "A1363SH";
  Pilot pilot = ExecutionContext.getInstance().getPilot();
  public WebDriver driver;

  /**
   * Retry Rule is used to define how many times a test execution will be attempted following a failure.
   * Applies to any one tests run i.e. different tests cannot be assigned different Retry attempts at any one time
   */

//  @Rule
//  public RetryRule retryRule = new RetryRule(1);

  /**
   * Method to be run prior to the execution of each individual test in the suite.
   */
  @Before
  public void beforeTests() {
    this.configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
    this.offenderFirstName = OffenderNameUtils.generateName();
    this.offenderLastName = OffenderNameUtils.generateName();

    System.out.println("******************************* STARTING " + ExecutionContext.getInstance().getTestRunDetails().getId() + " *******************************");
  }

  /**
   * Method to be run after the execution of each individual test in the suite.
   */
  @After
  public void afterTests() {
    System.out.println("******************************* FINISHING " + ExecutionContext.getInstance().getTestRunDetails().getId() + " *******************************");
  }


  /**
   * This is a crucial method to the project in that it is used to assert the vast majority of tests against their expected results. It first obtains
   * and parses the 'transaction' files (XML messages) from the CRC server using the parameters passed to the method; CRN and CRC ID. Having obtained
   * the parsed transactions, it then iterates through the AssertionPairs associated with each transaction grabbing the expected result using the
   * AssertionPair's test data field and the actual result using the XPath field. The assertion is then performed.
   * <p>
   * There are a few specific requirements for this method:
   * <ol>
   * <li>Must fail if nothing has been asserted</li>
   * <li>Must assert the expected transactions have been found as well as the data within the transactions</li>
   * <li>When multiple transactions of the same type are found, must assert the data in the correct one.</li>
   * 3.1. When determining the message to check, if the number is blank in the test data, this will behave the same as if the number were '1'.
   * </ol>
   * <p>
   * Requirement 1. is achieved through the use of a boolean named somethingAsserted.
   * <p>
   * Requirement 2. is achieved through the call to 'assertExpectedTransactions' towards the beginning of the test.
   * <p>
   * Requirement 3. is achieved through the use of a Map 'amounts' which keeps track of the amount of each transaction type found so far, thus
   * allowing the data to be asserted in the correct transaction.
   * <p>
   * Requirement 3.1. is achieved by making an initial pass over the transaction's assertion pairs prior to checking the assertion pairs with the
   * added amount parameters.
   *
   * @param caseReferenceNumber      the case reference number (CRN) to search for within the transaction files at the CRC.
   * @param crcID                    the ID of the CRC to search for the files.
   * @param expectedTransactionTypes a list of transactions (Class objects which extend BaseTransaction) which are expected to be found at the CRC.
   * @return returns a list of objects (BaseTransactions) which represent the transactions found at the CRC.
   * @see DeliusBaseTest#assertAssertionPair(String, String, BaseTransaction)
   * @see AssertionPair
   */

//  protected void StructuredMessage(String caseReferenceNumber, CRCID crcID, List<Class> expectedTransactionTypes) {
//      try {
//          List<BaseTransaction> transactions = TransactionUtils.getTransactions(caseReferenceNumber, crcID);
//
//          boolean somethingAsserted = false;
//          assertExpectedTransactions(transactions, expectedTransactionTypes);
//
//          for (BaseTransaction transaction : transactions) {
//              for (AssertionPair pair : transaction.getAssertionPairs()) {
//                  if (pair.getElement() != null) {
//                      for (int i = 0; i < pair.getElement().length; i++) {
//                          somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
//                      }
//                  }
//              }
//          }
//      } catch (UnknownTransactionTypeException e) {
//          assertor.fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
//      } catch (NoTransactionFoundException e) {
//          assertor.fail("NoTransactionFoundException caught. Message: " + e.getMessage());
//      }
//  }

  protected List<BaseTransaction> assertExpectedResults(String caseReferenceNumber, CRCID crcID, List<Class> expectedTransactionTypes) {
    List<BaseTransaction> transactions = null;

    try {
        transactions = TransactionUtils.getTransactions(caseReferenceNumber, crcID);

      boolean somethingAsserted = false;
      assertExpectedTransactions(transactions, expectedTransactionTypes);

      Map<String, Integer> amounts = new HashMap<>();

      for (BaseTransaction transaction : transactions) { // Iterating through transactions related to this test.

        for (AssertionPair pair : transaction.getAssertionPairs()) { // Iterating through transaction's AssertionPairs with empty parameter.
          if (pair.getElement() != null) {
            for (int i = 0; i < pair.getElement().length; i++) {
              if (pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])).contains("AllocateEvent.Contact")) {
                somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
              } else {
                checkOSRespOfficer(pair, i);
                checkLCSRespOfficer(pair, i);
                checkRQSRespOfficer(pair, i);
                checkProcMgrOfficer(pair, i);
                checkPSSRespOfficer(pair, i);
                checkInstRepOfficer(pair, i);
                checkCrtRepOfficer(pair, i);
                checkPrisonOfficer(pair, i);
                checkContactOfficer(pair, i);
                checkAPRefOfficer(pair, i);
                checkAPResOfficer(pair, i);
                checkRegOfficer(pair, i);
                checkOMOfficer(pair, i);
                checkRSPOfficer(pair, i);
                checkRefToOfficer(pair, i);
                checkAssessOfficer(pair, i);
                checkIntRspOfficer(pair, i);
                checkDecOfficer(pair, i);
                checkOmRspOfficer(pair, i);
                checkReviewOfficer(pair, i);
                checkInstReportOfficer(pair, i);
                checkUPWContactOfficer(pair, i);
                checkRegOfficer2(pair, i);

                somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[i]), transaction) || somethingAsserted;
              }

            }
          }
          // Check once again with an empty element number
          if (pair.getDataHeading(EMPTY, EMPTY).contains("AllocateEvent.Contact")) {
            somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, EMPTY), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
          }

          checkOSRespOfficer(pair, 0);
          checkLCSRespOfficer(pair, 0);
          checkRQSRespOfficer(pair, 0);
          checkProcMgrOfficer(pair, 0);
          checkPSSRespOfficer(pair, 0);
          checkInstRepOfficer(pair, 0);
          checkCrtRepOfficer(pair, 0);
          checkPrisonOfficer(pair, 0);
          checkContactOfficer(pair, 0);
          checkAPRefOfficer(pair, 0);
          checkAPResOfficer(pair, 0);
          checkRegOfficer(pair, 0);
          checkOMOfficer(pair, 0);
          checkRSPOfficer(pair, 0);
          checkRefToOfficer(pair, 0);
          checkAssessOfficer(pair, 0);
          checkIntRspOfficer(pair, 0);
          checkDecOfficer(pair, 0);
          checkOmRspOfficer(pair, 0);
          checkReviewOfficer(pair, 0);
          checkInstReportOfficer(pair, 0);
          checkUPWContactOfficer(pair, 0);
          checkRegOfficer2(pair, 0);

          somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, EMPTY), pair.getXPath(), transaction) || somethingAsserted;

        }

        int number = (amounts = updateValue(amounts, transaction.getTransactionName())).get(transaction.getTransactionName());
        for (AssertionPair pair : transaction.getAssertionPairs()) { // Iterating through transaction's AssertionPairs with transaction number.
          if (pair.getElement() != null) {
            for (int i = 0; i < pair.getElement().length; i++) {
              somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[i]), transaction) || somethingAsserted;
            }
          }

          somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), EMPTY), pair.getXPath(), transaction) || somethingAsserted;
        }
      }

      if (!somethingAsserted) {
        assertor.fail("No assertions were done. Please check the results against the expected results.");
      }

    } catch (UnknownTransactionTypeException e) {
      assertor.fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
    } catch (NoTransactionFoundException e) {
      assertor.fail("NoTransactionsFoundException caught. Message: " + e.getMessage());
    }

    return transactions;
  }

//  protected List<BaseTransaction> assertExpectedResults(List<Class> expectedTransactionTypes, List<BaseTransaction> transactions) {
//    boolean somethingAsserted = false;
//      assertExpectedTransactions(transactions, expectedTransactionTypes);
//
//      Map<String, Integer> amounts = new HashMap<>();
//
//      for (BaseTransaction transaction : transactions) { // Iterating through transactions related to this test.
//
//          for (AssertionPair pair : transaction.getAssertionPairs()) { // Iterating through transaction's AssertionPairs with empty parameter.
//          if (pair.getElement() != null) {
//            for (int i = 0; i < pair.getElement().length; i++) {
//              if (pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])).contains("AllocateEvent.Contact")) {
//                somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
//              } else {
//                checkOSRespOfficer(pair, i);
//                checkLCSRespOfficer(pair, i);
//                checkRQSRespOfficer(pair, i);
//                checkProcMgrOfficer(pair, i);
//                checkPSSRespOfficer(pair, i);
//                checkInstRepOfficer(pair, i);
//                checkCrtRepOfficer(pair, i);
//                checkPrisonOfficer(pair, i);
//                checkContactOfficer(pair, i);
//                checkAPRefOfficer(pair, i);
//                checkAPResOfficer(pair, i);
//                checkRegOfficer(pair, i);
//                checkOMOfficer(pair, i);
//                checkRSPOfficer(pair, i);
//                checkRefToOfficer(pair, i);
//                checkAssessOfficer(pair, i);
//                checkIntRspOfficer(pair, i);
//                checkDecOfficer(pair, i);
//                checkOmRspOfficer(pair, i);
//                checkReviewOfficer(pair, i);
//                checkInstReportOfficer(pair, i);
//                checkUPWContactOfficer(pair, i);
//                checkRegOfficer2(pair, i);
//
//                somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[i]), transaction) || somethingAsserted;
//              }
//
//            }
//          }
//          // Check once again with an empty element number
//          if (pair.getDataHeading(EMPTY, EMPTY).contains("AllocateEvent.Contact")) {
//            somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, EMPTY), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
//          }
//
//           checkOSRespOfficer(pair, 0);
//           checkLCSRespOfficer(pair, 0);
//           checkRQSRespOfficer(pair, 0);
//           checkProcMgrOfficer(pair, 0);
//           checkPSSRespOfficer(pair, 0);
//           checkInstRepOfficer(pair, 0);
//           checkCrtRepOfficer(pair, 0);
//           checkPrisonOfficer(pair, 0);
//           checkContactOfficer(pair, 0);
//           checkAPRefOfficer(pair, 0);
//           checkAPResOfficer(pair, 0);
//           checkRegOfficer(pair, 0);
//           checkOMOfficer(pair, 0);
//           checkRSPOfficer(pair, 0);
//           checkRefToOfficer(pair, 0);
//           checkAssessOfficer(pair, 0);
//           checkIntRspOfficer(pair, 0);
//           checkDecOfficer(pair, 0);
//           checkOmRspOfficer(pair, 0);
//           checkReviewOfficer(pair, 0);
//           checkInstReportOfficer(pair, 0);
//           checkUPWContactOfficer(pair, 0);
//           checkRegOfficer2(pair, 0);
//
//          somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, EMPTY), pair.getXPath(), transaction) || somethingAsserted;
//
//        }
//
//        int number = (amounts = updateValue(amounts, transaction.getTransactionName())).get(transaction.getTransactionName());
//        for (AssertionPair pair : transaction.getAssertionPairs()) { // Iterating through transaction's AssertionPairs with transaction number.
//          if (pair.getElement() != null) {
//            for (int i = 0; i < pair.getElement().length; i++) {
//              somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[i]), transaction) || somethingAsserted;
//            }
//          }
//
//          somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), EMPTY), pair.getXPath(), transaction) || somethingAsserted;
//        }
//      }
//
//      if (!somethingAsserted) {
//        assertor.fail("No assertions were done. Please check the results against the expected results.");
//      }
//
//    return transactions;
//  }

   /*
   * Similar to assertExpectedResults, this method instead asserts the responses obtained. 'Responses' are distinguished from 'transactions' by
   * the CRN being found in the 'receiverRef' element rather than the 'senderControlRef' element.
   *
   * @param controlReference the control reference of the response messages.
   * @param crcID            the ID of the CRC server to search for the responses.
   */
//  protected void assertExpectedResponse(String controlReference, CRCID crcID) {
//    try {
//      List<BaseTransaction> responses = TransactionUtils.getResponses(controlReference, crcID);
//      boolean somethingAsserted = false;
//
//      Map<String, Integer> amounts = new HashMap<>();
//
//      for (BaseTransaction response : responses) {
//        int number = (amounts = updateValue(amounts, response.getTransactionName())).get(response.getTransactionName());
//
//        if (response.getClass().equals(InterchangeStatusNotificationTransaction.class)) {
//
//          for (AssertionPair pair : response.getAssertionPairs()) {
//            try {
//              somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), EMPTY), pair.getXPath(), response) || somethingAsserted;
//            } catch (AssertionError e) {
//              String errorDetailsXPath = configData.getProperty("XPath.TransactionType") + "/Details/FieldValidationErrors/Code";
//              System.out.println("Reason: " + response.getXMLUtils().getNodeTextContent(errorDetailsXPath));
//
//              throw e;
//            }
//          }
//        }
//        else {
//          System.out.println("Response transaction: " + response.getTransactionName() + ", is a transaction " + MessageType.RSP + " from the injected " + response.getTransactionName() +
//                  " transaction, where nDelius is returning the " + response.getTransactionName() + " ID and is not to be asserted" +
//                  " as an InterchangeStatusNotification" );
//
//          somethingAsserted = true;
//        }
//      }
//
//      if (!somethingAsserted) {
//        assertor.fail("No assertions were done on the responses. Please check the results against the expected results.");
//      }
//    } catch (UnknownTransactionTypeException e) {
//      assertor.fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
//    } catch (NoTransactionFoundException e) {
//      assertor.fail("NoTransactionsFoundException caught. Message: " + e.getMessage());
//    }
//  }

//  protected void assertExpectedResponse(String controlReference, CRCID crcID, String schemaVersion) {
//    try {
//      List<BaseTransaction> responses = TransactionUtils.getResponses(controlReference, crcID, schemaVersion);
//      boolean somethingAsserted = false;
//
//      Map<String, Integer> amounts = new HashMap<>();
//
//      for (BaseTransaction response : responses) {
//        int number = (amounts = updateValue(amounts, response.getTransactionName())).get(response.getTransactionName());
//
//        if (response.getClass().equals(InterchangeStatusNotificationTransaction.class)) {
//
//          for (AssertionPair pair : response.getAssertionPairs()) {
//            try {
//              somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), EMPTY), pair.getXPath(), response) || somethingAsserted;
//            } catch (AssertionError e) {
//              String errorDetailsXPath = configData.getProperty("XPath.TransactionType") + "/Details/FieldValidationErrors/Code";
//              System.out.println("Reason: " + response.getXMLUtils().getNodeTextContent(errorDetailsXPath));
//
//              throw e;
//            }
//          }
//        }
//        else {
//          System.out.println("Response transaction: " + response.getTransactionName() + ", is a transaction " + MessageType.RSP + " from the injected " + response.getTransactionName() +
//                  " transaction, where nDelius is returning the " + response.getTransactionName() + " ID and is not to be asserted" +
//                  " as an InterchangeStatusNotification" );
//
//          somethingAsserted = true;
//        }
//      }
//
//      if (!somethingAsserted) {
//        assertor.fail("No assertions were done on the responses. Please check the results against the expected results.");
//      }
//    } catch (UnknownTransactionTypeException e) {
//      assertor.fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
//    } catch (NoTransactionFoundException e) {
//      assertor.fail("NoTransactionsFoundException caught. Message: " + e.getMessage());
//    }
//  }

//  protected void assertExpectedResults(Response response, String nomsNumber) throws IOException {
//
//
//    FileWriter file = new FileWriter(String.format(configData.getProperty("Alfresco.TestResultJSON.Location"), ExecutionContext.getInstance().getTestRunDetails().getId(), nomsNumber));
//    file.write(response.asString());
//    file.flush();
//
////    JSONParser parser = new JSONParser();
////    parser.parse(new FileReader(String.format(configData.getProperty("Alfresco.TestResultJSON.Location"), ExecutionContext.getInstance().getTestRunDetails().getId(), nomsNumber)));
//
//    assertor.assertTrue(String.format("Check Noms Number: Expected: %s, Actual: %s", nomsNumber, response.path("documents.id").toString()), response.path("documents.id").toString().contains(nomsNumber));
//
//
//  }


  private void assertSenderReceiverReferencesFromDelius(String transactionType, String senderReferenceHeaderXPath, String senderReferenceFooterXPath) {
    System.out.println(String.format("Checking that SenderControlReference at Transaction Header: %s, matches SenderControlReference at Transaction Footer: %s in Transaction %s ", senderReferenceHeaderXPath, senderReferenceHeaderXPath, transactionType));
    assertor.assertTrue(String.format("SenderControlReference %s at Transaction Header, matches SenderControlReference at Transaction Footer in Transaction %s ", senderReferenceHeaderXPath, transactionType), senderReferenceHeaderXPath.equals(senderReferenceFooterXPath));
  }

  private void assertSenderReceiverReferences(String controlReference, String receiverReference, String statusCode, String senderReferenceHeaderXPath, String senderReferenceFooterXPath, String receiverReferenceHeaderXPath, String receiverReferenceFooterXPath) {
    System.out.println(String.format("Checking that SenderControlReference %s in injected message matches ReceiverControlReference %s in Response message %s ", controlReference, receiverReference, statusCode));
    System.out.println(String.format("Checking that SenderControlReference %s at Response Header, matches SenderControlReference at Response Footer in Response message %s ", controlReference, statusCode));
    System.out.println(String.format("Checking that ReceiverControlReference %s at Response Header, matches ReceiverControlReference at Response Footer in Response message %s ", receiverReferenceHeaderXPath, statusCode));
    assertor.assertTrue(String.format("SenderControlReference %s in injected message matches ReceiverControlReference %s in Response message %s ", controlReference, receiverReference, statusCode), controlReference.equals(receiverReference));
    assertor.assertTrue(String.format("SenderControlReference %s at Response Header, matches SenderControlReference at Response Footer in Response message %s ", controlReference, statusCode), senderReferenceHeaderXPath.equals(senderReferenceFooterXPath));
    assertor.assertTrue(String.format("ReceiverControlReference %s at Response Header, matches ReceiverControlReference at Response Footer in Response message %s ", receiverReferenceHeaderXPath, statusCode), receiverReferenceHeaderXPath.equals(receiverReferenceFooterXPath));
  }

  /**
   * @see DeliusBaseTest#assertExpectedTransactions(List, String, CRCID, List)
   */
  private void assertExpectedTransactions(List<BaseTransaction> transactions, List<Class> expectedTransactionTypes) {
    assertExpectedTransactions(expectedTransactionTypes, null, null, transactions);
  }

  /**
   * Method to assert that the transactions which have been obtained are those which we expect to see for any given test.
   *
   * @param expectedTransactionTypes list of transactions which are expected to be found.
   * @param caseReferenceNumber      case reference number of the transactions to search for.
   * @param crcID                    ID of the CRC server to search for the transactions.
   * @param transactions             transactions which have previously been found. If null, these will be obtained using caseReferenceNumber and crcID.
   */
  public void assertExpectedTransactions(List<Class> expectedTransactionTypes, String caseReferenceNumber, CRCID crcID, List<BaseTransaction> transactions) {
    try {
      if (transactions == null) {
        transactions = TransactionUtils.getTransactions(caseReferenceNumber, crcID);
      }

      if (expectedTransactionTypes != null) {
        for (Class expectedType : expectedTransactionTypes) {
          boolean typeFound = false;
          for (BaseTransaction transaction : transactions) {
            if (transaction.getClass().equals(expectedType)) {
              typeFound = true;
              break;
            }
          }

          System.out.println(String.format("Expected transaction type %s found in results.", expectedType.toString())); // TODO - This should be an Action.
          assertor.assertTrue(String.format("Expected transaction type %s found in results.", expectedType.getSimpleName()), typeFound);
        }
      }
    } catch (UnknownTransactionTypeException e) {
      assertor.fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
    } catch (NoTransactionFoundException e) {
      assertor.fail("NoTransactionsFoundException caught. Message: " + e.getMessage());
    }
  }

  /**
   * Method used to inject a given data file (within the Data.Dir path) at a CRC so that it travels inbound towards the APICT systems. The ID of the
   * CRC is determined from the value in the SenderIdentity element of the data file. A control reference number is generated for the injected
   * message which is based upon the current date and time.
   * <p>
   * Despite 'inject' being the verb, the actual event is a copy of the file to the CRC server's inbound SPG directory. The SPG itself handles the
   * actual injection of the file onto ActiveMQ queues.
   *
   * @param dataFileName the name of the file contained within the Data.Dir directory which is to be injected.
   * @return the generated control reference number used for the inbound message.
   */
  protected String injectAtCRC(String dataFileName, String schemaVersion, CRCID crcID) {
    String dataFilePath = configData.getProperty("Data.Dir") + File.separator + dataFileName;
    String hostName = "";
    String controlReference = "";

    try {
      XMLUtils xmlUtils = new XMLUtils(dataFilePath);
      String crcId = xmlUtils.getNodeTextContent(configData.getProperty("XPath.SenderIdentity"));
      controlReference = SPGAgent.generateControlReference();

      if (crcId.equals("N00")) {
        assertor.assertTrue("Invalid sender ID, test data incorrect", false);
      }

      hostName = configData.getProperty("Host." + schemaVersion + ".Server");

      CRCAgent crcAgent = new CRCAgent(controlReference, dataFileName, hostName, schemaVersion, crcID);

      crcAgent.inject();
    } catch (ParserConfigurationException e) {
      TransactionAction.reportParserConfigurationExceptionCaught(hostName, dataFilePath, "n/a", dataFileName, e);
    } catch (SAXException e) {
      TransactionAction.reportSAXExceptionCaught(hostName, dataFilePath, "n/a", dataFileName, e);
    } catch (IOException e) {
      TransactionAction.reportIOExceptionCaught(hostName, dataFilePath, "n/a", dataFileName, e);
    }

    SleepUtils.longSleep();

    return controlReference;
  }

  /**
   * Method to perform an assertion using information from an AssertionPair. There are some edge cases which are handled
   * as part of this method, specifically the asserting of an offender's first name and last name which, being randomly
   * generated by the application, means the actual values cannot be checked against the test data.
   * <p>
   * Furthermore, an AssertionRule can be applied to the check for certain edge cases. These AssertionRules determine
   * what the method is for asserting the actual against the expected value; should that be EQUALS (default), CONTAINS
   * or MATCHES. For MATCHES, this allows regular expressions to be defined in the test data which is useful in certain
   * circumstances.
   *
   * @param dataHeading the data heading from the assertion pair
   * @param xPath       the xPath from the assertion pair
   * @param tx          the transaction to assert against
   * @return true if an assertion operation has been made, false otherwise.
   * @see AssertionRule
   */
  private boolean assertAssertionPair(String dataHeading, String xPath, BaseTransaction tx) {
    String expected = testData.getExpectedResult(dataHeading);

    if (isSpaceRemoved()) {
      expected = getNewExpected();
    }

    if (dataHeading.equals("OffenderSteps.Offender.Notes")) {
      expected = ExecutionContext.getInstance().getTestRunDetails().getId();
    }

    if ((dataHeading.equals("OffenderSteps.Offender.PNCNumber")) ||
            (dataHeading.equals("OffenderSteps.Offender.CRONumber")) ||
                    (dataHeading.equals("OffenderSteps.Offender.NOMSNumber")) ||
                            (dataHeading.equals("OffenderSteps.Offender.NINumber")) ||
                                    (dataHeading.equals("Offender.OffenderDetails.PNCNumber")) ||
                                            (dataHeading.equals("Offender.OffenderDetails.CRONumber")) ||
                                                    (dataHeading.equals("Offender.OffenderDetails.NOMSNumber")) ||
                                                              (dataHeading.equals("Offender.OffenderDetails.NINumber")) ||
                                                                    (dataHeading.equals("Offender.OffenderDetails.EqualityMonitoringNotes")) ||
                                                                              dataHeading.equals("Offender.OffenderDetails.DateDied")) {
      expected = null;
    }

    String contactXpath = "";
    int contactTypeCount = 0;
    if (dataHeading != null && dataHeading.contains("AllocateEvent.Contact") && expected != null) {
      contactTypeCount = tx.getXMLUtils().contactFindNumber(testData);
      if (contactTypeCount > 0) {
        char replacePosition = xPath.charAt(40);
        contactXpath = xPath.replaceAll(String.valueOf(replacePosition), String.valueOf(contactTypeCount));
      }
    }

    AssertionRule assertionRule = AssertionRule.EQUALS;

    if (expected == null || EMPTY.equals(expected))
      return false; // Skip assertion if there is no expected value.

    // Need to check some edge cases.
    if (dataHeading.matches("AllocateOffender\\d*\\.Offender\\d*\\.FirstName")) {
      expected = this.offenderFirstName;
    }

    if (dataHeading.matches("AllocateOffender\\d*\\.Offender\\d*\\.FamilyName")) {
      expected = this.offenderLastName;
    }

    if (dataHeading.matches("Offender\\d*\\.OffenderDetails\\d*\\.FirstName")) {
      expected = this.offenderFirstName;
    }

    if (dataHeading.matches("Offender\\d*\\.OffenderDetails\\d*\\.FamilyName")) {
      expected = this.offenderLastName;
    }

    if (dataHeading.matches("Contact\\d*\\.ContactDetails\\d*\\.Notes")) {
      assertionRule = AssertionRule.MATCHES;
    }

    DeliusBaseTestAction.reportNextAssertionPairToCheck(xPath, dataHeading, tx.getTransactionName());

    String actual = "";
    try {
      if (dataHeading != null && dataHeading.contains("AllocateEvent.Contact") && contactTypeCount > 0) {
        actual = tx.getXMLUtils().getNodeTextContent(contactXpath);
      } else {
        actual = tx.getXMLUtils().getNodeTextContent(xPath);
      }

    } catch (NodeNotFoundException e) {
      assertor.fail(e.getMessage());
    }

    boolean result;
    switch (assertionRule) {
      case EQUALS:
        result = actual.equals(expected);
        break;
      case CONTAINS:
        result = actual.contains(expected);
        break;
      case MATCHES:
        result = actual.matches(expected);
        break;
      default:
        result = actual.equals(expected);
        break;
    }

    assertor.assertTrue(String.format("Value at %s is correct. Expected: '%s'. Actual: '%s'", xPath, expected, actual), result);
    spaceRemoved = false;
    return true;
  }

  /**
   * Method to update the 'amounts' map from the assertExpectedResults and assertExpectedResponse methods.
   *
   * @param map the amounts map.
   * @param key the key to be updated.
   * @return the updated map.
   * @see DeliusBaseTest#assertExpectedResponse(String, CRCID)
   */
  private Map<String, Integer> updateValue(Map<String, Integer> map, String key) {
    if (map.containsKey(key)) {
      int value = map.get(key);
      map.put(key, value + 1);
    } else {
      map.put(key, 1);
    }

    return map;
  }

  public void assertTransactions(String crn, CRCID crcID, List<Class> expectedTransactions) {
    assertExpectedResultsWithPolling(crn, crcID, expectedTransactions);
  }


  protected List<BaseTransaction> assertExpectedResultsWithPolling(String caseReferenceNumber, CRCID crcID, List<Class> expectedTransactionTypes) {
    boolean somethingAsserted = false;

    //Introduce polling instead of fixed waits
    List<BaseTransaction> transactions = getExpectedTransactionsWithPolling(caseReferenceNumber, crcID, expectedTransactionTypes);
    Map<String, Integer> amounts = new HashMap<>();

    for (BaseTransaction transaction : transactions) { // Iterating through transactions related to this test.

      for (AssertionPair pair : transaction.getAssertionPairs()) { // Iterating through transaction's AssertionPairs with empty parameter.
        if (pair.getElement() != null) {
          for (int i = 0; i < pair.getElement().length; i++) {
            if (pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])).contains("AllocateEvent.Contact")) {
              somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
            } else {
              checkOSRespOfficer(pair, i);
              checkLCSRespOfficer(pair, i);
              checkRQSRespOfficer(pair, i);
              checkProcMgrOfficer(pair, i);
              checkPSSRespOfficer(pair, i);
              checkInstRepOfficer(pair, i);
              checkCrtRepOfficer(pair, i);
              checkPrisonOfficer(pair, i);
              checkContactOfficer(pair, i);
              checkAPRefOfficer(pair, i);
              checkAPResOfficer(pair, i);
              checkRegOfficer(pair, i);
              checkOMOfficer(pair, i);
              checkRSPOfficer(pair, i);
              checkRefToOfficer(pair, i);
              checkAssessOfficer(pair, i);
              checkIntRspOfficer(pair, i);
              checkDecOfficer(pair, i);
              checkOmRspOfficer(pair, i);
              checkReviewOfficer(pair, i);
              checkInstReportOfficer(pair, i);
              checkUPWContactOfficer(pair, i);
              checkRegOfficer2(pair, i);

              somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[i]), transaction) || somethingAsserted;
            }

          }
        }
        // Check once again with an empty element number
        if (pair.getDataHeading(EMPTY, EMPTY).contains("AllocateEvent.Contact")) {
          somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, EMPTY), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
        }

        checkOSRespOfficer(pair, 0);
        checkLCSRespOfficer(pair, 0);
        checkRQSRespOfficer(pair, 0);
        checkProcMgrOfficer(pair, 0);
        checkPSSRespOfficer(pair, 0);
        checkInstRepOfficer(pair, 0);
        checkCrtRepOfficer(pair, 0);
        checkPrisonOfficer(pair, 0);
        checkContactOfficer(pair, 0);
        checkAPRefOfficer(pair, 0);
        checkAPResOfficer(pair, 0);
        checkRegOfficer(pair, 0);
        checkOMOfficer(pair, 0);
        checkRSPOfficer(pair, 0);
        checkRefToOfficer(pair, 0);
        checkAssessOfficer(pair, 0);
        checkIntRspOfficer(pair, 0);
        checkDecOfficer(pair, 0);
        checkOmRspOfficer(pair, 0);
        checkReviewOfficer(pair, 0);
        checkInstReportOfficer(pair, 0);
        checkUPWContactOfficer(pair, 0);
        checkRegOfficer2(pair, 0);

        somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, EMPTY), pair.getXPath(), transaction) || somethingAsserted;

      }

      int number = (amounts = updateValue(amounts, transaction.getTransactionName())).get(transaction.getTransactionName());
      for (AssertionPair pair : transaction.getAssertionPairs()) { // Iterating through transaction's AssertionPairs with transaction number.
        if (pair.getElement() != null) {
          for (int i = 0; i < pair.getElement().length; i++) {
            somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[i]), transaction) || somethingAsserted;
          }
        }

        somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), EMPTY), pair.getXPath(), transaction) || somethingAsserted;
      }
    }

    if (!somethingAsserted) {
      assertor.fail("No assertions were done. Please check the results against the expected results.");
    }

    return transactions;
  }


  /**
   * Use only to get a list of transactions from a specific CRC
   *
   * @param caseReferenceNumber
   * @param crcID
   * @param expectedTransactionTypes
   * @return
   */
  protected List<BaseTransaction> getExpectedTransactionsWithPolling(String caseReferenceNumber, CRCID crcID, List<Class> expectedTransactionTypes) {
    List<BaseTransaction> transactions = null;
    long start = System.currentTimeMillis();
    boolean foundExpectedTransactions = false;
    long timeToWaitMilliSeconds = 1000;//ExecutionContext.getInstance().getConfigData().getLongProperty("SPG.Message.Medium.Wait.Millis") / 1000;
    long totalTime = 0;

    //Poll every couple of seconds and check if the transaction we are interested in is there
    do {
      try {
        WaitUtils.sleepSpecificTimeDontUseMeOverSeleniumWaits(timeToWaitMilliSeconds);
        transactions = TransactionUtils.getTransactionsUseWithPolling(caseReferenceNumber, crcID);
        assertExpectedTransactions(transactions, expectedTransactionTypes);
        foundExpectedTransactions = true;
      } catch (AssertionError ae) {
        foundExpectedTransactions = false;
      } catch (Exception e) {
        System.out.println("Waiting for messages to appear in the system : " + expectedTransactionTypes);
      }
      totalTime = totalTime + timeToWaitMilliSeconds;

      //Try again if all transactions not found and totalTime is less than 60 seoncds
    } while (!foundExpectedTransactions && totalTime <= 60000);

    if (!foundExpectedTransactions) {
      transactions = null;
      assertor.fail("Expected transaction types NOT found in results.");
    }
    long finish = System.currentTimeMillis();
    System.out.format("\n----------------------------------------\nTime taken for all the messages to appear in SPG : %s seconds\n----------------------------------------\n", ((finish - start) / 1000) );

    return transactions;
  }


  /**
   * This enum is used to differentiate between the different rules for checking edge cases.
   *
   * @see DeliusBaseTest#assertAssertionPair(String, String, BaseTransaction)
   */
  public enum AssertionRule {
    EQUALS,
    CONTAINS,
    MATCHES
  }

  public void removeSpace(AssertionPair pair, String element) {
    String expected;
    if (element.equals("0")) {
      expected = testData.getExpectedResult(pair.getDataHeading(EMPTY, EMPTY));
    }
    else {
      expected = testData.getExpectedResult(pair.getDataHeading(EMPTY, element));
    }

      if (expected != null && !expected.contains("Unallocated Staff")) {
          expectedNew = expected.replaceAll(" ", "");
          spaceRemoved = true;
      }

  }

  public Boolean isSpaceRemoved() {
    return spaceRemoved;
  }

  public String getNewExpected() {
    return expectedNew;
  }

  public void checkOSRespOfficer(AssertionPair pair, int element) {
    String OSOfficer = "AllocateEvent.Event.OSResponsibleOfficer";

      if (pair.getDataHeading(EMPTY, EMPTY).contains(OSOfficer)) {
        String element2 = Integer.toString(element);
        this.removeSpace(pair, element2);
      }
    }

  public void checkLCSRespOfficer(AssertionPair pair, int element) {
    String LCOfficer = "LCResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
        this.removeSpace(pair, element2);
      }
    }

  public void checkRQSRespOfficer(AssertionPair pair, int element) {
    String LCOfficer = "RQResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkProcMgrOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ProcessManagerOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkPSSRespOfficer(AssertionPair pair, int element) {
    String LCOfficer = "PSSResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkInstRepOfficer(AssertionPair pair, int element) {
    String LCOfficer = "InstReportOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkCrtRepOfficer(AssertionPair pair, int element) {
    String LCOfficer = "CrtReportOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkPrisonOfficer(AssertionPair pair, int element) {
    String LCOfficer = "PrisonOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkContactOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ContactOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkAPRefOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ReferringOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkAPResOfficer(AssertionPair pair, int element) {
    String LCOfficer = "APOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRegOfficer(AssertionPair pair, int element) {
    String LCOfficer = "RegisteringOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkOMOfficer(AssertionPair pair, int element) {
    String LCOfficer = "OMResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRSPOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRefToOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ReferredToOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkAssessOfficer(AssertionPair pair, int element) {
    String LCOfficer = "AssessmentOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkIntRspOfficer(AssertionPair pair, int element) {
    String LCOfficer = "InterventionResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkDecOfficer(AssertionPair pair, int element) {
    String LCOfficer = "DecisionOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkOmRspOfficer(AssertionPair pair, int element) {
    String LCOfficer = "OMTransferToResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkReviewOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ReviewingOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkInstReportOfficer(AssertionPair pair, int element) {
    String LCOfficer = "InstReportOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkUPWContactOfficer(AssertionPair pair, int element) {
    String LCOfficer = "UPWContactOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRegOfficer2(AssertionPair pair, int element) {
    String LCOfficer = "Officer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void printOut(String display) {
    System.out.println(display);
    BaseTracer tracer = null;
    tracer = ExecutionContext.getInstance().getTracer();
    ReportLinkAction myReportLinkAction = new ReportLinkAction("PRINT Message");
    tracer.trace(myReportLinkAction, new BaseElement(display));
  }


  /**
   * Use only to get a list of transactions
   * @param caseReferenceNumber
   * @param crcID
   * @param expectedTransactionTypes
   * @return
   */
//  protected List<BaseTransaction> getListOfTransactions(String caseReferenceNumber, CRCID crcID, List<Class> expectedTransactionTypes, boolean enableAsserting) {
//    List<BaseTransaction> transactions = null;
//    try {
//      transactions = TransactionUtils.getTransactions(caseReferenceNumber, crcID);
//      if(enableAsserting)
//      assertExpectedTransactions(transactions, expectedTransactionTypes);
//    } catch (UnknownTransactionTypeException e) {
//      assertor.fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
//    } catch (NoTransactionFoundException e) {
//      assertor.fail("NoTransactionsFoundException caught. Message: " + e.getMessage());
//    }
//    return transactions;
//  }

//  protected List<BaseTransaction> getListOfTransactionsFromResponse(String controlReference, CRCID crcID, String schemaVersion) {
//    List<BaseTransaction> transactions = null;
//    try {
//      transactions = TransactionUtils.getResponses(controlReference, crcID, schemaVersion);
//    } catch (UnknownTransactionTypeException e) {
//      assertor.fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
//    } catch (NoTransactionFoundException e) {
//      assertor.fail("NoTransactionsFoundException caught. Message: " + e.getMessage());
//    }
//    return transactions;
//  }

}





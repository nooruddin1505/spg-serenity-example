package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;
import dependencies.adapter.driver.PageElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.TablePageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class AddressListPage extends OffenderDetailsCommon implements TablePageAssertionInterface {
    private static final String TEST_DATA_PREFIX = "AddAddress.";

    //Other Address
    @FindBy(xpath = "//*/tr/td[11]/a")
    WebElementFacade linkViewOtherAddress;
    @FindBy(xpath = "//*/tr/td[11]/a")
    WebElementFacade linkUpdateOtherAddress;
    @FindBy(xpath = "//*/tr/td[11]/a")
    WebElementFacade linkDeleteOtherAddress;

    @FindBy(xpath = "//*/tr/td[11]/a")
    WebElementFacade linkViewHistoryAddress;
    @FindBy(xpath = "//*/tr/td[11]/a")
    WebElementFacade linkFirstAssessmentLink;

    //Buttons
    @FindBy(id = "addressListForm:j_id_id175")
    WebElementFacade btnAddAddress;
    @FindBy(xpath = "addressListForm:j_id_id97")
    WebElementFacade btnClose;
    @FindBy(xpath = "deleteAddressForm:j_id_id83")
    WebElementFacade btnDeleteAddressHistory;

    //Dynamic checks: Other Address
    private static final String XPATH_LINK_VIEW_OTHER_ADDRESS = "//*[@id='addressListForm:otherAddressTable:tbody_element']/tr[%s]/td[11]/a";
    private static final String XPATH_LINK_UPDATE_OTHER_ADDRESS = "//*[@id='addressListForm:otherAddressTable:tbody_element']/tr[%s]/td[12]/a";
    private static final String XPATH_LINK_DELETE_OTHER_ADDRESS = "//*[@id='addressListForm:otherAddressTable:tbody_element']/tr[%s]/td[13]/a";

    //Dynamic checks: Historical Address
    private static final String XPATH_LINK_VIEW_HISTORY_ADDRESS = "//*/tr[%s]/td[11]/a";
    private static final String XPATH_LINK_UPDATE_HISTORY_ADDRESS = "//*/tr[%s]/td[12]/a";
    private static final String XPATH_LINK_DELETE_HISTORY_ADDRESS = "//*/tr[%s]/td[13]/a";

    //Dynamic checks: Address details
    private static final String XPATH_HOUSE_NUMBER_BY_VALUE = "//*[@id='addressListForm:mainAddressTable']/tbody/tr[td[1]//text()[contains(., '%s')]]";
    private static final String XPATH_STREET_BY_VALUE = "//*[@id='addressListForm:mainAddressTable']/tbody/tr[td[2]//text()[contains(., '%s')]]";
    private static final String XPATH_DISTRICT_BY_VALUE = "//*[@id='addressListForm:mainAddressTable']/tbody/tr[td[3]//text()[contains(., '%s')]]";
    private static final String XPATH_TOWN_CITY_BY_VALUE = "//*[@id='addressListForm:mainAddressTable']/tbody/tr[td[4]//text()[contains(., '%s')]]";
    private static final String XPATH_POSTCODE_BY_VALUE = "//*[@id='addressListForm:mainAddressTable']/tbody/tr[td[5]//text()[contains(., '%s')]]";
    private static final String XPATH_START_DATE_BY_VALUE = "//*[@id='addressListForm:mainAddressTable']/tbody/tr[td[6]//text()[contains(., '%s')]]";
    private static final String XPATH_END_DATE_BY_VALUE = "//*[@id='addressListForm:mainAddressTable']/tbody/tr[td[7]//text()[contains(., '%s')]]";
    private static final String XPATH_STATUS_BY_VALUE = "//*[@id='addressListForm:mainAddressTable']/tbody/tr[td[8]//text()[contains(., '%s')]]";

    private static final String XPATH_FIELD_BY_NAME_AND_ROW = "//table/tbody/tr[%s]/td[count(//table/thead/tr/th/a/span/text()[contains(.,'%s')]/../../../preceding-sibling::th)+1]";

    public AddressAssessmentListPage clickViewHistoryAddressLink() {
        clickOn(linkViewHistoryAddress);
        return switchToPage(AddressAssessmentListPage.class);
    }

    public AddressAssessmentListPage clickFirstAssessmentLink() {
        clickOn(linkFirstAssessmentLink);
        return switchToPage(AddressAssessmentListPage.class);
    }

    public UpdateAddressPage clickUpdateOtherAddressLink() {
        clickOn(linkUpdateOtherAddress);
        return switchToPage(UpdateAddressPage.class);
    }

    public AddressAssessmentListPage clickViewOtherAddressLink() {
        clickOn(linkViewOtherAddress);
        return switchToPage(AddressAssessmentListPage.class);
    }

    public AddAddressPage clickAddAddressButton() {
        clickOn(btnAddAddress);
        return switchToPage(AddAddressPage.class);
    }

    public OffenderDetailsPage clickCloseButton() {
        clickOn(btnClose);
        return switchToPage(OffenderDetailsPage.class);
    }

    public boolean houseNumberExists(String houseNumber) {
        return pilot.elementExists(PageElement.byXpath(XPATH_HOUSE_NUMBER_BY_VALUE, "AddressListPage house number by value", houseNumber));
    }

    public boolean streetExists(String street) {
        return pilot.elementExists(PageElement.byXpath(XPATH_STREET_BY_VALUE, "AddressListPage street by value", street));
    }

    public boolean districtExists(String district) {
        return pilot.elementExists(PageElement.byXpath(XPATH_DISTRICT_BY_VALUE, "AddressListPage district by value", district));
    }

    public boolean townCityExists(String townCity) {
        return pilot.elementExists(PageElement.byXpath(XPATH_TOWN_CITY_BY_VALUE, "AddressListPage town/city by value", townCity));
    }

    public boolean postcodeExists(String postcode) {
        return pilot.elementExists(PageElement.byXpath(XPATH_POSTCODE_BY_VALUE, "AddressListPage postcode by value", postcode));
    }

    public boolean startDateExists(String startDate) {
        return pilot.elementExists(PageElement.byXpath(XPATH_START_DATE_BY_VALUE, "AddressListPage start date by value", startDate));
    }

    public boolean endDateExists(String endDate) {
        return pilot.elementExists(PageElement.byXpath(XPATH_END_DATE_BY_VALUE, "AddressListPage end date by value", endDate));
    }

    public boolean statusExists(String status) {
        return pilot.elementExists(PageElement.byXpath(XPATH_STATUS_BY_VALUE, "AddressListPage status by value", status));
    }



    private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
        add(new AssertionPair(TEST_DATA_PREFIX + "HouseNumber","/html/body/form/div[2]/div/div/main/section/table/tbody/tr/td[1]"));
        add(new AssertionPair(TEST_DATA_PREFIX + "Street","/html/body/form/div[2]/div/div/main/section/table/tbody/tr/td[2]"));
        add(new AssertionPair(TEST_DATA_PREFIX + "District","/html/body/form/div[2]/div/div/main/section/table/tbody/tr/td[3]"));
        add(new AssertionPair(TEST_DATA_PREFIX + "TownCity","/html/body/form/div[2]/div/div/main/section/table/tbody/tr/td[4]"));
        add(new AssertionPair(TEST_DATA_PREFIX + "Postcode","/html/body/form/div[2]/div/div/main/section/table/tbody/tr/td[5]"));
        add(new AssertionPair(TEST_DATA_PREFIX + "StartDate","/html/body/form/div[2]/div/div/main/section/table/tbody/tr/td[6]"));
        add(new AssertionPair(TEST_DATA_PREFIX + "Status","/html/body/form/div[2]/div/div/main/section/table/tbody/tr/td[7]"));
    }};

    @Override
    public boolean tableExists() {
        return false;
    }

    @Override
    public String getValueAtLocation(int row, int column) {
        return null;
    }

    @Override
    public String getValueAtLocation(int row, String columnName) {
        return null;
    }

    @Override
    public void assertTableRowNotExists(int rowLocation) {
        assertor.assertFalse("Results table does not exist on this page suggesting no Address exist for this offender.", tableExists());

        for (AssertionPair pair : ASSERTION_PAIRS) {
            String expected = testData.getExpectedResult(pair.getDataHeading());
            PageElement targetElement =
                    new PageElement(PageElement.Type.XPATH, pair.getXPath(rowLocation), "Asserting nothing exists at following xPath: " + pair.getXPath(rowLocation));

            boolean elementExists = pilot.elementExists(targetElement);

            // If there's something at the rowLocation, need to check whether it's the row we're expecting not to see.
            if (elementExists) {
                String actual = pilot.getText(targetElement);
                assertor
                        .assertNotEquals(String.format("Expected value at row %s does not exist. Expected: '%s', actual: '%s'", rowLocation, expected, actual),
                                expected, actual);
            } else {
                assertor.assertFalse(String.format("Value at row '%s' is null, as expected.", rowLocation), elementExists);
            }
        }
    }

    @Override
    public void assertTableRowExists(int rowLocation) {

    }

    @Override
    public void assertPageValues() {
        for (AssertionPair pair : ASSERTION_PAIRS) {
            DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "ContactDetailsPage");
            String expected = testData.getExpectedResult(pair.getDataHeading());

            if (expected == null || expected.isEmpty())
                continue;

            String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "AddressListPage get value by description"));

            assertor.assertEquals(String.format("Value is correct. Expected: %s, Actual: %s", expected, actual), expected, actual);
        }

    }

    public enum AddressFieldName {
        HOUSE_NUMBER("No"),
        STREET("Street"),
        DISTRICT("District"),
        TOWN_CITY("Town/City"),
        POSTCODE("Postcode"),
        START("Start"),
        END("End"),
        STATUS("Status");

        private final String name;

        AddressFieldName(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return otherName != null && name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }
}

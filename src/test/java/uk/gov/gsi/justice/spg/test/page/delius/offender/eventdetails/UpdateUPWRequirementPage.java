package uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails;

import dependencies.adapter.driver.PageElement;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;

public class UpdateUPWRequirementPage extends EventDetailsCommon implements FormPageInterface {
  private static final String TEST_DATA_PREFIX = "UpdateUPWRequirement";

  private static final String XPATH_INPUT_LENGTH = "//*[@id='updateComponentForm:requirement:Length']";
  private static final String XPATH_INPUT_IMPOSED_SENTENCE_DATE = "//*[@id='updateComponentForm:requirement:RequirementSentenceDate']";
  private static final String XPATH_INPUT_EXPECTED_START_DATE = "//*[@id='updateComponentForm:requirement:ExpectedStartDate']";
  private static final String XPATH_INPUT_ACTUAL_START_DATE = "//*[@id='updateComponentForm:requirement:ActualStartDate']";
  private static final String XPATH_SELECT_TEAM = "//*[@id='updateComponentForm:requirement:Team']";
  private static final String XPATH_SELECT_OFFICER = "//*[@id='updateComponentForm:requirement:Officer']";
  private static final String XPATH_INPUT_TRANSFER_DATE = "//*[@id='TransferDateReq']";
  private static final String XPATH_SELECT_TRANSFER_REASON = "//*[@id='ReasonForTransfer']";
  private static final String XPATH_INPUT_NOTES = "//*[@id='RequirementNewNote']";
  private static final String XPATH_INPUT_EXPECTED_END_DATE = "//*[@id='updateComponentForm:requirement:ExpectedEndDate']";
  private static final String XPATH_INPUT_ACTUAL_END_DATE = "//*[@id='updateComponentForm:requirement:EndDate']";
  private static final String XPATH_SELECT_TERMINATION_REASON = "//*[@id='updateComponentForm:requirement:ComponentTermimationReason']";
  private static final String XPATH_BUTTON_CANCEL = "//*[@value='Cancel']";
  private static final String XPATH_BUTTON_SAVE = "//*[@value='Save']";

  private static final String XPATH_BUTTON_WORKSHEET_SUMMARY = "//*[@value='Worksheet Summary']";
  private static final String XPATH_BUTTON_ADJUSTMENT = "//*[@value='Adjustment']";

  private static final PageElement LENGTH = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_LENGTH, "UpdateRequirementPage requirement length input");
  private static final PageElement IMPOSED_SENTENCE_DATE = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_IMPOSED_SENTENCE_DATE, "UpdateRequirementPage imposed sentence date input");
  private static final PageElement EXPECTED_START_DATE = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_EXPECTED_START_DATE, "UpdateRequirementPage expected start date input");
  private static final PageElement ACTUAL_START_DATE = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_ACTUAL_START_DATE, "UpdateRequirementPage actual start date input");
  private static final PageElement TEAM = new PageElement(PageElement.Type.XPATH, XPATH_SELECT_TEAM, "UpdateRequirementPage team select");
  private static final PageElement TRANSFER_DATE = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_TRANSFER_DATE, "UpdateRequirementPage transfer date input");
  private static final PageElement TRANSFER_REASON = new PageElement(PageElement.Type.XPATH, XPATH_SELECT_TRANSFER_REASON, "UpdateRequirementPage transfer reason select");
  private static final PageElement OFFICER = new PageElement(PageElement.Type.XPATH, XPATH_SELECT_OFFICER, "UpdateRequirementPage officer select");
  private static final PageElement NOTES = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_NOTES, "UpdateRequirementPage notes input");
  private static final PageElement EXPECTED_END_DATE = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_EXPECTED_END_DATE, "UpdateRequirementPage expected end date input");
  private static final PageElement ACTUAL_END_DATE = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_ACTUAL_END_DATE, "UpdateRequirementPage actual end date input");
  private static final PageElement TERMINATION_REASON = new PageElement(PageElement.Type.XPATH, XPATH_SELECT_TERMINATION_REASON, "UpdateRequirementPage termination reason select");
  private static final PageElement CANCEL = new PageElement(PageElement.Type.XPATH, XPATH_BUTTON_CANCEL, "UpdateRequirementPage cancel button");
  private static final PageElement SAVE = new PageElement(PageElement.Type.XPATH, XPATH_BUTTON_SAVE, "UpdateRequirementPage save button");

  private static final PageElement WORKSHEET_SUMMARY = new PageElement(PageElement.Type.XPATH, XPATH_BUTTON_WORKSHEET_SUMMARY, "UPWDetailsPage worksheet summary button");
  private static final PageElement ADJUSTMENT = new PageElement(PageElement.Type.XPATH, XPATH_BUTTON_ADJUSTMENT, "UPWDetailsPage adjustment button");

  // There are no required fields (update page)
  private void inputLength(String length) {
    if (length != null && !length.isEmpty())
      pilot.clearAndSendKeys(LENGTH, length);
  }

  private void inputImposedSentenceDate(String imposedSentenceDate) {
    if  (imposedSentenceDate != null && !imposedSentenceDate.isEmpty())
      pilot.clearAndSendKeys(IMPOSED_SENTENCE_DATE, imposedSentenceDate);
  }

  private void inputExpectedStartDate(String expectedStartDate) {
    if (expectedStartDate != null && !expectedStartDate.isEmpty())
      pilot.clearAndSendKeys(EXPECTED_START_DATE, expectedStartDate);
  }

  private void inputActualStartDate(String actualStartDate) {
    if (actualStartDate != null && !actualStartDate.isEmpty())
      pilot.clearAndSendKeys(ACTUAL_START_DATE, actualStartDate);
  }

  private void selectTeam(String team) {
    if (team != null && !team.isEmpty())
      pilot.selectFromVisibleText(TEAM, team);
  }

  private void selectOfficer(String officer) {
    if (officer != null && !officer.isEmpty())
      pilot.selectFromVisibleText(OFFICER, officer);
  }

  private void inputTransferDate(String transferDate) {
    if (transferDate != null && !transferDate.isEmpty())
      pilot.clearAndSendKeys(TRANSFER_DATE, transferDate);
  }

  private void selectTransferReason(String transferReason) {
    if (transferReason != null && !transferReason.isEmpty())
      pilot.selectFromVisibleText(TRANSFER_REASON, transferReason);
  }

  private void inputNotes(String notes) {
    if (notes != null && !notes.isEmpty())
      pilot.clearAndSendKeys(NOTES, notes);
  }

  private void inputExpectedEndDate(String expectedEndDate) {
    if (expectedEndDate != null && !expectedEndDate.isEmpty())
      pilot.clearAndSendKeys(EXPECTED_END_DATE, expectedEndDate);
  }

  private void inputActualEndDate(String actualEndDate) {
    if (actualEndDate != null && !actualEndDate.isEmpty())
      pilot.clearAndSendKeys(ACTUAL_END_DATE, actualEndDate);
  }

  private void selectTerminationReason(String terminationReason) {
    if (terminationReason != null && !terminationReason.isEmpty())
      pilot.selectFromVisibleText(TERMINATION_REASON, terminationReason);
  }

  private UPWDetailsPage clickCancel() {
    pilot.click(CANCEL);
    return new UPWDetailsPage();
  }

  private UPWDetailsPage clickSave() {
    pilot.click(SAVE);
    return new UPWDetailsPage();
  }

//  public UPWWorksheetSummaryPage clickWorksheetSummary() {
//    pilot.click(WORKSHEET_SUMMARY);
//    return new UPWWorksheetSummaryPage();
//  }
//
//  public UPWAdjustmentPage clickAdjustment() {
//    pilot.click(ADJUSTMENT);
//    return new UPWAdjustmentPage();
//  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
    String value = getInputValuesIndex(inputValuesIndex);

    this.inputLength(testData.getInputValue(TEST_DATA_PREFIX + value, "Length"));
    this.inputImposedSentenceDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ImposedDate"));
    this.inputExpectedStartDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ExpectedStartDate"));
    this.inputActualStartDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ActualStartDate"));
    this.selectTeam(testData.getInputValue(TEST_DATA_PREFIX + value, "Team"));
    this.selectOfficer(testData.getInputValue(TEST_DATA_PREFIX + value, "Officer"));
    this.inputTransferDate(testData.getInputValue(TEST_DATA_PREFIX + value, "TransferDate"));
    this.selectTransferReason(testData.getInputValue(TEST_DATA_PREFIX + value, "TransferReason"));
    this.inputNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "RequirementNotes"));
    this.inputExpectedEndDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ExpectedEndDate"));
    this.inputActualEndDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ActualEndDate"));
    this.selectTerminationReason(testData.getInputValue(TEST_DATA_PREFIX + value, "TerminationReason"));

    //FileUtils.saveScreenshot(this.getClass().getSimpleName());

    return type.cast(this.clickSave());
  }
}

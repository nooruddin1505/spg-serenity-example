package uk.gov.gsi.justice.spg.test.basetest;

import dependencies.adapter.utils.FileUtils;
import uk.gov.gsi.justice.spg.test.page.HomePage;
import uk.gov.gsi.justice.spg.test.page.LoginPage;
import uk.gov.gsi.justice.spg.test.page.delius.AddOffenderPage;
import uk.gov.gsi.justice.spg.test.page.delius.NationalSearchPage;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.AddEventPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddAddressAssessmentPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddAddressPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddressAssessmentListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddressListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.DiversityDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.OffenderDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.OffenderTransferRequestPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.UpdateDiversityDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.UpdateOffenderPage;

public class DeliusEvents extends DeliusBaseTest {
  public static final int DEFAULT_INPUT_VALUES_INDEX = 0;

  protected HomePage login() {
    String username = configData.getProperty("Delius.Username");
    String password = configData.getProperty("Delius.Password");
    String host = configData.getProperty("Delius.Host");
    String port = configData.getProperty("Delius.Port");

    return new LoginPage().login(username, password, host, port, driver);
  }
//
//  protected HomePage setUserPreferences(HomePage homePage, ShowStaffGrade status) {
//
//    UserPreferencesPage userPreferencesPage = homePage.clickUUserPreferencesButton();
//
//    if (status.equals(ShowStaffGrade.Yes)) {
//      homePage = userPreferencesPage.clickShowStaffGrade();
//    } else if (status.equals(ShowStaffGrade.NO)) {
//      homePage = userPreferencesPage.clickDoNotShowStaffGrade();
//    }
//
//    return homePage;
//  }
//
//  protected HomePage setUserPreferences(HomePage homePage, ShowRowNumber status) {
//    UserPreferencesPage userPreferencesPage = homePage.clickUUserPreferencesButton();
//
//    if (status.equals(ShowRowNumber.Yes)) {
//      homePage = userPreferencesPage.clickShowRowNumber();
//    } else if (status.equals(ShowRowNumber.NO)) {
//      homePage = userPreferencesPage.clickDoNotShowRowNumber();
//    }
//
//    return homePage;
//  }

  protected OffenderDetailsPage addOffender(HomePage homepage) {
    NationalSearchPage searchPage = homepage.clickNationalSearchButton();

    searchPage.inputFirstName(this.offenderFirstName);
    searchPage.inputLastName(this.offenderLastName);

    searchPage = searchPage.clickSearchButton();

    FileUtils.saveScreenshot(searchPage.getClass().getSimpleName());

    AddOffenderPage addOffenderPage = searchPage.clickAddOffender();

    return addOffenderPage.completePage(DEFAULT_INPUT_VALUES_INDEX, OffenderDetailsPage.class);
  }

  protected OffenderDetailsPage updateOffender(OffenderCommon offenderCommon) {
    return updateOffender(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
  }

  protected OffenderDetailsPage updateOffender(OffenderCommon offenderCommon, int inputValuesIndex) {
    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();

    UpdateOffenderPage updateOffenderPage = offenderDetailsPage.clickUpdate();

    return updateOffenderPage.completePage(inputValuesIndex, OffenderDetailsPage.class);
  }

//  protected OffenderDetailsPage addNOMSNumberToOffender(OffenderCommon offenderCommon, String NOMSNumber) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    UpdateOffenderPage updateOffenderPage = offenderDetailsPage.clickUpdate();
//
//    updateOffenderPage.inputNOMSNumber(NOMSNumber);
//
//    return updateOffenderPage.clickSave();
//  }
//
//  protected NationalSearchPage deleteOffender(OffenderCommon offenderCommon) {
//    SleepUtils.sleep();
//
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    DeleteOffenderPage deleteOffenderPage = offenderDetailsPage.clickDelete();
//
//    return deleteOffenderPage.clickConfirm();
//  }

  protected DiversityDetailsPage updateDiversityDetails(OffenderCommon offenderCommon) {
    return updateDiversityDetails(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
  }

  protected DiversityDetailsPage updateDiversityDetails(OffenderCommon offenderCommon, int inputValuesIndex) {
    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();

    DiversityDetailsPage diversityDetailsPage = offenderDetailsPage.clickEqualityMonitoringLink();

    UpdateDiversityDetailsPage updateDiversityDetailsPage = diversityDetailsPage.clickButtonUpdate();

    return updateDiversityDetailsPage.completePage(inputValuesIndex, DiversityDetailsPage.class);
  }

  protected EventDetailsCommon addEvent(OffenderCommon offenderCommon) {
    return addEvent(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
  }

  protected EventDetailsCommon addEvent(OffenderCommon offenderCommon, int inputValuesIndex) {
    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();

    EventListPage eventListPage = offenderDetailsPage.clickEventListLink();

    AddEventPage addEventPage = eventListPage.clickAddButton();

    return addEventPage.completePage(inputValuesIndex, EventDetailsCommon.class);
  }

//  protected EventListPage deleteEventByRow(OffenderCommon offenderCommon, int row) {
//    SleepUtils.sleep();
//
//    EventListPage eventListPage = offenderCommon.clickEventListLink();
//
//    DeleteEventPage deleteEventPage = eventListPage.clickDeleteEventByRow(row);
//
//    return deleteEventPage.clickConfirm();
//  }

  protected AddressListPage addAddress(OffenderCommon offenderCommon) {
    return addAddress(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
  }

  protected AddressListPage addAddress(OffenderCommon offenderCommon, int inputValuesIndex) {
    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();

    AddressListPage addressListPage = offenderDetailsCommon.clickAddressesLink();

    AddAddressPage addAddressPage = addressListPage.clickAddAddressButton();

    return addAddressPage.completePage(inputValuesIndex, AddressListPage.class);
  }

//  // TODO RL - We really need to fix these methods.
//  protected AddressListPage updateAddressByFirstRow(OffenderCommon offenderCommon) {
//    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
//
//    AddressListPage addressListPage = offenderDetailsCommon.clickAddressesLink();
//
//    // TODO RL - Updating the first row. I don't like this, needs changing to be dynamic. Currently the method doesn't do what the name describes.
//    UpdateAddressPage updateAddressPage = addressListPage.clickFirstUpdateLink();
//
//    return updateAddressPage.completePage(DEFAULT_INPUT_VALUES_INDEX, AddressListPage.class);
//  }
//
//  protected AddressListPage updateAddress(OffenderCommon offenderCommon) {
//    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
//
//    AddressListPage addressListPage = offenderDetailsCommon.clickAddressesLink();
//
//    UpdateAddressPage updateAddressPage = addressListPage.clickFirstUpdateLink();
//
//    return updateAddressPage.completePage(DEFAULT_INPUT_VALUES_INDEX, AddressListPage.class);
//  }
//
//  protected AddressListPage deleteAddressByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    AddressListPage addressListPage = offenderDetailsPage.clickAddressesLink();
//
//    DeleteAddressPage deleteAddressPage = addressListPage.clickDeleteAddressByRow(row);
//
//    return deleteAddressPage.clickConfirm();
//  }
//
//  protected DrugTestingReferralsListPage addDrugTestingReferral(EventDetailsCommon eventDetailsCommon) {
//    return addDrugTestingReferral(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected DrugTestingReferralsListPage addDrugTestingReferral(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    DrugTestingReferralsListPage drugTestingReferralsListPage = eventDetailsCommon.clickDrugTestingReferralsLink();
//
//    AddDrugTestingReferralPage addDrugTestingReferralPage = drugTestingReferralsListPage.clickAddReferral();
//
//    return addDrugTestingReferralPage.completePage(inputValuesIndex, DrugTestingReferralsListPage.class);
//  }
//
//  protected DrugTestingReferralsListPage updateDrugTestingReferral(EventDetailsCommon eventDetailsCommon, int referralrow) {
//    return updateDrugTestingReferral(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, referralrow);
//  }
//
//  protected DrugTestingReferralsListPage updateDrugTestingReferral(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//
//    DrugTestingReferralsListPage drugTestingReferralsListPage = eventDetailsCommon.clickDrugTestingReferralsLink();
//
//    UpdateDrugTestingReferralPage updateDrugTestingReferralPage = drugTestingReferralsListPage.clickUpdateByRow(row);
//
//    return updateDrugTestingReferralPage.completePage(inputValuesIndex, DrugTestingReferralsListPage.class);
//  }
//
//  protected DrugTestingAssessmentListPage addDrugTestingAssessment(DrugTestingReferralsListPage drugTestingReferralsListPage, int assessmentRow) {
//    return addDrugTestingAssessment(drugTestingReferralsListPage, DEFAULT_INPUT_VALUES_INDEX, assessmentRow);
//  }
//
//  protected DrugTestingAssessmentListPage addDrugTestingAssessment(DrugTestingReferralsListPage drugTestingReferralsListPage, int inputValuesIndex, int assessmentRow) {
//    DrugTestingAssessmentListPage drugTestingAssessmentListPage = drugTestingReferralsListPage.clickAssessmentByRow(assessmentRow);
//
//    AddDrugTestingAssessmentPage addDrugTestingAssessmentPage = drugTestingAssessmentListPage.clickAdd();
//
//    return addDrugTestingAssessmentPage.completePage(inputValuesIndex, DrugTestingAssessmentListPage.class);
//  }
//
//  protected DrugTestingAssessmentListPage updateDrugTestingAssessment(DrugTestingReferralsListPage drugTestingReferralsListPage, int assessmentRow) {
//    return addDrugTestingAssessment(drugTestingReferralsListPage, DEFAULT_INPUT_VALUES_INDEX, assessmentRow);
//  }
//
//  protected DrugTestingAssessmentListPage updateDrugTestingAssessment(DrugTestingReferralsListPage drugTestingReferralsListPage, int inputValuesIndex, int assessmentRow) {
//    DrugTestingAssessmentListPage drugTestingAssessmentListPage = drugTestingReferralsListPage.clickAssessmentByRow(assessmentRow);
//
//    UpdateDrugTestingAssessmentPage updateDrugTestingAssessmentPage = drugTestingAssessmentListPage.clickUpdateByRow(assessmentRow);
//
//    return updateDrugTestingAssessmentPage.completePage(inputValuesIndex, DrugTestingAssessmentListPage.class);
//  }
//
//  protected DrugTestDetailsListPage addDrugTestProfile(DrugTestingAssessmentListPage drugTestingAssessmentListPage) {
//    return addDrugTestProfile(drugTestingAssessmentListPage, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected DrugTestDetailsListPage addDrugTestProfile(DrugTestingAssessmentListPage drugTestingAssessmentListPage, int inputValuesIndex) {
//    DrugTestingReferralsListPage drugTestingReferralsListPage = drugTestingAssessmentListPage.clickDrugTestingReferralsLink();
//
//    // TODO - RL, why are we only clicking the first row? Surely this should be parameterised with this method too?
//    DrugTestingReferralDetailsPage drugTestingReferralDetailsPage = drugTestingReferralsListPage.clickViewByRow(1);
//
//    DrugTestListPage drugTestListPage = drugTestingReferralDetailsPage.clickDrugTests();
//
//    AddDrugTestProfilePage addDrugTestProfilePage = drugTestListPage.clickAddDrugTestProfile();
//
//    return addDrugTestProfilePage.completePage(inputValuesIndex, DrugTestDetailsListPage.class);
//  }
//
//  protected DrugTestDetailsListPage updateDrugTestProfile(DrugTestingAssessmentListPage drugTestingAssessmentListPage, int inputValuesIndex, int row) {
//    DrugTestingReferralsListPage drugTestingReferralsListPage = drugTestingAssessmentListPage.clickDrugTestingReferralsLink();
//
//    DrugTestingReferralDetailsPage drugTestingReferralDetailsPage = drugTestingReferralsListPage.clickViewByRow(row);
//
//    DrugTestListPage drugTestListPage = drugTestingReferralDetailsPage.clickDrugTests();
//
//    UpdateDrugTestProfilePage updateDrugTestProfilePage = drugTestListPage.updateDrugTestProfileByRow(row);
//
//    return updateDrugTestProfilePage.completePage(inputValuesIndex, DrugTestDetailsListPage.class);
//  }
//
//  protected DrugTestDetailsListPage addDrugTestProfileFromAssessment(DrugTestingReferralsListPage drugTestingReferralsListPage) {
//    return addDrugTestProfileFromAssessment(drugTestingReferralsListPage, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected DrugTestDetailsListPage addDrugTestProfileFromAssessment(DrugTestingReferralsListPage drugTestingReferralsListPage, int inputValuesIndex) {
//    DrugTestingReferralDetailsPage drugTestingReferralDetailsPage = drugTestingReferralsListPage.clickViewByRow(1);
//
//    DrugTestListPage drugTestListPage = drugTestingReferralDetailsPage.clickDrugTests();
//
//    AddDrugTestProfilePage addDrugTestProfilePage = drugTestListPage.clickAddDrugTestProfile();
//
//    return addDrugTestProfilePage.completePage(inputValuesIndex, DrugTestDetailsListPage.class);
//  }
//
//  protected DrugTestListPage deleteDrugTestByRow(EventDetailsCommon eventDetailsCommon) {
//    return deleteDrugTestByRow(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected DrugTestListPage deleteDrugTestByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    DrugTestingReferralsListPage drugTestingReferralsListPage = eventDetailsCommon.clickDrugTestingReferralsLink();
//
//    DrugTestingReferralDetailsPage drugTestingReferralDetailsPage = drugTestingReferralsListPage.clickViewByRow(1);
//
//    DrugTestListPage drugTestListPage = drugTestingReferralDetailsPage.clickDrugTests();
//
//    DeleteDrugTestPage deleteDrugTestPage = drugTestListPage.deleteDrugTestingTestByRow(row);
//
//    return deleteDrugTestPage.clickConfirmButton();
//  }
//
//  protected DrugTestDetailsListPage addDrugTest(DrugTestDetailsListPage drugTestDetailsListPage) {
//    return addDrugTest(drugTestDetailsListPage, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected DrugTestDetailsListPage addDrugTest(DrugTestDetailsListPage drugTestDetailsListPage, int inputValuesIndex) {
//    AddDrugTestPage addDrugTestPage = drugTestDetailsListPage.clickAddNewDrugTest();
//
//    return addDrugTestPage.completePage(inputValuesIndex, DrugTestDetailsListPage.class);
//  }
//
//  protected DrugTestDetailsListPage updateDrugTest(DrugTestDetailsListPage drugTestDetailsListPage, int row) {
//    return updateDrugTest(drugTestDetailsListPage, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected DrugTestDetailsListPage updateDrugTest(DrugTestDetailsListPage drugTestDetailsListPage, int inputValuesIndex, int row) {
//    UpdateDrugsTest updateDrugTest = drugTestDetailsListPage.clickUpdateDrugTest(row);
//
//    return updateDrugTest.completePage(inputValuesIndex, DrugTestDetailsListPage.class);
//  }
//
//  protected DrugTestingAssessmentListPage deleteDrugTestingAssessmentByReferralAndRow(EventDetailsCommon eventDetailsCommon, int referralRow, int assessmentRow) {
//    DrugTestingReferralsListPage drugTestingReferralsListPage = eventDetailsCommon.clickDrugTestingReferralsLink();
//
//    DrugTestingAssessmentListPage drugTestingAssessmentListPage = drugTestingReferralsListPage.clickAssessmentByRow(referralRow);
//
//    DeleteDrugTestingAssessmentPage deleteDrugTestingAssessmentPage = drugTestingAssessmentListPage.clickDeleteByRow(assessmentRow);
//
//    return deleteDrugTestingAssessmentPage.clickConfirm();
//  }
//
//  protected DrugTestingReferralDetailsPage viewDrugTestReferral(EventDetailsCommon eventDetailsCommon, int row) {
//    DrugTestingReferralsListPage drugTestingReferralsListPage = eventDetailsCommon.clickDrugTestingReferralsLink();
//
//    return drugTestingReferralsListPage.clickViewByRow(1);
//  }
//
//  protected DrugTestingAssessmentDetailsPage viewDrugTestAssessment(DrugTestingReferralsListPage drugTestingReferralsListPage, int row) {
//    DrugTestingAssessmentListPage drugTestingAssessmentListPage = drugTestingReferralsListPage.clickAssessmentByRow(row);
//
//    return drugTestingAssessmentListPage.clickViewByRow(1);
//  }
//
//  protected ReferralsListPage addReferral(EventDetailsCommon eventDetailsCommon) {
//    return addReferral(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ReferralsListPage addReferral(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ReferralsListPage referralsListPage = eventDetailsCommon.clickReferralsLink();
//
//    AddReferralPage addReferralPage = referralsListPage.clickAdd();
//
//    return addReferralPage.completePage(inputValuesIndex, ReferralsListPage.class);
//  }
//
//  protected ReferralsListPage deleteReferralByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    ReferralsListPage referralsListPage = eventDetailsCommon.clickReferralsLink();
//
//    DeleteReferralPage deleteReferralPage = referralsListPage.clickDeleteByRow(row);
//
//    return deleteReferralPage.clickConfirm();
//  }
//
//  protected ReferralsListPage addAssessment(ReferralsListPage referralsListPage, int row, int inputValuesIndex) {
//    AssessmentListPage assessmentListPage = referralsListPage.clickAssessmentsLinkByRow(row);
//
//    AddAssessmentPage addAssessmentPage = assessmentListPage.clickAdd();
//
//    assessmentListPage = addAssessmentPage.completePage(inputValuesIndex, AssessmentListPage.class);
//
//    return assessmentListPage.clickReferralsLink();
//  }
//
//  protected AssessmentListPage deleteAssessmentByReferralAndRow(EventDetailsCommon eventDetailsCommon, int referralRow, int assessmentRow) {
//    ReferralsListPage referralsListPage = eventDetailsCommon.clickReferralsLink();
//
//    AssessmentListPage assessmentListPage = referralsListPage.clickAssessmentsLinkByRow(referralRow);
//
//    DeleteAssessmentPage deleteAssessmentPage = assessmentListPage.clickDeleteByRow(assessmentRow);
//
//    return deleteAssessmentPage.clickConfirm();
//  }

  protected AddressAssessmentListPage addAddressAssessment(AddressListPage addressListPage) {
    return addAddressAssessment(addressListPage, DEFAULT_INPUT_VALUES_INDEX);
  }

  protected AddressAssessmentListPage addAddressAssessment(AddressListPage addressListPage, int inputValuesIndex) {
    AddressAssessmentListPage addressAssessmentListPage = addressListPage.clickFirstAssessmentLink();

    AddAddressAssessmentPage addAddressAssessmentPage = addressAssessmentListPage.clickAddAddressAssessmentButton();

    return addAddressAssessmentPage.completePage(inputValuesIndex, AddressAssessmentListPage.class);
  }

//  protected AddressAssessmentListPage updateAddressAssessment(AddressListPage addressListPage) {
//    return updateAddressAssessment(addressListPage, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected AddressAssessmentListPage updateAddressAssessment(AddressListPage addressListPage, int inputValuesIndex) {
//    AddressAssessmentListPage addressAssessmentListPage = addressListPage.clickFirstAssessmentLink();
//
//    UpdateAddressAssessmentPage updateAddressAssessmentPage = addressAssessmentListPage.clickFirstAssessmentUpdateLink();
//
//    return updateAddressAssessmentPage.completePage(inputValuesIndex, AddressAssessmentListPage.class);
//  }
//
//  protected AddressAssessmentListPage deleteAddressAssessmentByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    AddressListPage addressListPage = offenderDetailsPage.clickAddressesLink();
//
//    // TODO - Again, why are we clicking the first row?
//    AddressAssessmentListPage addressAssessmentListPage = addressListPage.clickFirstAssessmentLink();
//
//    DeleteAddressAssessmentPage deleteAddressAssessmentPage = addressAssessmentListPage.clickDeleteAssessmentByRow(row);
//
//    return deleteAddressAssessmentPage.clickConfirm();
//  }
//
//  protected AliasListPage addAlias(OffenderCommon offenderCommon) {
//    return addAlias(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected AliasListPage addAlias(OffenderCommon offenderCommon, int inputValuesIndex) {
//    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
//
//    AliasListPage aliasListPage = offenderDetailsCommon.clickOffenderAliasesLink();
//
//    AddAliasPage addAliasPage = aliasListPage.clickAdd();
//
//    return addAliasPage.completePage(inputValuesIndex, AliasListPage.class);
//  }
//
//  protected AliasListPage updateAliasByRow(OffenderCommon offenderCommon, int row) {
//    return updateAliasByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected AliasListPage updateAliasByRow(OffenderCommon offenderCommon, int inputValuesIndex, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    AliasListPage aliasListPage = offenderDetailsPage.clickOffenderAliasesLink();
//
//    UpdateAliasPage updateAliasPage = aliasListPage.updateAliasByRow(row);
//
//    return updateAliasPage.completePage(inputValuesIndex, AliasListPage.class);
//  }
//
//  protected OffenderDetailsCommon addPersonalContact(OffenderCommon offenderCommon) {
//    return addPersonalContact(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected OffenderDetailsCommon addPersonalContact(OffenderCommon offenderCommon, int inputValuesIndex) {
//    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
//
//    PersonalContactListPage personalContactsPage = offenderDetailsCommon.clickPersonalContactsLink();
//
//    AddPersonalContactPage addPersonalContactPage = personalContactsPage.clickAddPersonalContact();
//
//    return addPersonalContactPage.completePage(inputValuesIndex, OffenderDetailsCommon.class);
//  }
//
//  protected PersonalContactListPage updatePersonalContactByRow(OffenderCommon offenderCommon, int row) {
//    return updatePersonalContactByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected PersonalContactListPage updatePersonalContactByRow(OffenderCommon offenderCommon, int inputValuesIndex, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    PersonalContactListPage personalContactListPage = offenderDetailsPage.clickPersonalContactsLink();

//    UpdatePersonalContactsPage updatePersonalContactPage = personalContactListPage.clickUpdatePersonalContactByRow(row);
//
//    return updatePersonalContactPage.completePage(inputValuesIndex, PersonalContactListPage.class);
//  }
//
//  protected PersonalContactListPage deletePersonalContactByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    PersonalContactListPage personalContactListPage = offenderDetailsPage.clickPersonalContactsLink();
//
//    DeletePersonalContactPage deletePersonalContactPage = personalContactListPage.clickDeletePersonalContactByRow(row);
//
//    return deletePersonalContactPage.clickConfirm();
//  }
//
//  protected AdditionalIdentifierListPage addAdditionalIdentifier(OffenderCommon offenderCommon) {
//    return addAdditionalIdentifier(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected AdditionalIdentifierListPage addAdditionalIdentifier(OffenderCommon offenderCommon, int inputValuesIndex) {
//    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
//
//    AdditionalIdentifierListPage additionalIdentifierListPage = offenderDetailsCommon.clickAdditionalIdentifier();
//
//    AddAdditionalIdentifierPage addAdditionalIdentifierPage = additionalIdentifierListPage.clickButtonAdd();
//
//    return addAdditionalIdentifierPage.completePage(inputValuesIndex, AdditionalIdentifierListPage.class);
//  }
//
//  protected DisabilityListPage addDisability(OffenderCommon offenderCommon) {
//    return addDisability(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected DisabilityListPage addDisability(OffenderCommon offenderCommon, int inputValuesIndex) {
//    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
//
//    DisabilityListPage disabilityListPage = offenderDetailsCommon.clickDisabilitiesAndAdjustmentsLink();
//
//    AddDisabilityPage addDisabilityPage = disabilityListPage.clickAddDisability();
//
//    return addDisabilityPage.completePage(inputValuesIndex, DisabilityListPage.class);
//  }
//
//  protected DisabilityListPage updateDisabilityByRow(OffenderCommon offenderCommon, int row) {
//    return updateDisabilityByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected DisabilityListPage updateDisabilityByRow(OffenderCommon offenderCommon, int inputValuesIndex, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    DisabilityListPage disabilityListPage = offenderDetailsPage.clickDisabilitiesAndAdjustmentsLink();
//
//    UpdateDisabilityPage updateDisabilityPage = disabilityListPage.clickUpdateDisabilityByRow(row);
//
//    return updateDisabilityPage.completePage(inputValuesIndex, DisabilityListPage.class);
//  }
//
//  protected DisabilityListPage deleteDisabilityByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    DisabilityListPage disabilityListPage = offenderDetailsPage.clickDisabilitiesAndAdjustmentsLink();
//
//    DeleteDisabilityPage deleteDisabilityPage = disabilityListPage.clickDeleteDisabilityByRow(row);
//
//    return deleteDisabilityPage.clickConfirm();
//  }
//
//  protected DisabilityListPage addDisabilityAdjustment(DisabilityListPage disabilityListPage, int row) {
//    return addDisabilityAdjustment(disabilityListPage, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected DisabilityListPage addDisabilityAdjustment(DisabilityListPage disabilityListPage, int inputValuesIndex, int row) {
//    AddDisabilityAdjustmentPage addDisabilityAdjustmentPage = disabilityListPage.clickAddDisabilityAdjustment(row);
//
//    return addDisabilityAdjustmentPage.completePage(inputValuesIndex, DisabilityListPage.class);
//  }
//
//  protected DisabilityListPage updateDisabilityAdjustmentByRow(OffenderCommon offenderCommon, int row) {
//    return updateDisabilityAdjustmentByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected DisabilityListPage updateDisabilityAdjustmentByRow(OffenderCommon offenderCommon, int inputValuesIndex, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    DisabilityListPage disabilityListPage = offenderDetailsPage.clickDisabilitiesAndAdjustmentsLink();
//
//    UpdateDisabilityAdjustmentPage updateDisabilityAdjustmentPage = disabilityListPage.clickUpdateDisabilityAdjustmentByRow(row);
//
//    return updateDisabilityAdjustmentPage.completePage(inputValuesIndex, DisabilityListPage.class);
//  }
//
//  protected DisabilityListPage deleteDisabilityAdjustmentByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    DisabilityListPage disabilityListPage = offenderDetailsPage.clickDisabilitiesAndAdjustmentsLink();
//
//    DeleteDisabilityAdjustmentPage deleteDisabilityAdjustmentPage = disabilityListPage.clickDeleteDisabilityAdjustmentByRow(row);
//
//    return deleteDisabilityAdjustmentPage.clickConfirm();
//  }
//
//  protected AddLicenceConditionPage addRelease(EventDetailsCommon eventDetailsCommon, int row, String caseReferenceNumber) {
//    return addRelease(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row, caseReferenceNumber);
//  }
//
//  protected AddLicenceConditionPage addRelease(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row, String caseReferenceNumber) {
//
//    checkAndRemoveNOMSNumber(eventDetailsCommon, 1, nomsNumber);
//
//    OffenderSummaryPage offenderSummaryPage = searchOffender(eventDetailsCommon, caseReferenceNumber);
//
//    OffenderDetailsPage offenderDetailsPage = addNOMSNumberToOffender(offenderSummaryPage, nomsNumber);
//
//    eventDetailsCommon = viewEventByRow(offenderDetailsPage, 1);
//
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsCommon.clickThroughcare();
//
//    AddReleasePage addReleasePage = throughcareDetailsPage.clickAddRelease();
//
////     We get the value of the Release Date by selecting the relevant search result from the list
//    addReleasePage = addReleasePage.selectSearchResultByRow(row);
//
//    return addReleasePage.completePage(inputValuesIndex, AddLicenceConditionPage.class);
//  }
//
//
//  protected ThroughcareDetailsPage editUndoRelease(EventDetailsCommon eventDetailsCommon) {
//    return editUndoRelease(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ThroughcareDetailsPage editUndoRelease(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsCommon.clickThroughcare();
//
//    EditUndoReleasePage editUndoReleasePage = throughcareDetailsPage.clickEditRelease();
//
//    return editUndoReleasePage.completePage(inputValuesIndex, ThroughcareDetailsPage.class);
//  }
//
//  protected AddLicenceConditionPage addReleaseFromCourt(EventDetailsCommon eventDetailsCommon) {
//    return addReleaseFromCourt(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected AddLicenceConditionPage addReleaseFromCourt(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsCommon.clickThroughcare();
//
//    ReleaseFromCourtPage releaseFromCourtPage = throughcareDetailsPage.clickReleaseFromCourt();
//
//    return releaseFromCourtPage.completePage(inputValuesIndex, AddLicenceConditionPage.class);
//  }
//
//  // TODO - Why is the page parameter here AddLicenceConditionPage? That means the navigation needs to be done inside the test.
//  protected LicenceConditionListPage addLicenceCondition(AddLicenceConditionPage addLicenceConditionPage) {
//    return addLicenceCondition(addLicenceConditionPage, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected LicenceConditionListPage addLicenceCondition(AddLicenceConditionPage addLicenceConditionPage, int inputValuesIndex) {
//    return addLicenceConditionPage.completePage(inputValuesIndex, LicenceConditionListPage.class);
//  }
//
//  protected LicenceConditionListPage addLicenceConditionFromEvent(EventDetailsCommon eventDetailsCommon) {
//    return addLicenceConditionFromEvent(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected LicenceConditionListPage addLicenceConditionFromEvent(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    LicenceConditionListPage licenceConditionListPage = eventDetailsCommon.clickLicenceConditions();
//
//    AddLicenceConditionPage addLicenceConditionPage = licenceConditionListPage.clickAddLicenceConditions();
//    return addLicenceConditionPage.completePage(inputValuesIndex, LicenceConditionListPage.class);
//  }
//
//  protected LicenceConditionListPage editLicenceConditionByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return editLicenceConditionByRow(eventDetailsCommon, row, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected LicenceConditionListPage editLicenceConditionByRow(EventDetailsCommon eventDetailsCommon, int row, int inputValuesIndex) {
//    LicenceConditionListPage licenceConditionListPage = eventDetailsCommon.clickLicenceConditions();
//
//    UpdateLicenceConditionPage editLicenceConditionPage = licenceConditionListPage.updateLicenceConditionByRow(1);
//
//    return editLicenceConditionPage.completePage(inputValuesIndex, LicenceConditionListPage.class);
//  }
//
//  protected LicenceConditionDetailsPage viewLicenceConditionByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return viewLicenceConditionByRow(eventDetailsCommon, row, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected LicenceConditionDetailsPage viewLicenceConditionByRow(EventDetailsCommon eventDetailsCommon, int row, int inputValuesIndex) {
//    LicenceConditionListPage licenceConditionListPage = eventDetailsCommon.clickLicenceConditions();
//
//    LicenceConditionDetailsPage licenceConditionDetailsPage = licenceConditionListPage.viewLicenceConditionByRow(row);
//
//    return licenceConditionDetailsPage;
//  }
//
//  protected EventDetailsCommon addCourtAppearance(EventDetailsCommon eventDetailsCommon) {
//    return addCourtAppearance(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected EventDetailsCommon addCourtAppearance(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    CourtAppearanceListPage courtAppearanceListPage = eventDetailsCommon.clickCourtAppearancesLink();
//
//    AddCourtAppearancePage addCourtAppearancePage = courtAppearanceListPage.clickAddAppearanceButton();
//
//    return addCourtAppearancePage.completePage(inputValuesIndex, EventDetailsCommon.class);
//  }
//
//  protected CourtAppearanceListPage deleteCourtAppearanceByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    CourtAppearanceListPage courtAppearanceListPage = eventDetailsCommon.clickCourtAppearancesLink();
//
//    DeleteCourtAppearancePage deleteCourtAppearancePage = courtAppearanceListPage.clickDeleteAppearanceLinkByRow(row);
//
//    return deleteCourtAppearancePage.clickConfirm();
//  }
//
//  protected EventDetailsCommon addCourtReport(EventDetailsCommon eventDetailsCommon, int row) {
//    return addCourtReport(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected EventDetailsCommon addCourtReport(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    CourtAppearanceListPage courtAppearanceListPage = eventDetailsCommon.clickCourtAppearancesLink();
//
//    EditCourtAppearancePage editCourtAppearancePage = courtAppearanceListPage.clickUpdateAppearanceByRow(row);
//
//    AddCourtReportPage addCourtReportPage = editCourtAppearancePage.clickNewCourtReport();
//
//    return addCourtReportPage.completePage(inputValuesIndex, EventDetailsCommon.class);
//  }
//
//  protected EventDetailsCommon updateCourtReportByRow(EventDetailsCommon eventDetailsCommon, int appearanceRow, int reportRow) {
//    return updateCourtReportByRow(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, appearanceRow, reportRow);
//  }
//
//  // TODO - Should be a Javadoc comment
//  // This method allows one to specify the appearance and associated report that they wish to update
//  protected EventDetailsCommon updateCourtReportByRow(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int appearanceRow, int reportRow) {
//    CourtAppearanceListPage courtAppearanceListPage = eventDetailsCommon.clickCourtAppearancesLink();
//
//    EditCourtAppearancePage editCourtAppearancePage = courtAppearanceListPage.clickUpdateAppearanceByRow(appearanceRow);
//
//    // We have to fill in the "Plea" field before nDelius will allow us to successfully click on a link to update a court report
//    editCourtAppearancePage.selectPlea(testData.getInputValue("EditCourtAppearance.Plea"));
//    editCourtAppearancePage.selectOutcome(testData.getInputValue("EditCourtAppearance.Outcome"));
//    editCourtAppearancePage.inputLength(testData.getInputValue("EditCourtAppearance.Length"));
//
//    UpdateCourtReportPage updateCourtReportPage = editCourtAppearancePage.clickUpdateCourtReportByRow(reportRow);
//
//    return updateCourtReportPage.completePage(inputValuesIndex, EventDetailsCommon.class);
//  }
//
//  protected CourtAppearanceListPage updateCourtAppearanceByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return updateCourtAppearanceByRow(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected CourtAppearanceListPage updateCourtAppearanceByRow(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    CourtAppearanceListPage courtAppearanceListPage = eventDetailsCommon.clickCourtAppearancesLink();
//
//    EditCourtAppearancePage editCourtAppearancePage = courtAppearanceListPage.clickUpdateAppearanceByRow(row);
//
//    return editCourtAppearancePage.completePage(inputValuesIndex, CourtAppearanceListPage.class);
//  }
//
//  protected CourtAppearanceListPage updateCourtAppearanceByOutcome(EventDetailsCommon eventDetailsCommon, String outcome) {
//    return updateCourtAppearanceByOutcome(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, outcome);
//  }
//
//  protected CourtAppearanceListPage updateCourtAppearanceByOutcome(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, String outcome) {
//    CourtAppearanceListPage courtAppearanceListPage = eventDetailsCommon.clickCourtAppearancesLink();
//
//    EditCourtAppearancePage editCourtAppearancePage = courtAppearanceListPage.clickUpdateAppearanceByOutcome(outcome);
//
//    return editCourtAppearancePage.completePage(inputValuesIndex, CourtAppearanceListPage.class);
//  }
//
//  protected PersonalCircumstancesListPage addPersonalCircumstance(OffenderCommon offenderCommon) {
//    return addPersonalCircumstance(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected PersonalCircumstancesListPage addPersonalCircumstance(OffenderCommon offenderCommon, int inputValuesIndex) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    PersonalCircumstancesListPage personalCircumstancesListPage = offenderDetailsPage.clickPersonalCircumstancesLink();
//
//    AddPersonalCircumstancePage addPersonalCircumstancePage = personalCircumstancesListPage.clickAddButton();
//
//    return addPersonalCircumstancePage.completePage(inputValuesIndex, PersonalCircumstancesListPage.class);
//  }
//
//  protected PersonalCircumstancesListPage updatePersonalCircumstanceByRow(OffenderCommon offenderCommon, int row) {
//    return updatePersonalCircumstanceByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected PersonalCircumstancesListPage updatePersonalCircumstanceByRow(OffenderCommon offenderCommon, int inputValuesIndex, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    PersonalCircumstancesListPage personalCircumstancesListPage = offenderDetailsPage.clickPersonalCircumstancesLink();
//
//    UpdatePersonalCircumstancePage updatePersonalCircumstancePage = personalCircumstancesListPage.clickUpdateByRow(row);
//
//    return updatePersonalCircumstancePage.completePage(inputValuesIndex, PersonalCircumstancesListPage.class);
//  }
//
//  protected RequirementsListPage addRequirement(EventDetailsCommon eventDetailsCommon) {
//    return addRequirement(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected RequirementsListPage addRequirement(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    RequirementsListPage requirementsListPage = eventDetailsCommon.clickRequirementsLink();
//
//    AddRequirementPage addRequirementPage = requirementsListPage.clickAddRequirements();
//
//    return addRequirementPage.completePage(inputValuesIndex, RequirementsListPage.class);
//  }
//
//  protected RequirementsListPage updateRequirementByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return updateRequirementByRow(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected RequirementsListPage updateRequirementByRow(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    RequirementsListPage requirementsListPage = eventDetailsCommon.clickRequirementsLink();
//
//    UpdateRequirementPage updateRequirementPage = requirementsListPage.updateRequirementByRow(row);
//
//    return updateRequirementPage.completePage(inputValuesIndex, RequirementsListPage.class);
//  }
//
//  protected AddLicenceConditionPage forceRelease(EventDetailsCommon eventDetailsCommon) {
//    return forceRelease(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected AddLicenceConditionPage forceRelease(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsCommon.clickThroughcare();
//
//    ForceReleasePage forceReleasePage = throughcareDetailsPage.clickForceRelease();
//
//    return forceReleasePage.completePage(inputValuesIndex, AddLicenceConditionPage.class);
//  }
//
//  protected EventDetailsCommon addSupervisionRequirement(EventDetailsCommon eventDetailsCommon) {
//    return addSupervisionRequirement(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected EventDetailsCommon addSupervisionRequirement(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    SupervisionRequirementsListPage supervisionRequirementsListPage = eventDetailsCommon.clickSupervisionRequirementLink();
//
//    AddSupervisionRequirementPage addSupervisionRequirementListPage = supervisionRequirementsListPage.clickAddSupervisionRequirements();
//
//    return addSupervisionRequirementListPage.completePage(inputValuesIndex, EventDetailsCommon.class);
//  }
//
//  protected SupervisionRequirementsListPage updateSupervisionRequirement(EventDetailsCommon eventDetailsCommon) {
//    return updateSupervisionRequirement(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected SupervisionRequirementsListPage updateSupervisionRequirement(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    SupervisionRequirementsListPage supervisionRequirementsListPage = eventDetailsCommon.clickSupervisionRequirementLink();
//
//    UpdateSupervisionRequirementPage updateSupervisionRequirementPage = supervisionRequirementsListPage.clickUpdateByRow(1);
//
//    return updateSupervisionRequirementPage.completePage(inputValuesIndex, SupervisionRequirementsListPage.class);
//  }
//
//  protected SupervisionRequirementsListPage deleteSupervisionRequirementByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    SupervisionRequirementsListPage supervisionRequirementsListPage = eventDetailsCommon.clickSupervisionRequirementLink();
//
//    DeleteSupervisionRequirementPage deleteSupervisionRequirementPage = supervisionRequirementsListPage.clickDeleteByRow(row);
//
//    return deleteSupervisionRequirementPage.clickConfirm();
//  }
//
//  protected EventDetailsCommon commencePSS(EventDetailsCommon eventDetailsCommon) {
//    return commencePSS(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected EventDetailsCommon commencePSS(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsCommon.clickThroughcare();
//
//    CommencePSSPage commencePSSPage = throughcareDetailsPage.clickCommencePSS();
//
//    return commencePSSPage.completePage(inputValuesIndex, EventDetailsCommon.class);
//  }
//
//  protected UPWProjectsListPage addUPWProject(DeliusCommon deliusCommon) {
//    return addUPWProject(deliusCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected UPWProjectsListPage addUPWProject(DeliusCommon deliusCommon, int inputValuesIndex) {
//    UPWProjectsListPage upwProjectsListPage = deliusCommon.clickUPWProjects();
//
//    AddUPWProjectPage addUPWProjectPage = upwProjectsListPage.clickAddNewProject();
//
//    return addUPWProjectPage.completePage(inputValuesIndex, UPWProjectsListPage.class);
//  }
//
//  protected UPWDetailsPage addUPWAdjustment(EventDetailsCommon eventDetailsCommon) {
//    return addUPWAdjustment(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected UPWDetailsPage addUPWAdjustment(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    UPWDetailsPage upwDetailsPage = eventDetailsCommon.clickUnpaidWorkLink();
//
//    UPWAdjustmentPage upwAdjustmentPage = upwDetailsPage.clickAdjustment();
//
//    return upwAdjustmentPage.completePage(inputValuesIndex, UPWDetailsPage.class);
//  }
//
//  protected UPWDetailsPage updateUPWRequirement(EventDetailsCommon eventDetailsCommon) {
//    return updateUPWRequirement(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected UPWDetailsPage updateUPWRequirement(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    UPWDetailsPage upwDetailsPage = eventDetailsCommon.clickUnpaidWorkLink();
//
//    UpdateUPWDetailsPage updateUPWDetailsPage = upwDetailsPage.clickUpdate();
//
//    return updateUPWDetailsPage.completePage(inputValuesIndex, UPWDetailsPage.class);
//  }
//
//  protected UPWDetailsPage updateUPWRequirementByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return updateUPWRequirementByRow(eventDetailsCommon, row, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected UPWDetailsPage updateUPWRequirementByRow(EventDetailsCommon eventDetailsCommon, int row, int inputValuesIndex) {
//    UPWDetailsPage upwDetailsPage = eventDetailsCommon.clickUnpaidWorkLink();
//
//    UpdateUPWRequirementPage updateUPWRequirementPage = upwDetailsPage.clickUpdateByRow(row);
//
//    return updateUPWRequirementPage.completePage(inputValuesIndex, UPWDetailsPage.class);
//  }
//
//  protected UPWDetailsPage updateUPWDetails(EventDetailsCommon eventDetailsCommon) {
//    return updateUPWDetails(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected UPWDetailsPage updateUPWDetails(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    UPWDetailsPage upwDetailsPage = eventDetailsCommon.clickUnpaidWorkLink();
//
//    UpdateUPWDetailsPage updateUPWDetailsPage = upwDetailsPage.clickUpdate();
//
//    return updateUPWDetailsPage.completePage(inputValuesIndex, UPWDetailsPage.class);
//  }
//
//  protected UPWWorksheetSummaryPage updateUPWAppointment(EventDetailsCommon eventDetailsCommon) {
//    return updateUPWAppointment(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected UPWWorksheetSummaryPage updateUPWAppointment(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    UPWDetailsPage upwDetailsPage = eventDetailsCommon.clickUnpaidWorkLink();
//
//    UPWWorksheetSummaryPage upwWorksheetSummaryPage = upwDetailsPage.clickWorksheetSummary();
//
//    UpdateUPWAppointmentPage updateUPWAppointmentPage = upwWorksheetSummaryPage.clickUpdateUPWAppointmentByRow(1);
//
//    return updateUPWAppointmentPage.completePage(inputValuesIndex, UPWWorksheetSummaryPage.class);
//  }
//
//  protected ContactListPage addContact(OffenderCommon offenderCommon) {
//    return addContact(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ContactListPage addContact(OffenderCommon offenderCommon, int inputValuesIndex) {
//    ContactListPage contactListPage = offenderCommon.clickContactListLink();
//
//    AddContactDetailsPage addContactDetailsPage = contactListPage.clickAddContact();
//
//    if (testData.getInputValue("AddContact.RelatesTo").startsWith("Offender")) {
//      addContactDetailsPage.relatesTo = String.format("Offender - %s, %s", this.offenderLastName, this.offenderFirstName);
//    } else {
//      addContactDetailsPage.relatesTo = testData.getInputValue("AddContact.RelatesTo");
//    }
//
//    return addContactDetailsPage.completePage(inputValuesIndex, ContactListPage.class);
//  }
//
//  protected ContactListPage updateContactByLabel(OffenderCommon offenderCommon, String label) {
//    return updateContactByLabel(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, label);
//  }
//
//  protected ContactListPage updateContactByLabel(OffenderCommon offenderCommon, int inputValuesIndex, String label) {
//    ContactListPage contactListPage = offenderCommon.clickContactListLink();
//
//    contactListPage.broadSearch();
//
//    UpdateContactPage updateContactPage = contactListPage.clickUpdateContactByLabel(label);
//
//    return updateContactPage.completePage(inputValuesIndex, ContactListPage.class);
//  }
//
//  protected OffenderCommon deleteContactByRow(OffenderCommon offenderCommon, int row) {
//
//    SleepUtils.sleep(); //Added sleep here to make sure the INS/UPD Contact Transaction is sent, either as it own transaction or within OffenderSteps/Event Transactions
//
//    ContactListPage contactListPage = offenderCommon.clickContactListLink();
//
//    contactListPage = contactListPage.broadSearch();
//
//    DeleteContactPage deleteContactPage = contactListPage.deleteContactByRow(row);
//
//    return deleteContactPage.clickConfirm();
//  }
//
//  protected OffenderNonStatutoryInterventionListPage addOffenderNonStatutoryIntervention(OffenderDetailsCommon offenderDetailsCommon) {
//    return addOffenderNonStatutoryIntervention(offenderDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected OffenderNonStatutoryInterventionListPage addOffenderNonStatutoryIntervention(OffenderDetailsCommon offenderDetailsCommon, int inputValuesIndex) {
//    OffenderNonStatutoryInterventionListPage offenderNonStatutoryInterventionListPage = offenderDetailsCommon.clickNonStatutoryInterventions();
//
//    AddOffenderNonStatutoryInterventionPage addNonStatutoryInterventionPage = offenderNonStatutoryInterventionListPage.clickAdd();
//
//    return addNonStatutoryInterventionPage.completePage(inputValuesIndex, OffenderNonStatutoryInterventionListPage.class);
//  }
//
//  protected NonStatutoryInterventionListPage addEventNonStatutoryIntervention(EventDetailsCommon eventDetailsCommon) {
//    return addEventNonStatutoryIntervention(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected NonStatutoryInterventionListPage addEventNonStatutoryIntervention(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    NonStatutoryInterventionListPage nonStatutoryInterventionListPage = eventDetailsCommon.clickNonStatutoryInterventionsLink();
//
//    AddEventNonStatutoryInterventionPage addEventNonStatutoryInterventionPage = nonStatutoryInterventionListPage.clickAdd();
//
//    return addEventNonStatutoryInterventionPage.completePage(inputValuesIndex, NonStatutoryInterventionListPage.class);
//  }
//
//  protected NonStatutoryInterventionListPage updateNonStatutoryIntervention(EventDetailsCommon eventDetailsCommon, int row) {
//    return updateNonStatutoryIntervention(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected NonStatutoryInterventionListPage updateNonStatutoryIntervention(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    NonStatutoryInterventionListPage nonStatutoryInterventionListPage = eventDetailsCommon.clickNonStatutoryInterventionsLink();
//
//    UpdateNonStatutoryInterventionPage updateNonStatutoryInterventionPage = nonStatutoryInterventionListPage.clickUpdateNSIByRow(row);
//
//    return updateNonStatutoryInterventionPage.completePage(inputValuesIndex, NonStatutoryInterventionListPage.class);
//  }
//
//  protected OffenderNonStatutoryInterventionListPage updateOffenderNonStatutoryIntervention(OffenderDetailsCommon offenderDetailsCommon, int row) {
//    return updateOffenderNonStatutoryIntervention(offenderDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected OffenderNonStatutoryInterventionListPage updateOffenderNonStatutoryIntervention(OffenderDetailsCommon offenderDetailsCommon, int inputValuesIndex, int row) {
//    OffenderNonStatutoryInterventionListPage offenderNonStatutoryInterventionListPage = offenderDetailsCommon.clickNonStatutoryInterventions();
//
//    UpdateOffenderNonStatutoryInterventionPage updateOffenderNonStatutoryInterventionPage = offenderNonStatutoryInterventionListPage.clickUpdateNSIByRow(row);
//
//    return updateOffenderNonStatutoryInterventionPage.completePage(inputValuesIndex, OffenderNonStatutoryInterventionListPage.class);
//  }
//
//  protected EventDetailsCommon viewEventByRow(OffenderCommon offenderCommon, int row) {
//    EventListPage eventListPage = offenderCommon.clickEventListLink();
//
//    return eventListPage.clickViewEventByRow(row);
//  }
//
//  protected OffenderCommon updateEventByRow(OffenderCommon offenderCommon, int row) {
//    return updateEventByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected OffenderCommon updateEventByRow(OffenderCommon offenderCommon, int inputValuesIndex, int row) {
//    EventListPage eventListPage = offenderCommon.clickEventListLink();
//
//    AmendEventDetailsPage amendEventDetailsPage = eventListPage.clickUpdateByRow(row);
//
//    return amendEventDetailsPage.completePage(inputValuesIndex, OffenderCommon.class);
//  }
//
//  protected OffenderCommon terminateEventByRow(OffenderCommon offenderCommon, int row) {
//    return terminateEventByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected OffenderCommon terminateEventByRow(OffenderCommon offenderCommon, int inputValuesIndex, int row) {
//    EventListPage eventListPage = offenderCommon.clickEventListLink();
//
//    TerminateEventPage terminateEventPage = eventListPage.clickTerminateByRow(row);
//
//    return terminateEventPage.completePage(inputValuesIndex, OffenderCommon.class);
//  }

  protected OffenderCommon doTransfer(OffenderCommon offenderCommon) {
    return doTransfer(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
  }

  protected OffenderCommon doTransfer(OffenderCommon offenderCommon, int inputValuesIndex) {
    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();

    OffenderTransferRequestPage offenderTransferRequestPage = offenderDetailsPage.clickOffenderTransferRequestLink();

    return offenderTransferRequestPage.completePage(inputValuesIndex, OffenderCommon.class);
  }

//  protected OffenderCommon acceptTransfer(OffenderCommon offenderCommon) {
//    return acceptTransfer(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected OffenderCommon acceptTransfer(OffenderCommon offenderCommon, int inputValuesIndex) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    OffenderTransferReviewPage offenderTransferReviewPage = offenderDetailsPage.clickOffenderTransferReviewLink();
//
//    OffenderTransferAcceptPage offenderTransferAcceptPage = offenderTransferReviewPage.clickAccept();
//
//    return offenderTransferAcceptPage.completePage(inputValuesIndex, OffenderTransferReviewPage.class);
//  }
//
//  protected OffenderSummaryPage searchOffender(DeliusCommon deliusCommon, String caseReferenceNumber) {
//    NationalSearchPage nationalSearchPage = deliusCommon.clickNationalSearch();
//
//    return nationalSearchPage.searchOffender(caseReferenceNumber);
//  }
//
//  protected UPWDetailsPage addUPWAppointment(EventDetailsCommon eventDetailsCommon) {
//    return addUPWAppointment(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected UPWDetailsPage addUPWAppointment(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    UPWDetailsPage upwDetailsPage = eventDetailsCommon.clickUnpaidWorkLink();
//
//    UPWWorksheetSummaryPage upwWorksheetSummaryPage = upwDetailsPage.clickWorksheetSummary();
//
//    AddUPWAppointmentPage addUPWAppointmentPage = upwWorksheetSummaryPage.clickAddAppointment();
//
//    return addUPWAppointmentPage.completePage(inputValuesIndex, UPWDetailsPage.class);
//  }
//
//  protected RegistrationListPage addRegistration(OffenderCommon offenderCommon) {
//    return addRegistration(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected RegistrationListPage addRegistration(OffenderCommon offenderCommon, int inputValuesIndex) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    RegistrationListPage registrationListPage = offenderDetailsPage.clickRegistrationSummaryLink();
//
//    AddRegistrationPage addRegistrationPage = registrationListPage.clickAddRegistration();
//
//    return addRegistrationPage.completePage(inputValuesIndex, RegistrationListPage.class);
//  }
//
//  protected RegistrationListPage updateRegistration(OffenderCommon offenderCommon, int row) {
//    return updateRegistration(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected RegistrationListPage updateRegistration(OffenderCommon offenderCommon, int inputValuesIndex, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    RegistrationListPage registrationListPage = offenderDetailsPage.clickRegistrationSummaryLink();
//
//    UpdateRegistrationPage updateRegistrationPage = registrationListPage.clickUpdateByRow(row);
//
//    return updateRegistrationPage.completePage(inputValuesIndex, RegistrationListPage.class);
//  }
//
//  protected RegistrationListPage deleteRegistrationByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    RegistrationListPage registrationListPage = offenderDetailsPage.clickRegistrationSummaryLink();
//
//    DeleteRegistrationPage deleteRegistrationPage = registrationListPage.clickDeleteByRow(row);
//
//    return deleteRegistrationPage.clickConfirm();
//  }
//
//  protected RegistrationListPage deregisterByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    RegistrationListPage registrationListPage = offenderDetailsPage.clickRegistrationSummaryLink();
//
//    AddDeregistrationPage addDeregistrationPage = registrationListPage.clickDeregisterByRow(row);
//
//    return addDeregistrationPage.completePage(0, RegistrationListPage.class);
//  }
//
//  protected RegistrationDetailsPage updateRegistrationReviewByRow(OffenderCommon offenderCommon, int registrationRow, int registrationReviewRow) {
//    return updateRegistrationReviewByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX, registrationRow, registrationReviewRow);
//  }
//
//  protected RegistrationDetailsPage updateRegistrationReviewByRow(OffenderCommon offenderCommon, int inputValuesIndex, int registrationRow, int registrationReviewRow) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    RegistrationListPage registrationListPage = offenderDetailsPage.clickRegistrationSummaryLink();
//
//    RegistrationDetailsPage registrationDetailsPage = registrationListPage.clickViewByRow(registrationRow);
//
//    UpdateRegistrationReviewPage updateRegistrationReviewPage = registrationDetailsPage.clickUpdateByRow(registrationReviewRow);
//
//    return updateRegistrationReviewPage.completePage(inputValuesIndex, RegistrationDetailsPage.class);
//  }
//
//  protected ThroughcareDetailsPage returnToCustody(EventDetailsCommon eventDetailsCommon) {
//    return returnToCustody(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ThroughcareDetailsPage returnToCustody(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsCommon.clickThroughcare();
//
//    ReturnToCustodyPage returnToCustodyPage = throughcareDetailsPage.clickReturnToCustody();
//
//    return returnToCustodyPage.completePage(inputValuesIndex, ThroughcareDetailsPage.class);
//  }
//
//  protected ThroughcareDetailsPage addRecall(AddLicenceConditionPage addLicenceConditionPage) {
//    return addRecall(addLicenceConditionPage, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ThroughcareDetailsPage addRecall(AddLicenceConditionPage addLicenceConditionPage, int inputValuesIndex) {
//    ThroughcareDetailsPage throughcareDetailsPage = addLicenceConditionPage.clickThroughcare();
//
//    ReturnToCustodyPage returnToCustodyPage = throughcareDetailsPage.clickAddRecall();
//
//    return returnToCustodyPage.completePage(inputValuesIndex, ThroughcareDetailsPage.class);
//  }
//
//  protected CourtAndInstitutionalReportsListPage updateCourtReport(EventDetailsCommon eventDetailsCommon, int row) {
//    return updateCourtReport(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected CourtAndInstitutionalReportsListPage updateCourtReport(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    CourtAndInstitutionalReportsListPage courtAndInstitutionalReportsListPage = eventDetailsCommon.clickCourtAndInstitutionalReportsLink();
//
//    UpdateCourtReportPage updateCourtReportPage = courtAndInstitutionalReportsListPage.clickUpdateCourtReportByRow(row);
//
//    return updateCourtReportPage.completePage(inputValuesIndex, CourtAndInstitutionalReportsListPage.class);
//  }
//
//  protected CourtAndInstitutionalReportsListPage updateInstitutionalReportByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return updateInstitutionalReportByRow(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected CourtAndInstitutionalReportsListPage updateInstitutionalReportByRow(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    CourtAndInstitutionalReportsListPage courtAndInstitutionalReportsListPage = eventDetailsCommon.clickCourtAndInstitutionalReportsLink();
//
//    EditInstitutionalReportPage editInstitutionalReportPage = courtAndInstitutionalReportsListPage.clickUpdateInstitutionalReportByRow(row);
//
//    return editInstitutionalReportPage.completePage(inputValuesIndex, CourtAndInstitutionalReportsListPage.class);
//  }
//
//  protected UpdateProposalPage addProposal(CourtAndInstitutionalReportsListPage courtAndInstitutionalReportsListPage, int row) {
//    return addProposal(courtAndInstitutionalReportsListPage, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected UpdateProposalPage addProposal(CourtAndInstitutionalReportsListPage courtAndInstitutionalReportsListPage, int inputValuesIndex, int row) {
//    UpdateCourtReportPage updateCourtReportPage = courtAndInstitutionalReportsListPage.clickUpdateCourtReportByRow(row);
//
//    UpdateProposalPage updateProposalPage = updateCourtReportPage.clickProposal();
//
//    return updateProposalPage.completePage(inputValuesIndex, UpdateProposalPage.class);
//  }
//
//  protected UpdateCourtReportPage addProposedRequirement(UpdateProposalPage updateProposalPage) {
//    updateProposalPage = updateProposalPage.addProposedRequirement();
//    return updateProposalPage.clickClose();
//  }
//
//  protected UpdateCourtReportPage deleteProposedRequirement(UpdateProposalPage updateProposalPage, int row) {
//    updateProposalPage = updateProposalPage.clickDeleteByRow(row);
//    return updateProposalPage.clickClose();
//  }
//
//  protected ThroughcareDetailsPage addMaintainThroughDates(OffenderCommon offenderCommon) {
//    return addMaintainThroughDates(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ThroughcareDetailsPage addMaintainThroughDates(OffenderCommon offenderCommon, int inputValuesIndex) {
//    EventListPage eventListPage = offenderCommon.clickEventListLink();
//
//    // TODO - Why isn't this parameterised?
//    EventDetailsPage eventDetailsPage = eventListPage.clickViewEventByNumber(1);
//
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsPage.clickThroughcare();
//
//    MaintainThroughcareDatesPage maintainThroughcareDatesPage = throughcareDetailsPage.clickThroughcareDates();
//
//    return maintainThroughcareDatesPage.completePage(inputValuesIndex, ThroughcareDetailsPage.class);
//  }
//
//  protected CourtAndInstitutionalReportsListPage addInstitutionalReport(EventDetailsCommon eventDetailsCommon) {
//    return addInstitutionalReport(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected CourtAndInstitutionalReportsListPage addInstitutionalReport(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    CourtAndInstitutionalReportsListPage courtAndInstitutionalReportsListPage = eventDetailsCommon.clickCourtAndInstitutionalReportsLink();
//
//    AddInstitutionalReportPage addInstitutionalReportPage = courtAndInstitutionalReportsListPage.clickAddInstitutionalReport();
//
//    return addInstitutionalReportPage.completePage(inputValuesIndex, CourtAndInstitutionalReportsListPage.class);
//  }
//
//  protected CourtAndInstitutionalReportsListPage deleteInstitutionalReportByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    CourtAndInstitutionalReportsListPage courtAndInstitutionalReportsListPage = eventDetailsCommon.clickCourtAndInstitutionalReportsLink();
//
//    DeleteInstitutionalReportPage deleteInstitutionalReportPage = courtAndInstitutionalReportsListPage.clickDeleteInstitutionalReportByRow(row);
//
//    return deleteInstitutionalReportPage.clickConfirm();
//  }
//
//  protected PersonalCircumstancesListPage deletePersonalCircumstanceByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsPage offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
//
//    PersonalCircumstancesListPage personalCircumstancesListPage = offenderDetailsPage.clickPersonalCircumstancesLink();
//
//    DeletePersonalCircumstanceConfirmationPage deletePersonalCircumstanceConformationPage = personalCircumstancesListPage.clickDeleteByRow(row);
//
//    return deletePersonalCircumstanceConformationPage.clickConfirmButton();
//
//  }
//
//  protected AddAliasPage deleteAliasByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
//
//    AliasListPage aliasListPage = offenderDetailsCommon.clickOffenderAliasesLink();
//
//    DeleteAliasConformationPage deleteAliasConformationPage = aliasListPage.deleteAliasByRow(row);
//
//    return deleteAliasConformationPage.clickConfirm();
//  }
//
//  protected NonStatutoryInterventionListPage deleteTerminatedNSIByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    NonStatutoryInterventionListPage nonStatutoryInterventionListPage = addEventNonStatutoryIntervention(eventDetailsCommon);
//
//    nonStatutoryInterventionListPage.showTerminatedNSI();
//
//    DeleteNonStatutoryInterventionConformationPage deleteNonStatutoryInterventionConformationPage =
//            nonStatutoryInterventionListPage.clickDeleteNSIByRow(row);
//
//    return deleteNonStatutoryInterventionConformationPage.clickButtonConfirm();
//  }
//
//  protected NonStatutoryInterventionListPage deleteNSIByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    NonStatutoryInterventionListPage nonStatutoryInterventionListPage = eventDetailsCommon.clickNonStatutoryInterventionsLink();
//
//    DeleteNonStatutoryInterventionConformationPage deleteNonStatutoryInterventionConformationPage =
//            nonStatutoryInterventionListPage.clickDeleteNSIByRow(row);
//
//    return deleteNonStatutoryInterventionConformationPage.clickButtonConfirm();
//  }
//
//  protected InstitutionalReportDetailsPage viewCourtAndInstitutionalReportByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    CourtAndInstitutionalReportsListPage courtAndInstitutionalReportsListPage = eventDetailsCommon.clickCourtAndInstitutionalReportsLink();
//
//    return courtAndInstitutionalReportsListPage.clickViewInstitutionalReportByRow(row);
//  }
//
//  protected ThroughcareDetailsPage editThroughcare(EventDetailsCommon eventDetailsCommon) {
//    return editThroughcare(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ThroughcareDetailsPage editThroughcare(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsCommon.clickThroughcare();
//
//    EditThroughcareDetailsPage editThroughcareDetailsPage = throughcareDetailsPage.clickThroughcareDetails();
//
//    return editThroughcareDetailsPage.completePage(inputValuesIndex, ThroughcareDetailsPage.class);
//  }
//
//  protected RequirementsListPage deleteRequirementByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    RequirementsListPage requirementsListPage = eventDetailsCommon.clickRequirementsLink();
//
//    DeleteRequirementPage deleteRequirementPage = requirementsListPage.deleteRequirementByRow(row);
//
//    return deleteRequirementPage.clickConfirm();
//  }
//
//  protected EditCourtAppearancePage addCourtReportToCourtAppearanceByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return addCourtReportToCourtAppearanceByRow(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected EditCourtAppearancePage addCourtReportToCourtAppearanceByRow(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    CourtAppearanceListPage courtAppearanceListPage = eventDetailsCommon.clickCourtAppearancesLink();
//
//    EditCourtAppearancePage editCourtAppearancePage = courtAppearanceListPage.clickUpdateAppearanceByRow(row);
//
//    AddCourtReportPage addCourtReportPage = editCourtAppearancePage.clickNewCourtReport();
//
//    return addCourtReportPage.completePage(inputValuesIndex, EditCourtAppearancePage.class);
//  }
//
//  protected EditCourtAppearancePage deleteCourtReportByRow(EventDetailsCommon eventDetailsCommon, int courtAppearanceRow, int courtReportRow) {
//    CourtAppearanceListPage courtAppearanceListPage = eventDetailsCommon.clickCourtAppearancesLink();
//
//    EditCourtAppearancePage editCourtAppearancePage = courtAppearanceListPage.clickUpdateAppearanceByRow(courtAppearanceRow);
//
//    DeleteCourtReportPage deleteCourtReportPage = editCourtAppearancePage.clickDeleteCourtReportByRow(courtReportRow);
//
//    return deleteCourtReportPage.clickConfirm();
//  }
//
//  protected LicenceConditionListPage deleteLicenceConditionByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    LicenceConditionListPage licenceConditionListPage = eventDetailsCommon.clickLicenceConditions();
//
//    DeleteLicenceConditionPage deleteLicenceConditionPage = licenceConditionListPage.deleteLicenceConditionByRow(row);
//
//    return deleteLicenceConditionPage.clickConfirm();
//  }
//
//  protected EventDetailsPage addAdditionalOffence(EventDetailsCommon eventDetailsCommon) {
//    return addAdditionalOffence(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected EventDetailsPage addAdditionalOffence(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    UpdateAdditionalOffencesPage updateAdditionalOffencesPage = eventDetailsCommon.clickAdditionalOffencesLink();
//
//    return updateAdditionalOffencesPage.completePage(inputValuesIndex, EventDetailsPage.class);
//  }
//
//  protected UpdateAdditionalOffencesPage deleteAdditionalOffenceByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    UpdateAdditionalOffencesPage updateAdditionalOffencesPage = eventDetailsCommon.clickAdditionalOffencesLink();
//
//    DeleteAdditionalOffencePage deleteAdditionalOffencePage = updateAdditionalOffencesPage.selectDeleteAdditionalOffenceByRow(row);
//
//    return deleteAdditionalOffencePage.clickConfirm();
//  }
//
//  protected EventDetailsPage updateAdditionalSentence(EventDetailsCommon eventDetailsCommon) {
//    return updateAdditionalSentence(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected EventDetailsPage updateAdditionalSentence(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    UpdateAdditionalSentencesPage updateAdditionalSentencesPage = eventDetailsCommon.clickAdditionalSentencesLink();
//
//    updateAdditionalSentencesPage = updateAdditionalSentencesPage.completePage(inputValuesIndex, UpdateAdditionalSentencesPage.class);
//
//    return updateAdditionalSentencesPage.clickClose();
//  }
//
//  protected UpdateAdditionalSentencesPage deleteAdditionalSentenceByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    UpdateAdditionalSentencesPage updateAdditionalSentencesPage = eventDetailsCommon.clickAdditionalSentencesLink();
//
//    DeleteAdditionalSentencePage deleteAdditionalSentencePage = updateAdditionalSentencesPage.clickDeleteByRow(row);
//
//    return deleteAdditionalSentencePage.clickConfirm();
//  }
//
//  protected EventDetailsPage calculateOGRS(EventDetailsCommon eventDetailsCommon) {
//    return calculateOGRS(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected EventDetailsPage calculateOGRS(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    OGRSCalculationPage ogrsCalculationPage = eventDetailsCommon.clickOGRSCalculationLink();
//
//    return ogrsCalculationPage.completePage(inputValuesIndex, EventDetailsPage.class);
//  }
//
//  protected EventDetailsPage updateAdditionalOffence(EventDetailsCommon eventDetailsCommon) {
//    return updateAdditionalOffence(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected EventDetailsPage updateAdditionalOffence(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    UpdateAdditionalOffencesPage updateAdditionalOffencesPage = eventDetailsCommon.clickAdditionalOffencesLink();
//
//    return updateAdditionalOffencesPage.completePage(inputValuesIndex, EventDetailsPage.class);
//  }
//
//  protected AdditionalIdentifierListPage deleteAdditionalIdentifierByRow(OffenderCommon offenderCommon) {
//    return deleteAdditionalIdentifierByRow(offenderCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected AdditionalIdentifierListPage deleteAdditionalIdentifierByRow(OffenderCommon offenderCommon, int row) {
//    OffenderDetailsCommon offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
//
//    AdditionalIdentifierListPage additionalIdentifierListPage = offenderDetailsCommon.clickAdditionalIdentifier();
//
//    return additionalIdentifierListPage.clickDeleteAdditionalIdentifierByRow(row);
//  }
//
//  protected CohortDetailsPage administerCohort(EventDetailsCommon eventDetailsCommon) {
//    return administerCohort(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected CohortDetailsPage administerCohort(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    CohortDetailsPage cohortDetailsPage = eventDetailsCommon.clickCohortLink();
//
//    AdministerCohortPage administerCohortPage = cohortDetailsPage.clickAdminister();
//
//    return administerCohortPage.completePage(inputValuesIndex, CohortDetailsPage.class);
//  }
//
//  protected ApprovedPremisesReferralsListPage addApprovedPremisesReferral(EventDetailsCommon eventDetailsCommon) {
//    return addApprovedPremisesReferral(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ApprovedPremisesReferralsListPage addApprovedPremisesReferral(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ApprovedPremisesReferralsListPage approvedPremisesReferralsListPage = eventDetailsCommon.clickApprovedPremisesReferralsLink();
//
//    AddApprovedPremisesReferralPage addApprovedPremisesReferralPage = approvedPremisesReferralsListPage.clickNewReferral();
//
//    return addApprovedPremisesReferralPage.completePage(inputValuesIndex, ApprovedPremisesReferralsListPage.class);
//  }
//
//  protected ApprovedPremisesReferralsListPage updateApprovedPremisesReferralByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return updateApprovedPremisesReferralByRow(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected ApprovedPremisesReferralsListPage updateApprovedPremisesReferralByRow(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    ApprovedPremisesReferralsListPage approvedPremisesReferralsListPage = eventDetailsCommon.clickApprovedPremisesReferralsLink();
//
//    UpdateApprovedPremisesReferralPage updateApprovedPremisesReferralPage = approvedPremisesReferralsListPage.clickUpdateByRow(row);
//
//    return updateApprovedPremisesReferralPage.completePage(inputValuesIndex, ApprovedPremisesReferralsListPage.class);
//  }
//
//  protected ApprovedPremisesReferralsListPage deleteApprovedPremisesReferralByRow(EventDetailsCommon eventDetailsCommon, int row) {
//    return deleteApprovedPremisesReferralByRow(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX, row);
//  }
//
//  protected ApprovedPremisesReferralsListPage deleteApprovedPremisesReferralByRow(EventDetailsCommon eventDetailsCommon, int inputValuesIndex, int row) {
//    ApprovedPremisesReferralsListPage approvedPremisesReferralsListPage = eventDetailsCommon.clickApprovedPremisesReferralsLink();
//
//    DeleteApprovedPremisesReferralPage deleteApprovedPremisesReferralPage = approvedPremisesReferralsListPage.clickDeleteByRow(row);
//
//    return deleteApprovedPremisesReferralPage.clickConfirm();
//  }
//
//  protected AwaitingArrivalPage addApprovedPremisesAdmission(EventDetailsCommon eventDetailsCommon, String caseReferenceNumber) throws CaseReferenceNumberNotFoundException {
//    return addApprovedPremisesAdmission(eventDetailsCommon, caseReferenceNumber, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected AwaitingArrivalPage addApprovedPremisesAdmission(EventDetailsCommon eventDetailsCommon, String caseReferenceNumber, int inputValuesIndex) throws CaseReferenceNumberNotFoundException {
//    CurrentResidentsPage currentResidentsPage = eventDetailsCommon.clickApprovedPremisesDiary();
//
//    AwaitingArrivalPage awaitingArrivalPage = currentResidentsPage.clickAwaitingArrivalLink();
//
//    AddApprovedPremisesAdmissionPage addApprovedPremisesAdmissionPage = awaitingArrivalPage.clickAdmitByCaseReferenceNumber(caseReferenceNumber);
//
//    return addApprovedPremisesAdmissionPage.completePage(inputValuesIndex, AwaitingArrivalPage.class);
//  }
//
//  protected CurrentResidentsPage updateApprovedPremisesAdmission(OffenderCommon offenderCommon, String caseReferenceNumber) throws CaseReferenceNumberNotFoundException {
//    return updateApprovedPremisesAdmission(offenderCommon, caseReferenceNumber, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected CurrentResidentsPage updateApprovedPremisesAdmission(OffenderCommon offenderCommon, String caseReferenceNumber, int inputValuesIndex) throws CaseReferenceNumberNotFoundException {
//    ApprovedPremisesDiaryCommon approvedPremisesDiaryCommon = offenderCommon.clickApprovedPremisesDiary();
//
//    CurrentResidentsPage currentResidentsPage = approvedPremisesDiaryCommon.clickCurrentResidencesLink();
//
//    UpdateApprovedPremisesAdmissionPage updateApprovedPremisesAdmissionPage = currentResidentsPage.clickAdmissionByCaseReferenceNumber(caseReferenceNumber);
//
//    return updateApprovedPremisesAdmissionPage.completePage(inputValuesIndex, CurrentResidentsPage.class);
//  }
//
//  protected CurrentResidentsPage deleteApprovedPremisesAdmission(OffenderCommon offenderCommon, String caseReferenceNumber) throws CaseReferenceNumberNotFoundException {
//    CurrentResidentsPage currentResidentsPage = offenderCommon.clickApprovedPremisesDiary();
//
//    DeleteApprovedPremisesAdmissionPage deleteApprovedPremisesAdmissionPage = currentResidentsPage.clickDeleteByCaseReferenceNumber(caseReferenceNumber);
//
//    return deleteApprovedPremisesAdmissionPage.clickConfirm();
//  }
//
//  protected CurrentResidentsPage addApprovedPremisesDeparture(AwaitingArrivalPage awaitingArrivalPage, String caseReferenceNumber) throws CaseReferenceNumberNotFoundException {
//    return addApprovedPremisesDeparture(awaitingArrivalPage, caseReferenceNumber, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected CurrentResidentsPage addApprovedPremisesDeparture(AwaitingArrivalPage awaitingArrivalPage, String caseReferenceNumber, int inputValuesIndex) throws CaseReferenceNumberNotFoundException {
//    CurrentResidentsPage currentResidentsPage = awaitingArrivalPage.clickCurrentResidencesLink();
//
//    AddApprovedPremisesDeparturePage addApprovedPremisesDeparturePage = currentResidentsPage.clickDepartureByCaseReferenceNumber(caseReferenceNumber);
//
//    return addApprovedPremisesDeparturePage.completePage(inputValuesIndex, CurrentResidentsPage.class);
//  }
//
//  protected ThroughcareDetailsPage undoSupervisionRequirement(EventDetailsCommon eventDetailsCommon) {
//    return undoSupervisionRequirement(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected ThroughcareDetailsPage undoSupervisionRequirement(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    ThroughcareDetailsPage throughcareDetailsPage = eventDetailsCommon.clickThroughcare();
//
//    UpdateUndoReleasePage updateUndoReleasePage = throughcareDetailsPage.clickEditUndoRelease();
//
//    return updateUndoReleasePage.completePage(inputValuesIndex, ThroughcareDetailsPage.class);
//  }
//
//  protected OutboundMessageDetailsPage searchSPGMessageErrorOut(OffenderCommon offenderCommon, String caseReferenceNumber) {
//    return searchSPGMessageErrorOut(offenderCommon, caseReferenceNumber, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected OutboundMessageDetailsPage searchSPGMessageErrorOut(OffenderCommon offenderCommon, String caseReferenceNumber, int inputValuesIndex) {
//    SPGAdministrationPage spgAdministrationPage = offenderCommon.clickSPGAdministration();
//
//    SPGMessageSearchPage spgMessageSearchPage = spgAdministrationPage.clickSearchSPGErrors(caseReferenceNumber);
//
//    return spgMessageSearchPage.completePage(inputValuesIndex, OutboundMessageDetailsPage.class);
//  }
//
//  protected InboundMessageDetailsPage searchSPGMessageErrorIn(OffenderCommon offenderCommon, String caseReferenceNumber) {
//    return searchSPGMessageErrorIn(offenderCommon, caseReferenceNumber, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected InboundMessageDetailsPage searchSPGMessageErrorIn(OffenderCommon offenderCommon, String caseReferenceNumber, int inputValuesIndex) {
//    SPGAdministrationPage spgAdministrationPage = offenderCommon.clickSPGAdministration();
//
//    SPGMessageSearchPage spgMessageSearchPage = spgAdministrationPage.clickSearchSPGErrors(caseReferenceNumber);
//
//    return spgMessageSearchPage.completePage(inputValuesIndex, InboundMessageDetailsPage.class);
//  }
//
//  protected CaseAllocationListPage addRSRScoreAndDate(EventDetailsCommon eventDetailsCommon) {
//    return addRSRScoreAndDate(eventDetailsCommon, DEFAULT_INPUT_VALUES_INDEX);
//  }
//
//  protected CaseAllocationListPage addRSRScoreAndDate(EventDetailsCommon eventDetailsCommon, int inputValuesIndex) {
//    CaseAllocationListPage caseAllocationListPage = eventDetailsCommon.clickCaseAllocationLink();
//
//    return caseAllocationListPage.completePage(inputValuesIndex, CaseAllocationListPage.class);
//  }
//
//  protected Connection connectToDatabase(String sqlConnectionHost, String userName, String password) throws SQLException, ClassNotFoundException {
//    Class.forName("oracle.jdbc.driver.OracleDriver");
//
//    return DriverManager.getConnection(sqlConnectionHost, userName, password);
//  }
//
//  protected void executeScript(Connection connection, String sqlFilePath)  {
//    try {
//      Statement statement = connection.createStatement();
//        BufferedReader in = new BufferedReader(new FileReader(sqlFilePath));
//        String str;
//        StringBuffer stringBuffer = new StringBuffer();
//        while ((str = in.readLine()) != null) {
//          stringBuffer.append(str + "\n ");
//        }
//        in.close();
//        statement.executeUpdate(stringBuffer.toString());
//
//    } catch (SQLException | IOException e ) {
//      e.printStackTrace();
//    }
//  }
//
//  protected Response runReleaseAPI(String documentID, String caseReferenceNumber, String user) {
//    baseURI = "http://" + configData.getProperty("Alfresco.Host");
//    basePath = "/noms-spg/release/";
//
//    Response response = given().
//            header("X-DocRepository-Remote-User", user).
//            header("X-DocRepository-Real-Remote-User", "nDelius01").
//            when().
//            put(documentID).
//            then().
//            statusCode(200).
//            contentType(JSON).
//            extract().response();
//
//    writeAPIResponseToFile(response, String.format(configData.getProperty("Alfresco.TestResultJSON.Location"), ExecutionContext.getInstance().getTestRunDetails().getId(), caseReferenceNumber));
//
//    return runCaseNotesAPI(caseReferenceNumber);
//  }
//
//  public Response runCaseNotesAPI(String nomsNumber) {
//    baseURI = "http://" + configData.getProperty("Delius.Host") + ":" + configData.getProperty("Delius.Port");
//    basePath = "/api/nomisCaseNotes/";
//
//    System.out.println(baseURI + basePath);
//
//    return given().
//            auth().basic("APIUser", "Password1").
//            body(JSON).
//            when().
//            get("{nomsNumber}", nomsNumber, "100000").
//            then().
//            statusCode(201).
//            contentType(JSON).
//            extract().response();
//  }
//
//  protected void writeAPIResponseToFile(Response response, String fileLocation) {
//    FileWriter file;
//    try {
//      file = new FileWriter(fileLocation);
//      file.write(response.asString());
//      file.flush();
//
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//  }
//
//  protected Boolean searchNOMSNumber(DeliusCommon deliusCommon, String nomsNumber) {
//    HomePage homePage = deliusCommon.clickHome();
//    NationalSearchPage nationalSearchPage = homePage.clickNationalSearchButton();
//
//    return nationalSearchPage.searchNOMSNumber(nomsNumber);
//  }
//
//  protected void checkAndRemoveNOMSNumber(DeliusCommon deliusCommon, int row, String nomsNumber) {
//    boolean offenderExists = searchNOMSNumber(deliusCommon, nomsNumber);
//
//    if (offenderExists) {
//      printOut("Removing NOMS number " + nomsNumber + " from Offender to make it unique");
//      HomePage homePage = deliusCommon.clickHome();
//      NationalSearchPage nationalSearchPage = homePage.clickNationalSearchButton();
//      OffenderSummaryPage offenderSummaryPage = nationalSearchPage.clickViewByRow(row);
//      OffenderDetailsPage offenderDetailsPage = offenderSummaryPage.clickOffenderIndexLink();
//      UpdateOffenderPage updateOffenderPage = offenderDetailsPage.clickUpdate();
//
//      updateOffenderPage.clickDeleteByRow(row);
//
//      updateOffenderPage.clickSave();
//    }
//  }
//
//  protected ContactDetailsPage viewContactbyName(OffenderCommon offenderCommon, String typeToCheck) throws Exception {
//    ContactListPage contactListPage = offenderCommon.clickContactListLink();
//
//    contactListPage.broadSearch();
//
//    Integer contactRow = contactListPage.getContactTypeByRow(typeToCheck);
//    return contactListPage.clickViewContactByRow(contactRow);
//  }
}

package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class RegistrationTransaction extends BaseTransaction {

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("Registration%s.RegistrationDetails%s.OffenderID",            "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/OffenderID"));
    add(new AssertionPair("Registration%s.RegistrationDetails%s.RegisterType",          "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/RegisterType"));
    add(new AssertionPair("Registration%s.RegistrationDetails%s.RegistrationDate",      "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/RegistrationDate"));
    add(new AssertionPair("Registration%s.RegistrationDetails%s.NextReviewDate",        "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/NextReviewDate"));
    add(new AssertionPair("Registration%s.RegistrationDetails%s.RegistrationProvider",  "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/RegistrationProvider"));
    add(new AssertionPair("Registration%s.RegistrationDetails%s.RegisteringTeam",       "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/RegisteringTeam"));
    add(new AssertionPair("Registration%s.RegistrationDetails%s.RegisteringOfficer",    "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/RegisteringOfficer"));
    add(new AssertionPair("Registration%s.RegistrationDetails%s.RegisteringCategory",    "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/RegistrationCategory"));
    add(new AssertionPair("Registration%s.RegistrationDetails%s.RegistrationNotes",     "/SPGInterchange/SPGMessage/*[2]/RegistrationDetails%s/Registration/RegistrationNotes"));
  }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

package uk.gov.gsi.justice.spg.test.page.delius;

import dependencies.adapter.driver.PageElement;
import dependencies.adapter.utils.FileUtils;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.DeliusCommon;

public class NationalSearchPage extends DeliusCommon implements FormPageInterface {

  @FindBy(id = "SearchForm:FirstName")
  WebElementFacade txtFirstName;
  @FindBy(id = "SearchForm:LastName")
  WebElementFacade txtLastName;
  @FindBy(id = "SearchForm:searchButton")
  WebElementFacade btnSearch;

  @FindBy(xpath = "//*[@value='Add Offender']")
  WebElementFacade btnAddOffender;

  public void inputFirstName(String firstName) {
    txtFirstName.sendKeys(firstName);
  }

  public void inputLastName(String lastName) {
    txtLastName.sendKeys(lastName);
  }

  public NationalSearchPage clickSearchButton() {
    txtFirstName.sendKeys(offenderFirstName);
    txtLastName.sendKeys(offenderLastName);
    btnSearch.click();
    return switchToPage(NationalSearchPage.class);
  }

  public AddOffenderPage clickAddOffender() {
    btnAddOffender.click();
    return switchToPage(AddOffenderPage.class);
  }

  //  private static final String TEST_DATA_PREFIX = "OffenderDetails";
//
//  private static final String XPATH_INPUT_FIRST_NAME = "//*[@id='SearchForm:FirstName']";
//  private static final String XPATH_INPUT_MIDDLE_NAME = "//*[@id='SearchForm:MiddleName']";
//  private static final String XPATH_INPUT_LAST_NAME = "//*[@id='SearchForm:LastName']";
//  private static final String XPATH_SELECT_INCLUDE_ALIAS = "//*[@id='SearchForm:IncludeAlias']";
//  private static final String XPATH_SELECT_GENDER = "//*[@id='SearchForm:Gender']";
//  private static final String XPATH_SELECT_OTHER_IDENTIFIER = "//*[@id='otherIdentifier']";
//  private static final String XPATH_SELECT_NATIONAL_SEARCH = "//*[@id='SearchForm:NationalSearch']";
//  private static final String XPATH_SELECT_PROVIDER = "//*[@id='SearchForm:Trust']";
//  private static final String XPATH_BTN_SEARCH = "//*[@id='SearchForm:searchButton']";
  private static final String XPATH_BTN_ADD_OFFENDER = "//*[@value='Add Offender']";
  //  private static final String XPATH_INPUT_NOMS = "//*[@id='SearchForm:NOMSNumber']";
//
//  private static final String XPATH_INPUT_CRN = "//*[@id='SearchForm:CRN']";
//
//  private static final String XPATH_VIEW_BY_ROW = "//*[@id='offendersTable']/tbody/tr[1]/td[10]/a";
//
//  private static final PageElement INPUT_FIRST_NAME =
//      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_FIRST_NAME, "Search Form First Name Input Field");
//  private static final PageElement INPUT_MIDDLE_NAME =
//      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_MIDDLE_NAME, "Search Form Middle Name Input Field");
//  private static final PageElement INPUT_LAST_NAME =
//      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_LAST_NAME, "Search Form Last Name Input Field");
//  private static final PageElement SELECT_INCLUDE_ALIAS =
//      new PageElement(PageElement.Type.XPATH, XPATH_SELECT_INCLUDE_ALIAS, "Search Form Include Alias Select Field");
//  private static final PageElement SELECT_GENDER = new PageElement(PageElement.Type.XPATH, XPATH_SELECT_GENDER, "Search Form Gender Select Field");
//  private static final PageElement SELECT_OTHER_IDENTIFIER =
//      new PageElement(PageElement.Type.XPATH, XPATH_SELECT_OTHER_IDENTIFIER, "Search Form Other Identifier Select Field");
//  private static final PageElement SELECT_NATIONAL_SEARCH =
//      new PageElement(PageElement.Type.XPATH, XPATH_SELECT_NATIONAL_SEARCH, "Search Form National Search Select Field");
//  private static final PageElement SELECT_PROVIDER =
//      new PageElement(PageElement.Type.XPATH, XPATH_SELECT_PROVIDER, "Search Form Provider Select Field");
//  private static final PageElement BTN_SEARCH = new PageElement(PageElement.Type.XPATH, XPATH_BTN_SEARCH, "Search Form Search Button");
  private static final PageElement BTN_ADD_OFFENDER =
          new PageElement(PageElement.Type.XPATH, XPATH_BTN_ADD_OFFENDER, "Search Form Add Offender Button");
//  private static final PageElement INPUT_NOMS = new PageElement(PageElement.Type.XPATH, XPATH_INPUT_NOMS, "Search Form Other Identifier NOMS Number Input Field");
//
//
//  private static final PageElement INPUT_CRN =
//      new PageElement(PageElement.Type.XPATH, XPATH_INPUT_CRN, "Search Form Other Identifier CRN Input Field");
//
//  public void inputFirstName(String firstName) {
//    pilot.clearAndSendKeys(INPUT_FIRST_NAME, firstName);
//  }
//
//  public void inputMiddleName(String middleName) {
//    pilot.sendKeys(INPUT_MIDDLE_NAME, middleName);
//  }
//
//  public void inputLastName(String lastName) {
//    pilot.clearAndSendKeys(INPUT_LAST_NAME, lastName);
//  }
//
//  public void selectIncludeAlias(String includeAlias) {
//    pilot.selectFromVisibleText(SELECT_INCLUDE_ALIAS, includeAlias);
//  }
//
//  public void selectGender(String gender) {
//    pilot.selectFromVisibleText(SELECT_GENDER, gender);
//  }
//
//  public void selectOtherIdentifier(String otherIdentifier) {
//    pilot.selectFromVisibleText(SELECT_OTHER_IDENTIFIER, otherIdentifier);
//  }
//
//  public void inputCRN(String crn) {
//    //this.selectOtherIdentifier("CRN Number");
//
//      pilot.clearAndSendKeys(INPUT_CRN, crn);
//
//  }
//
//  public void selectNationalSearch(String nationalSearch) {
//    pilot.selectFromVisibleText(SELECT_NATIONAL_SEARCH, nationalSearch);
//  }
//
//  public void selectProvider(String provider) {
//    pilot.selectFromVisibleText(SELECT_PROVIDER, provider);
//  }
//
//  public NationalSearchPage clickSearchButton() {
//    pilot.click(BTN_SEARCH);
//    return new NationalSearchPage();
//  }
//
//  public void waitForAddOffenderBtnVisible() {
//    pilot.waitForElementPresent(BTN_ADD_OFFENDER);
//  }
//
//  public boolean isAddOffenderBtnVisible() {
//    return pilot.isDisplayed(BTN_ADD_OFFENDER);
//  }
//
//  public AddOffenderPage clickAddOffender() {
//    //pilot.click(BTN_ADD_OFFENDER);
//    return new AddOffenderPage();
//  }
//
////  public OffenderSummaryPage clickViewByRow(int i) {
////    pilot.click(PageElement.byXpath(XPATH_VIEW_BY_ROW, "National Search Page View Offender By Row " + i, i));
////    return new OffenderSummaryPage();
////  }
//
//  public void selectPersonalIdentifier(String pi) {
//    pilot.selectFromVisibleText(SELECT_OTHER_IDENTIFIER, pi);
//  }
//
//  public void inputPersonalIdentifier(String pi) {
//    pilot.sendKeys(XPATH_INPUT_NOMS, pi);
//  }
//
////  public OffenderSummaryPage searchOffender(String caseReferenceNumber) {
////    pilot.clear(INPUT_FIRST_NAME);
////    pilot.clear(INPUT_LAST_NAME);
////    this.selectPersonalIdentifier("[Not Selected]");
////    this.inputCRN(caseReferenceNumber);
////    NationalSearchPage nationalSearchPage = this.clickSearchButton();
////
////    // Need to do a short sleep here to let the page load the search results.
////    try {
////      Thread.sleep(2000);
////    } catch (InterruptedException e) {
////      e.printStackTrace();
////    }
////
////    return nationalSearchPage.clickViewByRow(1);
////  }
//
//  public Boolean searchNOMSNumber(String nomsNumber) {
//    Boolean offenderFound = false;
//    pilot.clear(INPUT_FIRST_NAME);
//    pilot.clear(INPUT_LAST_NAME);
//    this.selectPersonalIdentifier("NOMS Number");
//    pilot.click(INPUT_NOMS);
//    this.inputPersonalIdentifier(nomsNumber);
//    pilot.clearAndSendKeys(INPUT_NOMS, nomsNumber);
//
//    NationalSearchPage nationalSearchPage = this.clickSearchButton();
//
//    // Need to do a short sleep here to let the page load the search results.
//    try {
//      Thread.sleep(2000);
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
//
//    return (pilot.elementExists(PageElement.byXpath(XPATH_VIEW_BY_ROW,"National Search Page View Offender By Row")));
//  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
    //String value = getInputValuesIndex(inputValuesIndex);

    FileUtils.saveScreenshot(this.getClass().getSimpleName());

    throw new UnsupportedOperationException("Not implemented, yet");
  }
}

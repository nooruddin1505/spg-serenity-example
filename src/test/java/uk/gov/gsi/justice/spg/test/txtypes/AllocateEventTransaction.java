package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class AllocateEventTransaction extends BaseTransaction {

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    // Event Details
    add(new AssertionPair("AllocateEvent%s.Event%s.CaseReferenceNumber", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Offender/CaseReferenceNumber"));
    add(new AssertionPair("AllocateEvent%s.Event%s.OffenderID", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/OffenderID"));
    add(new AssertionPair("AllocateEvent%s.Event%s.EventID", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/EventID"));
    add(new AssertionPair("AllocateEvent%s.Event%s.ReferralDate", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/ReferralDate"));
    add(new AssertionPair("AllocateEvent%s.Event%s.EventNumber", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/EventNumber"));
    add(new AssertionPair("AllocateEvent%s.Event%s.OffenceDate", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/OffenceDate"));
    add(new AssertionPair("AllocateEvent%s.Event%s.OffenceCount", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/OffenceCount"));
    add(new AssertionPair("AllocateEvent%s.Event%s.TICS", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/TICS"));
    add(new AssertionPair("AllocateEvent%s.Event%s.ConvictionDate", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/ConvictionDate"));
    add(new AssertionPair("AllocateEvent%s.Event%s.OSProvider", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/OSProvider"));
    add(new AssertionPair("AllocateEvent%s.Event%s.OSResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/OSResponsibleTeam"));
    add(new AssertionPair("AllocateEvent%s.Event%s.OSResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/OSResponsibleOfficer"));
    add(new AssertionPair("AllocateEvent%s.Event%s.OffenceCode", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/OffenceCode"));
    add(new AssertionPair("AllocateEvent%s.Event%s.OrderType", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/OrderType"));
    add(new AssertionPair("AllocateEvent%s.Event%s.SentenceDate", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/SentenceDate"));
    add(new AssertionPair("AllocateEvent%s.Event%s.EnteredLength", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/EnteredLength"));
    add(new AssertionPair("AllocateEvent%s.Event%s.Length", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/Length"));
    add(new AssertionPair("AllocateEvent%s.Event%s.EnteredLengthUnit", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/EnteredLengthUnit"));
    add(new AssertionPair("AllocateEvent%s.Event%s.LengthUnit", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/LengthUnit"));
    add(new AssertionPair("AllocateEvent%s.Event%s.LengthDays", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/LengthDays"));
    add(new AssertionPair("AllocateEvent%s.Event%s.SecondLengthUnit", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/SecondLengthUnit"));
    add(new AssertionPair("AllocateEvent%s.Event%s.ExpectedEndDate", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/ExpectedEndDate"));
    add(new AssertionPair("AllocateEvent%s.Event%s.ReleaseDate", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/ReleaseDate"));
    add(new AssertionPair("AllocateEvent%s.Event%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/Notes"));
    add(new AssertionPair("AllocateEvent%s.Event%s.TerminationReason", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/TerminationReason"));
    add(new AssertionPair("AllocateEvent%s.Event%s.RSRScore", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/RSRScore"));
    add(new AssertionPair("AllocateEvent%s.Event%s.RSRDate", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/RSRDate"));
    add(new AssertionPair("AllocateEvent%s.Event%s.RSRProvider", "/SPGInterchange/SPGMessage/*[2]/Event%s/EventDetails/Event/RSRProvider"));
    // Event Requirements
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.RequirementTypeMainCategory", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/RequirementTypeMainCategory"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.RequirementTypeSubCategory", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/RequirementTypeSubCategory"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.Length", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/Length"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.ImposedDate", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/ImposedDate"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.ExpectedStartDate", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/ExpectedStartDate"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.ActualStartDate", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/ActualStartDate"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.RQProvider", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/RQProvider"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.RQResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/RQResponsibleTeam"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.RQResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/RQResponsibleOfficer"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.Notes", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/Notes"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.ExpectedEndDate", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/ExpectedEndDate"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.ActualEndDate", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/ActualEndDate"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.TerminationReason", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/TerminationReason"));
    add(new AssertionPair("AllocateEvent%s.CommunityRequirement%s.AttendanceCount", "/SPGInterchange/SPGMessage/*[2]/CommunityRequirement%s/CommunityRequirementDetails/CommunityRequirement/AttendanceCount"));
    // CommReqtManager
    add(new AssertionPair("AllocateEvent%s.CommReqtManager%s.AllocationDate", "/SPGInterchange/SPGMessage/*[2]/CommReqtManager%s/CommReqtManagerDetails/RqmntManager/AllocationDate", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CommReqtManager%s.AllocationReason", "/SPGInterchange/SPGMessage/*[2]/CommReqtManager%s/CommReqtManagerDetails/RqmntManager/AllocationReason", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CommReqtManager%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/CommReqtManager%s/CommReqtManagerDetails/RqmntManager/EndDate", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CommReqtManager%s.Provider", "/SPGInterchange/SPGMessage/*[2]/CommReqtManager%s/CommReqtManagerDetails/RqmntManager/Provider", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CommReqtManager%s.ResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/CommReqtManager%s/CommReqtManagerDetails/RqmntManager/ResponsibleTeam", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CommReqtManager%s.ResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/CommReqtManager%s/CommReqtManagerDetails/RqmntManager/ResponsibleOfficer", 1, 2, 3));
    // PSSReqtManager
    add(new AssertionPair("AllocateEvent%s.PSSReqtManager%s.AllocationDate", "/SPGInterchange/SPGMessage/*[2]/PSSReqtManager%s/PSSReqtManagerDetails/PSSRqmntManager/AllocationDate", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.PSSReqtManager%s.AllocationReason", "/SPGInterchange/SPGMessage/*[2]/PSSReqtManager%s/PSSReqtManagerDetails/PSSRqmntManager/AllocationReason", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.PSSReqtManager%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/PSSReqtManager%s/PSSReqtManagerDetails/PSSRqmntManager/EndDate", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.PSSReqtManager%s.Provider", "/SPGInterchange/SPGMessage/*[2]/PSSReqtManager%s/PSSReqtManagerDetails/PSSRqmntManager/Provider", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.PSSReqtManager%s.ResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/PSSReqtManager%s/PSSReqtManagerDetails/PSSRqmntManager/ResponsibleTeam", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.PSSReqtManager%s.ResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/PSSReqtManager%s/PSSReqtManagerDetails/PSSRqmntManager/ResponsibleOfficer", 1, 2, 3));
    // LicCondManager
    add(new AssertionPair("AllocateEvent%s.LicCondManager%s.AllocationDate", "/SPGInterchange/SPGMessage/*[2]/LicCondManager%s/LicCondManagerDetails/LicCondManager/AllocationDate", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.LicCondManager%s.AllocationReason", "/SPGInterchange/SPGMessage/*[2]/LicCondManager%s/LicCondManagerDetails/LicCondManager/AllocationReason", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.LicCondManager%s.EndDate", "/SPGInterchange/SPGMessage/*[2]/LicCondManager%s/LicCondManagerDetails/LicCondManager/EndDate", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.LicCondManager%s.Provider", "/SPGInterchange/SPGMessage/*[2]/LicCondManager%s/LicCondManagerDetails/LicCondManager/Provider", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.LicCondManager%s.ResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/LicCondManager%s/LicCondManagerDetails/LicCondManager/ResponsibleTeam", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.LicCondManager%s.ResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/LicCondManager%s/LicCondManagerDetails/LicCondManager/ResponsibleOfficer", 1, 2, 3));
    // Licence Conditions
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.LicCondTypeMainCategory", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/LicCondTypeMainCategory", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.LicCondTypeSubCategory", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/LicCondTypeSubCategory", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.SentenceDate", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/SentenceDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.ExpectedStartDate", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/ExpectedStartDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.ActualStartDate", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/ActualStartDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.LCProvider", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/LCProvider", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.LCResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/LCResponsibleTeam", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.LCResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/LCResponsibleOfficer", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.LicenceNotes", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/LicenceNotes", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.ExpectedEndDate", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/ExpectedEndDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.ActualEndDate", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/ActualEndDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.TerminationReason", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/TerminationReason", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.AttendanceCount", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/AttendanceCount", 1, 2));
    add(new AssertionPair("AllocateEvent%s.LicenceCondition%s.Notes", "/SPGInterchange/SPGMessage/*[2]/LicenceCondition%s/LicenceConditionDetails/LicCondition/Notes", 1, 2));
    // Court Appearance
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.RemandStatus", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/RemandStatus", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.Court", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/Court", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.CourtAppearanceType", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/CourtAppearanceType", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.Plea", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/Plea", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.Outcome", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/Outcome", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.Notes", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/Notes", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.CourtDate", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/CourtDate", 1, 2, 3));
    // Court Appearance Details
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.RemandStatus", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/RemandStatus", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.Court", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/Court", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.CourtAppearanceType", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/CourtAppearanceType", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.Plea", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/Plea", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.Outcome", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/Outcome", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtAppearance%s.Notes", "/SPGInterchange/SPGMessage/*[2]/CourtAppearance%s/CourtAppearanceDetails/CourtAppearance/Notes", 1, 2));
    // Process Contact
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessType", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessType"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessSubType", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessSubType"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessStage", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessStage"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessOutcome", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessOutcome"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessRefDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessRefDate"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessExpStartDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessExpStartDate"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessStartDate", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessStartDate"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessStageDateTime", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessStageDateTime"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessNotes", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessNotes"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessManagerProvider", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessManagerProvider"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessManagerTeam", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessManagerTeam"));
    add(new AssertionPair("AllocateEvent%s.ProcessContact%s.ProcessManagerOfficer", "/SPGInterchange/SPGMessage/*[2]/ProcessContact%s/ProcessContactDetails/ProcessContact/ProcessManagerOfficer"));
    // Referral Details
    add(new AssertionPair("AllocateEvent%s.Referral%s.ReferralType", "/SPGInterchange/SPGMessage/*[2]/Referral%s/ReferralDetails/Referral/ReferralType", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Referral%s.ReferralDate", "/SPGInterchange/SPGMessage/*[2]/Referral%s/ReferralDetails/Referral/ReferralDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Referral%s.ReferralSource", "/SPGInterchange/SPGMessage/*[2]/Referral%s/ReferralDetails/Referral/ReferralSource", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Referral%s.ReferredToProvider", "/SPGInterchange/SPGMessage/*[2]/Referral%s/ReferralDetails/Referral/ReferredToProvider", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Referral%s.ReferredToTeam", "/SPGInterchange/SPGMessage/*[2]/Referral%s/ReferralDetails/Referral/ReferredToTeam", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Referral%s.ReferredToOfficer", "/SPGInterchange/SPGMessage/*[2]/Referral%s/ReferralDetails/Referral/ReferredToOfficer", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Referral%s.ReferralOutcome", "/SPGInterchange/SPGMessage/*[2]/Referral%s/ReferralDetails/Referral/ReferralOutcome", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Referral%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Referral%s/ReferralDetails/Referral/Notes", 1, 2));
    // Assessment Details
    add(new AssertionPair("AllocateEvent%s.Assessment%s.AssessmentDate", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/AssessmentDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.AssessmentType", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/AssessmentType", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.OffenderRequiredToAttend", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/OffenderRequiredtoAttend", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.OffenderAttended", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/OffenderAttended", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.AssessmentProvider", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/AssessmentProvider", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.AssessmentTeam", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/AssessmentTeam", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.AssessmentOfficer", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/AssessmentOfficer", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.AssessmentOutcome", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/AssessmentOutcome", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.DurationMinutes", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/DurationMinutes", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.AssessmentScore", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/AssessmentScore", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.OffenderAgreement", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/OffenderAgreement", 1, 2));
    add(new AssertionPair("AllocateEvent%s.Assessment%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Assessment%s/AssessmentDetails/Assessment/Notes", 1, 2));
    // Drugs Test Header Details
    add(new AssertionPair("AllocateEvent%s.DrugTestHdr%s.DateOfTest", "/SPGInterchange/SPGMessage/*[2]/DrugsTestHDR%s/DrugsTestHDRDetails/DrugsTest/DateOfTest"));
    add(new AssertionPair("AllocateEvent%s.DrugTestHdr%s.TestedBy", "/SPGInterchange/SPGMessage/*[2]/DrugsTestHDR%s/DrugsTestHDRDetails/DrugsTest/TestedBy"));
    add(new AssertionPair("AllocateEvent%s.DrugTestHdr%s.OffenderCompliance", "/SPGInterchange/SPGMessage/*[2]/DrugsTestHDR%s/DrugsTestHDRDetails/DrugsTest/OffenderCompliance"));
    // Drugs Test Detail Details
    add(new AssertionPair("AllocateEvent%s.DrugTestDtl%s.DrugType", "/SPGInterchange/SPGMessage/*[2]/DrugsTestDTL%s/DrugsTestDTLDetails/DrugTestResult/DrugType", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.DrugTestDtl%s.AdmittedUse", "/SPGInterchange/SPGMessage/*[2]/DrugsTestDTL%s/DrugsTestDTLDetails/DrugTestResult/AdmittedUse", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.DrugTestDtl%s.TestResult", "/SPGInterchange/SPGMessage/*[2]/DrugsTestDTL%s/DrugsTestDTLDetails/DrugTestResult/TestResult", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.DrugTestDtl%s.Agreed", "/SPGInterchange/SPGMessage/*[2]/DrugsTestDTL%s/DrugsTestDTLDetails/DrugTestResult/Agreed", 1, 2, 3));
    // Rate Card Intervention Details
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.InterventionTypeMainCategory", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionTypeMainCategory"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.InterventionTypeSubCategory", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionTypeSubCategory"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.ReferralDate", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/ReferralDate"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.ExpectedStartDate", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/ExpectedStartDate"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.ActualStartDate", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/ActualStartDate"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.Notes", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/Notes"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.InterventionStatus", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionStatus"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.InterventionStatusDate", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionStatusDateTime"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.ExpectedEndDate", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/ExpectedEndDate"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.InterventionProvider", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionProvider"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.InterventionResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionResponsibleTeam"));
    add(new AssertionPair("AllocateEvent%s.RateCardIntervention%s.InterventionResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/RateCardIntervention%s/RateCardInterventionDetails/RateCardIntervention/InterventionResponsibleOfficer"));
    // Recall Details
    add(new AssertionPair("AllocateEvent%s.Recall%s.RecallDate", "/SPGInterchange/SPGMessage/*[2]/Recall%s/RecallDetails/Recall/RecallDate"));
    add(new AssertionPair("AllocateEvent%s.Recall%s.RecallReason", "/SPGInterchange/SPGMessage/*[2]/Recall%s/RecallDetails/Recall/RecallReason"));
    add(new AssertionPair("AllocateEvent%s.Recall%s.RecallLocation", "/SPGInterchange/SPGMessage/*[2]/Recall%s/RecallDetails/Recall/RecallLocation"));
    add(new AssertionPair("AllocateEvent%s.Recall%s.RecallNotes", "/SPGInterchange/SPGMessage/*[2]/Recall%s/RecallDetails/Recall/RecallNotes"));
    // UPW Contact Details
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.Date", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/AppointmentDate"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.ProjectName", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/ProjectName"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.ProjectType", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/ProjectType"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.StartTime", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/StartTime"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.EndTime", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/EndTime"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.UPWOutcome", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/UPWOutcome"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.HighVisibilityVest", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/HighVisibilityVest"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.MinutesOffered", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/MinutesOffered"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.MinutesCredited", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/MinutesCredited"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.TravelTime", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/TravelTime"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.Notes", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/Notes"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.WorkQuality", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/WorkQuality"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.Behaviour", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/Behaviour"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.PenaltyTime", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/PenaltyTime"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.Attended", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/Attended"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.Complied", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/Complied"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.Provider", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/Provider"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.UPWTeam", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/UPWTeam"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.UPWContactOfficer", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/UPWContactOfficer"));
    add(new AssertionPair("AllocateEvent%s.UPWContact%s.Intensive", "/SPGInterchange/SPGMessage/*[2]/UPWContact%s/UPWContactDetails/UPWAppointment/Intensive"));
    // UPW Details
    add(new AssertionPair("AllocateEvent%s.UPWDetails%s.AgreedTravelFare", "/SPGInterchange/SPGMessage/*[2]/UPWDetails%s/UPWDetailsDetails/UPWDetails/AgreedTravelFare"));
    add(new AssertionPair("AllocateEvent%s.UPWDetails%s.Notes", "/SPGInterchange/SPGMessage/*[2]/UPWDetails%s/UPWDetailsDetails/UPWDetails/Notes"));
    add(new AssertionPair("AllocateEvent%s.UPWDetails%s.UPWLengthMinutes", "/SPGInterchange/SPGMessage/*[2]/UPWDetails%s/UPWDetailsDetails/UPWDetails/UPWLengthMinutes"));
    add(new AssertionPair("AllocateEvent%s.UPWDetails%s.UPWStatus", "/SPGInterchange/SPGMessage/*[2]/UPWDetails%s/UPWDetailsDetails/UPWDetails/UPWStatus"));
    add(new AssertionPair("AllocateEvent%s.UPWDetails%s.WorkedIntensively", "/SPGInterchange/SPGMessage/*[2]/UPWDetails%s/UPWDetailsDetails/UPWDetails/WorkedIntensively"));
    // PSS Requirement
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.PSSRqmntTypeMainCategory", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/PSSRqmntTypeMainCategory", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.PSSRqmntTypeSubCategory", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/PSSRqmntTypeSubCategory", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.PSProvider", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/PSProvider", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.PSResponsibleTeam", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/PSResponsibleTeam", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.PSResponsibleOfficer", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/PSResponsibleOfficer", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.ExpectedEndDate", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/ExpectedEndDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.ActualEndDate", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/ActualEndDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.TerminationReason", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/TerminationReason", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.Notes", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/Notes", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.Length", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/Length", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.ImposedDate", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/ImposedDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.PSSRequirement%s.ExpectedStartDate", "/SPGInterchange/SPGMessage/*[2]/PSSRequirement%s/PSSRequirementDetails/PSSRequirement/ExpectedStartDate", 1, 2));
    // UPW Adjustment
    add(new AssertionPair("AllocateEvent%s.UPWAdjustment%s.AdjustmentAmount", "/SPGInterchange/SPGMessage/*[2]/UPWAdjustment%s/UPWAdjustmentDetails/UPWAdjustment/AdjustmentAmount"));
    add(new AssertionPair("AllocateEvent%s.UPWAdjustment%s.AdjustmentDate", "/SPGInterchange/SPGMessage/*[2]/UPWAdjustment%s/UPWAdjustmentDetails/UPWAdjustment/AdjustmentDate"));
    add(new AssertionPair("AllocateEvent%s.UPWAdjustment%s.AdjustmentType", "/SPGInterchange/SPGMessage/*[2]/UPWAdjustment%s/UPWAdjustmentDetails/UPWAdjustment/AdjustmentType"));
    add(new AssertionPair("AllocateEvent%s.UPWAdjustment%s.AdjustmentReason", "/SPGInterchange/SPGMessage/*[2]/UPWAdjustment%s/UPWAdjustmentDetails/UPWAdjustment/AdjustmentReason"));
    add(new AssertionPair("AllocateEvent%s.UPWAdjustment%s.AdjustmentAmount", "/SPGInterchange/SPGMessage/*[2]/UPWAdjustment%s/UPWAdjustmentDetails/UPWAdjustment/AdjustmentAmount"));
    add(new AssertionPair("AllocateEvent%s.UPWAdjustment%s.AdjustedByProvider", "/SPGInterchange/SPGMessage/*[2]/UPWAdjustment%s/UPWAdjustmentDetails/UPWAdjustment/AdjustedByProvider"));
    // Institutional Report Details
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.RequestedDate", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/RequestedDate", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.RequiredByDate", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/RequiredByDate", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.RequestedReportType", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/RequestedReportType", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.AllocatedDate", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/AllocatedDate", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.VideoLink", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/VideoLink", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.CompletedDate", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/CompletedDate", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.Institution", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/Institution", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.DeliveredToEstablishment", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/DeliveredtoEstablishment", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.DateAbandoned", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/DateAbandoned", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.InstReportProvider", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/InstReportProvider", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.InstReportTeam", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/InstReportTeam", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.InstReportOfficer", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/InstReportOfficer", 1));
    add(new AssertionPair("AllocateEvent%s.InstitutionalReport%s.Notes", "/SPGInterchange/SPGMessage/*[2]/InstitutionalReport%s/InstitutionalReportDetails/InstitutionalReport/Notes", 1));
    // Event Court Report
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.RequestedDate", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/RequestedDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.RequiredByDate", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/RequiredByDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.RequiredByCourt", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/RequiredByCourt", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.RequestedReportType", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/RequestedReportType", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.DeliveredReportType", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/DeliveredReportType", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.AllocationDate", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/AllocationDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.CrtReportProvider", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/CrtReportProvider", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.CrtReportTeam", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/CrtReportTeam", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.CrtReportOfficer", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/CrtReportOfficer", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.CompletedDate", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/CompletedDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.Notes", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/Notes", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.SentToCourtDate", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/SentToCourtDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.ReceivedByCourtDate", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/ReceivedByCourtDate", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.ProposalType", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/ProposalType", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.DeliveredReportReason", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/DeliveredReportReason", 1, 2));
    add(new AssertionPair("AllocateEvent%s.CourtReport%s.ProposalLength", "/SPGInterchange/SPGMessage/*[2]/CourtReport%s/CourtReportDetails/CourtReport/Length", 1, 2));
    // Proposed Requirement
    add(new AssertionPair("AllocateEvent%s.ProposedRequirement%s.ADRequirementTypeMainCategory", "/SPGInterchange/SPGMessage/*[2]/ProposedRequirement%s/ProposedRequirementDetails/ProposedRequirement/ADRequirementTypeMainCategory"));
    add(new AssertionPair("AllocateEvent%s.ProposedRequirement%s.ADRequirementTypeSubCategory", "/SPGInterchange/SPGMessage/*[2]/ProposedRequirement%s/ProposedRequirementDetails/ProposedRequirement/ADRequirementTypeSubCategory"));
    // Custody Location
    add(new AssertionPair("AllocateEvent%s.CustodyLocation%s.CurrentLocation", "/SPGInterchange/SPGMessage/*[2]/CustodyLocation%s/CustodyLocationDetails/CustodyLocation/CurrentLocation"));
    add(new AssertionPair("AllocateEvent%s.CustodyLocation%s.LocationStartDate", "/SPGInterchange/SPGMessage/*[2]/CustodyLocation%s/CustodyLocationDetails/CustodyLocation/LocationStartDate"));
    add(new AssertionPair("AllocateEvent%s.CustodyLocation%s.PrisonerNumber", "/SPGInterchange/SPGMessage/*[2]/CustodyLocation%s/CustodyLocationDetails/CustodyLocation/PrisonerNumber"));
    add(new AssertionPair("AllocateEvent%s.CustodyLocation%s.ParoleNumber", "/SPGInterchange/SPGMessage/*[2]/CustodyLocation%s/CustodyLocationDetails/CustodyLocation/ParoleNumber"));
    add(new AssertionPair("AllocateEvent%s.CustodyLocation%s.ProbationContact", "/SPGInterchange/SPGMessage/*[2]/CustodyLocation%s/CustodyLocationDetails/CustodyLocation/ProbationContact"));
    add(new AssertionPair("AllocateEvent%s.CustodyLocation%s.PCTelephoneNumber", "/SPGInterchange/SPGMessage/*[2]/CustodyLocation%s/CustodyLocationDetails/CustodyLocation/PCTelephoneNumber"));
    add(new AssertionPair("AllocateEvent%s.CustodyLocation%s.PrisonOfficer", "/SPGInterchange/SPGMessage/*[2]/CustodyLocation%s/CustodyLocationDetails/CustodyLocation/PrisonOfficer"));
    add(new AssertionPair("AllocateEvent%s.CustodyLocation%s.POTelephoneNumber", "/SPGInterchange/SPGMessage/*[2]/CustodyLocation%s/CustodyLocationDetails/CustodyLocation/POTelephoneNumber"));
    // Release Details
    add(new AssertionPair("AllocateEvent%s.Release%s.ReleaseDate", "/SPGInterchange/SPGMessage/*[2]/Release%s/ReleaseDetails/Release/ReleaseDate"));
    add(new AssertionPair("AllocateEvent%s.Release%s.ReleaseType", "/SPGInterchange/SPGMessage/*[2]/Release%s/ReleaseDetails/Release/ReleaseType"));
    add(new AssertionPair("AllocateEvent%s.Release%s.Location", "/SPGInterchange/SPGMessage/*[2]/Release%s/ReleaseDetails/Release/Location"));
    // Recall Details
    add(new AssertionPair("AllocateEvent%s.Recall%s.RecallDate", "/SPGInterchange/SPGMessage/*[2]/Recall%s/RecallDetails/Recall/RecallDate"));
    // Custody Date
    add(new AssertionPair("AllocateEvent%s.CustodyDate%s.KeyDateType", "/SPGInterchange/SPGMessage/*[2]/CustodyDate%s/CustodyDateDetails/CustodyKeyDate/KeyDateType", 1, 2, 3));
    add(new AssertionPair("AllocateEvent%s.CustodyDate%s.KeyDate", "/SPGInterchange/SPGMessage/*[2]/CustodyDate%s/CustodyDateDetails/CustodyKeyDate/KeyDate", 1, 2, 3));
    // Contact Details
    add(new AssertionPair("AllocateEvent%s.Contact%s.Alert", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Alert", 1, 2, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.ContactType", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactType", 1, 2, 3, 4, 5, 6, 7));
    add(new AssertionPair("AllocateEvent%s.Contact%s.ContactDate", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactDate", 1, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.ContactStartTime", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactStartTime", 1, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.ContactEndTime", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactEndTime", 1, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.Provider", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Provider", 1, 2,  3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.Team", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Team", 1, 2, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.ContactOfficer", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactOfficer", 1, 2, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.Location", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Location", 1, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.ContactOutcome", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/ContactOutcome", 1, 2, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.EnforcementAction", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/EnforcementAction", 1, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.SensitiveContact", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/SensitiveContact", 1, 3, 4, 5));
    add(new AssertionPair("AllocateEvent%s.Contact%s.Notes", "/SPGInterchange/SPGMessage/*[2]/Contact%s/ContactDetails/Contact/Notes", 1, 2, 3, 4, 5));
    // PSS Commence
    add(new AssertionPair("AllocateEvent%s.CustodyPSS%s.PSSDate", "/SPGInterchange/SPGMessage/*[2]/CustodyPSS%s/CustodyPSSDetails/CustodyPSS/PSSDate"));
    // Approved Premises Referral
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ReferralDate", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ReferralDate"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ExpectedArrivalDate", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ExpectedArrivalDate"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ExpectedDepartureDate", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ExpectedDepartureDate"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.DecisionDate", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/DecisionDate"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ReferringProvider", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ReferringProvider"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ReferringTeam", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ReferringTeam"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ReferringOfficer", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ReferringOfficer"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.DecisionProvider", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/DecisionProvider"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.DecisionTeam", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/DecisionTeam"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.DecisionOfficer", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/DecisionOfficer"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ReferralCategory", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ReferralCategory"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ReferralDecision", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ReferralDecision"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ReferralGroup", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ReferralGroup"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ApprovedPremises", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ApprovedPremises"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.APReferralSource", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/APReferralSource"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.ReferralNotes", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/ReferralNotes"));
    add(new AssertionPair("AllocateEvent%s.APReferral%s.SourceType", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesReferral%s/ApprovedPremisesReferralDetails/ApprovedPremisesReferral/SourceType"));
    // Approved Premises Residence
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.ArrivalDate", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/ArrivalDate"));
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.ExpectedDepartureDate", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/ExpectedDepartureDate"));
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.ArrivalNotes", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/ArrivalNotes"));
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.DepartureDate", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/DepartureDate"));
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.DepartureReason", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/DepartureReason"));
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.ApprovedPremises", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/ApprovedPremises"));
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.MoveOnCategory", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/MoveOnCategory"));
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.APOfficer", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/APOfficer"));
    add(new AssertionPair("AllocateEvent%s.ApprovedPremisesResidence%s.APKeyWorker", "/SPGInterchange/SPGMessage/AllocateEvent/ApprovedPremisesResidence%s/ApprovedPremisesResidenceDetails/ApprovedPremisesResidence/APKeyWorker"));
    // Additional Sentence
    add(new AssertionPair("AllocateEvent%s.AdditionalSentence%s.AdditionalSentence", "/SPGInterchange/SPGMessage/AllocateEvent/AdditionalSentence%s/AdditionalSentenceDetails/AdditionalSentence/AdditionalSentence", 1, 2));
    add(new AssertionPair("AllocateEvent%s.AdditionalSentence%s.Length", "/SPGInterchange/SPGMessage/AllocateEvent/AdditionalSentence%s/AdditionalSentenceDetails/AdditionalSentence/Length", 1, 2));
    add(new AssertionPair("AllocateEvent%s.AdditionalSentence%s.Amount", "/SPGInterchange/SPGMessage/AllocateEvent/AdditionalSentence%s/AdditionalSentenceDetails/AdditionalSentence/Amount", 1, 2));
    add(new AssertionPair("AllocateEvent%s.AdditionalSentence%s.Notes", "/SPGInterchange/SPGMessage/AllocateEvent/AdditionalSentence%s/AdditionalSentenceDetails/AdditionalSentence/Notes", 1, 2));
    // Order Manager
    add(new AssertionPair("AllocateEvent%s.OrderManager%s.Provider", "/SPGInterchange/SPGMessage/AllocateEvent/OrderManager%s/OrderManagerDetails/OrderManager/Provider", 1, 2));

  }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

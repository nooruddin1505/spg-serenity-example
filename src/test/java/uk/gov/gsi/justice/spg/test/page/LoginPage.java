package uk.gov.gsi.justice.spg.test.page;

import org.openqa.selenium.WebDriver;
import uk.gov.gsi.justice.spg.test.utils.cukes.ConfigUtils;

public class LoginPage extends DeliusCommon {
  private static final String NDELIUS_APP_WINDOW_TITLE = "National Delius Home Page";

  public LoginPage() {
  }

  public HomePage login(String username, String password, String host, String port, WebDriver driver) {

    //Load data related to the current test
    testData = ConfigUtils.loadTestDataFiles();
    ConfigUtils.loadAdditionalPropertyFiles();

    //Login to nDelius
    String url = String.format("http://%s:%s@%s:%s/NDelius-war/", username, password, host, port);
    driver.get(url);

    for (String handle : driver.getWindowHandles()) {
      if (driver.switchTo().window(handle).getTitle().equals(NDELIUS_APP_WINDOW_TITLE)) {
        return new HomePage();
      }
    }

    return null;
  }

}

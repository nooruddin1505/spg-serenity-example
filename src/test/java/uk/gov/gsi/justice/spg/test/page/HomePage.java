package uk.gov.gsi.justice.spg.test.page;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.page.delius.NationalSearchPage;

public class HomePage extends DeliusCommon {

  @FindBy(linkText = "National Search")
  WebElementFacade btnNationalSearch;
  @FindBy(linkText = "Recently Viewed")
  WebElementFacade btnRecentlyViewed;
  @FindBy(linkText = "Officer Diary")
  WebElementFacade btnOfficerDiary;
  @FindBy(linkText = "Court Diary")
  WebElementFacade btnCourtDiary;
  @FindBy(linkText = "Approved Premises Diary")
  WebElementFacade btnApprovedPremisesDiary;
  @FindBy(linkText = "UPW Project Diary")
  WebElementFacade btnUPWProjectDiary;
  @FindBy(linkText = "User Preferences")
  WebElementFacade btnUserPreferences;

  public NationalSearchPage clickNationalSearchButton() {
    btnNationalSearch.click();
    return switchToPage(NationalSearchPage.class);
  }

//  public RecentlyViewedPage clickRecentlyViewedButton() {
//    pilot.click(BTN_RECENTLY_VIEWED);
//    return new RecentlyViewedPage();
//  }
//
//  public CaseLoadDiaryPage clickOfficerDiaryButton() {
//    pilot.click(BTN_OFFICER_DIARY);
//    return new CaseLoadDiaryPage();
//  }
//
//  public CourtDiaryPage clickCourtDiaryButton() {
//    pilot.click(BTN_COURT_DIARY);
//    return new CourtDiaryPage();
//  }
//
//  public CurrentResidentsPage clickApprovedPremisesDiaryButton() {
//    pilot.click(BTN_APPROVED_PREMISES_DIARY);
//    return new CurrentResidentsPage();
//  }
//
//  public UPWProjectDiaryPage clickUPWProjectDiaryButton() {
//    pilot.click(BTN_UPW_PROJECT_DIARY);
//    return new UPWProjectDiaryPage();
//  }
//
//  public UserPreferencesPage clickUUserPreferencesButton() {
//    pilot.click(BTN_USER_PREFERENCES);
//    return new UserPreferencesPage();
//  }
}

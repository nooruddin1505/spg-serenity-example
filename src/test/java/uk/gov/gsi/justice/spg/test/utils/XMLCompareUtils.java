package uk.gov.gsi.justice.spg.test.utils;//package test.utils;
//
//import org.xmlunit.builder.DiffBuilder;
//import org.xmlunit.diff.Diff;
//import org.xmlunit.diff.Difference;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * XML Utils to help comparing and getting specific values from files
// */
//public class XMLCompareUtils {
//
//  public static void main(String[] args){
//
//    String source = "C:\\workspace\\spg-functional-automation\\target\\test-results\\1_TXN_2017-09-27_10-03-25_X100001.xml";
//    String target = "C:\\workspace\\spg-functional-automation\\target\\test-results\\1_TXN_2017-09-27_10-20-42_X100006.xml";
//    try {
//      XMLCompareUtils.isXMLFilesSame(source, target, new ArrayList<>(), true);
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//  }
//
//  public static boolean isXMLFilesSame(String sourceFile, String targetFile, List<String> tagsToIgnore, boolean printDifferences) throws IOException {
//
//    File s = new File(sourceFile);
//    File t = new File(targetFile);
//    System.out.println(s.getAbsolutePath());
//    System.out.println(t.getAbsolutePath());
//
//    // using BufferedReader for improved performance
//    BufferedReader source = new BufferedReader(new InputStreamReader(new FileInputStream(s)));
//    BufferedReader target = new BufferedReader(new InputStreamReader(new FileInputStream(t)));
//
//    //comparing two XML using XMLUnit in Java
//    Diff differences = compare(source, target, tagsToIgnore);
//
//    if(printDifferences){
//      printDifferences(differences);
//    }
//
//    return differences.hasDifferences();
//  }
//
//
//  private static Diff compare(BufferedReader expectedSource, BufferedReader actualSource, List<String> tagsToIgnore) throws IOException {
//    String source = removeAnyPrologErrors(expectedSource);
//    String target = removeAnyPrologErrors(actualSource);
//
//    final Diff documentDiff = DiffBuilder
//            .compare(source)
//            .withTest(target)
//            .ignoreComments()
//            .ignoreWhitespace()
//            //.withNodeFilter(node -> !node.getNodeName().equals(someName))
//            .withNodeFilter(node -> !tagsToIgnore.contains(node.getNodeName()))
//            .build();
//
//    return documentDiff;
//  }
//
//  private static String removeAnyPrologErrors(BufferedReader expectedSource) throws IOException {
//    StringBuilder sb = new StringBuilder();
//    String line = "";
//    while((line = expectedSource.readLine())!=null){
//      sb.append(line);
//    }
//    return sb.toString();
//  }
//
//  private static void printDifferences(Diff differences) {
//    Iterable<Difference> allDifferences = differences.getDifferences();
//
//    for(Difference diff:allDifferences){
//      System.out.print(diff.toString()+" ");
//      System.out.println(" ");
//    }
//  }
//}

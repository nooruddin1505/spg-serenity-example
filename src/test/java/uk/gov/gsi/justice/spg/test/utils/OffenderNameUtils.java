package uk.gov.gsi.justice.spg.test.utils;

import java.util.Random;

/**
 * When using a static name for multiple tests - such as John Doe - after 30 of the same name have been entered into Delius, it becomes difficult to enter more due to Delius
 * functionality. For this reason, and because specific name values do not affect test functionality, the offender names used for tests are generated randomly. This class
 * contains the functionality for achieving this.
 */
public class OffenderNameUtils {
  private static Random rand = new Random();

  private static String[] beginning =
      {"Jan", "Feb", "Mar", "Apr", "Mar", "Jan", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "One", "Two", "Thr", "Fou", "Fiv", "Six", "Sev", "Eig", "Nin", "Ten", "Ele", "Twe", "Thi"};
  private static String[] middle = {"Zaq", "Cdw", "Egx", "Lve", "Leg", "Rff", "Ggg", "Nev", "Bhg", "Web", "Lfh", "Egs", "Asf", "Vgf", "MJg", "Tjg", "Lvd", "Ppp", "Rgv", "Vvv"};
  private static String[] end = {"AA", "BB", "CC", "DD", "EE", "FF", "GG", "HH", "II", "JJ", "KK", "LL", "MM", "NN", "OO", "PP", "QQ", "RR", "SS", "TT", "UU", "VV", "WW", "XX", "YY", "ZZ"};

  /**
   * Generates a random name from the contents of three sets of pre-populated strings.
   *
   * @return a new random name.
   */
  public static String generateName() {
    return beginning[rand.nextInt(beginning.length)] + middle[rand.nextInt(middle.length)] + end[rand.nextInt(end.length)];
  }
}

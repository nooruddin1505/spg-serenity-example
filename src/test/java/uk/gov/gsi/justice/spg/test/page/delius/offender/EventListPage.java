package uk.gov.gsi.justice.spg.test.page.delius.offender;

import dependencies.adapter.driver.PageElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.EventDetailsPage;

public class EventListPage extends OffenderDetailsCommon {

  @FindBy(xpath = ".//*[@value='Add']")
  WebElementFacade btnAdd;


  public AddEventPage clickAddButton() {
    clickOn(btnAdd);
    return switchToPage(AddEventPage.class);
  }

  private static final String XPATH_BTN_ADD = "//*[@value='Add']";
  private static final String XPATH_BTN_RECOVER_DELETED_EVENTS = "//*[@value='Recover Deleted Events']";
  private static final String XPATH_BTN_CLOSE = "//*[@value='Close']";
  private static final String XPATH_LINK_VIEW_BY_ROW = "//*[@id='eventsListForm:eventsTable:tbody_element']/tr[%s]/td[6]/a";
  private static final String XPATH_LINK_DELETE_BY_ROW = "//*[@id='eventsListForm:eventsTable:tbody_element']/tr[%s]/td[8]/a";
  private static final String XPATH_LINK_UPDATE_BY_ROW = "//*[@id='eventsListForm:eventsTable:tbody_element']/tr[%s]/td[7]/a";
  private static final String XPATH_TERMINATE_BY_ROW = "//*[@id='eventsListForm:eventsTable:tbody_element']/tr[%s]/td[9]/a";
  private static final String XPATH_LINK_VIEW_BY_NUMBER = "//*[@id='eventsListForm:eventsTable:tbody_element']/tr/td[1]/span[contains(text(),'1')]/../../td[6]/a[contains(text(), 'view')]";

  private static final PageElement BTN_ADD = new PageElement(PageElement.Type.XPATH, XPATH_BTN_ADD, "EventListPage add button");
  private static final PageElement BTN_RECOVER_DELETED_EVENTS = new PageElement(PageElement.Type.XPATH, XPATH_BTN_RECOVER_DELETED_EVENTS, "EventListPage recover deleted events button");
  private static final PageElement BTN_CLOSE = new PageElement(PageElement.Type.XPATH, XPATH_BTN_CLOSE, "EventListPage close button");


//  public RecoverEventsPage clickRecoverDeletedEventsButton() {
//    pilot.click(BTN_RECOVER_DELETED_EVENTS);
//    return new RecoverEventsPage();
//  }
//
//  public OffenderDetailsPage clickCloseButton() {
//    pilot.click(BTN_CLOSE);
//    return new OffenderDetailsPage();
//  }

  public EventDetailsPage clickViewEventByRow(int row) {
    By viewEventByRow = By.xpath(String.format(XPATH_LINK_VIEW_BY_ROW, row));
    clickOn(find(viewEventByRow));
    return switchToPage(EventDetailsPage.class);
  }

//  public DeleteEventPage clickDeleteEventByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_LINK_DELETE_BY_ROW, "EventListPage delete event by row", Integer.toString(row)));
//    return new DeleteEventPage();
//  }
//
//  public AmendEventDetailsPage clickUpdateByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_LINK_UPDATE_BY_ROW, "EventListPage update event by row", Integer.toString(row)));
//    return new AmendEventDetailsPage();
//  }
//
//  public EventDetailsPage clickViewEventByNumber(int number) {
//    pilot.click(PageElement.byXpath(XPATH_LINK_VIEW_BY_NUMBER, "EventListPage view event by number", Integer.toString(number)));
//    return new EventDetailsPage();
//  }
//
//  public TerminateEventPage clickTerminateByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_TERMINATE_BY_ROW, "EventListPage terminate event by row", Integer.toString(row)));
//    return new TerminateEventPage();
//  }
}

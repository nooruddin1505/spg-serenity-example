package uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails;

import dependencies.adapter.driver.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uk.gov.gsi.justice.spg.test.interfaces.TablePageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class RequirementsListPage extends EventDetailsCommon implements TablePageAssertionInterface {
  private static final String EXPECTED_VALUES_PREFIX = "Requirement.";

  @FindBy(xpath = "//*[@value='Add Requirements']")
  WebElement btnAddRequirements;
  @FindBy(xpath = "//*[@value='Recover Deleted Requirements']")
  WebElement btnRecoverDeletedRequirements;
  @FindBy(xpath = "//*[@value='Close']")
  WebElement btnClose;

  //Dynamically find elements
  private static final String XPATH_VIEW_REQUIREMENT_BY_ROW = "//table/tbody/tr[%s]/td[7]/a";
  private static final String XPATH_UPDATE_REQUIREMENT_BY_ROW = "//table/tbody/tr[%s]/td[8]/a";
  private static final String XPATH_DELETE_REQUIREMENT_BY_ROW = "//table/tbody/tr[%s]/td[9]/a";
  private static final String XPATH_LINK_VIEW_REQUIREMENT_BY_ROW = "//table/tbody/tr[%s]/td[7]/a";


  public RequirementDetailsPage viewRequirementByRow(int row) {
    By viewByRow = By.xpath(String.format(XPATH_VIEW_REQUIREMENT_BY_ROW, row));
    clickOn(find(viewByRow));
    return switchToPage(RequirementDetailsPage.class);
  }

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair(EXPECTED_VALUES_PREFIX + "ImposedDate", "//table/tbody/tr%s/td[1]/span"));
  }};

  public AddRequirementPage clickAddRequirements() {
    clickOn(btnAddRequirements);
    return switchToPage(AddRequirementPage.class);
  }
//
//  public RecoverRequirementsPage clickRecoverDeletedRequirements() {
//    pilot.click(RECOVER_DELETED_REQUIREMENTS);
//    return new RecoverRequirementsPage();
//  }
//
//  public EventDetailsPage clickClose() {
//    pilot.click(CLOSE);
//    return new EventDetailsPage();
//  }
//
//  public RequirementDetailsPage viewRequirementByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_VIEW_REQUIREMENT_BY_ROW, "RequirementsListPage view requirement by row", row));
//    return new RequirementDetailsPage();
//  }
//
//  public UpdateRequirementPage updateRequirementByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_UPDATE_REQUIREMENT_BY_ROW, "RequirementsListPage update requirement by row", row));
//    return new UpdateRequirementPage();
//  }
//
//  public DeleteRequirementPage deleteRequirementByRow(int row) {
//    pilot.click(PageElement.byXpath(XPATH_DELETE_REQUIREMENT_BY_ROW, "RequirementsListPage delete requirement by row", row));
//    return new DeleteRequirementPage();
//  }

  @Override
  public boolean tableExists() {
    return false;
  }

  @Override
  public String getValueAtLocation(int row, int column) {
    return null;
  }

  @Override
  public String getValueAtLocation(int row, String columnName) {
    return null;
  }

  @Override
  public void assertTableRowNotExists(int rowLocation) {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      PageElement targetElement = new PageElement(PageElement.Type.XPATH, pair.getXPath(rowLocation), "Asserting nothing exists at following xPath: " + pair.getXPath(rowLocation));

      boolean elementExists = pilot.elementExists(targetElement);

      // If there's something at the rowLocation, need to check whether it's the row we're expecting not to see.
      if (elementExists) {
        String actual = pilot.getText(targetElement);
        assertor.assertNotEquals(String.format("Expected value at row %s does not exist. Expected: '%s', actual: '%s'", rowLocation, expected, actual), expected, actual);
      } else {
        assertor.assertFalse(String.format("Value at row '%s' is null, as expected.", rowLocation), elementExists);
      }
    }

  }

  @Override
  public void assertTableRowExists(int rowLocation) {

  }

  @Override
  public void assertPageValues() {

  }

}

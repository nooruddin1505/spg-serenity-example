package uk.gov.gsi.justice.spg.test.utils;

public enum ShowRowNumber {
  Yes("Yes"),
  NO("No");

  private final String status;

  ShowRowNumber(String status) {
    this.status = status;
  }

  public String getStatus() {
    return this.status;
  }
}
package uk.gov.gsi.justice.spg.test.txtypes;

import dependencies.adapter.utils.XMLUtils;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import uk.gov.gsi.justice.spg.test.exceptions.TransactionFieldNotFoundException;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseTransaction {

  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
  protected static final String XPATH_TRANSACTION_TYPE = configData.getProperty("XPath.TransactionType");

  private String transactionFilePath;

  private XMLUtils xmlUtils;

  public String getTransactionFilePath() {
    return this.transactionFilePath;
  }

  public void setTransactionFilePath(String transactionFilePath) {
    this.transactionFilePath = transactionFilePath;
  }

  public List<String> getFields() {
    List<AssertionPair> assertionPairs = this.getAssertionPairs();
    List<String> fields = new ArrayList<>();

    for (AssertionPair assertionPair : assertionPairs) {
      String xPath = assertionPair.getXPath();
      fields.add(xPath.substring(xPath.lastIndexOf("/") + 1));
    }

    return fields;
  }

  public boolean hasField(String fieldName) {
    for (String field : getFields()) {
      if (field.equals(fieldName))
        return true;
    }

    return false;
  }

  public String getXPathFromFieldName(String fieldName) throws TransactionFieldNotFoundException {
    for (AssertionPair assertionPair : this.getAssertionPairs()) {
      String xPath = assertionPair.getXPath();
      String field = xPath.substring(xPath.lastIndexOf("/") + 1);

      if (field.equals(fieldName)) {
        return xPath;
      }
    }

    throw new TransactionFieldNotFoundException("Could not find field with name " + fieldName);
  }

  public String getNodeContents(String fieldName) {
    XMLUtils xmlUtils;
    String contents = "";
    List<AssertionPair> assertionPairs;

    try {
      xmlUtils = new XMLUtils(this.transactionFilePath);

      assertionPairs = this.getAssertionPairs();

      for (AssertionPair assertionPair : assertionPairs) {
        String xPath = assertionPair.getXPath();
        if (xPath.substring(xPath.lastIndexOf("/") + 1).equals(fieldName)) {
          contents = xmlUtils.getNodeTextContent(xPath);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return contents;
  }

  public abstract ArrayList<AssertionPair> getAssertionPairs();

  public String getTransactionName() {
    return this.getClass().getSimpleName().replace("Transaction", "");
  }

  public XMLUtils getXMLUtils() {
    return xmlUtils;
  }

  public void setXMLUtils(XMLUtils xmlUtils) {
    this.xmlUtils = xmlUtils;
  }

  @Override
  public boolean equals(Object obj) {
    return obj.getClass().equals(this.getClass());
  }

  @Override
  public int hashCode() {
    return 17 * this.getClass().hashCode();
  }
}

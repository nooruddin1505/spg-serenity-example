package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class InterchangeStatusNotificationTransaction extends BaseTransaction {

  private final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("InterchangeStatusNotification%s.Response.StatusCode", "/SPGInterchange/SPGMessage/*[2]/StatusCode"));
  }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

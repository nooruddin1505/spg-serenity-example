package uk.gov.gsi.justice.spg.test.utils;

public enum ShowStaffGrade {
  Yes("Yes"),
  NO("No");

  private final String status;

  ShowStaffGrade(String status) {
    this.status = status;
  }

  public String getStatus() {
    return this.status;
  }
}


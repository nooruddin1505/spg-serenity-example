package uk.gov.gsi.justice.spg.test.utils;

import dependencies.adapter.abstractstuff.CRCID;
import dependencies.adapter.utils.Pair;
import dependencies.adapter.utils.XMLUtils;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import uk.gov.gsi.justice.spg.test.txtypes.BaseTransaction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * For inbound tests (tests where a message is to be injected from the CRC endpoint) it is required that some data included in
 * the inbound message has to be obtained from the outbound message. This is the case for Delius-generated data such as IDs
 * and case reference numbers.
 * <p>
 * The transaction interchange solves this problem by providing a method for exchanging data between an outbound XML message
 * which has been obtained from the CRC server and an inbound XML message which is being stored as test data.
 */
public class TransactionInterchange {

  private static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
  private HashMap<Pair<Class, MessageType>, HashMap<String, String>> interchangeData;

  /**
   * Constructor to creates a new TransactionInterchange object with the given interchange data.
   *
   * @param interchangeData the interchange data which contains the outbound transaction types as the key. The value is a
   *                        map containing an XPath to the data source as its key, and an XPath to the target element as its
   *                        value. Confusing, I know, but check out some existing inbound tests for examples.
   */
  public TransactionInterchange(HashMap<Pair<Class, MessageType>, HashMap<String, String>> interchangeData) {
    this.interchangeData = interchangeData;
  }

  /**
   * Method to exchange the data between the source and target XML messages using the interchangeData provided with the
   * constructor. The source XML messages are obtained using the given CRN and CRC ID parameters.
   *
   * @param caseReferenceNumber the CRN of the outbound messages to search for.
   * @param crcID               the ID of the CRC to search for the outbound messages.
   * @param targetXMLPath       the path to the XML for the inbound message stored as test data.
   * @return true if the operation was successful, false otherwise.
   */
  public List<BaseTransaction> interchange(String caseReferenceNumber, CRCID crcID, String targetXMLPath) {
    List<BaseTransaction> transactions = null;
    try {
      XMLUtils targetXMLUtils = new XMLUtils(targetXMLPath);
      transactions = TransactionUtils.getTransactions(caseReferenceNumber, crcID);

      for (BaseTransaction sourceTransaction : transactions) {
        for (Map.Entry<Pair<Class, MessageType>, HashMap<String, String>> interchangeDataEntry : interchangeData.entrySet()) {
          if (sourceTransaction.getClass().equals(interchangeDataEntry.getKey().getFirst()) &&
                  sourceTransaction.getXMLUtils().getNodeTextContent(configData.getProperty("XPath.MessageType")).equals(interchangeDataEntry.getKey().getSecond().getName())) {
            for (Map.Entry<String, String> sourceTarget : interchangeDataEntry.getValue().entrySet()) {
              String sourceXPath = sourceTarget.getKey();
              String targetXPath = sourceTarget.getValue();

              String sourceValue = sourceTransaction.getXMLUtils().getNodeTextContent(sourceXPath);

              targetXMLUtils.updateNode(targetXPath, sourceValue);
            }
          }
        }
      }

      targetXMLUtils.saveDocument();
      return transactions;
    } catch (Exception e) {
      e.printStackTrace();
    }

    return transactions;
  }

  public List<BaseTransaction> interchange(String caseReferenceNumber, CRCID crcID, String targetXMLPath, String schemaVersion) {
    List<BaseTransaction> transactions = null;
    try {
      XMLUtils targetXMLUtils = new XMLUtils(targetXMLPath);
      transactions = TransactionUtils.getTransactions(caseReferenceNumber, crcID, schemaVersion);

      for (BaseTransaction sourceTransaction : transactions) {
        for (Map.Entry<Pair<Class, MessageType>, HashMap<String, String>> interchangeDataEntry : interchangeData.entrySet()) {
          if (sourceTransaction.getClass().equals(interchangeDataEntry.getKey().getFirst()) &&
                  sourceTransaction.getXMLUtils().getNodeTextContent(configData.getProperty("XPath.MessageType")).equals(interchangeDataEntry.getKey().getSecond().getName())) {
            for (Map.Entry<String, String> sourceTarget : interchangeDataEntry.getValue().entrySet()) {
              String sourceXPath = sourceTarget.getKey();
              String targetXPath = sourceTarget.getValue();

              String sourceValue = sourceTransaction.getXMLUtils().getNodeTextContent(sourceXPath);

              targetXMLUtils.updateNode(targetXPath, sourceValue);
            }
          }
        }
      }

      targetXMLUtils.saveDocument();
      return transactions;
    } catch (Exception e) {
      e.printStackTrace();
    }

    return transactions;
  }


  public static List<BaseTransaction> getInterchangeTransactionsWithPolling(String caseReferenceNumber, CRCID crcID, Class expectedTransactionType, String messageType) {
    List<BaseTransaction> transactions = null;
    long start = System.currentTimeMillis();
    boolean foundExpectedTransactions = false;
    long timeToWaitMilliSeconds = 1000;//ExecutionContext.getInstance().getConfigData().getLongProperty("SPG.Message.Medium.Wait.Millis") / 1000;
    long totalTime = 0;

    //Poll every couple of seconds and check if the transaction we are interested in is there
    do {
      try {
        WaitUtils.sleepSpecificTimeDontUseMeOverSeleniumWaits(timeToWaitMilliSeconds);
        transactions = TransactionUtils.getTransactionsUseWithPolling(caseReferenceNumber, crcID);

        //Verify we have a transaction with the specified message type
        String messagePath = configData.getProperty("XPath.MessageType");
        for (BaseTransaction sourceTransaction : transactions) {
          if(sourceTransaction.getClass().getName().equals(expectedTransactionType.getName())){
            String v = sourceTransaction.getXMLUtils().getNodeTextContent(messagePath);
            if (v.equals(messageType)) {
              foundExpectedTransactions = true;
              break;
            }
          }
        }
      } catch (AssertionError ae) {
        foundExpectedTransactions = false;
      } catch (Exception e) {
        System.out.println("Waiting for messages to appear in the system : " + expectedTransactionType);
      }
      totalTime = totalTime + timeToWaitMilliSeconds;

      //Try again if all transactions not found and totalTime is less than 60 seoncds
    } while (!foundExpectedTransactions && totalTime <= 60000);

    if (!foundExpectedTransactions) {
      transactions = null;
      //assertor.fail("Expected transaction types NOT found in results.");
    }
    long finish = System.currentTimeMillis();
    System.out.format("\n----------------------------------------\nTime taken for all the messages to appear in SPG : %s seconds\n----------------------------------------\n", ((finish - start) / 1000) );

    return transactions;
  }

  /**
   * ONLY USE IF YOU ALREADY HAVE A LIST OF TRANSACTION MESSAGES
   * - AND ONLY INTERCHANGING DATA WITH 1 SPECIFIC FILE
   *
   * Method to exchange the data between the source and target XML messages using the interchangeData provided with the
   * constructor. The source XML messages are obtained using the given CRN and CRC ID parameters.
   *
   * @param transactions  Use previously returned transaction data to do interchanges
   * @param targetXMLPath the path to the XML for the inbound message stored as test data.
   * @return true if the operation was successful, false otherwise.
   */
  public List<BaseTransaction> interchangeWithRecentTransactionData(List<BaseTransaction> transactions, String targetXMLPath) {

    if (transactions != null && transactions.size() > 0) {
      try {
        XMLUtils targetXMLUtils = new XMLUtils(targetXMLPath);

        for (BaseTransaction sourceTransaction : transactions) {
          for (Map.Entry<Pair<Class, MessageType>, HashMap<String, String>> interchangeDataEntry : interchangeData.entrySet()) {
            if (sourceTransaction.getClass().equals(interchangeDataEntry.getKey().getFirst()) &&
                    sourceTransaction.getXMLUtils().getNodeTextContent(configData.getProperty("XPath.MessageType")).equals(interchangeDataEntry.getKey().getSecond().getName())) {
              for (Map.Entry<String, String> sourceTarget : interchangeDataEntry.getValue().entrySet()) {
                String sourceXPath = sourceTarget.getKey();
                String targetXPath = sourceTarget.getValue();

                String sourceValue = sourceTransaction.getXMLUtils().getNodeTextContent(sourceXPath);

                targetXMLUtils.updateNode(targetXPath, sourceValue);
              }
            }
          }
        }

        targetXMLUtils.saveDocument();
        return transactions;
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    return transactions;
  }

  /**
   * Method to exchange the data between the source and target XML messages using the interchangeData provided with the
   * constructor. The source XML messages are obtained using the given CRN and CRC ID parameters.
   *
   * @param targetXPath   path to update
   * @param sourceValue   specific value we want
   * @param targetXMLPath the path to the XML for the inbound message stored as test data.
   */
  public void interchangeSpecificValues(String targetXPath, String sourceValue, String targetXMLPath) {
    try {
      XMLUtils targetXMLUtils = new XMLUtils(targetXMLPath);
      targetXMLUtils.updateNode(targetXPath, sourceValue);
      targetXMLUtils.saveDocument();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}

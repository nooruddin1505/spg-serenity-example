package uk.gov.gsi.justice.spg.test.exceptions;

/**
 * Exception class for when a particular field has not been found inside a transaction as part of a test.
 */
public class TransactionFieldNotFoundException extends Exception {

  public TransactionFieldNotFoundException(String description) {
    super(description);
  }
}

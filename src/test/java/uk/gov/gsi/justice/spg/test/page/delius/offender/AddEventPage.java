package uk.gov.gsi.justice.spg.test.page.delius.offender;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;
import uk.gov.gsi.justice.spg.test.utils.cukes.TextUtils;

public class AddEventPage extends OffenderCommon implements FormPageInterface {
  private static final String TEST_DATA_PREFIX = "CreateEvent";

  @FindBy(id = "ReferralDate")
  WebElementFacade inputReferralDate;
  @FindBy(id = "OffenceDate")
  WebElementFacade inputOffenceDate;
  @FindBy(id = "OffenceCount")
  WebElementFacade inputOffenceCount;
  @FindBy(id = "addEventForm:Tics")
  WebElementFacade inputTics;
  @FindBy(id = "ConvictionDate")
  WebElementFacade inputConvictionDate;
  @FindBy(id = "MainOffence")
  WebElementFacade selectMainOffence;
  @FindBy(id = "addEventForm:SubOffence")
  WebElementFacade selectOffenceSubCat;
  @FindBy(id = "addEventForm:EventNotes")
  WebElementFacade inputOffenceNotes;
  @FindBy(id = "Court")
  WebElementFacade selectCourt;
  @FindBy(id = "addEventForm:CourtCaseNumber")
  WebElementFacade inputCourtCaseNumber;
  @FindBy(id = "addEventForm:Area")
  WebElementFacade selectProvider;

  @FindBy(id = "addEventForm:Team")
  WebElement selectTeam;
  @FindBy(id = "addEventForm:Staff")
  WebElementFacade selectOfficer;
  @FindBy(id = "AppearanceType")
  WebElementFacade selectAppearanceType;
  @FindBy(id = "Plea")
  WebElementFacade selectPlea;
  @FindBy(id = "addEventForm:Outcome")
  WebElementFacade selectOutcome;
  @FindBy(id = "addEventForm:Length")
  WebElementFacade inputLength;
  @FindBy(id = "LengthUnit")
  WebElementFacade selectLengthUnit;
  @FindBy(id = "EnteredExpectedEndDate")
  WebElementFacade inputEnteredExpectedEndDate;
  @FindBy(id = "OutcomeArea")
  WebElementFacade selectSupervisingProvider;
  @FindBy(id = "addEventForm:OutcomeTeam")
  WebElementFacade selectOutcomeTeam;
  @FindBy(id = "addEventForm:OutcomeStaff")
  WebElementFacade selectOutcomeStaff;

  @FindBy(id = "addEventForm:PurposeNotStated")
  WebElementFacade selectPurposeOfSentencingStated;
  @FindBy(id = "addEventForm:CourtSeriousness")
  WebElementFacade selectCourtViewOfSeriousness;

  @FindBy(id = "addEventForm:Remand")
  WebElementFacade selectRemandStatus;
  @FindBy(id = "addEventForm:NextAppearanceDate")
  WebElementFacade inputNextAppearanceDate;
  @FindBy(id = "addEventForm:NextAppearanceTime")
  WebElementFacade inputNextAppearanceTime;
  @FindBy(id = "NextCourt")
  WebElementFacade selectNextCourt;

  @FindBy(id = "addEventForm:j_id_id374")
  WebElementFacade btnSave;
  @FindBy(id = "NextCourt")
  WebElementFacade btnConfirm;


  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {

    String value = getInputValuesIndex(inputValuesIndex);

    //Fill form
    inputReferralDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ReferralDate"));
    inputOffenceDate(testData.getInputValue(TEST_DATA_PREFIX + value, "OffenceDate"));
    inputOffenceCount(testData.getInputValue(TEST_DATA_PREFIX + value, "OffenceCount"));
    inputTics(testData.getInputValue(TEST_DATA_PREFIX + value, "TICS"));
    inputConvictionDate(testData.getInputValue(TEST_DATA_PREFIX + value, "ConvictionDate"));
    selectMainOffence(testData.getInputValue(TEST_DATA_PREFIX + value, "MainOffence"));
    selectOffenceSubCat(testData.getInputValue(TEST_DATA_PREFIX + value, "OffenceSubCat"));
    inputOffenceNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "Notes"));
    selectCourt(testData.getInputValue(TEST_DATA_PREFIX + value, "Court"));
    inputCourtCaseNumber(testData.getInputValue(TEST_DATA_PREFIX + value, "CourtCaseNumber"));
    selectProvider(testData.getInputValue(TEST_DATA_PREFIX + value, "Provider"));
    selectTeam(testData.getInputValue(TEST_DATA_PREFIX + value, "Team"));

    selectOfficer(testData.getInputValue(TEST_DATA_PREFIX + value, "Officer"));
    selectAppearanceType(testData.getInputValue(TEST_DATA_PREFIX + value, "AppearanceType"));
    selectPlea(testData.getInputValue(TEST_DATA_PREFIX + value, "Plea"));
    selectOutcome(testData.getInputValue(TEST_DATA_PREFIX + value, "Outcome"));
    inputLength(testData.getInputValue(TEST_DATA_PREFIX + value, "Length"));
    selectLengthUnit(testData.getInputValue(TEST_DATA_PREFIX + value, "LengthUnit"));

    selectSupervisingProvider(testData.getInputValue(TEST_DATA_PREFIX + value, "SupervisingProvider"));
    selectTeam2(testData.getInputValue(TEST_DATA_PREFIX + value, "Team2"));
    selectOfficer2(testData.getInputValue(TEST_DATA_PREFIX + value, "Officer2"));

    selectPurposeOfSentencingStated(testData.getInputValue(TEST_DATA_PREFIX + value, "PurposeOfSentencingStated"));
    selectCourtViewOfSeriousness(testData.getInputValue(TEST_DATA_PREFIX + value, "CourtViewOfSeriousness"));
    inputCourtNotes(testData.getInputValue(TEST_DATA_PREFIX + value, "Notes2"));

    //selectRemandStatus(testData.getInputValue(TEST_DATA_PREFIX + value, "RemandStatus"));
    inputNextAppearanceDate(testData.getInputValue(TEST_DATA_PREFIX + value, "NextAppearanceDate"));
    inputNextAppearanceTime(testData.getInputValue(TEST_DATA_PREFIX + value, "NextAppearanceTime"));
    selectNextCourt(testData.getInputValue(TEST_DATA_PREFIX + value, "NextCourt"));

    //Save
    clickOn(btnSave);

//    // TODO - CT: If the Event contains a Report, the Court Report screen appears asking the user to save it
      if(containsElements(By.xpath("//*[@value='Save']")))
        clickOn(btnSave);

//
//    // TODO - RL: I think this is forcing an implicit wait each time.

    if(containsElements(By.xpath("//*[@value='Confirm']")))
      clickOn(btnConfirm);

    return type.cast(new AddEventPage());
  }

  private void inputNextAppearanceTime(String nextAppearanceTime) {
    if(TextUtils.isEmptyOrNull(nextAppearanceTime)) return;
    typeInto(inputNextAppearanceTime, nextAppearanceTime);
  }

  private void inputCourtNotes(String notes2) {

  }

  private void selectCourtViewOfSeriousness(String courtViewOfSeriousness) {
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:CourtSeriousness"), courtViewOfSeriousness);
    selectFromDropdown(selectCourtViewOfSeriousness, courtViewOfSeriousness);
  }

  private void selectPurposeOfSentencingStated(String purposeOfSentencingStated) {
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:PurposeNotStated"), purposeOfSentencingStated);
    selectFromDropdown(selectPurposeOfSentencingStated, purposeOfSentencingStated);
  }

  private void selectOfficer2(String officer2) {
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:OutcomeStaff"), officer2);
    selectFromDropdown(selectOutcomeStaff, officer2);
  }

  private void selectTeam2(String team2) {
    WaitUtils.sleepSpecificTimeDontUseMeOverSeleniumWaits(this, 300);
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:OutcomeTeam"), team2);
    selectFromDropdown(selectOutcomeTeam, team2);
  }

  private void selectSupervisingProvider(String supervisingProvider) {
    WaitUtils.isDropDownReadyForSelection(By.id("OutcomeArea"), supervisingProvider);
    selectFromDropdown(selectSupervisingProvider, supervisingProvider);
  }

  private void selectLengthUnit(String lengthUnit) {
    WaitUtils.isDropDownReadyForSelection(By.id("LengthUnit"), lengthUnit);
    selectFromDropdown(selectLengthUnit, lengthUnit);
  }


  private void selectNextCourt(String nextCourt) {
    if(TextUtils.isEmptyOrNull(nextCourt)) return;
    WaitUtils.isDropDownReadyForSelection(By.id("NextCourt"), nextCourt);
    selectFromDropdown(selectNextCourt, nextCourt);
  }

  private void inputNextAppearanceDate(String nextAppearanceDate) {
    if(TextUtils.isEmptyOrNull(nextAppearanceDate)) return;
    typeInto(inputNextAppearanceDate, nextAppearanceDate);
  }

  private void selectRemandStatus(String remandStatus) {
    //WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:Remand"), remandStatus);
    waitForTextToAppear(selectRemandStatus, remandStatus);
    selectFromDropdown(selectRemandStatus, remandStatus);
  }

  private void selectOutcome(String outcome) {
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:Outcome"), outcome);
    selectFromDropdown(selectOutcome, outcome);
  }

  private void selectPlea(String plea) {
    WaitUtils.isDropDownReadyForSelection(By.id("Plea"), plea);
    selectFromDropdown(selectPlea, plea);
  }

  private void selectAppearanceType(String appearanceType) {
    WaitUtils.isDropDownReadyForSelection(By.id("AppearanceType"), appearanceType);
    selectFromDropdown(selectAppearanceType, appearanceType);
  }

  private void selectTeam(String team) {
    WaitUtils.sleepSpecificTimeDontUseMeOverSeleniumWaits(this, 300);
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:Team"), team);
    selectFromDropdown(selectTeam, team);
  }


  public void selectOffenceSubCat(String offenceSubCat) {  // Required field
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:SubOffence"), offenceSubCat);
    selectFromDropdown(selectOffenceSubCat, offenceSubCat);
  }

  private void selectOfficer(String officer) {
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:Staff"), officer);
    selectFromDropdown(selectOfficer, officer);
  }

  private void selectProvider(String provider) {
    WaitUtils.isDropDownReadyForSelection(By.id("addEventForm:Area"), provider);
    selectFromDropdown(selectProvider, provider);
  }

  private void inputCourtCaseNumber(String courtCaseNumber) {
    typeInto(inputCourtCaseNumber, courtCaseNumber);
  }

  private void selectCourt(String court) {
    WaitUtils.isDropDownReadyForSelection(By.id("Court"), court);
    selectFromDropdown(selectCourt, court);
  }

  private void inputOffenceNotes(String notes) {
    typeInto(inputOffenceNotes, notes);
  }

  private void selectMainOffence(String mainOffence) {
    WaitUtils.isDropDownReadyForSelection(By.id("MainOffence"), mainOffence);
    selectFromDropdown(selectMainOffence, mainOffence);
  }

  private void inputLength(String length) {
    typeInto(inputLength, length);
  }

  private void inputConvictionDate(String convictionDate) {
    typeInto(inputConvictionDate, convictionDate);
  }

  private void inputTics(String tics) {
    typeInto(inputTics, tics);
  }

  private void inputOffenceCount(String offenceCount) {
    typeInto(inputOffenceCount, offenceCount);
  }

  private void inputOffenceDate(String offenceDate) {
    typeInto(inputOffenceDate, offenceDate);
  }

  private void inputReferralDate(String referralDate) {
    if(TextUtils.isEmptyOrNull(referralDate)) return;
    typeInto(inputReferralDate, referralDate);
  }

}

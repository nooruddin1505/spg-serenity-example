package uk.gov.gsi.justice.spg.test.page.delius;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.DeliusCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.OffenderDetailsPage;
import uk.gov.gsi.justice.spg.test.session.SessionKey;

public class AddOffenderPage extends DeliusCommon implements FormPageInterface {

  public int personaIDCount = 0;
  private static final String TEST_DATA_PREFIX = "OffenderDetails";

  @FindBy(id = "addOffenderForm:Trust")
  WebElementFacade selectOwningProvider;
  @FindBy(id = "addOffenderForm:Title")
  WebElementFacade selectTitle;
  @FindBy(id = "addOffenderForm:FirstName")
  WebElementFacade inputFirstName;
  @FindBy(id = "addOffenderForm:Surname")
  WebElementFacade inputSurname;
  @FindBy(id = "addOffenderForm:SecondName")
  WebElementFacade inputSecondName;
  @FindBy(id = "addOffenderForm:ThirdName")
  WebElementFacade inputThirdName;
  @FindBy(id = "addOffenderForm:PreviousSurname")
  WebElementFacade inputPreviousSurname;
  @FindBy(id = "DateOfBirth")
  WebElementFacade inputDateOfBirth;
  @FindBy(id = "addOffenderForm:Notes")
  WebElementFacade inputNotes;
  @FindBy(id = "addOffenderForm:Gender")
  WebElementFacade selectGender;
  @FindBy(id = "Mobile")
  WebElementFacade inputMobile;
  @FindBy(id = "SMS")
  WebElementFacade selectSMS;
  @FindBy(id = "addOffenderForm:Telephone")
  WebElementFacade inputTelephone;
  @FindBy(id = "addOffenderForm:Email")
  WebElementFacade inputEmail;

  @FindBy(id = "addOffenderForm:PNC")
  WebElementFacade inputPNC;
  @FindBy(id = "addOffenderForm:CRO")
  WebElementFacade inputCRO;
  @FindBy(id = "addOffenderForm:NOMS")
  WebElementFacade inputNOMS;
  @FindBy(id = "addOffenderForm:NI")
  WebElementFacade inputNI;

  @FindBy(id = "addOffenderForm:identifierType")
  WebElementFacade selectIdentifierType;
  @FindBy(id = "addOffenderForm:identifierValue")
  WebElementFacade inputIdentifierValue;

  @FindBy(xpath = "//*[@value='Add/Update']")
  WebElementFacade btnAddUpdate;
  @FindBy(xpath = "//*[@value='Save']")
  WebElementFacade btnSave;
  @FindBy(xpath = "//*[@value='Confirm']")
  WebElementFacade btnConfirm;
  @FindBy(xpath = "//*[@value='Cancel']")
  WebElementFacade btnCancel;
  @FindBy(xpath = "//*[@value='Cancel']")
  WebElementFacade getBtnCancel2;

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
    String value = getInputValuesIndex(inputValuesIndex);

    String testName = Serenity.getCurrentSession().get(SessionKey.testDataFileLocation).toString();

    selectOwningProvider(testData.getInputValue(TEST_DATA_PREFIX + value, "OwningProvider"));
    selectTitle(testData.getInputValue(TEST_DATA_PREFIX + value, "Title"));

    inputFirstName(testData.getInputValue(TEST_DATA_PREFIX + value, "Forenames"));
    inputSecondName(testData.getInputValue(TEST_DATA_PREFIX + value, "SecondName"));
    inputThirdName(testData.getInputValue(TEST_DATA_PREFIX + value, "ThirdName"));
    inputSurname(testData.getInputValue(TEST_DATA_PREFIX + value, "Surname"));
    inputPreviousSurname(testData.getInputValue(TEST_DATA_PREFIX + value, "PreviousName"));

    selectGender(testData.getInputValue(TEST_DATA_PREFIX + value, "Gender"));
    inputDateOfBirth(testData.getInputValue(TEST_DATA_PREFIX + value, "DoB"));
    inputNotes(testName);

    inputMobile(testData.getInputValue(TEST_DATA_PREFIX + value, "MobileNo"));
    selectSMS(testData.getInputValue(TEST_DATA_PREFIX + value, "SMSContact"));
    inputTelephone(testData.getInputValue(TEST_DATA_PREFIX + value, "TelephoneNumber"));
    inputEmail(testData.getInputValue(TEST_DATA_PREFIX + value, "EMailAddress"));

    //save
    clickOn(btnSave);

    if (containsElements(By.xpath("//*[@value='Save']")))
      clickOn(btnSave);

    // If the offender details already exist, we need to click a confirm button.
    if (containsElements(By.xpath("//*[@value='Confirm']")))
      clickOn(btnConfirm);

    return type.cast(new OffenderDetailsPage());
  }

  private void selectSMS(String smsContact) {
    selectFromDropdown(selectSMS, smsContact);
  }

  private void selectGender(String gender) {
    selectFromDropdown(selectGender, gender);
  }

  private void selectOwningProvider(String owningProvider) {
    selectFromDropdown(selectOwningProvider, owningProvider);
  }

  private void inputEmail(String eMailAddress) {
    typeInto(inputEmail, eMailAddress);
  }

  private void inputTelephone(String telephoneNumber) {
    typeInto(inputTelephone, telephoneNumber);
  }

  private void inputMobile(String mobileNo) {
    typeInto(inputMobile, mobileNo);
  }

  private void inputNotes(String notes) {
    typeInto(inputNotes, notes);
  }

  private void inputPreviousSurname(String testName) {
    typeInto(inputPreviousSurname, testName);
  }

  private void inputDateOfBirth(String dob) {
    typeInto(inputDateOfBirth, dob);
  }

  private void inputSurname(String surname) {
    if(surname==null || surname.isEmpty())
      surname = offenderLastName;
    typeInto(inputSurname, surname);
  }

  private void inputThirdName(String thirdName) {
    typeInto(inputThirdName, thirdName);
  }

  private void inputSecondName(String secondName) {
    typeInto(inputSecondName, secondName);
  }

  private void inputFirstName(String forenames) {
    if(forenames==null || forenames.isEmpty())
      forenames = offenderFirstName;
    typeInto(inputFirstName, forenames);
  }

  private void selectTitle(String title) {
    selectFromDropdown(selectTitle, title);
  }


}

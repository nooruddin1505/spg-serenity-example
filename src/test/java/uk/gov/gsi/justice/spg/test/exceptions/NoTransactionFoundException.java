package uk.gov.gsi.justice.spg.test.exceptions;

/**
 * Exception class for when no transactions have been found as part of the test.
 */
public class NoTransactionFoundException extends Exception {

  public NoTransactionFoundException(String description) {
    super(description);
  }
}

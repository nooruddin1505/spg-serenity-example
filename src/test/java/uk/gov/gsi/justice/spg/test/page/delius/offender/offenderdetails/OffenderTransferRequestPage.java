package uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails;

import dependencies.adapter.driver.PageElement;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.interfaces.FormPageInterface;
import uk.gov.gsi.justice.spg.test.interfaces.TablePageAssertionInterface;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.session.SessionKey;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;
import uk.gov.gsi.justice.spg.test.utils.cukes.SessionUtils;
import uk.gov.gsi.justice.spg.test.utils.cukes.TextUtils;

import java.util.ArrayList;

public class OffenderTransferRequestPage extends OffenderDetailsCommon implements FormPageInterface, TablePageAssertionInterface {
  private static final String TEST_DATA_PREFIX = "TransferOffender";

  @FindBy(id = "offenderTransferRequestListForm:Trust")
  WebElementFacade selectProvider;
  @FindBy(id = "offenderTransferRequestListForm:Cluster")
  WebElementFacade selectCluster;
  @FindBy(id = "offenderTransferRequestListForm:LDU")
  WebElementFacade selectLDU;
  @FindBy(id = "offenderTransferRequestListForm:Team")
  WebElementFacade selectTeam;
  @FindBy(id = "offenderTransferRequestListForm:Staff")
  WebElementFacade selectOfficer;

  //Dynamic lookup
  private static final String XPATH_SELECT_REASON_BY_TYPE = "//*[@id='offenderTransferRequestListForm:offenderTransferRequestTable']/tbody/tr/td[contains(text(),'%s')]/../td[7]/select";
  private static final String XPATH_SELECT_REASON_BY_TYPE_NUMBER = "(//*[@id='offenderTransferRequestListForm:offenderTransferRequestTable']/tbody/tr/td[contains(text(),'%s')])[%s]/../td[7]/select";

  @FindBy(xpath = "//*[@value='Transfer']")
  WebElementFacade btnTransfer;

  public void selectReasonByType(String type, String reason) {
    By selectReasonByType = By.xpath(String.format(XPATH_SELECT_REASON_BY_TYPE, type));
    if ((reason != null && !reason.isEmpty()) && containsElements(selectReasonByType))
      selectFromDropdown(find(selectReasonByType), reason);
  }

  public void selectReasonByType(String type, String reason, int number) {
    if (TextUtils.isEmptyOrNull(reason)) return;

    By selectReasonByTypeNumber = By.xpath(String.format(XPATH_SELECT_REASON_BY_TYPE_NUMBER, type, number));
    if ((reason != null && !reason.isEmpty()) && containsElements(selectReasonByTypeNumber))
      selectFromDropdown(find(selectReasonByTypeNumber), reason);
  }

  public void selectProvider(int inputValuesIndex) {
    String provider = "CPA London";
    if (inputValuesIndex > 0) {
      provider = testData.getInputValue("TransferOffender" + inputValuesIndex, "Provider");
    }
    selectFromDropdown(selectProvider, provider);
    SessionUtils.updateListSessionData(SessionKey.listOfProviders, provider);
  }

  public void selectCluster(String cluster) {
    waitForTextToAppear(cluster);
    selectFromDropdown(selectCluster, cluster);
  }

  public void selectLDU(String ldu) {
    waitForTextToAppear(ldu);
    selectFromDropdown(selectLDU, ldu);
  }

  public void selectTeam(String team) {
    WaitUtils.isDropDownReadyForSelection(By.id("offenderTransferRequestListForm:Team"), team);
    selectFromDropdown(selectTeam, team);
  }

  public void selectOfficer(String officer) {
    waitForTextToAppear(officer);
    selectFromDropdown(selectOfficer, officer);
  }

  public OffenderTransferRequestPage clickTransfer() {
    clickOn(btnTransfer);
    return switchToPage(OffenderTransferRequestPage.class);
  }


  private static final String XPATH_SELECT_PROVIDER = "//*[@id='offenderTransferRequestListForm:Trust']";
  private static final String XPATH_SELECT_CLUSTER = "//*[@id='offenderTransferRequestListForm:Cluster']";
  private static final String XPATH_SELECT_LDU = "//*[@id='offenderTransferRequestListForm:LDU']";
  private static final String XPATH_SELECT_TEAM = "//*[@id='offenderTransferRequestListForm:Team']";
  private static final String XPATH_SELECT_OFFICER = "//*[@id='offenderTransferRequestListForm:Staff']";
  private static final String XPATH_BUTTON_TRANSFER = "//*[@value='Transfer']";
  private static final String XPATH_BUTTON_RETURN = "//*[@value='Close']";

  private static final String XPATH_TEMPLATE_CLUSTER_OPTION = XPATH_SELECT_CLUSTER + "/option[contains(.,'%s')]";
  private static final String XPATH_TEMPLATE_CLUSTER_DESCRIPTION = "Offender Transfer Request Cluster Dropdown";
  private static final String XPATH_TEMPLATE_LDU_OPTION = XPATH_SELECT_LDU + "/option[contains(.,'%s')]";
  private static final String XPATH_TEMPLATE_LDU_DESCRIPTION = "Offender Transfer Request LDU Dropdown";
  private static final String XPATH_TEMPLATE_TEAM_OPTION = XPATH_SELECT_TEAM + "/option[contains(.,'%s')]";
  private static final String XPATH_TEMPLATE_TEAM_DESCRIPTION = "Offender Transfer Request Team Dropdown";
  private static final String XPATH_TEMPLATE_OFFICER_OPTION = XPATH_SELECT_OFFICER + "/option[contains(.,'%s')]";
  private static final String XPATH_TEMPLATE_OFFICER_DESCRIPTION = "Offender Transfer Request Officer Dropdown";

  private static final PageElement SELECT_PROVIDER =
          new PageElement(PageElement.Type.XPATH, XPATH_SELECT_PROVIDER, "Offender Transfer Request Provider Dropdown");
  private static final PageElement SELECT_CLUSTER =
          new PageElement(PageElement.Type.XPATH, XPATH_SELECT_CLUSTER, "Offender Transfer Request Cluster Dropdown");
  private static final PageElement SELECT_LDU = new PageElement(PageElement.Type.XPATH, XPATH_SELECT_LDU, "Offender Transfer Request LDU Dropdown");
  private static final PageElement SELECT_TEAM =
          new PageElement(PageElement.Type.XPATH, XPATH_SELECT_TEAM, "Offender Transfer Request Team Dropdown");
  private static final PageElement SELECT_OFFICER =
          new PageElement(PageElement.Type.XPATH, XPATH_SELECT_OFFICER, "Offender Transfer Request Officer Dropdown");
  private static final PageElement BUTTON_TRANSFER =
          new PageElement(PageElement.Type.XPATH, XPATH_BUTTON_TRANSFER, "Offender Transfer Request Transfer Button");
  private static final PageElement BUTTON_RETURN =
          new PageElement(PageElement.Type.XPATH, XPATH_BUTTON_RETURN, "Offender Transfer Request Return Button");

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {
    {
      add(new AssertionPair("OMTransferRequest.OMTransferProvider", "//*[@id='offenderTransferRequestListForm:offenderTransferRequestTable:tbody_element']/tr/td[5]"));
    }
  };


  public OffenderTransferRequestPage clickReturn() {
    pilot.click(BUTTON_RETURN);
    return new OffenderTransferRequestPage();
  }

  @Override
  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
    String value = getInputValuesIndex(inputValuesIndex);

    selectProvider(inputValuesIndex);
    selectTeam(testData.getInputValue(TEST_DATA_PREFIX + value, "Team"));

    //selectOfficer(testData.getInputValue(TEST_DATA_PREFIX + value, "Officer"));
    selectReasonByType("Offender", testData.getInputValue(TEST_DATA_PREFIX + value, "Offender", "Reason"));
    selectReasonByType("Event", testData.getInputValue(TEST_DATA_PREFIX + value, "Event", "Reason"));
    selectReasonByType("Requirement", testData.getInputValue(TEST_DATA_PREFIX + value, "Requirement", "Reason"));
    selectReasonByType("Requirement", testData.getInputValue(TEST_DATA_PREFIX + value, "Requirement2", "Reason"), 2);
    selectReasonByType("Event NSI", testData.getInputValue(TEST_DATA_PREFIX + value, "NSIEvent", "Reason"));
    selectReasonByType("Offender NSI", testData.getInputValue(TEST_DATA_PREFIX + value, "NSIOffender", "Reason"));
    selectReasonByType("PSSR1", testData.getInputValue(TEST_DATA_PREFIX + value, "PSSR1", "Reason"));
    selectReasonByType("PSSR2", testData.getInputValue(TEST_DATA_PREFIX + value, "PSSR2", "Reason"));
    selectReasonByType("Licence Condition", testData.getInputValue(TEST_DATA_PREFIX + value, "LicenceCondition", "Reason"));
    selectReasonByType("Institutional Report", testData.getInputValue(TEST_DATA_PREFIX + value, "InstitutionalReport", "Reason"));
    selectReasonByType("PSS Requirement", testData.getInputValue(TEST_DATA_PREFIX + value, "PSSRequirement", "Reason"));
    selectReasonByType("Court Report", testData.getInputValue(TEST_DATA_PREFIX + value, "CourtReport", "Reason"));


    //FileUtils.saveScreenshot(getClass().getSimpleName());

    return type.cast(clickTransfer());
  }

  @Override
  public boolean tableExists() {
    return false;
  }

  @Override
  public String getValueAtLocation(int row, int column) {
    return null;
  }

  @Override
  public String getValueAtLocation(int row, String columnName) {
    return null;
  }

  @Override
  public void assertTableRowNotExists(int rowLocation) {

  }

  @Override
  public void assertTableRowExists(int rowLocation) {

  }

  @Override
  public void assertPageValues() {
    for (AssertionPair pair : ASSERTION_PAIRS) {
      String expected = testData.getExpectedResult(pair.getDataHeading());
      String actual = pilot.getText(PageElement.byXpath(pair.getXPath(), "OffenderTransferReviewPage get value by description"));

      if (expected == null || expected.isEmpty())
        continue;

      DeliusBaseTestAction.reportNextAssertionPairToCheck(pair.getXPath(), pair.getDataHeading(), "OffenderTransferReviewPage");
      assertor.assertEquals(String.format("%s value is correct. Expected: %s, Actual: %s", pair.getDataHeading(), expected, actual), expected, actual);
    }
  }
}

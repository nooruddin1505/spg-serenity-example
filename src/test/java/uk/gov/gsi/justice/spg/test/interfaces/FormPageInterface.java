package uk.gov.gsi.justice.spg.test.interfaces;

import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;

/**
 * All Delius page can be considered either a form page (where data is input against an entity), a details page (where data against one entity
 * is shown) or a table page (where data against multiple entities is shown), or a combination of two or more of the above. To represent this,
 * all Delius page representations in this project must implement one or more of these PageInterfaces wherever relevant to each individual page.
 *
 * This interface defines methods which must be implemented for any Delius 'form' pages such as the add offender page. Form pages contain multiple
 * input fields for which data from test should be input to.
 */
public interface FormPageInterface {

  /**
   * This method should be used to input all data from test data into the relevant input fields on the page.
   *
   * @param inputValuesIndex the number of the data items, i.e. if two offenders are being added, the first call to this method
   *                         should have number=1, the second number=2. This should also be reflected in test data key names.
   * @param type the Class of the page to be returned to the calling method.
   * @param <T> the generic type of the method to be returned. Must extend DeliusBasePage.
   * @return a page object of type given by the 'type' parameter.
   */
  <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type);

}

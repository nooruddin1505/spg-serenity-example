package uk.gov.gsi.justice.spg.test.utils.cukes;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.SessionMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by InternetUser on 29/11/2017.
 */
public class SessionUtils {

  public static String getStringSessionData(String key){
    String value = (String) Serenity.getCurrentSession().get(key);
    return value;
  }

  public static void updateStringSessionData(String key, String value){
    Serenity.getCurrentSession().put(key, value);
  }

  public static void updateListSessionData(String key, String value){
    SessionMap<Object, Object> session = Serenity.getCurrentSession();
    List<String> list = (List<String>) session.get(key);

    if(list==null){
      list = new ArrayList<>();
      session.put(key, list);
    }

    if(!list.contains(value)){
      list.add(value);
    }
  }
}

package uk.gov.gsi.justice.spg.test.page.delius.offender;

import dependencies.adapter.driver.PageElement;
import dependencies.adapter.utils.FileUtils;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import uk.gov.gsi.justice.spg.test.page.DeliusBasePage;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddressListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.DiversityDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.OffenderTransferRequestPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.RegistrationListPage;

public class OffenderDetailsCommon extends OffenderCommon {

  @FindBy(id="linkNavigation3OffenderTransferRequest")
  WebElementFacade linkNavigation3OffenderTransferRequest;
  @FindBy(id="linkNavigation3OffenderTransferReview")
  WebElementFacade linkNavigation3OffenderTransferReview;
  @FindBy(id="linkNavigation3Alias")
  WebElementFacade linkNavigation3Alias;
  @FindBy(id="linkNavigation3Address")
  WebElementFacade linkNavigation3Address;
  @FindBy(id="linkNavigation3Disability")
  WebElementFacade linkNavigation3Disability;
  @FindBy(id="linkNavigation3Diversity")
  WebElementFacade linkNavigation3Diversity;
  @FindBy(id="linkNavigation3Circumstances")
  WebElementFacade linkNavigation3Circumstances;
  @FindBy(id="linkNavigation3PersonalContact")
  WebElementFacade linkNavigation3PersonalContact;
  @FindBy(id="linkNavigation3ManagementTierHistory")
  WebElementFacade linkNavigation3ManagementTierHistory;
  @FindBy(id="linkNavigation3AdditionalIdentifiers")
  WebElementFacade linkNavigation3AdditionalIdentifiers;
  @FindBy(id="linkNavigation3Registration")
  WebElementFacade linkNavigation3Registration;
  @FindBy(id="linkNavigation3OffenderNsi")
  WebElementFacade linkNavigation3OffenderNsi;

  public OffenderTransferRequestPage clickOffenderTransferRequestLink() {
    clickOn(linkNavigation3OffenderTransferRequest);
    return switchToPage(OffenderTransferRequestPage.class);
  }

  public AddressListPage clickAddressesLink() {
    clickOn(linkNavigation3Address);
    return switchToPage(AddressListPage.class);
  }

  public DiversityDetailsPage clickEqualityMonitoringLink() {
    clickOn(linkNavigation3Diversity);
    return switchToPage(DiversityDetailsPage.class);
  }

  public RegistrationListPage clickRegistrationSummaryLink() {
    clickOn(linkNavigation3Registration);
    return switchToPage(RegistrationListPage.class);
  }

  private static final String XPATH_LINK_OFFENDER_TRANSFER_REQUEST = "//*[@id='linkNavigation3OffenderTransferRequest']";
  private static final String XPATH_LINK_OFFENDER_TRANSFER_REVIEW = "//*[@id='linkNavigation3OffenderTransferReview']";
  private static final String XPATH_LINK_ALIASES = "//*[@id='linkNavigation3Alias']";
  private static final String XPATH_LINK_ADDRESSES = "//*[@id='linkNavigation3Address']";
  private static final String XPATH_LINK_DISABILITIES_AND_ADJUSTMENTS = "//*[@id='linkNavigation3Disability']";
  private static final String XPATH_LINK_EQUALITY_MONITORING = "//*[@id='linkNavigation3Diversity']";
  private static final String XPATH_LINK_PERSONAL_CIRCUMSTANCES = "//*[@id='linkNavigation3Circumstances']";
  private static final String XPATH_LINK_PERSONAL_CONTACTS = "//*[@id='linkNavigation3PersonalContact']";
  private static final String XPATH_LINK_MANAGEMENT_TIER = "//*[@id='linkNavigation3ManagementTierHistory']";
  private static final String XPATH_LINK_TRANSFER_HISTORY = "//*[@id='linkNavigation3OffenderManagerHistory']";
  private static final String XPATH_LINK_ADDITIONAL_IDENTIFIER = "//*[@id='linkNavigation3AdditionalIdentifiers']";
  private static final String XPATH_LINK_REGISTRATION_SUMMARY = "//*[@id='linkNavigation3Registration']";
  private static final String XPATH_LINK_NON_STATUTORY_INTERVENTIONS = "//*[@id='linkNavigation3OffenderNsi']";

  private static final PageElement LINK_OFFENDER_TRANSFER_REQUEST =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_OFFENDER_TRANSFER_REQUEST, "Offender Details Offender Transfer Request Link");
  private static final PageElement LINK_OFFENDER_TRANSFER_REVIEW =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_OFFENDER_TRANSFER_REVIEW, "Offender Details Offender Transfer Review Link");
  private static final PageElement LINK_ALIASES = new PageElement(PageElement.Type.XPATH, XPATH_LINK_ALIASES, "Offender Details Aliases Link");
  private static final PageElement LINK_ADDRESSES = new PageElement(PageElement.Type.XPATH, XPATH_LINK_ADDRESSES, "Offender Details Addresses Link");
  private static final PageElement LINK_DISABILITIES_AND_ADJUSTMENTS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_DISABILITIES_AND_ADJUSTMENTS, "Offender Details Disabilities and Adjustments Link");
  private static final PageElement LINK_EQUALITY_MONITORING =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_EQUALITY_MONITORING, "Offender Details Equality Monitoring Link");
  private static final PageElement LINK_PERSONAL_CIRCUMSTANCES =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_PERSONAL_CIRCUMSTANCES, "Offender Details Personal Circumstances Link");
  private static final PageElement LINK_PERSONAL_CONTACTS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_PERSONAL_CONTACTS, "Offender Details Personal Contacts Link");
  private static final PageElement LINK_MANAGEMENT_TIER =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_MANAGEMENT_TIER, "Offender Details Management Tier Link");
  private static final PageElement LINK_TRANSFER_HISTORY =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_TRANSFER_HISTORY, "Offender Details Transfer History Link");
  private static final PageElement LINK_ADDITIONAL_IDENTIFIER =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_ADDITIONAL_IDENTIFIER, "Offender Details Additional Identifier Link");
  private static final PageElement LINK_REGISTRATION_SUMMARY =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_REGISTRATION_SUMMARY, "Offender Details Registration Summary Link");
  private static final PageElement LINK_NON_STATUTORY_INTERVENTIONS =
      new PageElement(PageElement.Type.XPATH, XPATH_LINK_NON_STATUTORY_INTERVENTIONS, "Offender Details Non-Statutory Interventions Link");


//  public OffenderTransferReviewPage clickOffenderTransferReviewLink() {
//    pilot.click(LINK_OFFENDER_TRANSFER_REVIEW);
//    return new OffenderTransferReviewPage();
//  }
//
//  public AliasListPage clickOffenderAliasesLink() {
//    pilot.click(LINK_ALIASES);
//    return new AliasListPage();
//  }

//  public DisabilityListPage clickDisabilitiesAndAdjustmentsLink() {
//    pilot.click(LINK_DISABILITIES_AND_ADJUSTMENTS);
//    return new DisabilityListPage();
//  }

//  public PersonalCircumstancesListPage clickPersonalCircumstancesLink() {
//    pilot.click(LINK_PERSONAL_CIRCUMSTANCES);
//    return new PersonalCircumstancesListPage();
//  }
//
//  public PersonalContactListPage clickPersonalContactsLink() {
//    pilot.click(LINK_PERSONAL_CONTACTS);
//    return new PersonalContactListPage();
//  }
//
//  public ManagementTierHistoryPage clickManagementTierLink() {
//    pilot.click(LINK_MANAGEMENT_TIER);
//    return new ManagementTierHistoryPage();
//  }
//
//  public TransferHistoryPage clickTransferHistory() {
//    pilot.click(LINK_TRANSFER_HISTORY);
//    return new TransferHistoryPage();
//  }
//
//  public AdditionalIdentifierListPage clickAdditionalIdentifier() {
//    pilot.click(LINK_ADDITIONAL_IDENTIFIER);
//    return new AdditionalIdentifierListPage();
//  }
//
//  public RegistrationListPage clickRegistrationSummaryLink() {
//    pilot.click(LINK_REGISTRATION_SUMMARY);
//    return new RegistrationListPage();
//  }
//
//  public OffenderNonStatutoryInterventionListPage clickNonStatutoryInterventions() {
//    pilot.click(LINK_NON_STATUTORY_INTERVENTIONS);
//    return new OffenderNonStatutoryInterventionListPage();
//  }

  public <T extends DeliusBasePage> T completePage(int inputValuesIndex, Class<T> type) {
		String value = getInputValuesIndex(inputValuesIndex);

    FileUtils.saveScreenshot(this.getClass().getSimpleName());

    return null;
  }
}

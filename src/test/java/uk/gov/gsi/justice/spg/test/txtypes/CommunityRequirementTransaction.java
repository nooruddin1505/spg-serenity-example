package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class CommunityRequirementTransaction extends BaseTransaction {

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.RequirementTypeMainCategory",     "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/RequirementTypeMainCategory"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.RequirementTypeSubCategory",      "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/RequirementTypeSubCategory"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.ImposedDate",                     "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/ImposedDate"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.Length",                          "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/Length"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.ExpectedStartDate",               "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/ExpectedStartDate"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.ActualStartDate",                 "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/ActualStartDate"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.RQProvider",                      "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/RQProvider"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.RQResponsibleTeam",               "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/RQResponsibleTeam"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.RQResponsibleOfficer",            "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/RQResponsibleOfficer"));
      add(new AssertionPair("CommunityRequirement%s.CommunityRequirementDetails%s.Notes",                           "/SPGInterchange/SPGMessage/*[2]/CommunityRequirementDetails%s/CommunityRequirement/Notes"));
    }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

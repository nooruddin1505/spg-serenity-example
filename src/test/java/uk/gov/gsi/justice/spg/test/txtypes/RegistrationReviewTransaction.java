package uk.gov.gsi.justice.spg.test.txtypes;

import uk.gov.gsi.justice.spg.test.utils.AssertionPair;

import java.util.ArrayList;

public class RegistrationReviewTransaction extends BaseTransaction {

  private static final ArrayList<AssertionPair> ASSERTION_PAIRS = new ArrayList<AssertionPair>() {{
    add(new AssertionPair("RegistrationReview%s.RegistrationReviewDetails%s.DateOfReview",      "/SPGInterchange/SPGMessage/*[2]/RegistrationReviewDetails%s/RegistrationReview/DateOfReview"));
    add(new AssertionPair("RegistrationReview%s.RegistrationReviewDetails%s.ReviewProvider",    "/SPGInterchange/SPGMessage/*[2]/RegistrationReviewDetails%s/RegistrationReview/ReviewProvider"));
    add(new AssertionPair("RegistrationReview%s.RegistrationReviewDetails%s.ReviewingTeam",     "/SPGInterchange/SPGMessage/*[2]/RegistrationReviewDetails%s/RegistrationReview/ReviewingTeam"));
    add(new AssertionPair("RegistrationReview%s.RegistrationReviewDetails%s.ReviewingOfficer",  "/SPGInterchange/SPGMessage/*[2]/RegistrationReviewDetails%s/RegistrationReview/ReviewingOfficer"));
    add(new AssertionPair("RegistrationReview%s.RegistrationReviewDetails%s.Notes",             "/SPGInterchange/SPGMessage/*[2]/RegistrationReviewDetails%s/RegistrationReview/Notes"));
    add(new AssertionPair("RegistrationReview%s.RegistrationReviewDetails%s.NextReviewDate",    "/SPGInterchange/SPGMessage/*[2]/RegistrationReviewDetails%s/RegistrationReview/NextReviewDate"));
    add(new AssertionPair("RegistrationReview%s.RegistrationReviewDetails%s.ReviewCompleted",   "/SPGInterchange/SPGMessage/*[2]/RegistrationReviewDetails%s/RegistrationReview/ReviewCompleted"));
    }};

  @Override
  public ArrayList<AssertionPair> getAssertionPairs() {
    return ASSERTION_PAIRS;
  }
}

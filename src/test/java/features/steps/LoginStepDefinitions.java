package features.steps;

import cucumber.api.java.en.Given;
import features.steps.serenity.LoginSteps;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;
import uk.gov.gsi.justice.spg.test.session.SessionKey;

import java.util.Map;

/**
 * Glue
 */
public class LoginStepDefinitions {
  
  @Managed
  WebDriver driver;

  @Steps
  public LoginSteps loginSteps;

  //ALL METHODS below are glued to FEATURES available within AllocateOffender.feature
  @Given("^I am logged into nDelius$")
  public void loginToDelius() {
    loginSteps.login();
  }

  @Given("^I am logged into nDelius with following data:$")
  public void loginToDelius(Map<String, String> mapOfDataFiles) {
    //Load test data from excel sheet
    Serenity.getCurrentSession().put(SessionKey.mapOfDataFiles, mapOfDataFiles);

    //Login to nDelius
    loginSteps.login();
  }
}

package features.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.steps.serenity.RequirementSteps;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;

/**
 * Glue
 */
public class RequirementStepDefinitions {

  @Managed
  WebDriver driver;

  @Steps
  public RequirementSteps requirementSteps;

  //ALL METHODS below are glued to FEATURES available within AllocateOffender.feature
  @When("^I view requirement by row (\\d+)$")
  public void i_view_requirement_by_row(int index) throws Throwable {
    requirementSteps.viewRequirementByRow(index);
  }

  @Then("^Verify correct requirements details displayed$")
  public void verify_correct_requirements_details_displayed() throws Throwable {
    requirementSteps.viewRequirementByRow(1);
    requirementSteps.assertRequirementDetailsPageValues();
  }


  @And("^I add requirement$")
  public void iAddRequirement() throws Throwable {
    requirementSteps.addRequirement();
  }
}

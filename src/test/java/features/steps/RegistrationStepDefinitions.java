package features.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.steps.serenity.RegistrationSteps;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;

/**
 * Glue
 */
public class RegistrationStepDefinitions {

  @Managed
  WebDriver driver;

  @Steps
  public RegistrationSteps registrationSteps;

  //ALL METHODS below are glued to FEATURES available within AllocateOffender.feature
  @When("^I add new registration details$")
  public void iAddNewRegistrationDetails() throws Throwable {
    registrationSteps.addRegistraton(1);
  }

  @When("^I view registraton details by row (\\d+)$")
  public void iViewRegistratonDetailsByRow(int row) throws Throwable {
    registrationSteps.viewRegistrationDetailsByRow(row);
  }

  @Then("^Verify correct registration details displayed$")
  public void verifyCorrectRegistrationDetailsDisplayed() throws Throwable {
    registrationSteps.assertRegistrationReviewDetailsPageValues();
  }
}

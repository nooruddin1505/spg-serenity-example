package features.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.steps.serenity.TransactionInterchangeSteps;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;
import uk.gov.gsi.justice.spg.test.session.SessionKey;

import java.util.Map;

/**
 *
 */
public class TransactionInterchangeStepDefinitions {

  @Managed
  WebDriver driver;

  @Steps
  public TransactionInterchangeSteps transactionInterchangeSteps;

  @When("^I perform an interchange at the specified CRC with following xml file:$")
  public void i_perform_an_interchange_at_the_specified_CRC_with_following_xml_file(Map<String, String> mapOfXMLFiles) throws Throwable {
    Serenity.getCurrentSession().put(SessionKey.xmlInterchangeFileNames, mapOfXMLFiles);

    String xmlInterchangeFile1 = mapOfXMLFiles.get("xmlInterchangeFile1");
    String controlReference = transactionInterchangeSteps.interchangeDataWithCRC(xmlInterchangeFile1);
  }

  @Then("^I should see the correct response messages$")
  public void iShouldSeeTheCorrectResponseMessages() throws Throwable {
    boolean responsesReceived = transactionInterchangeSteps.isResponsesReceived();

  }
}

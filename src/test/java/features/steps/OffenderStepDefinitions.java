package features.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import features.steps.serenity.OffenderSteps;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;
import uk.gov.gsi.justice.spg.test.session.SessionKey;
import uk.gov.gsi.justice.spg.test.utils.cukes.ConfigUtils;

import java.util.Map;

/**
 * Glue
 */
public class OffenderStepDefinitions {
  
  @Managed
  WebDriver driver;

  @Steps
  public OffenderSteps allocateOffender;


  @Given("^I have added a new Offender to nDelius$")
  public void addOffenderInDelius() {
    allocateOffender.addOffender(0);
  }

  @Given("^I have added an Offender to nDelius with following data:$")
  public void i_have_added_an_Offender_to_nDelius_with_following_data(Map<String, String> mapOfDataFiles) throws Throwable {
    //Load test data from excel sheet
    Serenity.getCurrentSession().put(SessionKey.mapOfDataFiles, mapOfDataFiles);
    ConfigUtils.loadTestDataFiles();

    //Add an offender, brand new offender index = 0
    allocateOffender.addOffender(0);
  }

  @Given("^I add an Event to the newly created offender$")
  public void i_add_an_Event_to_for_the_newly_created_offender() throws Throwable {
    allocateOffender.addEvent(0);
  }

  @And("^I add a new Offender to nDelius with an Event$")
  public void iAddANewOffenderToNDeliusWithAndEvent() throws Throwable {
    allocateOffender.addOffender(0);
    allocateOffender.addEvent(0);
  }

  @And("^Add diversity details$")
  public void addDiversityDetails() throws Throwable {
    allocateOffender.addDiversity(0);
  }

  @And("^Add address details$")
  public void addAddressDetails() throws Throwable {
    allocateOffender.addAddress(0);
  }

  @And("^Add address assessment details$")
  public void addAddressAssessmentDetails() throws Throwable {
    allocateOffender.addAddressAssessment(0);
  }

  @When("^I view event by row (\\d+)$")
  public void iViewEventByRow(int index) throws Throwable {
    allocateOffender.viewEventByRow(index);
  }
}

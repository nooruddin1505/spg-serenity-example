package features.steps.serenity;

import dependencies.adapter.abstractstuff.CRCID;
import net.serenitybdd.core.Serenity;
import uk.gov.gsi.justice.spg.test.session.SessionKey;
import uk.gov.gsi.justice.spg.test.utils.CRCUtils;
import uk.gov.gsi.justice.spg.test.utils.TransactionInterchange;
import uk.gov.gsi.justice.spg.test.utils.transactions.InterchangeData;

import java.io.File;
import java.util.Map;

/**
 * Created by InternetUser on 30/10/2017.
 *
 * These are not related to any pages
 *
 * Separate action to interchange message data with our xml datasheets which are then injected at a specified CRC
 */
public class TransactionInterchangeSteps extends BaseSteps {


  public String interchangeDataWithCRC(String xmlInterchangeFile) {
    Map<String, String> mapOfDataFiles = (Map<String, String>) Serenity.getCurrentSession().get(SessionKey.mapOfDataFiles);
    String subPath = mapOfDataFiles.get("location");
    String xmlFilePath = configData.getProperty("Data.Dir") + File.separator + subPath + File.separator + xmlInterchangeFile;

    CRCID crcID = CRCUtils.getCRC(1);
    String schemaVersion = CRCUtils.getSchemaVersion(crcID);
    String crn = offenderDetailsCommon.getOffenderCRN();

    //Inject at CRC stub
    TransactionInterchange txInterchange = InterchangeData.getInterchangeData(xmlFilePath, 1);
    txInterchange.interchange(crn, crcID, xmlFilePath);
    String controlReference = injectAtCRC(subPath, xmlInterchangeFile, schemaVersion, crcID);

    //Keep track of data
    Serenity.getCurrentSession().put(SessionKey.controlReference, controlReference);
    Serenity.getCurrentSession().put(SessionKey.crcID, crcID);
    return controlReference;
  }

  public boolean isResponsesReceived() {
    String controlReference = Serenity.getCurrentSession().get(SessionKey.controlReference).toString();
    CRCID crcID = (CRCID) Serenity.getCurrentSession().get(SessionKey.crcID);
    assertExpectedResponse(controlReference, crcID);
    return true;
  }
}

package features.steps.serenity;

import net.serenitybdd.core.Serenity;
import uk.gov.gsi.justice.spg.test.page.delius.offender.AddEventPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddressAssessmentListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.AddressListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.DiversityDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.OffenderDetailsPage;
import uk.gov.gsi.justice.spg.test.session.SessionKey;

/**
 * Created by InternetUser on 30/10/2017.
 */
public class OffenderSteps extends BaseSteps {

  public void addOffender(int index) {

    nationalSearchPage = homePage.clickNationalSearchButton();

    //Search for offender
    nationalSearchPage = nationalSearchPage.clickSearchButton();
    addOffenderPage = nationalSearchPage.clickAddOffender();

    //Add offender
    addOffenderPage.completePage(index, OffenderDetailsPage.class);

    //Keep track of the CRN
    String crn = offenderDetailsCommon.getOffenderCRN();
    Serenity.getCurrentSession().put(SessionKey.crn, crn);
    System.out.println("CRN : " + crn);
  }

  public void addEvent(int index) {

    //Go to add event page
    offenderDetailsPage = offenderDetailsPage.clickOffenderIndexLink();
    eventListPage = offenderDetailsPage.clickEventListLink();
    addEvent = eventListPage.clickAddButton();

    //Add a new event
    addEvent.completePage(index, AddEventPage.class);
  }


  public void addDiversity(int index) {
    offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
    diversityDetailsPage = offenderDetailsPage.clickEqualityMonitoringLink();
    updateDiversityDetailsPage = diversityDetailsPage.clickButtonUpdate();

    updateDiversityDetailsPage.completePage(index, DiversityDetailsPage.class);
  }

  public void addAddress(int index) {
    offenderDetailsCommon = offenderCommon.clickOffenderIndexLink();
    addressListPage = offenderDetailsCommon.clickAddressesLink();
    addAddressPage = addressListPage.clickAddAddressButton();
    addAddressPage.completePage(index, AddressListPage.class);
  }

  public void addAddressAssessment(int index) {
    addressAssessmentListPage = addressListPage.clickViewHistoryAddressLink();
    addAddressAssessmentPage = addressAssessmentListPage.clickAddAddressAssessmentButton();
    addAddressAssessmentPage.completePage(index, AddressAssessmentListPage.class);
  }

  public void viewEventByRow(int index) {
    offenderCommon = offenderCommon.clickOffenderSummaryLink();
    eventListPage = offenderCommon.clickEventListLink();
    eventDetailsPage = eventListPage.clickViewEventByRow(index);
  }
}

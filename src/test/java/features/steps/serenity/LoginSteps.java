package features.steps.serenity;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import dependencies.adapter.utils.SleepUtils;
import net.serenitybdd.core.Serenity;
import uk.gov.gsi.justice.spg.test.page.HomePage;
import uk.gov.gsi.justice.spg.test.session.SessionKey;

/**
 * Created by InternetUser on 30/10/2017.
 */
public class LoginSteps extends BaseSteps {

  @Before
  public void initialise(Scenario scenario) {
    System.out.println("Scenario : " + scenario.getName());
    Serenity.getCurrentSession().put(SessionKey.testName, scenario.getName());
  }

  @After
  public void clearUp(Scenario scenario){
    System.out.println("Finished : " + scenario.getName());
    //Serenity.getWebdriverManager().getCurrentDriver().quit();
  }

  public HomePage login() {
    SleepUtils.sleep();
    //Set this up in serenity.properties
    String username = configData.getProperty("Delius.Username");
    String password = configData.getProperty("Delius.Password");
    String host = configData.getProperty("Delius.Host");
    String port = configData.getProperty("Delius.Port");

    return loginPage.login(username, password, host, port, getDriver());
  }
}

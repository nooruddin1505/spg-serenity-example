package features.steps.serenity;

import dependencies.adapter.abstractstuff.CRCID;
import dependencies.adapter.abstractstuff.TestData;
import dependencies.adapter.others.CRCAgent;
import dependencies.adapter.others.NodeNotFoundException;
import dependencies.adapter.others.SPGAgent;
import dependencies.adapter.utils.SleepUtils;
import dependencies.adapter.utils.XMLUtils;
import net.serenitybdd.core.Serenity;
import org.junit.Assert;
import org.xml.sax.SAXException;
import uk.gov.gsi.justice.spg.test.actions.DeliusBaseTestAction;
import uk.gov.gsi.justice.spg.test.actions.TransactionAction;
import uk.gov.gsi.justice.spg.test.basetest.DeliusBaseTest;
import uk.gov.gsi.justice.spg.test.exceptions.NoTransactionFoundException;
import uk.gov.gsi.justice.spg.test.exceptions.UnknownTransactionTypeException;
import uk.gov.gsi.justice.spg.test.session.SessionKey;
import uk.gov.gsi.justice.spg.test.txtypes.BaseTransaction;
import uk.gov.gsi.justice.spg.test.txtypes.InterchangeStatusNotificationTransaction;
import uk.gov.gsi.justice.spg.test.utils.AssertionPair;
import uk.gov.gsi.justice.spg.test.utils.MessageType;
import uk.gov.gsi.justice.spg.test.utils.TransactionUtils;
import uk.gov.gsi.justice.spg.test.utils.WaitUtils;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by InternetUser on 30/10/2017.
 */
public class BaseSteps extends CommonSteps {

  private static final String EMPTY = "";
  private Boolean spaceRemoved = false;
  private String expectedNew;
  protected String offenderFirstName;
  protected String offenderLastName;


  public void assertTransactions(String crn, CRCID crcID, List<Class> expectedTransactions) {

    assertExpectedResultsWithPolling(crn, crcID, expectedTransactions);
  }


  protected List<BaseTransaction> assertExpectedResultsWithPolling(String caseReferenceNumber, CRCID crcID, List<Class> expectedTransactionTypes) {
    boolean somethingAsserted = false;

    //Introduce polling instead of fixed waits
    List<BaseTransaction> transactions = getExpectedTransactionsWithPolling(caseReferenceNumber, crcID, expectedTransactionTypes);
    Map<String, Integer> amounts = new HashMap<>();

    for (BaseTransaction transaction : transactions) { // Iterating through transactions related to this test.

      for (AssertionPair pair : transaction.getAssertionPairs()) { // Iterating through transaction's AssertionPairs with empty parameter.
        if (pair.getElement() != null) {
          for (int i = 0; i < pair.getElement().length; i++) {
            if (pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])).contains("AllocateEvent.Contact")) {
              somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
            } else {
              checkOSRespOfficer(pair, i);
              checkLCSRespOfficer(pair, i);
              checkRQSRespOfficer(pair, i);
              checkProcMgrOfficer(pair, i);
              checkPSSRespOfficer(pair, i);
              checkInstRepOfficer(pair, i);
              checkCrtRepOfficer(pair, i);
              checkPrisonOfficer(pair, i);
              checkContactOfficer(pair, i);
              checkAPRefOfficer(pair, i);
              checkAPResOfficer(pair, i);
              checkRegOfficer(pair, i);
              checkOMOfficer(pair, i);
              checkRSPOfficer(pair, i);
              checkRefToOfficer(pair, i);
              checkAssessOfficer(pair, i);
              checkIntRspOfficer(pair, i);
              checkDecOfficer(pair, i);
              checkOmRspOfficer(pair, i);
              checkReviewOfficer(pair, i);
              checkInstReportOfficer(pair, i);
              checkUPWContactOfficer(pair, i);
              checkRegOfficer2(pair, i);

              somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[i]), transaction) || somethingAsserted;
            }

          }
        }
        // Check once again with an empty element number
        if (pair.getDataHeading(EMPTY, EMPTY).contains("AllocateEvent.Contact")) {
          somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, EMPTY), pair.getXPath(pair.getElement()[0]), transaction) || somethingAsserted;
        }

        checkOSRespOfficer(pair, 0);
        checkLCSRespOfficer(pair, 0);
        checkRQSRespOfficer(pair, 0);
        checkProcMgrOfficer(pair, 0);
        checkPSSRespOfficer(pair, 0);
        checkInstRepOfficer(pair, 0);
        checkCrtRepOfficer(pair, 0);
        checkPrisonOfficer(pair, 0);
        checkContactOfficer(pair, 0);
        checkAPRefOfficer(pair, 0);
        checkAPResOfficer(pair, 0);
        checkRegOfficer(pair, 0);
        checkOMOfficer(pair, 0);
        checkRSPOfficer(pair, 0);
        checkRefToOfficer(pair, 0);
        checkAssessOfficer(pair, 0);
        checkIntRspOfficer(pair, 0);
        checkDecOfficer(pair, 0);
        checkOmRspOfficer(pair, 0);
        checkReviewOfficer(pair, 0);
        checkInstReportOfficer(pair, 0);
        checkUPWContactOfficer(pair, 0);
        checkRegOfficer2(pair, 0);

        somethingAsserted = assertAssertionPair(pair.getDataHeading(EMPTY, EMPTY), pair.getXPath(), transaction) || somethingAsserted;

      }

      int number = (amounts = updateValue(amounts, transaction.getTransactionName())).get(transaction.getTransactionName());
      for (AssertionPair pair : transaction.getAssertionPairs()) { // Iterating through transaction's AssertionPairs with transaction number.
        if (pair.getElement() != null) {
          for (int i = 0; i < pair.getElement().length; i++) {
            somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), Integer.toString(pair.getElement()[i])), pair.getXPath(pair.getElement()[i]), transaction) || somethingAsserted;
          }
        }

        somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), EMPTY), pair.getXPath(), transaction) || somethingAsserted;
      }
    }

    if (!somethingAsserted) {
      fail("No assertions were done. Please check the results against the expected results.");
    }

    return transactions;
  }


  /**
   * Method to update the 'amounts' map from the assertExpectedResults and assertExpectedResponse methods.
   *
   * @param map the amounts map.
   * @param key the key to be updated.
   * @return the updated map.
   */
  private Map<String, Integer> updateValue(Map<String, Integer> map, String key) {
    if (map.containsKey(key)) {
      int value = map.get(key);
      map.put(key, value + 1);
    } else {
      map.put(key, 1);
    }

    return map;
  }
  /**
   * Use only to get a list of transactions from a specific CRC
   *
   * @param caseReferenceNumber
   * @param crcID
   * @param expectedTransactionTypes
   * @return
   */
  protected List<BaseTransaction> getExpectedTransactionsWithPolling(String caseReferenceNumber, CRCID crcID, List<Class> expectedTransactionTypes) {
    List<BaseTransaction> transactions = null;
    long start = System.currentTimeMillis();
    boolean foundExpectedTransactions = false;
    long timeToWaitMilliSeconds = 1000;//ExecutionContext.getInstance().getConfigData().getLongProperty("SPG.Message.Medium.Wait.Millis") / 1000;
    long totalTime = 0;

    //Poll every couple of seconds and check if the transaction we are interested in is there
    do {
      try {
        WaitUtils.sleepSpecificTimeDontUseMeOverSeleniumWaits(timeToWaitMilliSeconds);
        transactions = TransactionUtils.getTransactionsUseWithPolling(caseReferenceNumber, crcID);
        assertExpectedTransactions(transactions, expectedTransactionTypes);
        foundExpectedTransactions = true;
      } catch (AssertionError ae) {
        foundExpectedTransactions = false;
      } catch (Exception e) {
        System.out.println("Waiting for messages to appear in the system : " + expectedTransactionTypes);
      }
      totalTime = totalTime + timeToWaitMilliSeconds;

      //Try again if all transactions not found and totalTime is less than 60 seoncds
    } while (!foundExpectedTransactions && totalTime <= 60000);

    if (!foundExpectedTransactions) {
      transactions = null;
      fail("Expected transaction types NOT found in results.");
    }
    long finish = System.currentTimeMillis();
    System.out.format("\n----------------------------------------\nTime taken for all the messages to appear in SPG : %s seconds\n----------------------------------------\n", ((finish - start) / 1000));

    return transactions;
  }


  /**
   */
  private void assertExpectedTransactions(List<BaseTransaction> transactions, List<Class> expectedTransactionTypes) {
    assertExpectedTransactions(expectedTransactionTypes, null, null, transactions);
  }

  /**
   * Method to assert that the transactions which have been obtained are those which we expect to see for any given test.
   *
   * @param expectedTransactionTypes list of transactions which are expected to be found.
   * @param caseReferenceNumber      case reference number of the transactions to search for.
   * @param crcID                    ID of the CRC server to search for the transactions.
   * @param transactions             transactions which have previously been found. If null, these will be obtained using caseReferenceNumber and crcID.
   */
  public void assertExpectedTransactions(List<Class> expectedTransactionTypes, String caseReferenceNumber, CRCID crcID, List<BaseTransaction> transactions) {
    try {
      if (transactions == null) {
        transactions = TransactionUtils.getTransactions(caseReferenceNumber, crcID);
      }

      if (expectedTransactionTypes != null) {
        for (Class expectedType : expectedTransactionTypes) {
          boolean typeFound = false;
          for (BaseTransaction transaction : transactions) {
            if (transaction.getClass().equals(expectedType)) {
              typeFound = true;
              break;
            }
          }

          System.out.println(String.format("Expected transaction type %s found in results.", expectedType.toString())); // TODO - This should be an Action.
          assertTrue(String.format("Expected transaction type %s found in results.", expectedType.getSimpleName()), typeFound);
        }
      }
    } catch (UnknownTransactionTypeException e) {
      fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
    } catch (NoTransactionFoundException e) {
      fail("NoTransactionsFoundException caught. Message: " + e.getMessage());
    }
  }


  /**
   * Method to perform an assertion using information from an AssertionPair. There are some edge cases which are handled
   * as part of this method, specifically the asserting of an offender's first name and last name which, being randomly
   * generated by the application, means the actual values cannot be checked against the test data.
   * <p>
   * Furthermore, an AssertionRule can be applied to the check for certain edge cases. These AssertionRules determine
   * what the method is for asserting the actual against the expected value; should that be EQUALS (default), CONTAINS
   * or MATCHES. For MATCHES, this allows regular expressions to be defined in the test data which is useful in certain
   * circumstances.
   *
   * @param dataHeading the data heading from the assertion pair
   * @param xPath       the xPath from the assertion pair
   * @param tx          the transaction to assert against
   * @return true if an assertion operation has been made, false otherwise.
   */
  private boolean assertAssertionPair(String dataHeading, String xPath, BaseTransaction tx) {
    if(testData == null)
      testData = (TestData) Serenity.getCurrentSession().get(SessionKey.testData);
    String expected = testData.getExpectedResult(dataHeading);

    if (isSpaceRemoved()) {
      expected = getNewExpected();
    }

    if (dataHeading.equals("AllocateOffender.Offender.Notes")) {
      expected = Serenity.getCurrentSession().get(SessionKey.testDataFileLocation).toString();
    }

    if ((dataHeading.equals("AllocateOffender.Offender.PNCNumber")) ||
            (dataHeading.equals("AllocateOffender.Offender.CRONumber")) ||
            (dataHeading.equals("AllocateOffender.Offender.NOMSNumber")) ||
            (dataHeading.equals("AllocateOffender.Offender.NINumber")) ||
            (dataHeading.equals("Offender.OffenderDetails.PNCNumber")) ||
            (dataHeading.equals("Offender.OffenderDetails.CRONumber")) ||
            (dataHeading.equals("Offender.OffenderDetails.NOMSNumber")) ||
            (dataHeading.equals("Offender.OffenderDetails.NINumber")) ||
            (dataHeading.equals("Offender.OffenderDetails.EqualityMonitoringNotes")) ||
            dataHeading.equals("Offender.OffenderDetails.DateDied")) {
      expected = null;
    }

    String contactXpath = "";
    int contactTypeCount = 0;
    if (dataHeading != null && dataHeading.contains("AllocateEvent.Contact") && expected != null) {
      contactTypeCount = tx.getXMLUtils().contactFindNumber(testData);
      if (contactTypeCount > 0) {
        char replacePosition = xPath.charAt(40);
        contactXpath = xPath.replaceAll(String.valueOf(replacePosition), String.valueOf(contactTypeCount));
      }
    }

    DeliusBaseTest.AssertionRule assertionRule = DeliusBaseTest.AssertionRule.EQUALS;

    if (expected == null || EMPTY.equals(expected))
      return false; // Skip assertion if there is no expected value.

    // Need to check some edge cases.
    if (dataHeading.matches("AllocateOffender\\d*\\.Offender\\d*\\.FirstName")) {
      expected = this.offenderFirstName;
    }

    if (dataHeading.matches("AllocateOffender\\d*\\.Offender\\d*\\.FamilyName")) {
      expected = this.offenderLastName;
    }

    if (dataHeading.matches("Offender\\d*\\.OffenderDetails\\d*\\.FirstName")) {
      expected = this.offenderFirstName;
    }

    if (dataHeading.matches("Offender\\d*\\.OffenderDetails\\d*\\.FamilyName")) {
      expected = this.offenderLastName;
    }

    if (dataHeading.matches("Contact\\d*\\.ContactDetails\\d*\\.Notes")) {
      assertionRule = DeliusBaseTest.AssertionRule.MATCHES;
    }

    DeliusBaseTestAction.reportNextAssertionPairToCheck(xPath, dataHeading, tx.getTransactionName());

    String actual = "";
    try {
      if (dataHeading != null && dataHeading.contains("AllocateEvent.Contact") && contactTypeCount > 0) {
        actual = tx.getXMLUtils().getNodeTextContent(contactXpath);
      } else {
        actual = tx.getXMLUtils().getNodeTextContent(xPath);
      }

    } catch (NodeNotFoundException e) {
      fail(e.getMessage());
    }

    boolean result;
    switch (assertionRule) {
      case EQUALS:
        result = actual.equals(expected);
        break;
      case CONTAINS:
        result = actual.contains(expected);
        break;
      case MATCHES:
        result = actual.matches(expected);
        break;
      default:
        result = actual.equals(expected);
        break;
    }

    assertTrue(String.format("Value at %s is correct. Expected: '%s'. Actual: '%s'", xPath, expected, actual), result);
    spaceRemoved = false;
    return true;
  }

  /**
   * This enum is used to differentiate between the different rules for checking edge cases.
   *
   * @see DeliusBaseTest#assertAssertionPair(String, String, BaseTransaction)
   */
  public enum AssertionRule {
    EQUALS,
    CONTAINS,
    MATCHES
  }

  public void removeSpace(AssertionPair pair, String element) {
    if(testData == null)
      testData = (TestData) Serenity.getCurrentSession().get(SessionKey.testData);
    String expected;
    if (element.equals("0")) {
      expected = testData.getExpectedResult(pair.getDataHeading(EMPTY, EMPTY));
    } else {
      expected = testData.getExpectedResult(pair.getDataHeading(EMPTY, element));
    }

    if (expected != null && !expected.contains("Unallocated Staff")) {
      expectedNew = expected.replaceAll(" ", "");
      spaceRemoved = true;
    }

  }

  public Boolean isSpaceRemoved() {
    return spaceRemoved;
  }

  public String getNewExpected() {
    return expectedNew;
  }

  public void checkOSRespOfficer(AssertionPair pair, int element) {
    String OSOfficer = "AllocateEvent.Event.OSResponsibleOfficer";

    if (pair.getDataHeading(EMPTY, EMPTY).contains(OSOfficer)) {
      String element2 = Integer.toString(element);
      this.removeSpace(pair, element2);
    }
  }

  public void checkLCSRespOfficer(AssertionPair pair, int element) {
    String LCOfficer = "LCResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRQSRespOfficer(AssertionPair pair, int element) {
    String LCOfficer = "RQResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkProcMgrOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ProcessManagerOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkPSSRespOfficer(AssertionPair pair, int element) {
    String LCOfficer = "PSSResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkInstRepOfficer(AssertionPair pair, int element) {
    String LCOfficer = "InstReportOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkCrtRepOfficer(AssertionPair pair, int element) {
    String LCOfficer = "CrtReportOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkPrisonOfficer(AssertionPair pair, int element) {
    String LCOfficer = "PrisonOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkContactOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ContactOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkAPRefOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ReferringOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkAPResOfficer(AssertionPair pair, int element) {
    String LCOfficer = "APOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRegOfficer(AssertionPair pair, int element) {
    String LCOfficer = "RegisteringOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkOMOfficer(AssertionPair pair, int element) {
    String LCOfficer = "OMResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRSPOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRefToOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ReferredToOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkAssessOfficer(AssertionPair pair, int element) {
    String LCOfficer = "AssessmentOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkIntRspOfficer(AssertionPair pair, int element) {
    String LCOfficer = "InterventionResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkDecOfficer(AssertionPair pair, int element) {
    String LCOfficer = "DecisionOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkOmRspOfficer(AssertionPair pair, int element) {
    String LCOfficer = "OMTransferToResponsibleOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkReviewOfficer(AssertionPair pair, int element) {
    String LCOfficer = "ReviewingOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkInstReportOfficer(AssertionPair pair, int element) {
    String LCOfficer = "InstReportOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkUPWContactOfficer(AssertionPair pair, int element) {
    String LCOfficer = "UPWContactOfficer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void checkRegOfficer2(AssertionPair pair, int element) {
    String LCOfficer = "Officer";
    String element2 = Integer.toString(element);

    if (pair.getDataHeading(EMPTY, EMPTY).contains(LCOfficer)) {
      this.removeSpace(pair, element2);
    }
  }

  public void printOut(String display) {
    System.out.println(display);
  }

  /**
   * Method used to inject a given data file (within the Data.Dir path) at a CRC so that it travels inbound towards the APICT systems. The ID of the
   * CRC is determined from the value in the SenderIdentity element of the data file. A control reference number is generated for the injected
   * message which is based upon the current date and time.
   * <p>
   * Despite 'inject' being the verb, the actual event is a copy of the file to the CRC server's inbound SPG directory. The SPG itself handles the
   * actual injection of the file onto ActiveMQ queues.
   *
   * @param dataFileName the name of the file contained within the Data.Dir directory which is to be injected.
   * @return the generated control reference number used for the inbound message.
   */
  protected String injectAtCRC(String subDir, String dataFileName, String schemaVersion, CRCID crcID) {
    String dataFilePath = configData.getProperty("Data.Dir") +  File.separator + subDir + File.separator + dataFileName;

    //dataFilePath = dataFileName;
    String hostName = "";
    String controlReference = "";

    try {
      XMLUtils xmlUtils = new XMLUtils(dataFilePath);
      String crcId = xmlUtils.getNodeTextContent(configData.getProperty("XPath.SenderIdentity"));
      controlReference = SPGAgent.generateControlReference();

      if (crcId.equals("N00")) {
        assertTrue("Invalid sender ID, test data incorrect", false);
      }

      hostName = configData.getProperty("Host." + schemaVersion + ".Server");
      CRCAgent crcAgent = new CRCAgent(controlReference, subDir, dataFileName, hostName, schemaVersion, crcID);

      crcAgent.inject();
    } catch (ParserConfigurationException e) {
      TransactionAction.reportParserConfigurationExceptionCaught(hostName, dataFilePath, "n/a", dataFileName, e);
    } catch (SAXException e) {
      TransactionAction.reportSAXExceptionCaught(hostName, dataFilePath, "n/a", dataFileName, e);
    } catch (IOException e) {
      TransactionAction.reportIOExceptionCaught(hostName, dataFilePath, "n/a", dataFileName, e);
    }

    SleepUtils.longSleep();

    return controlReference;
  }


  /*
  * Similar to assertExpectedResults, this method instead asserts the responses obtained. 'Responses' are distinguished from 'transactions' by
  * the CRN being found in the 'receiverRef' element rather than the 'senderControlRef' element.
  *
  * @param controlReference the control reference of the response messages.
  * @param crcID            the ID of the CRC server to search for the responses.
  */
  protected void assertExpectedResponse(String controlReference, CRCID crcID) {
    try {
      List<BaseTransaction> responses = TransactionUtils.getResponses(controlReference, crcID);
      boolean somethingAsserted = false;

      Map<String, Integer> amounts = new HashMap<>();

      for (BaseTransaction response : responses) {
        int number = (amounts = updateValue(amounts, response.getTransactionName())).get(response.getTransactionName());

        if (response.getClass().equals(InterchangeStatusNotificationTransaction.class)) {

          for (AssertionPair pair : response.getAssertionPairs()) {
            try {
              somethingAsserted = assertAssertionPair(pair.getDataHeading(Integer.toString(number), EMPTY), pair.getXPath(), response) || somethingAsserted;
            } catch (AssertionError e) {
              String errorDetailsXPath = configData.getProperty("XPath.TransactionType") + "/Details/FieldValidationErrors/Code";
              System.out.println("Reason: " + response.getXMLUtils().getNodeTextContent(errorDetailsXPath));

              throw e;
            }
          }
        } else {
          System.out.println("Response transaction: " + response.getTransactionName() + ", is a transaction " + MessageType.RSP + " from the injected " + response.getTransactionName() +
                  " transaction, where nDelius is returning the " + response.getTransactionName() + " ID and is not to be asserted" +
                  " as an InterchangeStatusNotification");

          somethingAsserted = true;
        }
      }

      if (!somethingAsserted) {
        Assert.fail("No assertions were done on the responses. Please check the results against the expected results.");
      }
    } catch (UnknownTransactionTypeException e) {
      Assert.fail("UnknownTransactionTypeException caught. Message: " + e.getMessage());
    } catch (NoTransactionFoundException e) {
      Assert.fail("NoTransactionsFoundException caught. Message: " + e.getMessage());
    }
  }

}

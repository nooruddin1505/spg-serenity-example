package features.steps.serenity;

import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.RegistrationListPage;

/**
 * Created by InternetUser on 30/10/2017.
 */
public class RegistrationSteps extends BaseSteps {


  public void addRegistraton(int index) {
    offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
    registrationListPage = offenderDetailsPage.clickRegistrationSummaryLink();
    addRegistrationPage = registrationListPage.clickAddRegistration();
    addRegistrationPage.completePage(index-1, RegistrationListPage.class);
  }

  public void viewRegistrationDetailsByRow(int row) {
    offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
    registrationListPage = offenderDetailsPage.clickRegistrationSummaryLink();

    registrationDetailsPage = registrationListPage.clickViewByRow(row);
    registrationReviewDetailsPage = registrationDetailsPage.clickViewByRow(row);
  }

  public void assertRegistrationReviewDetailsPageValues() {
    registrationReviewDetailsPage.assertPageValues();
  }
}

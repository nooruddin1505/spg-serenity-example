package features.steps.serenity;

import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.RequirementsListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.UPWDetailsPage;

/**
 * Created by InternetUser on 30/10/2017.
 */
public class UnpaidWorkSteps extends BaseSteps {

  public void viewRequirementByRow(int index) {
    requirementsListPage = eventDetailsPage.clickRequirementsButton();
    requirementDetailsPage = requirementsListPage.viewRequirementByRow(index);
  }

  public void assertRequirementDetailsPageValues() {
    requirementDetailsPage.assertPageValues();
  }

  public void addRequirement() {
    requirementsListPage = eventDetailsCommon.clickRequirementsLink();
    addRequirementPage = requirementsListPage.clickAddRequirements();
    addRequirementPage.completePage(0, RequirementsListPage.class);
  }

  public void updateUnpaidWorkDetails() {
    upwDetailsPage = eventDetailsCommon.clickUnpaidWorkLink();
    updateUPWDetailsPage = upwDetailsPage.clickUpdate();
    updateUPWDetailsPage.completePage(0, UPWDetailsPage.class);
  }
}

package features.steps.serenity;

import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.RequirementsListPage;

/**
 * Created by InternetUser on 30/10/2017.
 */
public class RequirementSteps extends BaseSteps {

  public void viewRequirementByRow(int index) {
    requirementsListPage = eventDetailsPage.clickRequirementsButton();
    requirementDetailsPage = requirementsListPage.viewRequirementByRow(index);
  }

  public void assertRequirementDetailsPageValues() {
    requirementDetailsPage.assertPageValues();
  }

  public void addRequirement() {
    requirementsListPage = eventDetailsCommon.clickRequirementsLink();
    addRequirementPage = requirementsListPage.clickAddRequirements();
    addRequirementPage.completePage(0, RequirementsListPage.class);
  }
}

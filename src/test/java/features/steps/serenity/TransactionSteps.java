package features.steps.serenity;

import dependencies.adapter.abstractstuff.CRCID;
import net.serenitybdd.core.Serenity;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.session.SessionKey;
import uk.gov.gsi.justice.spg.test.utils.cukes.TransactionNameUtils;

import java.util.List;

/**
 * Created by InternetUser on 30/10/2017.
 */
public class TransactionSteps extends BaseSteps {

  public void transferOffender(int number) {
    number = number - 1;
    offenderDetailsPage = offenderCommon.clickOffenderIndexLink();
    offenderTransferRequestPage = offenderDetailsPage.clickOffenderTransferRequestLink();
    offenderTransferRequestPage.completePage(number, OffenderCommon.class);
  }

  public void assertListTransactions(CRCID crcID, List<String> transactions) {
    List<Class> expectedTransactions = TransactionNameUtils.getExpectedTransactions(transactions);
    String crn = (String) Serenity.getCurrentSession().get(SessionKey.crn);
    assertTransactions(crn, crcID, expectedTransactions);
  }

  public void assertTransactions(CRCID crcID, String transaction) {
    List<Class> expectedTransactions = TransactionNameUtils.getExpectedTransactionClass(transaction);
    String crn = (String) Serenity.getCurrentSession().get(SessionKey.crn);
    assertTransactions(crn, crcID, expectedTransactions);

  }
}

package features.steps.serenity;

import dependencies.adapter.abstractstuff.TestData;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.steps.ScenarioSteps;
import net.thucydides.core.util.EnvironmentVariables;
import uk.gov.gsi.justice.spg.test.page.HomePage;
import uk.gov.gsi.justice.spg.test.page.LoginPage;
import uk.gov.gsi.justice.spg.test.page.delius.AddOffenderPage;
import uk.gov.gsi.justice.spg.test.page.delius.NationalSearchPage;
import uk.gov.gsi.justice.spg.test.page.delius.OffenderCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.AddEventPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventDetailsCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.EventListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.OffenderDetailsCommon;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.AddRequirementPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.EventDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.RequirementDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.RequirementsListPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.UPWDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.eventdetails.UpdateUPWDetailsPage;
import uk.gov.gsi.justice.spg.test.page.delius.offender.offenderdetails.*;
import uk.gov.gsi.justice.spg.test.session.SessionKey;

/**
 * Created by InternetUser on 30/10/2017.
 */
public class CommonSteps extends ScenarioSteps {

  public static EnvironmentVariables configData = Injectors.getInjector().getProvider(EnvironmentVariables.class).get();
  public static TestData testData = (TestData) Serenity.getCurrentSession().get(SessionKey.testData);

  LoginPage loginPage;
  HomePage homePage;
  NationalSearchPage nationalSearchPage;

  //Offender Pages
  OffenderCommon offenderCommon;
  OffenderDetailsCommon offenderDetailsCommon;
  OffenderDetailsPage offenderDetailsPage;
  AddOffenderPage addOffenderPage;
  OffenderTransferRequestPage offenderTransferRequestPage;

  //Event pages
  AddEventPage addEvent;
  EventListPage eventListPage;
  EventDetailsPage eventDetailsPage;
  EventDetailsCommon eventDetailsCommon;

  //Diversity pages
  DiversityDetailsPage diversityDetailsPage;
  UpdateDiversityDetailsPage updateDiversityDetailsPage;

  //Address pages
  AddressListPage addressListPage;
  UpdateAddressPage updateAddressPage;
  AddressAssessmentListPage addressAssessmentListPage;
  AddAddressAssessmentPage addAddressAssessmentPage;
  AddAddressPage addAddressPage;

  //Requirement page
  RequirementsListPage requirementsListPage;
  RequirementDetailsPage requirementDetailsPage;
  AddRequirementPage addRequirementPage;

  //Registration pages
  RegistrationListPage registrationListPage;
  AddRegistrationPage addRegistrationPage;
  RegistrationDetailsPage registrationDetailsPage;
  RegistrationReviewDetailsPage registrationReviewDetailsPage;

  //Unpaid work pages
  UPWDetailsPage upwDetailsPage;
  UpdateUPWDetailsPage updateUPWDetailsPage;

}

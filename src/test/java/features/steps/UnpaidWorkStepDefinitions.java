package features.steps;

import cucumber.api.java.en.And;
import features.steps.serenity.UnpaidWorkSteps;
import net.thucydides.core.annotations.Steps;

/**
 * Created by InternetUser on 29/01/2018.
 */
public class UnpaidWorkStepDefinitions {

  @Steps
  UnpaidWorkSteps unpaidWorkSteps;

  @And("^update UPW details$")
  public void updateUPWDetails() throws Throwable {
    unpaidWorkSteps.updateUnpaidWorkDetails();
  }
}

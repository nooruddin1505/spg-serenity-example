package features.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dependencies.adapter.abstractstuff.CRCID;
import features.steps.serenity.TransactionSteps;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;
import uk.gov.gsi.justice.spg.test.utils.CRCUtils;

import java.util.List;

/**
 * Glue
 */
public class TransferStepDefinitions {

  @Managed
  WebDriver driver;

  @Steps
  public TransactionSteps transactionSteps;


  @When("^I transfer the Offender number (.*)$")
  public void i_transfer_the_Offender(int number) throws Throwable {
    transactionSteps.transferOffender(number);
  }

  @When("^I dd transfer the Offender number (.*)$")
  public void i_ssdstransfer_the_Offender(int number) throws Throwable {
    System.out.println(number);
    //transactionSteps.transferOffender(number);
  }

  @Then("^Verify correct transactions sent to the CRC Stub (.*) and contain the expected data:$")
  public void verify_correct_transactions_sent_to_the_CRC_Stub_and_contain_the_expected_data(String crc, List<String> expectedTransactions) throws Throwable {
    CRCID crcid = CRCUtils.getCRCID(crc);
    transactionSteps.assertListTransactions(crcid, expectedTransactions);
  }

  @Then("^Verify transactions sent to the correct CRC Stub and contain the expected data:$")
  public void verify_transactions_sent_to_the_correct_CRC_Stub_and_contain_the_expected_data(List<String> expectedTransactions) throws Throwable {
    CRCID crcid = CRCUtils.getCRC(1);
    transactionSteps.assertListTransactions(crcid, expectedTransactions);
  }

  @Then("^Verify correct transaction (.*) sent to the CRC Stub (.*) and contain the expected data$")
  public void verify_transaction_results(String expectedTransaction, String crc) {
    CRCID crcid = CRCUtils.getCRCID(crc);
    transactionSteps.assertTransactions(crcid, expectedTransaction);
  }

  @Then("^Verify correct transaction (.*) sent to the correct CRC Stub and contain the expected data$")
  public void verify_transaction_results(String expectedTransaction) {
    CRCID crcid = CRCUtils.getCRC(1);
    transactionSteps.assertTransactions(crcid, expectedTransaction);
  }


}
